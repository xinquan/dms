Ext.define('scaffold.view.South', {
			extend : 'Ext.Toolbar',
			initComponent : function() {
				Ext.apply(this, {
							id : 'bottom',
							region : "south",
							height : 23,
							items : ['当前用户：' + _username + '&nbsp;&nbsp;&nbsp;&nbsp;所属部门：' + _deptname + '&nbsp;&nbsp;&nbsp;&nbsp;角色：' + _roles]
						});
				this.callParent(arguments);
			}
		})