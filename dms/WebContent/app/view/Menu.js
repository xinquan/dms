Ext.define('scaffold.view.Menu', {
			extend : 'Ext.panel.Panel',
			initComponent : function() {
				Ext.apply(this, {
							id : 'menu-panel',
							title : '系统菜单',
							iconCls : 'contract',
							margins : '0 0 -1 1',
							region : 'west',
							border : false,
							enableDD : false,
							split : true,
							width : 222,
							minSize : 130,
							maxSize : 300,
							containerScroll : true,
							collapsible : true,
							autoScroll : false,
							layout : {
								sequence : false,
								type : 'accordion'
							},
							items : []
						});
				this.callParent(arguments);
			}
		})