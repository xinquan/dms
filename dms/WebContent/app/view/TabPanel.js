Ext.define('scaffold.view.TabPanel', {
			extend : 'Ext.tab.Panel',
			initComponent : function() {
				Ext.apply(this, {
							id : 'content-panel',
							region : 'center',
							defaults : {
								autoScroll : true
							},
							activeTab : 0,
							border : false,
							plugins : Ext.create('Ext.ux.TabCloseMenu', {
										closeTabText : '关闭当前',
										closeOthersTabsText : '关闭其他',
										closeAllTabsText : '关闭所有'
									}),
							items : [{
										id : 'HomePage',
										title : '首页',
										border : false,
										iconCls : 'home',
										bodyStyle : 'padding:60px;text-align:center;background-color:#F0F0F0',
										html : '<img src="images/background.png">'
									}]
						});
				this.callParent(arguments);
			}
		})