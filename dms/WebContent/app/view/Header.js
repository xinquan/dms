Ext.define('scaffold.view.Header', {
			extend : 'Ext.Component',
			initComponent : function() {
				Ext.applyIf(this, {
							xtype : 'box',
							region : 'north',
							height : 65,
							border : false,
							html : '<div id="background"><div id="text_logo"/></div><div id="B1" onclick="logout()"></div><div id="B2" onclick="resetPassWord()"></div>'
						});
				this.callParent(arguments);
			}
		});