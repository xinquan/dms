var header = Ext.create('scaffold.view.Header');
var menu = Ext.create('scaffold.view.Menu');
var tabs = Ext.create('scaffold.view.TabPanel');
var foot = Ext.create('scaffold.view.South');
Ext.define('scaffold.view.Viewport', {
			extend : 'Ext.Viewport',
			layout : 'fit',
			hideBorders : true,
			requires : ['scaffold.view.Header', 'scaffold.view.Menu', 'scaffold.view.TabPanel', 'scaffold.view.South'],
			initComponent : function() {
				var me = this;
				Ext.apply(me, {
							items : [{
										id : 'desk',
										layout : 'border',
										items : [header, menu, tabs, foot]
									}]
						});
				me.callParent(arguments);
			}
		})