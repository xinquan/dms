Ext.define('scaffold.controller.Main', {
			extend : 'Ext.app.Controller',
			requires : ['scaffold.view.Viewport'],
			init : function() {
				Ext.create('scaffold.view.Viewport')
			}
		});
//function showWin() {
//	showWindow(PATH + 'system/flow!showWorks.do', "查看待办工作", 800, 600, "yes");
//}
//var task = {
//	run : function() {
//		Ext.Ajax.request({
//					url : PATH + 'system/flow!countMyWorks.do',
//					callback : function(opt, success, response) {
//						toast = new Ext.ux.ToastWindow({
//									title : '待办工作',
//									padding : '0 0 0 10',
//									border : 0,
//									html : '<a href="javascript:showWin();">' + response.responseText + '</a>',
//									iconCls : 'info'
//								}).show();
//					}
//				})
//	},
//	interval : 5000
//}
function resetPassWord() {
	var changePasswordFrom = new Ext.form.FormPanel({
				frame : false,
				border : false,
				labelAlign : 'right',
				labeWidth : 80,
				defaultType : 'textfield',
				defaults : {
					width : 260,
					allowBlank : false,
					msgTarget : 'side',
					minLength : 6,
					minLengthText : '密码不能少于6位',
					maxLength : 10,
					maxLengthText : '密码不能超过10位'
				},
				items : [{
							fieldLabel : '输入新密码',
							name : 'password',
							inputType : 'password',
							id : 'pword'
						}, {
							fieldLabel : '再次输入密码',
							name : 'secondPassword',
							inputType : 'password',
							blankText : '密码不能为空',
							id : 'opword'
						}]

			});
	var pwWin = new Ext.Window({
				width : 320,
				height : 180,
				layout : 'fit',
				closable : true,
				autoScroll : true,
				title : '修改密码',
				modal : true,
				padding : '10',
				border : false,
				buttonAlign : 'center',
				items : changePasswordFrom,
				buttons : [{
							text : '修改密码',
							handler : function() {
								if (!changePasswordFrom.getForm().isValid()) {
									return;
								}
								if (Ext.getCmp('pword').getValue() == Ext.getCmp('opword').getValue()) {
									changePasswordFrom.getForm().submit({
												url : PATH + 'system/menu!changePassword.do',
												success : function(f, action) {
													if (action.result.success) {
														msgShow('提示信息', '修改成功');
														pwWin.close()
													}
												},
												failure : function(f, action) {
													changePasswordFrom.getForm().reset();
													msgShow('提示信息', '修改失败');
												}
											});
								} else {
									msgShow('提示', '两次输入的密码不一样！', Ext.Msg.WARNING)
								}
							}
						}]
			}).show();
}

function logout() {
	Ext.Msg.confirm('系统提示', '确定退出吗？', function(btn) {
				if (btn == 'yes') {
					Ext.Ajax.request({
								url : PATH + 'system/menu!logout.do',
								callback : function(opt, success, response) {
									window.location.href = PATH
								}
							})
				} else {

				}

			}, this);
}
Ext.onReady(function() {
			// 请求数据，添加菜单树
			Ext.Ajax.request({
						url : PATH + 'system/menu!listMenuView.do',
						params : {
							roleids : _roleids,
							action : 'accordions'
						},
						callback : function(opt, success, response) {
							pushAccordions(Ext.JSON.decode(response.responseText));
						}
					})

			// 将生成的菜单树添加到财务统计panel中。
			// 暂时还没有实现点击手风琴再加载该手风琴下的菜单的功能，但是点击父节点菜单，加载子菜单实现了动态加载
			function pushAccordions(data) {
				var accordions = data.res
				Ext.each(accordions, function(item) {
							menu.add(buildTree(item))
						})
			}

			function buildTree(item) {
				return Ext.create('Ext.tree.Panel', {
							title : item.text,
							iconCls : item.iconCls,
							rootVisible : false,
							border : false,
							store : createTreeStore(item.id),
							listeners : {
								'itemclick' : function(view, record, item, index, e) {
									var item = record.raw
									if (item.leaf) {
										var tab = Ext.getCmp(item.id + 'tab')
										if (!tab) {
											url = "<iframe frameborder='0' width='100%' height='100%' src='" + item.url + "'>"
											tabs.add({
														id : item.id + 'tab',
														border : false,
														closable : true,
														html : url,
														baseCls : 'no-border',
														iconCls : item.iconCls,
														title : item.text
													}).show();
										} else {
											tabs.setActiveTab(item.id + 'tab');
										}
									}
								},
								scope : this
							}
						});
			}

			function createTreeStore(id) {
				return Ext.create('Ext.data.TreeStore', {
							defaultRootId : id,
							model : 'TreeModel',
							proxy : {
								type : 'ajax',
								extraParams : {
									roleids : _roleids,
									action : 'treenode'
								},
								url : PATH + "system/menu!listMenuView.do",
								reader : {
									type : 'json',
									root : 'res'
								}
							},
							root : {
								expanded : true
							},
							nodeParam : "id"
						})
			}

//			Ext.Ajax.request({
//						url : PATH + 'system/flow!countMyWorks.do',
//						callback : function(opt, success, response) {
//							new Ext.ux.ToastWindow({
//										title : '待办工作',
//										padding : '0 0 0 10',
//										border : 0,
//										html : '<a href="javascript:showWin();">' + response.responseText + '</a>',
//										iconCls : 'info'
//									}).show();
//						}
//					})
		})