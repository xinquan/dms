<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<w:wordDocument xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:sl="http://schemas.microsoft.com/schemaLibrary/2003/core" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882" w:macrosPresent="no" w:embeddedObjPresent="no" w:ocxPresent="no" xml:space="preserve">
<o:DocumentProperties>
<o:Title>淮海工学院新增课程入库审批表</o:Title>
<o:Author>HHIT-ZXQ</o:Author>
<o:LastAuthor>Win</o:LastAuthor>
<o:Revision>3</o:Revision>
<o:TotalTime>0</o:TotalTime>
<o:Created>2016-04-15T01:16:00Z</o:Created>
<o:LastSaved>2016-04-15T01:57:00Z</o:LastSaved>
<o:Pages>1</o:Pages>
<o:Words>75</o:Words>
<o:Characters>432</o:Characters>
<o:Company>HHIT-ZXQ</o:Company>
<o:Bytes>0</o:Bytes>
<o:Lines>3</o:Lines>
<o:Paragraphs>1</o:Paragraphs>
<o:CharactersWithSpaces>506</o:CharactersWithSpaces>
<o:Version>11.5604</o:Version>
</o:DocumentProperties>
<o:CustomDocumentProperties>
<o:KSOProductBuildVer dt:dt="string">2052-10.1.0.5458</o:KSOProductBuildVer>
</o:CustomDocumentProperties>
<w:fonts>
<w:defaultFonts w:ascii="Calibri" w:fareast="宋体" w:h-ansi="Calibri" w:cs="Times New Roman"/>
<w:font w:name="宋体">
<w:altName w:val="SimSun"/>
<w:panose-1 w:val="02010600030101010101"/>
<w:charset w:val="86"/>
<w:family w:val="Auto"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
</w:font>
<w:font w:name="黑体">
<w:altName w:val="SimHei"/>
<w:panose-1 w:val="02010609060101010101"/>
<w:charset w:val="86"/>
<w:family w:val="Modern"/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
</w:font>
<w:font w:name="Calibri">
<w:panose-1 w:val="020F0502020204030204"/>
<w:charset w:val="00"/>
<w:family w:val="Swiss"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="E00002FF" w:usb-1="4000ACFF" w:usb-2="00000001" w:usb-3="00000000" w:csb-0="0000019F" w:csb-1="00000000"/>
</w:font>
<w:font w:name="楷体_GB2312">
<w:altName w:val="楷体"/>
<w:charset w:val="86"/>
<w:family w:val="Swiss"/>
<w:pitch w:val="default"/>
<w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
</w:font>
<w:font w:name="@宋体">
<w:panose-1 w:val="02010600030101010101"/>
<w:charset w:val="86"/>
<w:family w:val="Auto"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
</w:font>
<w:font w:name="@黑体">
<w:panose-1 w:val="02010609060101010101"/>
<w:charset w:val="86"/>
<w:family w:val="Modern"/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
</w:font>
<w:font w:name="仿宋_GB2312">
<w:altName w:val="仿宋"/>
<w:charset w:val="86"/>
<w:family w:val="Swiss"/>
<w:pitch w:val="default"/>
<w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
</w:font>
<w:font w:name="@楷体_GB2312">
<w:charset w:val="86"/>
<w:family w:val="Swiss"/>
<w:pitch w:val="default"/>
<w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
</w:font>
<w:font w:name="@仿宋_GB2312">
<w:charset w:val="86"/>
<w:family w:val="Swiss"/>
<w:pitch w:val="default"/>
<w:sig w:usb-0="00000000" w:usb-1="00000000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
</w:font>
</w:fonts>
<w:styles>
<w:versionOfBuiltInStylenames w:val="4"/>
<w:latentStyles w:defLockedState="off" w:latentStyleCount="156"/>
<w:style w:type="paragraph" w:default="on" w:styleId="a">
<w:name w:val="Normal"/>
<wx:uiName wx:val="正文"/>
<w:rsid w:val="00962E43"/>
<w:pPr>
<w:widowControl w:val="off"/>
<w:jc w:val="both"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman"/>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="2"/>
<w:sz w:val="21"/>
<w:sz-cs w:val="24"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:default="on" w:styleId="a0">
<w:name w:val="Default Paragraph Font"/>
<wx:uiName wx:val="默认段落字体"/>
<w:semiHidden/>
</w:style>
<w:style w:type="table" w:default="on" w:styleId="a1">
<w:name w:val="Normal Table"/>
<wx:uiName wx:val="普通表格"/>
<w:semiHidden/>
<w:rPr>
<wx:font wx:val="Calibri"/>
</w:rPr>
<w:tblPr>
<w:tblInd w:w="0" w:type="dxa"/>
<w:tblCellMar>
<w:top w:w="0" w:type="dxa"/>
<w:left w:w="108" w:type="dxa"/>
<w:bottom w:w="0" w:type="dxa"/>
<w:right w:w="108" w:type="dxa"/>
</w:tblCellMar>
</w:tblPr>
</w:style>
<w:style w:type="list" w:default="on" w:styleId="a2">
<w:name w:val="No List"/>
<wx:uiName wx:val="无列表"/>
<w:semiHidden/>
</w:style>
<w:style w:type="character" w:styleId="Char">
<w:name w:val="页脚 Char"/>
<w:basedOn w:val="a0"/>
<w:link w:val="a3"/>
<w:rsid w:val="00962E43"/>
<w:rPr>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="Char0">
<w:name w:val="页眉 Char"/>
<w:basedOn w:val="a0"/>
<w:link w:val="a4"/>
<w:rsid w:val="00962E43"/>
<w:rPr>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="a4">
<w:name w:val="header"/>
<wx:uiName wx:val="页眉"/>
<w:basedOn w:val="a"/>
<w:link w:val="Char0"/>
<w:rsid w:val="00962E43"/>
<w:pPr>
<w:pStyle w:val="a4"/>
<w:pBdr>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="1" w:color="auto"/>
</w:pBdr>
<w:tabs>
<w:tab w:val="center" w:pos="4153"/>
<w:tab w:val="right" w:pos="8306"/>
</w:tabs>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:h-ansi="Calibri"/>
<wx:font wx:val="Calibri"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="a3">
<w:name w:val="footer"/>
<wx:uiName wx:val="页脚"/>
<w:basedOn w:val="a"/>
<w:link w:val="Char"/>
<w:rsid w:val="00962E43"/>
<w:pPr>
<w:pStyle w:val="a3"/>
<w:tabs>
<w:tab w:val="center" w:pos="4153"/>
<w:tab w:val="right" w:pos="8306"/>
</w:tabs>
<w:snapToGrid w:val="off"/>
<w:jc w:val="left"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Calibri" w:h-ansi="Calibri"/>
<wx:font wx:val="Calibri"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
</w:styles>
<w:shapeDefaults>
<o:shapedefaults v:ext="edit" spidmax="4098" fillcolor="white">
<v:fill color="white"/>
</o:shapedefaults>
<o:shapelayout v:ext="edit">
<o:idmap v:ext="edit" data="2"/>
</o:shapelayout>
</w:shapeDefaults>
<w:bgPict>
<w:background w:bgcolor="white"/>
</w:bgPict>
<w:docPr>
<w:view w:val="print"/>
<w:zoom w:percent="100"/>
<w:doNotEmbedSystemFonts/>
<w:bordersDontSurroundHeader/>
<w:bordersDontSurroundFooter/>
<w:proofState w:spelling="clean" w:grammar="clean"/>
<w:attachedTemplate w:val=""/>
<w:defaultTabStop w:val="420"/>
<w:drawingGridVerticalSpacing w:val="156"/>
<w:displayHorizontalDrawingGridEvery w:val="0"/>
<w:displayVerticalDrawingGridEvery w:val="2"/>
<w:punctuationKerning/>
<w:characterSpacingControl w:val="CompressPunctuation"/>
<w:webPageEncoding w:val="x-cp20936"/>
<w:optimizeForBrowser/>
<w:validateAgainstSchema/>
<w:saveInvalidXML w:val="off"/>
<w:ignoreMixedContent w:val="off"/>
<w:alwaysShowPlaceholderText w:val="off"/>
<w:compat>
<w:spaceForUL/>
<w:balanceSingleByteDoubleByteWidth/>
<w:doNotLeaveBackslashAlone/>
<w:ulTrailSpace/>
<w:doNotExpandShiftReturn/>
<w:adjustLineHeightInTable/>
<w:breakWrappedTables/>
<w:snapToGridInCell/>
<w:wrapTextWithPunct/>
<w:useAsianBreakRules/>
<w:dontGrowAutofit/>
<w:useFELayout/>
</w:compat>
</w:docPr>
<w:body>
<wx:sect>
<w:p>
<w:pPr>
<w:spacing w:line="400" w:line-rule="exact"/>
<w:ind w:first-line-chars="400"/>
<w:rPr>
<w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="宋体"/>
<wx:font wx:val="黑体"/>
<w:color w:val="000000"/>
<w:sz w:val="36"/>
<w:sz-cs w:val="36"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="黑体"/>
<w:color w:val="000000"/>
<w:sz w:val="36"/>
<w:sz-cs w:val="36"/>
</w:rPr>
<w:t>淮海工学院新增课程入库审批表</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="400" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="宋体"/>
<wx:font wx:val="黑体"/>
<w:color w:val="000000"/>
<w:sz w:val="36"/>
<w:sz-cs w:val="36"/>
</w:rPr>
</w:pPr>
</w:p>
<w:tbl>
<w:tblPr>
<w:tblW w:w="0" w:type="auto"/>
<w:jc w:val="center"/>
<w:tblBorders>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:tblBorders>
<w:tblLayout w:type="Fixed"/>
</w:tblPr>
<w:tblGrid>
<w:gridCol w:w="1008"/>
<w:gridCol w:w="802"/>
<w:gridCol w:w="1185"/>
<w:gridCol w:w="1019"/>
<w:gridCol w:w="630"/>
<w:gridCol w:w="770"/>
<w:gridCol w:w="994"/>
<w:gridCol w:w="360"/>
<w:gridCol w:w="1620"/>
<w:gridCol w:w="900"/>
</w:tblGrid>
<w:tr>
<w:trPr>
<w:trHeight w:val="414"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="1810" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="540" w:line-rule="exact"/>
<w:ind w:first-line-chars="100"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>课程名称</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2834" w:type="dxa"/>
<w:gridSpan w:val="3"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>${c.kcmc?if_exists} </w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2124" w:type="dxa"/>
<w:gridSpan w:val="3"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="440" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>课程代码</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="440" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:w w:val="80"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="楷体_GB2312" w:fareast="楷体_GB2312" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="楷体_GB2312"/>
<w:w w:val="80"/>
<w:sz w:val="22"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>（审批通过后由教务处确定）</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2520" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:trHeight w:val="642"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="1810" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="540" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>课程归属学院</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2834" w:type="dxa"/>
<w:gridSpan w:val="3"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>${c.department?if_exists}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2124" w:type="dxa"/>
<w:gridSpan w:val="3"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="540" w:line-rule="exact"/>
<w:ind w:first-line-chars="100"/>
<w:rPr>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>课程归属系</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2520" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>${c.kcssx?if_exists}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:trHeight w:val="706"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="1810" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="540" w:line-rule="exact"/>
<w:ind w:first-line-chars="100"/>
<w:rPr>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>面向专业</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2834" w:type="dxa"/>
<w:gridSpan w:val="3"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>${c.major?if_exists}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2124" w:type="dxa"/>
<w:gridSpan w:val="3"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="540" w:line-rule="exact"/>
<w:ind w:first-line-chars="150"/>
<w:rPr>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>课程性质</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2520" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>${c.kcxz?if_exists}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:trHeight w:val="614"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="1008" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>学分</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="802" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>${c.xf?if_exists}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1185" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>总学时</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1019" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>${c.zxs?if_exists}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1400" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>理论学时</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="994" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>${c.llxs?if_exists}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1980" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:w w:val="80"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:w w:val="80"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>实验（实践）学时</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="900" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>${c.syxs?if_exists}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="9288" w:type="dxa"/>
<w:gridSpan w:val="10"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>课程归属系意见：</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>${ssxyj?if_exists}</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>                               系主任（签字）：</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:ind w:first-line-chars="2250"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t> 年    月    日</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:trHeight w:val="2308"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="9288" w:type="dxa"/>
<w:gridSpan w:val="10"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>课程归属单位意见：</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>${dwyj?if_exists}</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="400" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>                               教学院长（签字）：</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="400" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>                                              年    月    日</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:trHeight w:val="1919"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="9288" w:type="dxa"/>
<w:gridSpan w:val="10"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>教务处意见：</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>${jwcyj?if_exists}</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:ind w:first-line-chars="1600"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>负责人签字：</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>                                              年    月    日</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:trHeight w:val="1198"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="9288" w:type="dxa"/>
<w:gridSpan w:val="10"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="480" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>备注：</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
</w:tbl>
<w:p>
<w:pPr>
<w:spacing w:line="400" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:color w:val="000000"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>说明：1.本表一式二份，一份存课程归属单位，一份存教务处；2.本表需附课程教学大纲；</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312" w:hint="fareast"/>
<wx:font wx:val="仿宋_GB2312"/>
</w:rPr>
<w:t> </w:t>
</w:r>
</w:p>
<w:p/>
<w:sectPr>
<w:pgSz w:w="11906" w:h="16838"/>
<w:pgMar w:top="1440" w:right="1800" w:bottom="1440" w:left="1800" w:header="851" w:footer="992" w:gutter="0"/>
<w:cols w:space="720"/>
<w:docGrid w:type="lines" w:line-pitch="312"/>
</w:sectPr>
</wx:sect>
</w:body>
</w:wordDocument>
