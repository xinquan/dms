<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<w:wordDocument xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:sl="http://schemas.microsoft.com/schemaLibrary/2003/core" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882" xmlns:wsp="http://schemas.microsoft.com/office/word/2003/wordml/sp2" w:macrosPresent="no" w:embeddedObjPresent="no" w:ocxPresent="no" xml:space="preserve">
	<w:ignoreElements w:val="http://schemas.microsoft.com/office/word/2003/wordml/sp2"/>
	<o:DocumentProperties>
		<o:Title></o:Title>
		<o:Author>Administrator</o:Author>
		<o:LastAuthor>AutoBVT</o:LastAuthor>
		<o:Revision>2</o:Revision>
		<o:TotalTime>1</o:TotalTime>
		<o:Created>2016-04-23T05:43:00Z</o:Created>
		<o:LastSaved>2016-04-23T05:43:00Z</o:LastSaved>
		<o:Pages>1</o:Pages>
		<o:Words>283</o:Words>
		<o:Characters>1615</o:Characters>
		<o:Company>Microsoft</o:Company>
		<o:Lines>13</o:Lines>
		<o:Paragraphs>3</o:Paragraphs>
		<o:CharactersWithSpaces>1895</o:CharactersWithSpaces>
		<o:Version>11.0000</o:Version>
	</o:DocumentProperties>
	<w:fonts>
		<w:defaultFonts w:ascii="Times New Roman" w:fareast="宋体" w:h-ansi="Times New Roman" w:cs="Times New Roman"/>
		<w:font w:name="宋体">
			<w:altName w:val="SimSun"/>
			<w:panose-1 w:val="02010600030101010101"/>
			<w:charset w:val="86"/>
			<w:family w:val="Auto"/>
			<w:pitch w:val="variable"/>
			<w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
		</w:font>
		<w:font w:name="黑体">
			<w:altName w:val="SimHei"/>
			<w:panose-1 w:val="02010609060101010101"/>
			<w:charset w:val="86"/>
			<w:family w:val="Modern"/>
			<w:pitch w:val="fixed"/>
			<w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
		</w:font>
		<w:font w:name="方正宋黑简体">
			<w:altName w:val="SimSun-ExtB"/>
			<w:panose-1 w:val="00000000000000000000"/>
			<w:charset w:val="86"/>
			<w:family w:val="Script"/>
			<w:notTrueType/>
			<w:pitch w:val="fixed"/>
			<w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
		</w:font>
		<w:font w:name="@宋体">
			<w:panose-1 w:val="02010600030101010101"/>
			<w:charset w:val="86"/>
			<w:family w:val="Auto"/>
			<w:pitch w:val="variable"/>
			<w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
		</w:font>
		<w:font w:name="@黑体">
			<w:panose-1 w:val="02010609060101010101"/>
			<w:charset w:val="86"/>
			<w:family w:val="Modern"/>
			<w:pitch w:val="fixed"/>
			<w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
		</w:font>
		<w:font w:name="@方正宋黑简体">
			<w:panose-1 w:val="00000000000000000000"/>
			<w:charset w:val="86"/>
			<w:family w:val="Script"/>
			<w:notTrueType/>
			<w:pitch w:val="fixed"/>
			<w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
		</w:font>
	</w:fonts>
	<w:styles>
		<w:versionOfBuiltInStylenames w:val="4"/>
		<w:latentStyles w:defLockedState="off" w:latentStyleCount="156"/>
		<w:style w:type="paragraph" w:default="on" w:styleId="a">
			<w:name w:val="Normal"/>
			<wx:uiName wx:val="正文"/>
			<w:rsid w:val="009657E6"/>
			<w:pPr>
				<w:widowControl w:val="off"/>
				<w:jc w:val="both"/>
			</w:pPr>
			<w:rPr>
				<wx:font wx:val="Times New Roman"/>
				<w:kern w:val="2"/>
				<w:sz w:val="21"/>
				<w:sz-cs w:val="24"/>
				<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="1">
			<w:name w:val="heading 1"/>
			<wx:uiName wx:val="标题 1"/>
			<w:basedOn w:val="a"/>
			<w:next w:val="a"/>
			<w:link w:val="CharChar14"/>
			<w:rsid w:val="009657E6"/>
			<w:pPr>
				<w:pStyle w:val="1"/>
				<w:keepNext/>
				<w:keepLines/>
				<w:spacing w:before="340" w:after="330" w:line="576" w:line-rule="auto"/>
				<w:outlineLvl w:val="0"/>
			</w:pPr>
			<w:rPr>
				<wx:font wx:val="Times New Roman"/>
				<w:b/>
				<w:b-cs/>
				<w:kern w:val="44"/>
				<w:sz w:val="44"/>
				<w:sz-cs w:val="44"/>
			</w:rPr>
		</w:style>
		<w:style w:type="character" w:default="on" w:styleId="a0">
			<w:name w:val="Default Paragraph Font"/>
			<wx:uiName wx:val="默认段落字体"/>
			<w:semiHidden/>
		</w:style>
		<w:style w:type="table" w:default="on" w:styleId="a1">
			<w:name w:val="Normal Table"/>
			<wx:uiName wx:val="普通表格"/>
			<w:semiHidden/>
			<w:rPr>
				<wx:font wx:val="Times New Roman"/>
			</w:rPr>
			<w:tblPr>
				<w:tblInd w:w="0" w:type="dxa"/>
				<w:tblCellMar>
					<w:top w:w="0" w:type="dxa"/>
					<w:left w:w="108" w:type="dxa"/>
					<w:bottom w:w="0" w:type="dxa"/>
					<w:right w:w="108" w:type="dxa"/>
				</w:tblCellMar>
			</w:tblPr>
		</w:style>
		<w:style w:type="list" w:default="on" w:styleId="a2">
			<w:name w:val="No List"/>
			<wx:uiName wx:val="无列表"/>
			<w:semiHidden/>
		</w:style>
		<w:style w:type="character" w:styleId="CharChar8">
			<w:name w:val=" Char Char8"/>
			<w:link w:val="a3"/>
			<w:locked/>
			<w:rsid w:val="009657E6"/>
			<w:rPr>
				<w:rFonts w:ascii="宋体" w:fareast="宋体" w:h-ansi="Courier New" w:cs="Courier New"/>
				<w:kern w:val="2"/>
				<w:sz w:val="21"/>
				<w:sz-cs w:val="21"/>
				<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="a3">
			<w:name w:val="Plain Text"/>
			<wx:uiName wx:val="纯文本"/>
			<w:basedOn w:val="a"/>
			<w:link w:val="CharChar8"/>
			<w:rsid w:val="009657E6"/>
			<w:pPr>
				<w:pStyle w:val="a3"/>
			</w:pPr>
			<w:rPr>
				<w:rFonts w:ascii="宋体" w:h-ansi="Courier New" w:cs="Courier New"/>
				<wx:font wx:val="Courier New"/>
				<w:sz-cs w:val="21"/>
			</w:rPr>
		</w:style>
		<w:style w:type="character" w:styleId="CharChar14">
			<w:name w:val=" Char Char14"/>
			<w:link w:val="1"/>
			<w:locked/>
			<w:rsid w:val="009657E6"/>
			<w:rPr>
				<w:rFonts w:fareast="宋体"/>
				<w:b/>
				<w:b-cs/>
				<w:kern w:val="44"/>
				<w:sz w:val="44"/>
				<w:sz-cs w:val="44"/>
				<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
			</w:rPr>
		</w:style>
	</w:styles>
	<w:docPr>
		<w:view w:val="print"/>
		<w:zoom w:percent="100"/>
		<w:dontDisplayPageBoundaries/>
		<w:bordersDontSurroundHeader/>
		<w:bordersDontSurroundFooter/>
		<w:proofState w:spelling="clean" w:grammar="clean"/>
		<w:attachedTemplate w:val=""/>
		<w:defaultTabStop w:val="420"/>
		<w:drawingGridVerticalSpacing w:val="156"/>
		<w:displayHorizontalDrawingGridEvery w:val="0"/>
		<w:displayVerticalDrawingGridEvery w:val="2"/>
		<w:punctuationKerning/>
		<w:characterSpacingControl w:val="CompressPunctuation"/>
		<w:optimizeForBrowser/>
		<w:validateAgainstSchema/>
		<w:saveInvalidXML w:val="off"/>
		<w:ignoreMixedContent w:val="off"/>
		<w:alwaysShowPlaceholderText w:val="off"/>
		<w:compat>
			<w:spaceForUL/>
			<w:balanceSingleByteDoubleByteWidth/>
			<w:doNotLeaveBackslashAlone/>
			<w:ulTrailSpace/>
			<w:doNotExpandShiftReturn/>
			<w:adjustLineHeightInTable/>
			<w:breakWrappedTables/>
			<w:snapToGridInCell/>
			<w:wrapTextWithPunct/>
			<w:useAsianBreakRules/>
			<w:dontGrowAutofit/>
			<w:useFELayout/>
		</w:compat>
		<wsp:rsids>
			<wsp:rsidRoot wsp:val="009657E6"/>
			<wsp:rsid wsp:val="004962C6"/>
			<wsp:rsid wsp:val="004E3E8B"/>
			<wsp:rsid wsp:val="007E7D14"/>
			<wsp:rsid wsp:val="008C7507"/>
			<wsp:rsid wsp:val="009657E6"/>
			<wsp:rsid wsp:val="00A353BF"/>
			<wsp:rsid wsp:val="00C0661B"/>
			<wsp:rsid wsp:val="00C43E14"/>
			<wsp:rsid wsp:val="00DB37A7"/>
			<wsp:rsid wsp:val="00F6236E"/>
		</wsp:rsids>
	</w:docPr>
	<w:body>
		<wx:sect>
			<wx:sub-section>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="00C43E14" wsp:rsidP="009657E6">
					<w:pPr>
						<w:pStyle w:val="1"/>
						<w:jc w:val="center"/>
						<w:rPr>
							<w:sz w:val="18"/>
							<w:sz-cs w:val="18"/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<w:sz w:val="32"/>
							<w:sz-cs w:val="32"/>
						</w:rPr>
						<w:t>${plan.zy}</w:t>
					</w:r>
					<w:r wsp:rsidR="009657E6">
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:sz w:val="32"/>
							<w:sz-cs w:val="32"/>
						</w:rPr>
						<w:t>专业本科人才培养方案</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:jc w:val="center"/>
						<w:rPr>
							<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体"/>
							<wx:font wx:val="方正宋黑简体"/>
							<w:u w:val="single"/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
							<wx:font wx:val="方正宋黑简体"/>
						</w:rPr>
						<w:t>门类：</w:t>
					</w:r>
					<w:r wsp:rsidR="00C43E14">
						<w:rPr>
							<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
							<wx:font wx:val="方正宋黑简体"/>
							<w:u w:val="single"/>
						</w:rPr>
						<w:t>${plan.ml}</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体"/>
							<wx:font wx:val="方正宋黑简体"/>
						</w:rPr>
						<w:t>  </w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
							<wx:font wx:val="方正宋黑简体"/>
						</w:rPr>
						<w:t>专业代码：</w:t>
					</w:r>
					<w:r wsp:rsidR="00C43E14">
						<w:rPr>
							<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
							<wx:font wx:val="方正宋黑简体"/>
							<w:u w:val="single"/>
						</w:rPr>
						<w:t>${plan.zydm}</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体"/>
							<wx:font wx:val="方正宋黑简体"/>
						</w:rPr>
						<w:t>  </w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
							<wx:font wx:val="方正宋黑简体"/>
						</w:rPr>
						<w:t>标准学制：</w:t>
					</w:r>
					<w:r wsp:rsidR="00C43E14">
						<w:rPr>
							<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
							<wx:font wx:val="方正宋黑简体"/>
							<w:u w:val="single"/>
						</w:rPr>
						<w:t>${plan.bzxz}</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体"/>
							<wx:font wx:val="方正宋黑简体"/>
						</w:rPr>
						<w:t>  </w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
							<wx:font wx:val="方正宋黑简体"/>
						</w:rPr>
						<w:t>授予学位：</w:t>
					</w:r>
					<w:r wsp:rsidR="00C43E14">
						<w:rPr>
							<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
							<wx:font wx:val="方正宋黑简体"/>
							<w:u w:val="single"/>
						</w:rPr>
						<w:t>${plan.ml}</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体"/>
							<wx:font wx:val="方正宋黑简体"/>
							<w:u w:val="single"/>
						</w:rPr>
						<w:t>    </w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
							<wx:font wx:val="方正宋黑简体"/>
						</w:rPr>
						<w:t>学士</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:jc w:val="center"/>
					</w:pPr>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:rPr>
							<w:rFonts w:ascii="宋体"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
						<w:t>一、培养目标</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
						<w:t> </w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00636ADC" wsp:rsidRDefault="00C43E14" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:ind w:first-line-chars="200"/>
						<w:rPr>
							<w:rFonts w:ascii="宋体"/>
							<wx:font wx:val="宋体"/>
							<w:spacing w:val="-6"/>
							<w:sz-cs w:val="21"/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
						</w:rPr>
						<w:t>${plan.pymb}</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
					</w:pPr>
					<w:r wsp:rsidRPr="00976207">
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
						<w:t>二、</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
						<w:t>毕业</w:t>
					</w:r>
					<w:r wsp:rsidRPr="00976207">
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
						<w:t>要求</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="00C43E14" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:ind w:first-line="420"/>
						<w:rPr>
							<w:rFonts w:ascii="宋体"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
						</w:rPr>
						<w:t>${plan.jbyq}</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:rPr>
							<w:rFonts w:ascii="宋体"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
						<w:t>三、主干学科</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00DB37A7" wsp:rsidRDefault="00C43E14" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:ind w:first-line="420"/>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
						</w:rPr>
					</w:pPr>
					<w:r wsp:rsidRPr="00DB37A7">
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
						</w:rPr>
						<w:t>${plan.zgxk}</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:rPr>
							<w:rFonts w:ascii="宋体"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
						<w:t>四、主干课程</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
						<w:t>  </w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00976207" wsp:rsidRDefault="00C43E14" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:ind w:first-line="420"/>
						<w:rPr>
							<w:rFonts w:ascii="宋体"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
						</w:rPr>
						<w:t>${plan.zykc}</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
						<w:t>五、主要实践环节</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRPr="004E3E8B" wsp:rsidRDefault="004E3E8B" wsp:rsidP="004E3E8B">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:ind w:first-line="420"/>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
					</w:pPr>
					<w:r wsp:rsidRPr="00CA77F6">
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
						</w:rPr>
						<w:t>${plan.zysjhj}</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
						<w:t>六、相关职业资格证书</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00FA563D" wsp:rsidRDefault="004E3E8B" wsp:rsidP="004E3E8B">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:ind w:first-line="420"/>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
					</w:pPr>
					<w:r wsp:rsidRPr="00CA77F6">
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
						</w:rPr>
						<w:t>${plan.zdxfyq}</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:pStyle w:val="a3"/>
						<w:spacing w:line="580" w:line-rule="exact"/>
						<w:jc w:val="left"/>
						<w:rPr>
							<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
						</w:rPr>
						<w:t>七、</w:t>
					</w:r>
					<w:r wsp:rsidRPr="00FA1DDC">
						<w:rPr>
							<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
						</w:rPr>
						<w:t>毕业</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
						</w:rPr>
						<w:t>及学位授予</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:ind w:first-line="420"/>
						<w:rPr>
							<w:rFonts w:ascii="宋体"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>1.</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>毕业标准</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:ind w:first-line="420"/>
						<w:rPr>
							<w:rFonts w:ascii="宋体"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>（</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>1</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>）具有良好的思想品德和身体素质，符合学校规定的德育和体育标准，《国家学生体质健康标准（2014年修订）》测试成绩达到50分（含50）以上；</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:ind w:first-line="420"/>
						<w:rPr>
							<w:rFonts w:ascii="宋体"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>（</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>2</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>）在规定的修业年限内，完成人才培养方案规定的所有课程和环节，取得规定的170个学业学分；</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRPr="009C3C2A" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:ind w:first-line="420"/>
						<w:rPr>
							<w:rFonts w:ascii="宋体"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
					</w:pPr>
					<w:r wsp:rsidRPr="009C3C2A">
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>（</w:t>
					</w:r>
					<w:r wsp:rsidRPr="009C3C2A">
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>3</w:t>
					</w:r>
					<w:r wsp:rsidRPr="009C3C2A">
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>）</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>取得规定的10个素质拓展学分（其中A类4个学分，B类6个学分）。</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:ind w:first-line="420"/>
						<w:rPr>
							<w:rFonts w:ascii="宋体"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>2.</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>学位授予</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRPr="0097638C" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:pStyle w:val="a3"/>
						<w:spacing w:line="580" w:line-rule="exact"/>
						<w:ind w:first-line="420"/>
						<w:jc w:val="left"/>
						<w:rPr>
							<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>符合淮海工学院学士学位授予条例规定，可授予</w:t>
					</w:r>
					<w:r wsp:rsidR="004E3E8B">
						<w:rPr>
							<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:u w:val="single"/>
						</w:rPr>
						<w:t>${plan.ml}</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>学士学位。</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:pStyle w:val="a3"/>
						<w:spacing w:line="580" w:line-rule="exact"/>
						<w:jc w:val="left"/>
						<w:rPr>
							<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
						</w:rPr>
					</w:pPr>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:pStyle w:val="a3"/>
						<w:spacing w:line="580" w:line-rule="exact"/>
						<w:jc w:val="left"/>
						<w:rPr>
							<w:rFonts w:h-ansi="宋体"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
						</w:rPr>
						<w:t>八、</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
						<w:t>课程构成及学分分配汇总表</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="580" w:line-rule="exact"/>
						<w:jc w:val="center"/>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>表</w:t>
					</w:r>
					<w:r>
						<w:t>1</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>课程构成及学分分配汇总表</w:t>
					</w:r>
				</w:p>
				<w:tbl>
					<w:tblPr>
						<w:tblW w:w="4783" w:type="pct"/>
						<w:jc w:val="center"/>
						<w:tblBorders>
							<w:top w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
							<w:left w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
							<w:bottom w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
							<w:right w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
							<w:insideH w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
							<w:insideV w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
						</w:tblBorders>
						<w:tblLayout w:type="Fixed"/>
						<w:tblCellMar>
							<w:left w:w="0" w:type="dxa"/>
							<w:right w:w="0" w:type="dxa"/>
						</w:tblCellMar>
					</w:tblPr>
					<w:tblGrid>
						<w:gridCol w:w="1865"/>
						<w:gridCol w:w="2937"/>
						<w:gridCol w:w="1498"/>
						<w:gridCol w:w="1363"/>
						<w:gridCol w:w="1537"/>
						<w:gridCol w:w="1518"/>
					</w:tblGrid>
					<w:tr wsp:rsidR="009657E6" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:cantSplit/>
							<w:trHeight w:val="459"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2240" w:type="pct"/>
								<w:gridSpan w:val="2"/>
								<w:vmerge w:val="restart"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>课</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t> </w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>程</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t> </w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>类</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t> </w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>别</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1335" w:type="pct"/>
								<w:gridSpan w:val="2"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>学分</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1425" w:type="pct"/>
								<w:gridSpan w:val="2"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>占总学分比例</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t> %</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="009657E6" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:cantSplit/>
							<w:trHeight w:val="524"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2240" w:type="pct"/>
								<w:gridSpan w:val="2"/>
								<w:vmerge/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="699" w:type="pct"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="636" w:type="pct"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="0048312F" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
								</w:pPr>
								<w:r wsp:rsidRPr="0048312F">
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>其中：实践环节学分</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="717" w:type="pct"/>
								<w:tcBorders>
									<w:top w:val="nil"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="708" w:type="pct"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="0048312F" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
								</w:pPr>
								<w:r wsp:rsidRPr="0048312F">
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>其中：实践环节比例</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="004962C6" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:cantSplit/>
							<w:trHeight w:val="376"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="870" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>通识教育平台</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1370" w:type="pct"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>公共基础必修课程</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="699" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>${pt1}+</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>【</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00860D50">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>10</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>】</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="636" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="004962C6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:spacing w:line="240" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>${sj1}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="717" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="004962C6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:spacing w:line="240" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>${bl1}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="708" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="004962C6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:spacing w:line="240" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>${sjbl1}</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="004962C6" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:cantSplit/>
							<w:trHeight w:val="365"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="870" w:type="pct"/>
								<w:vmerge/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1370" w:type="pct"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRPr="00401BAB" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
										<w:w w:val="90"/>
									</w:rPr>
								</w:pPr>
								<w:r wsp:rsidRPr="004A72FB">
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:w w:val="90"/>
										<w:sz w:val="18"/>
									</w:rPr>
									<w:t>创新创业教育与素质拓展课程</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="699" w:type="pct"/>
								<w:vmerge/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="636" w:type="pct"/>
								<w:vmerge/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="717" w:type="pct"/>
								<w:vmerge/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="708" w:type="pct"/>
								<w:vmerge/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="004962C6" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:cantSplit/>
							<w:trHeight w:val="408"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="870" w:type="pct"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>大类教育平台</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1370" w:type="pct"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>学科基础课程</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="699" w:type="pct"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>${pt2}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="636" w:type="pct"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="004962C6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:spacing w:line="240" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>${sj2}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="717" w:type="pct"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="004962C6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:spacing w:line="240" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>${bl2}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="708" w:type="pct"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="004962C6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:spacing w:line="240" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>${sjbl2}</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="004962C6" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:cantSplit/>
							<w:trHeight w:val="335"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="870" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>专业教育平台</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1370" w:type="pct"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>专业核心课程</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="699" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRPr="000C1D24" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>${pt3}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="636" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRPr="000C1D24" wsp:rsidRDefault="004962C6" wsp:rsidP="004962C6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:spacing w:line="240" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>${sj3}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="717" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRPr="000C1D24" wsp:rsidRDefault="004962C6" wsp:rsidP="004962C6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:spacing w:line="240" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>${bl3}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="708" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRPr="000C1D24" wsp:rsidRDefault="004962C6" wsp:rsidP="004962C6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:spacing w:line="240" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>${sjbl3}</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="004962C6" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:cantSplit/>
							<w:trHeight w:val="334"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="870" w:type="pct"/>
								<w:vmerge/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1370" w:type="pct"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>专业拓展课程</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="699" w:type="pct"/>
								<w:vmerge/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRPr="000C1D24" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="636" w:type="pct"/>
								<w:vmerge/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRPr="000C1D24" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="717" w:type="pct"/>
								<w:vmerge/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRPr="000C1D24" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="708" w:type="pct"/>
								<w:vmerge/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRPr="000C1D24" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="004962C6" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:cantSplit/>
							<w:trHeight w:val="447"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2240" w:type="pct"/>
								<w:gridSpan w:val="2"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>合</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>    </w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>计</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="699" w:type="pct"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRPr="000C1D24" wsp:rsidRDefault="004962C6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>${pthj}+</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>【</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00860D50">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>10</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>】</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="636" w:type="pct"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRPr="000C1D24" wsp:rsidRDefault="004962C6" wsp:rsidP="004962C6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:spacing w:line="240" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>${sjhj}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="717" w:type="pct"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRPr="000C1D24" wsp:rsidRDefault="004962C6" wsp:rsidP="004962C6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:spacing w:line="240" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>100</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="708" w:type="pct"/>
								<w:tcBorders>
									<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
									<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
									<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
								</w:tcBorders>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="004962C6" wsp:rsidRPr="000C1D24" wsp:rsidRDefault="004962C6" wsp:rsidP="004962C6">
								<w:pPr>
									<w:adjustRightInd w:val="off"/>
									<w:snapToGrid w:val="off"/>
									<w:spacing w:line="240" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>${sjblhj}</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
				</w:tbl>
				<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<w:b/>
						</w:rPr>
					</w:pPr>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<w:b/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
						</w:rPr>
						<w:t>九、课程指导性修读计划</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:rPr>
							<w:b/>
						</w:rPr>
					</w:pPr>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00BB6AAC" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="240" w:line-rule="at-least"/>
						<w:jc w:val="center"/>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>表</w:t>
					</w:r>
					<w:r>
						<w:t>2</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
						</w:rPr>
						<w:t> </w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>课程指导性修读计划</w:t>
					</w:r>
					<w:r>
						<w:t> </w:t>
					</w:r>
				</w:p>
				<w:tbl>
					<w:tblPr>
						<w:tblW w:w="8747" w:type="dxa"/>
						<w:jc w:val="center"/>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
						</w:tblBorders>
						<w:tblLayout w:type="Fixed"/>
						<w:tblCellMar>
							<w:left w:w="0" w:type="dxa"/>
							<w:right w:w="0" w:type="dxa"/>
						</w:tblCellMar>
						<w:tblLook w:val="00A0"/>
					</w:tblPr>
					<w:tblGrid>
						<w:gridCol w:w="421"/>
						<w:gridCol w:w="475"/>
						<w:gridCol w:w="294"/>
						<w:gridCol w:w="1369"/>
						<w:gridCol w:w="18"/>
						<w:gridCol w:w="2489"/>
						<w:gridCol w:w="462"/>
						<w:gridCol w:w="120"/>
						<w:gridCol w:w="12"/>
						<w:gridCol w:w="569"/>
						<w:gridCol w:w="707"/>
						<w:gridCol w:w="7"/>
						<w:gridCol w:w="1804"/>
					</w:tblGrid>
					<w:tr wsp:rsidR="009657E6" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:trHeight w:val="841"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="896" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="260" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>课程类别</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="294" w:type="dxa"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="260" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>课程性质</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1387" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="260" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>课程代码</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2489" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="260" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>课程名称</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="582" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="260" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>学分</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="260" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>总学时（周数）</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="260" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>讲课学时</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="260" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>实验（实践）学时</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="581" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="260" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>开课学期</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="707" w:type="dxa"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="200" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:w w:val="80"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:w w:val="80"/>
									</w:rPr>
									<w:t>集中性</w:t>
								</w:r>
							</w:p>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="200" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:w w:val="80"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:w w:val="80"/>
									</w:rPr>
									<w:t>实践环节</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1811" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="260" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:sz-cs w:val="21"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>修读说明</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="009657E6" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:left="113" w:right="113"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>通识教育平台</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="475" w:type="dxa"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:left="113" w:right="113"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>公共基础必修课程</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="294" w:type="dxa"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:left="113" w:right="113"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>必修</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1387" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2489" w:type="dxa"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00BB6AAC" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:rPr>
										<w:sz-cs w:val="21"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="582" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="581" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="707" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00C3495C" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:color w:val="FF0000"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1811" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00C3495C" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:color w:val="FF0000"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
					</w:tr>
					<#list planlist1 as c>
					<w:tr wsp:rsidR="007E7D14" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="007E7D14">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="475" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="007E7D14">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="294" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="007E7D14">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1387" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="007E7D14">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kcdm}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2489" w:type="dxa"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="007E7D14">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kcmc}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="582" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="007E7D14">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.xf?if_exists}</w:t>
									<#if c.qtxf?exists>
									<w:t>(${c.qtxf})</w:t>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.zxs?exists>
									<#if c.zxs!=0>
									<w:t>${c.zxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.llxs?exists>
									<#if c.llxs!=0>
									<w:t>${c.llxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.syxs?exists>
									<#if c.syxs!=0>
									<w:t>${c.syxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="581" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="007E7D14">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kkxq?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="707" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="007E7D14">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.sjhj}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1811" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="007E7D14">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.xdsm?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					</#list>
					<w:tr wsp:rsidR="009657E6" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="475" w:type="dxa"/>
								<w:vmerge w:val="restart"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r wsp:rsidRPr="0047499D">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="20"/>
									</w:rPr>
									<w:t>创新创业教育与素质拓展课程</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="294" w:type="dxa"/>
								<w:vmerge w:val="restart"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>必修</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1387" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2489" w:type="dxa"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00BB6AAC" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:rPr>
										<w:sz-cs w:val="21"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="582" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="581" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="707" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00C3495C" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:color w:val="FF0000"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1811" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00C3495C" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:color w:val="FF0000"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
					</w:tr>
					<#list planlist2 as c>
					<w:tr wsp:rsidR="007E7D14" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="475" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="294" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1387" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kcdm}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2489" w:type="dxa"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>${c.kcmc}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="582" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRPr="005255AB" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.xf?if_exists}</w:t>
									<#if c.qtxf?exists>
									<w:t>(${c.qtxf})</w:t>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.zxs?exists>
									<#if c.zxs!=0>
									<w:t>${c.zxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.llxs?exists>
									<#if c.llxs!=0>
									<w:t>${c.llxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.syxs?exists>
									<#if c.syxs!=0>
									<w:t>${c.syxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="581" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kkxq?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="714" w:type="dxa"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.sjhj}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1804" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRPr="00985310" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r wsp:rsidRPr="00985310">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.xdsm?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					</#list>
					<w:tr wsp:rsidR="009657E6" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:trHeight w:val="95"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="475" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="294" w:type="dxa"/>
								<w:vmerge w:val="restart"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>选修</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1387" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00BB6AAC" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:rPr>
										<w:sz-cs w:val="21"/>
									</w:rPr>
								</w:pPr>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>公共选修课程</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2489" w:type="dxa"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00BB6AAC" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
								</w:pPr>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>人文</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>修养教育</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>类</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="582" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>6</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="3099" w:type="dxa"/>
								<w:gridSpan w:val="8"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00191FAC" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:first-line-chars="150"/>
									<w:rPr>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>学生应至少在每类课程中选修</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>1</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>门，且此模块须修满</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>6</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>学分。</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="009657E6" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:trHeight w:val="261"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="475" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="294" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:left="113" w:right="113"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1387" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:vmerge/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00BB6AAC" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:rPr>
										<w:sz-cs w:val="21"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2489" w:type="dxa"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00BB6AAC" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>自然科技教育</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>类</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="582" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:vmerge/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="3099" w:type="dxa"/>
								<w:gridSpan w:val="8"/>
								<w:vmerge/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="009657E6" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:trHeight w:val="178"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="475" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="294" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:left="113" w:right="113"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1387" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:vmerge/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00BB6AAC" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:rPr>
										<w:sz-cs w:val="21"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2489" w:type="dxa"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00BB6AAC" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
								</w:pPr>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>创新创业教育类</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="582" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:vmerge/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="3099" w:type="dxa"/>
								<w:gridSpan w:val="8"/>
								<w:vmerge/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="009657E6" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:trHeight w:val="56"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="475" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="294" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1387" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00BB6AAC" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:rPr>
										<w:sz-cs w:val="21"/>
									</w:rPr>
								</w:pPr>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>素质拓展学分</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2489" w:type="dxa"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="003A27D3" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:rPr>
										<w:w w:val="90"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
								</w:pPr>
								<w:r wsp:rsidRPr="003A27D3">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>创新</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>创业实践</w:t>
								</w:r>
								<w:r wsp:rsidRPr="003A27D3">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:w w:val="90"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>（</w:t>
								</w:r>
								<w:r wsp:rsidRPr="003A27D3">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:w w:val="90"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>A</w:t>
								</w:r>
								<w:r wsp:rsidRPr="003A27D3">
									<w:rPr>
										<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:w w:val="90"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>类）</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="594" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00860D50" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>【</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00860D50">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>4</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>】</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="3087" w:type="dxa"/>
								<w:gridSpan w:val="8"/>
								<w:vmerge w:val="restart"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:first-line-chars="50"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>根据《淮海工学院素质拓展学分认定实施办法》（淮工院发</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>[</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>2016</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>]</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>168</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>号）认定。</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="009657E6" wsp:rsidRPr="00B43EFA" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:trHeight w:val="56"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="475" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="294" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1387" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:vmerge/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="000573A7" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:rPr>
										<w:sz w:val="20"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2489" w:type="dxa"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00BB6AAC" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:rPr>
										<w:sz-cs w:val="21"/>
									</w:rPr>
								</w:pPr>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>社会实践</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>活动（</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>B</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>类</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>）</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="594" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="000573A7" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>【</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>6</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>】</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="3087" w:type="dxa"/>
								<w:gridSpan w:val="8"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="000573A7" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="009657E6" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:trHeight w:val="333"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="5066" w:type="dxa"/>
								<w:gridSpan w:val="8"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>平台应修学分合计</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="3681" w:type="dxa"/>
								<w:gridSpan w:val="8"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${pt1}</w:t>
								</w:r>
								<w:r wsp:rsidR="009657E6">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>+</w:t>
								</w:r>
								<w:r wsp:rsidR="009657E6" wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>【</w:t>
								</w:r>
								<w:r wsp:rsidR="009657E6" wsp:rsidRPr="00860D50">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>10</w:t>
								</w:r>
								<w:r wsp:rsidR="009657E6" wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>】</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="009657E6" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:left="113" w:right="113"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>大类教育平台</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="475" w:type="dxa"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>学科基础课程</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="294" w:type="dxa"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>必修</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1387" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2489" w:type="dxa"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00BB6AAC" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:rPr>
										<w:sz-cs w:val="21"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="582" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="581" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="707" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00C3495C" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:color w:val="FF0000"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1811" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00C3495C" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:color w:val="FF0000"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
					</w:tr>
					<#list planlist3 as c>
					<w:tr wsp:rsidR="007E7D14" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="475" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="294" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1369" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kcdm}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2507" w:type="dxa"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>${c.kcmc}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="462" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRPr="005255AB" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.xf?if_exists}</w:t>
									<#if c.qtxf?exists>
									<w:t>(${c.qtxf})</w:t>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.zxs?exists>
									<#if c.zxs!=0>
									<w:t>${c.zxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.llxs?exists>
									<#if c.llxs!=0>
									<w:t>${c.llxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.syxs?exists>
									<#if c.syxs!=0>
									<w:t>${c.syxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="701" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kkxq?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="707" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.sjhj}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1811" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRPr="00985310" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r wsp:rsidRPr="00985310">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.xdsm?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					</#list>
					<w:tr wsp:rsidR="007E7D14" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="475" w:type="dxa"/>
								<w:vmerge/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="294" w:type="dxa"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>必修</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1387" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2489" w:type="dxa"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00BB6AAC" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:rPr>
										<w:sz-cs w:val="21"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="582" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="581" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="707" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00C3495C" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:color w:val="FF0000"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1811" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00C3495C" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:color w:val="FF0000"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
					</w:tr>
					<#list planlist4 as c>
					<w:tr wsp:rsidR="007E7D14" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="475" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="294" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1369" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kcdm}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2507" w:type="dxa"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>${c.kcmc}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="462" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRPr="005255AB" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.xf?if_exists}</w:t>
									<#if c.qtxf?exists>
									<w:t>(${c.qtxf})</w:t>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.zxs?exists>
									<#if c.zxs!=0>
									<w:t>${c.zxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.llxs?exists>
									<#if c.llxs!=0>
									<w:t>${c.llxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.syxs?exists>
									<#if c.syxs!=0>
									<w:t>${c.syxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="701" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kkxq?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="707" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.sjhj}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1811" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRPr="00985310" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r wsp:rsidRPr="00985310">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.xdsm?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					</#list>
					<w:tr wsp:rsidR="007E7D14" wsp:rsidTr="00C0661B">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="4645" w:type="dxa"/>
								<w:gridSpan w:val="8"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>平台应修学分合计</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="3681" w:type="dxa"/>
								<w:gridSpan w:val="7"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${pt2}</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="007E7D14" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:left="113" w:right="113"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>专业教育平台</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="475" w:type="dxa"/>
								<w:vmerge w:val="restart"/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:left="113" w:right="113"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>专业核心课程</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="294" w:type="dxa"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:left="113" w:right="113"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>必修</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1387" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2489" w:type="dxa"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00BB6AAC" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:rPr>
										<w:sz-cs w:val="21"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="582" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="581" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="707" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00C3495C" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:color w:val="FF0000"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1811" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00C3495C" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:color w:val="FF0000"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
					</w:tr>
					<#list planlist5 as c>
					<w:tr wsp:rsidR="007E7D14" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:left="113" w:right="113"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="475" w:type="dxa"/>
								<w:vmerge/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:left="113" w:right="113"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="294" w:type="dxa"/>
								<w:vmerge/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:left="113" w:right="113"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1369" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kcdm}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2507" w:type="dxa"/>
								
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>${c.kcmc}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="462" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRPr="005255AB" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.xf?if_exists}</w:t>
									<#if c.qtxf?exists>
									<w:t>(${c.qtxf})</w:t>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.zxs?exists>
									<#if c.zxs!=0>
									<w:t>${c.zxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.llxs?exists>
									<#if c.llxs!=0>
									<w:t>${c.llxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.syxs?exists>
									<#if c.syxs!=0>
									<w:t>${c.syxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="701" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kkxq?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="707" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.sjhj}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1811" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRPr="00985310" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r wsp:rsidRPr="00985310">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.xdsm?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					</#list>
					<w:tr wsp:rsidR="007E7D14" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:trHeight w:val="347"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="4645" w:type="dxa"/>
								<w:gridSpan w:val="8"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>本模块应修学分小计</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="3681" w:type="dxa"/>
								<w:gridSpan w:val="7"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${xfhj3}</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="009657E6" wsp:rsidTr="007E7D14">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge w:val="restart"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="475" w:type="dxa"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:left="113" w:right="113"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:sz-cs w:val="21"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>专业拓展课程</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="294" w:type="dxa"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>选修</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1387" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2489" w:type="dxa"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00BB6AAC" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:rPr>
										<w:sz-cs w:val="21"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="582" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="581" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="707" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00C3495C" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:color w:val="FF0000"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1811" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00C3495C" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:color w:val="FF0000"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
					</w:tr>
					<#list planlist6 as c>
					<w:tr wsp:rsidR="007E7D14" wsp:rsidTr="007E7D14">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="475" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="294" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1369" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kcdm}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2507" w:type="dxa"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>${c.kcmc}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="582" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRPr="005255AB" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.xf?if_exists}</w:t>
									<#if c.qtxf?exists>
									<w:t>(${c.qtxf})</w:t>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.zxs?exists>
									<#if c.zxs!=0>
									<w:t>${c.zxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.llxs?exists>
									<#if c.llxs!=0>
									<w:t>${c.llxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="587" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.syxs?exists>
									<#if c.syxs!=0>
									<w:t>${c.syxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="581" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kkxq?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="707" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.sjhj}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1811" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRPr="00985310" wsp:rsidRDefault="007E7D14" wsp:rsidP="00C0661B">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r wsp:rsidRPr="00985310">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.xdsm?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					</#list>
					<w:tr wsp:rsidR="007E7D14" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="475" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="294" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1369" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>专业类创新创业实践</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2507" w:type="dxa"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>与专业背景相关的“创新创业实践</w:t>
								</w:r>
								<w:r wsp:rsidRPr="000B244B">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>（</w:t>
								</w:r>
								<w:r wsp:rsidRPr="000B244B">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>A</w:t>
								</w:r>
								<w:r wsp:rsidRPr="000B244B">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>类）”</w:t>
								</w:r>
								<w:r wsp:rsidRPr="000B244B">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t> </w:t>
								</w:r>
								<w:r wsp:rsidRPr="000B244B">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>学分</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="462" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="701" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="707" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="707" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="707" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="707" w:type="dxa"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1811" w:type="dxa"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>经认定可冲抵本</w:t>
								</w:r>
							</w:p>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>模块最多</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>6</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>学分</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="007E7D14" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="421" w:type="dxa"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="4645" w:type="dxa"/>
								<w:gridSpan w:val="8"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>本模块应修学分小计</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="3681" w:type="dxa"/>
								<w:gridSpan w:val="7"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${xfhj4}</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="007E7D14" wsp:rsidTr="009657E6">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="5066" w:type="dxa"/>
								<w:gridSpan w:val="12"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="009657E6">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>平台应修学分合计</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="3681" w:type="dxa"/>
								<w:gridSpan w:val="4"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="007E7D14" wsp:rsidRDefault="007E7D14" wsp:rsidP="00A353BF">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:kern w:val="0"/>
										<w:sz w:val="20"/>
										<w:sz-cs w:val="20"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:kern w:val="0"/>
										<w:sz w:val="20"/>
										<w:sz-cs w:val="20"/>
									</w:rPr>
									<w:t>${pt3}</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
				</w:tbl>
				<#if kcjz?exists>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="240" w:line-rule="at-least"/>
						<w:rPr>
							<w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="黑体"/>
							<w:kern w:val="0"/>
							<w:sz w:val="32"/>
							<w:sz-cs w:val="32"/>
						</w:rPr>
					</w:pPr>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<w:b/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
						</w:rPr>
						<w:t>十</w:t>
					</w:r>
					<w:r wsp:rsidRPr="003369D0">
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
						</w:rPr>
						<w:t>、课程设置与毕业要求关系矩阵</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00FD5959" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="420" w:line-rule="exact"/>
						<w:ind w:first-line-chars="150"/>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
						</w:rPr>
					</w:pPr>
					<w:r wsp:rsidRPr="00656909">
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>【</w:t>
					</w:r>
					<w:r wsp:rsidRPr="00FD5959">
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>拟参加工程教育认证专业填写</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>此表】</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
					<w:pPr>
						<w:spacing w:line="580" w:line-rule="exact"/>
						<w:ind w:first-line-chars="1150"/>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
						</w:rPr>
					</w:pPr>
					<w:r wsp:rsidRPr="00F95D66">
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>表</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
						</w:rPr>
						<w:t>3</w:t>
					</w:r>
					<w:r wsp:rsidRPr="00F95D66">
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
						</w:rPr>
						<w:t>  </w:t>
					</w:r>
					<w:r wsp:rsidRPr="00F95D66">
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>课程设置与</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>毕业</w:t>
					</w:r>
					<w:r wsp:rsidRPr="00F95D66">
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>要求</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>关系</w:t>
					</w:r>
					<w:r wsp:rsidRPr="00F95D66">
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>矩阵表</w:t>
					</w:r>
				</w:p>
				<w:tbl>
					<w:tblPr>
						<w:tblW w:w="0" w:type="auto"/>
						<w:jc w:val="center"/>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
						</w:tblBorders>
						<w:tblLook w:val="04A0"/>
					</w:tblPr>
					<w:tblGrid>
						<w:gridCol w:w="861"/>
						<w:gridCol w:w="696"/>
						<w:gridCol w:w="696"/>
						<w:gridCol w:w="696"/>
						<w:gridCol w:w="696"/>
						<w:gridCol w:w="696"/>
						<w:gridCol w:w="696"/>
						<w:gridCol w:w="696"/>
						<w:gridCol w:w="696"/>
						<w:gridCol w:w="696"/>
						<w:gridCol w:w="771"/>
						<w:gridCol w:w="771"/>
						<w:gridCol w:w="771"/>
					</w:tblGrid>
					<w:tr wsp:rsidR="009657E6" wsp:rsidRPr="00E32D3B" wsp:rsidTr="00F6236E">
						<w:trPr>
							<w:trHeight w:val="170"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="782" w:type="dxa"/>
								<w:vmerge w:val="restart"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:ind w:first-line-chars="200"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r wsp:rsidRPr="00E32D3B">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>课程名称</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="7740" w:type="dxa"/>
								<w:gridSpan w:val="12"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
								</w:pPr>
								<w:r wsp:rsidRPr="00E32D3B">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>                               </w:t>
								</w:r>
								<w:r wsp:rsidRPr="00E32D3B">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>    </w:t>
								</w:r>
								<w:r wsp:rsidRPr="00E32D3B">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>培养要求</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="009657E6" wsp:rsidRPr="00E32D3B" wsp:rsidTr="00F6236E">
						<w:trPr>
							<w:trHeight w:val="241"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="782" w:type="dxa"/>
								<w:vmerge/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="629" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>1</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="629" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>2</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="629" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>3</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="629" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>4</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="629" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>5</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="629" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>6</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="629" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>7</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="629" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>8</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="629" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>9</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="695" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>10</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="689" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>11</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="695" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="009657E6" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="009657E6" wsp:rsidP="009657E6">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>12</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					<#list courselist as c>
					<w:tr wsp:rsidR="00F6236E" wsp:rsidRPr="00E32D3B" wsp:rsidTr="00F6236E">
						<w:trPr>
							<w:trHeight w:val="349"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="782" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="00F6236E" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="00F6236E" wsp:rsidP="00F6236E">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>${c.kcmc}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="629" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="00F6236E" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="00F6236E" wsp:rsidP="00F6236E">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>${c.k1}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="629" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="00F6236E" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="00F6236E" wsp:rsidP="00F6236E">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>${c.k2}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="629" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="00F6236E" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="00F6236E" wsp:rsidP="00F6236E">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>${c.k3}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="629" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="00F6236E" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="00F6236E" wsp:rsidP="00F6236E">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>${c.k4}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="629" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="00F6236E" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="00F6236E" wsp:rsidP="00F6236E">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>${c.k5}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="629" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="00F6236E" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="00F6236E" wsp:rsidP="00F6236E">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>${c.k6}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="629" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="00F6236E" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="00F6236E" wsp:rsidP="00F6236E">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>${c.k7}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="629" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="00F6236E" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="00F6236E" wsp:rsidP="00F6236E">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>${c.k8}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="629" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="00F6236E" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="00F6236E" wsp:rsidP="00F6236E">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>${c.k9}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="695" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="00F6236E" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="00F6236E" wsp:rsidP="00F6236E">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>${c.k10}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="689" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="00F6236E" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="00F6236E" wsp:rsidP="00F6236E">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>${c.k11}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="695" w:type="dxa"/>
								<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
							</w:tcPr>
							<w:p wsp:rsidR="00F6236E" wsp:rsidRPr="00E32D3B" wsp:rsidRDefault="00F6236E" wsp:rsidP="00F6236E">
								<w:pPr>
									<w:spacing w:line="580" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="15"/>
										<w:sz-cs w:val="15"/>
									</w:rPr>
									<w:t>${c.k12}</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					</#list>
				</w:tbl>
				</#if>
				<w:p wsp:rsidR="009657E6" wsp:rsidRDefault="009657E6"/>
				<w:sectPr wsp:rsidR="009657E6">
					<w:pgSz w:w="11906" w:h="16838"/>
					<w:pgMar w:top="1440" w:right="1800" w:bottom="1440" w:left="1800" w:header="851" w:footer="992" w:gutter="0"/>
					<w:cols w:space="425"/>
					<w:docGrid w:type="lines" w:line-pitch="312"/>
				</w:sectPr>
			</wx:sub-section>
		</wx:sect>
	</w:body>
</w:wordDocument>
