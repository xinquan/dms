<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<w:wordDocument xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:sl="http://schemas.microsoft.com/schemaLibrary/2003/core" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882" w:macrosPresent="no" w:embeddedObjPresent="no" w:ocxPresent="no" xml:space="preserve">
<o:DocumentProperties>
<o:Title>${zy}专业教育阶段培养方案</o:Title>
<o:Author>Win</o:Author>
<o:LastAuthor>Win</o:LastAuthor>
<o:Revision>2</o:Revision>
<o:TotalTime>0</o:TotalTime>
<o:Created>2016-03-25T08:56:00Z</o:Created>
<o:LastSaved>2016-03-25T08:56:00Z</o:LastSaved>
<o:Pages>1</o:Pages>
<o:Words>118</o:Words>
<o:Characters>679</o:Characters>
<o:Company>jarisoft</o:Company>
<o:Lines>5</o:Lines>
<o:Paragraphs>1</o:Paragraphs>
<o:CharactersWithSpaces>796</o:CharactersWithSpaces>
<o:Version>11.5604</o:Version>
</o:DocumentProperties>
<w:fonts>
<w:defaultFonts w:ascii="Times New Roman" w:fareast="宋体" w:h-ansi="Times New Roman" w:cs="Times New Roman"/>
<w:font w:name="宋体">
<w:altName w:val="SimSun"/>
<w:panose-1 w:val="02010600030101010101"/>
<w:charset w:val="86"/>
<w:family w:val="Auto"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
</w:font>
<w:font w:name="方正宋黑简体">
<w:altName w:val="宋体-方正超大字符集"/>
<w:panose-1 w:val="00000000000000000000"/>
<w:charset w:val="86"/>
<w:family w:val="Script"/>
<w:notTrueType/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
</w:font>
<w:font w:name="@宋体">
<w:panose-1 w:val="02010600030101010101"/>
<w:charset w:val="86"/>
<w:family w:val="Auto"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
</w:font>
<w:font w:name="@方正宋黑简体">
<w:panose-1 w:val="00000000000000000000"/>
<w:charset w:val="86"/>
<w:family w:val="Script"/>
<w:notTrueType/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
</w:font>
</w:fonts>
<w:styles>
<w:versionOfBuiltInStylenames w:val="4"/>
<w:latentStyles w:defLockedState="off" w:latentStyleCount="156"/>
<w:style w:type="paragraph" w:default="on" w:styleId="a">
<w:name w:val="Normal"/>
<wx:uiName wx:val="正文"/>
<w:rsid w:val="000F0D4E"/>
<w:pPr>
<w:widowControl w:val="off"/>
<w:jc w:val="both"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="2"/>
<w:sz w:val="21"/>
<w:sz-cs w:val="24"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:default="on" w:styleId="a0">
<w:name w:val="Default Paragraph Font"/>
<wx:uiName wx:val="默认段落字体"/>
<w:semiHidden/>
</w:style>
<w:style w:type="table" w:default="on" w:styleId="a1">
<w:name w:val="Normal Table"/>
<wx:uiName wx:val="普通表格"/>
<w:semiHidden/>
<w:rPr>
<wx:font wx:val="Times New Roman"/>
</w:rPr>
<w:tblPr>
<w:tblInd w:w="0" w:type="dxa"/>
<w:tblCellMar>
<w:top w:w="0" w:type="dxa"/>
<w:left w:w="108" w:type="dxa"/>
<w:bottom w:w="0" w:type="dxa"/>
<w:right w:w="108" w:type="dxa"/>
</w:tblCellMar>
</w:tblPr>
</w:style>
<w:style w:type="list" w:default="on" w:styleId="a2">
<w:name w:val="No List"/>
<wx:uiName wx:val="无列表"/>
<w:semiHidden/>
</w:style>
<w:style w:type="character" w:styleId="CharChar8">
<w:name w:val=" Char Char8"/>
<w:link w:val="a3"/>
<w:locked/>
<w:rsid w:val="000F0D4E"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="宋体" w:h-ansi="Courier New" w:cs="Courier New"/>
<w:kern w:val="2"/>
<w:sz w:val="21"/>
<w:sz-cs w:val="21"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="a3">
<w:name w:val="Plain Text"/>
<wx:uiName wx:val="纯文本"/>
<w:basedOn w:val="a"/>
<w:link w:val="CharChar8"/>
<w:rsid w:val="000F0D4E"/>
<w:pPr>
<w:pStyle w:val="a3"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="Courier New" w:cs="Courier New"/>
<wx:font wx:val="Courier New"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
</w:styles>
<w:docPr>
<w:view w:val="print"/>
<w:zoom w:percent="110"/>
<w:doNotEmbedSystemFonts/>
<w:bordersDontSurroundHeader/>
<w:bordersDontSurroundFooter/>
<w:proofState w:spelling="clean" w:grammar="clean"/>
<w:attachedTemplate w:val=""/>
<w:defaultTabStop w:val="420"/>
<w:drawingGridVerticalSpacing w:val="156"/>
<w:displayHorizontalDrawingGridEvery w:val="0"/>
<w:displayVerticalDrawingGridEvery w:val="2"/>
<w:punctuationKerning/>
<w:characterSpacingControl w:val="CompressPunctuation"/>
<w:optimizeForBrowser/>
<w:validateAgainstSchema/>
<w:saveInvalidXML w:val="off"/>
<w:ignoreMixedContent w:val="off"/>
<w:alwaysShowPlaceholderText w:val="off"/>
<w:compat>
<w:spaceForUL/>
<w:balanceSingleByteDoubleByteWidth/>
<w:doNotLeaveBackslashAlone/>
<w:ulTrailSpace/>
<w:doNotExpandShiftReturn/>
<w:adjustLineHeightInTable/>
<w:breakWrappedTables/>
<w:snapToGridInCell/>
<w:wrapTextWithPunct/>
<w:useAsianBreakRules/>
<w:dontGrowAutofit/>
<w:useFELayout/>
</w:compat>
</w:docPr>
<w:body>
<wx:sect>
<w:p>
<w:pPr>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="32"/>
<w:sz-cs w:val="32"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="32"/>
<w:sz-cs w:val="32"/>
</w:rPr>
<w:t>${zy}</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="32"/>
<w:sz-cs w:val="32"/>
</w:rPr>
<w:t>专业教育阶段培养方案</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体"/>
<wx:font wx:val="方正宋黑简体"/>
<w:u w:val="single"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
<wx:font wx:val="方正宋黑简体"/>
</w:rPr>
<w:t>门类：</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
<wx:font wx:val="方正宋黑简体"/>
<w:u w:val="single"/>
</w:rPr>
<w:t>${ml}</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体"/>
<wx:font wx:val="方正宋黑简体"/>
</w:rPr>
<w:t>  </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
<wx:font wx:val="方正宋黑简体"/>
</w:rPr>
<w:t>专业代码：</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
<wx:font wx:val="方正宋黑简体"/>
<w:u w:val="single"/>
</w:rPr>
<w:t>${zydm}</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体"/>
<wx:font wx:val="方正宋黑简体"/>
</w:rPr>
<w:t>  </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
<wx:font wx:val="方正宋黑简体"/>
</w:rPr>
<w:t>标准学制：</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
<wx:font wx:val="方正宋黑简体"/>
<w:u w:val="single"/>
</w:rPr>
<w:t>${bzxz}</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体"/>
<wx:font wx:val="方正宋黑简体"/>
</w:rPr>
<w:t>  </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
<wx:font wx:val="方正宋黑简体"/>
</w:rPr>
<w:t>授予学位：</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
<wx:font wx:val="方正宋黑简体"/>
<w:u w:val="single"/>
</w:rPr>
<w:t>${syxw}</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
<w:t>一、培养目标</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:ind w:first-line-chars="200"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:spacing w:val="-6"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${pymb}</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
<w:t>二、培养要求</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:ind w:first-line-chars="200"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:spacing w:val="-6"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${jbyq}</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
<w:t>三、主干学科</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:ind w:first-line-chars="200"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:spacing w:val="-6"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${zgxk}</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
<w:t>四、主干课程</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
<w:t>  </w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:ind w:first-line="420"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${zykc}</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
<w:t>五、主要实践环节</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:ind w:first-line="420"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${zysjhj}</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
<w:t>六、相关职业资格证书</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:ind w:first-line="420"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${zdxfyq}</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:b/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
</w:rPr>
<w:t>七、毕业及学位授予</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:ind w:first-line="420"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${ssfa}</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:pStyle w:val="a3"/>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:rPr>
<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
<w:t>八、课程构成及学分分配汇总表</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
</w:pPr>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>表</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>2</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>课程构成及学分分配汇总表</w:t>
</w:r>
</w:p>
<w:tbl>
<w:tblPr>
<w:tblW w:w="4783" w:type="pct"/>
<w:jc w:val="center"/>
<w:tblBorders>
<w:top w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:insideH w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:insideV w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
</w:tblBorders>
<w:tblLayout w:type="Fixed"/>
<w:tblCellMar>
<w:left w:w="0" w:type="dxa"/>
<w:right w:w="0" w:type="dxa"/>
</w:tblCellMar>
</w:tblPr>
<w:tblGrid>
<w:gridCol w:w="1865"/>
<w:gridCol w:w="2937"/>
<w:gridCol w:w="1498"/>
<w:gridCol w:w="1363"/>
<w:gridCol w:w="1537"/>
<w:gridCol w:w="1518"/>
</w:tblGrid>
<w:tr>
<w:trPr>
<w:cantSplit/>
<w:trHeight w:val="459"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="2240" w:type="pct"/>
<w:gridSpan w:val="2"/>
<w:vmerge w:val="restart"/>
<w:tcBorders>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>课</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t> </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>程</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t> </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>类</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t> </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>别</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1335" w:type="pct"/>
<w:gridSpan w:val="2"/>
<w:tcBorders>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="nil"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>学分</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1425" w:type="pct"/>
<w:gridSpan w:val="2"/>
<w:tcBorders>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="nil"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>占总学分比例</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t> %</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:cantSplit/>
<w:trHeight w:val="524"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="2240" w:type="pct"/>
<w:gridSpan w:val="2"/>
<w:vmerge/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="699" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="636" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>其中：实践环节学分</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="717" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>其中：实践环节比例</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>


<#list distributeList as distribute>
<w:tr>
<w:trPr>
<w:cantSplit/>
<w:trHeight w:val="762"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="870" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${distribute.pt}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1370" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${distribute.mk}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="699" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>


<w:t>${distribute.xf}
<#if distribute.qtxf!=0>
(${distribute.qtxf})
</#if>


</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="636" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${distribute.sjxf}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="717" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${distribute.zzxfbl}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${distribute.sjbl}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
</#list>


<w:tr>
<w:trPr>
<w:cantSplit/>
<w:trHeight w:val="447"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="2240" w:type="pct"/>
<w:gridSpan w:val="2"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>合</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>    </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>计</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="699" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${hj}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="636" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${sjhj}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="717" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${blhj}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${sjblhj}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
</w:tbl>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
</w:p>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:br w:type="page"/>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:rPr>
<w:b/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
</w:rPr>
<w:t>九、</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
</w:rPr>
<w:t>专业教育阶段课程指导性修读计划</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>表</w:t>
</w:r>
<w:r>
<w:t>3   </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>×××专业教育阶段课程指导性修读计划</w:t>
</w:r>
</w:p>
<w:tbl>
<w:tblPr>
<w:tblW w:w="8549" w:type="dxa"/>
<w:jc w:val="center"/>
<w:tblBorders>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:tblBorders>
<w:tblLayout w:type="Fixed"/>
<w:tblCellMar>
<w:left w:w="0" w:type="dxa"/>
<w:right w:w="0" w:type="dxa"/>
</w:tblCellMar>
<w:tblLook w:val="00A0"/>
</w:tblPr>
<w:tblGrid>
<w:gridCol w:w="411"/>
<w:gridCol w:w="469"/>
<w:gridCol w:w="342"/>
<w:gridCol w:w="1273"/>
<w:gridCol w:w="2564"/>
<w:gridCol w:w="504"/>
<w:gridCol w:w="615"/>
<w:gridCol w:w="584"/>
<w:gridCol w:w="1787"/>
</w:tblGrid>
<w:tr>
<w:trPr>
<w:trHeight w:val="413"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="880" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="260" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>课程类别</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="220" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>课程性质</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="260" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>课程代码</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="260" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>课程名称</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="260" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>学分</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="260" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>开课</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="260" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>学期</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="200" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:w w:val="80"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:w w:val="80"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>集中性</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="200" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:w w:val="80"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:w w:val="80"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>实践环节</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="260" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>修读说明</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>专业教育平台</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>专业核心课程</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>必修</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:vmerge/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:vmerge/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>毕业实习与设计</w:t>
</w:r>
<w:r>
<w:t>(</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>论文</w:t>
</w:r>
<w:r>
<w:t>)</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:t>12</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:t>8</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>√</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:trHeight w:val="347"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="4179" w:type="dxa"/>
<w:gridSpan w:val="3"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>本模块应修学分小计</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="3490" w:type="dxa"/>
<w:gridSpan w:val="4"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
<w:rPr>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>专业拓展课程</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>选</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>  </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>修</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>建议××专业方向选修</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
<w:rPr>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:vmerge/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:vmerge/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>建议××专业方向选修</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>建议××专业方向选修</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="342" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1273" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>专业</w:t>
</w:r>
<w:proofErr w:type="gramStart"/>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>类创新</w:t>
</w:r>
<w:proofErr w:type="gramEnd"/>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>创业实践</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2564" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>与专业背景相关的“创新创业实践</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>（</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>A</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>类）”</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t> </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>学分</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="504" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>6</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="615" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="584" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1787" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>经认定可冲抵本</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>模块最多</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>6</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>学分</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="411" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="469" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="4179" w:type="dxa"/>
<w:gridSpan w:val="3"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>本模块应修学分小计</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="3490" w:type="dxa"/>
<w:gridSpan w:val="4"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="5059" w:type="dxa"/>
<w:gridSpan w:val="5"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>平台应修学分合计</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="3490" w:type="dxa"/>
<w:gridSpan w:val="4"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
<w:rPr>
<w:kern w:val="0"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
</w:tbl>
<w:p/>
<w:sectPr>
<w:pgSz w:w="11906" w:h="16838"/>
<w:pgMar w:top="1440" w:right="1800" w:bottom="1440" w:left="1800" w:header="851" w:footer="992" w:gutter="0"/>
<w:cols w:space="425"/>
<w:docGrid w:type="lines" w:line-pitch="312"/>
</w:sectPr>
</wx:sect>
</w:body>
</w:wordDocument>
