<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<w:wordDocument xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:sl="http://schemas.microsoft.com/schemaLibrary/2003/core" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882" xmlns:wsp="http://schemas.microsoft.com/office/word/2003/wordml/sp2" w:macrosPresent="no" w:embeddedObjPresent="no" w:ocxPresent="no" xml:space="preserve">
	<w:ignoreElements w:val="http://schemas.microsoft.com/office/word/2003/wordml/sp2"/>
	<o:DocumentProperties>
		<o:Title></o:Title>
		<o:Author>Administrator</o:Author>
		<o:LastAuthor>AutoBVT</o:LastAuthor>
		<o:Revision>2</o:Revision>
		<o:TotalTime>0</o:TotalTime>
		<o:Created>2016-04-22T15:12:00Z</o:Created>
		<o:LastSaved>2016-04-22T15:12:00Z</o:LastSaved>
		<o:Pages>1</o:Pages>
		<o:Words>107</o:Words>
		<o:Characters>613</o:Characters>
		<o:Company>Microsoft</o:Company>
		<o:Lines>5</o:Lines>
		<o:Paragraphs>1</o:Paragraphs>
		<o:CharactersWithSpaces>719</o:CharactersWithSpaces>
		<o:Version>11.0000</o:Version>
	</o:DocumentProperties>
	<w:fonts>
		<w:defaultFonts w:ascii="Times New Roman" w:fareast="宋体" w:h-ansi="Times New Roman" w:cs="Times New Roman"/>
		<w:font w:name="宋体">
			<w:altName w:val="SimSun"/>
			<w:panose-1 w:val="02010600030101010101"/>
			<w:charset w:val="86"/>
			<w:family w:val="Auto"/>
			<w:pitch w:val="variable"/>
			<w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
		</w:font>
		<w:font w:name="@宋体">
			<w:panose-1 w:val="02010600030101010101"/>
			<w:charset w:val="86"/>
			<w:family w:val="Auto"/>
			<w:pitch w:val="variable"/>
			<w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
		</w:font>
	</w:fonts>
	<w:styles>
		<w:versionOfBuiltInStylenames w:val="4"/>
		<w:latentStyles w:defLockedState="off" w:latentStyleCount="156"/>
		<w:style w:type="paragraph" w:default="on" w:styleId="a">
			<w:name w:val="Normal"/>
			<wx:uiName wx:val="正文"/>
			<w:rsid w:val="00685977"/>
			<w:pPr>
				<w:widowControl w:val="off"/>
				<w:jc w:val="both"/>
			</w:pPr>
			<w:rPr>
				<wx:font wx:val="Times New Roman"/>
				<w:kern w:val="2"/>
				<w:sz w:val="21"/>
				<w:sz-cs w:val="24"/>
				<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="1">
			<w:name w:val="heading 1"/>
			<wx:uiName wx:val="标题 1"/>
			<w:basedOn w:val="a"/>
			<w:next w:val="a"/>
			<w:link w:val="CharChar14"/>
			<w:rsid w:val="00685977"/>
			<w:pPr>
				<w:pStyle w:val="1"/>
				<w:keepNext/>
				<w:keepLines/>
				<w:spacing w:before="340" w:after="330" w:line="576" w:line-rule="auto"/>
				<w:outlineLvl w:val="0"/>
			</w:pPr>
			<w:rPr>
				<wx:font wx:val="Times New Roman"/>
				<w:b/>
				<w:b-cs/>
				<w:kern w:val="44"/>
				<w:sz w:val="44"/>
				<w:sz-cs w:val="44"/>
			</w:rPr>
		</w:style>
		<w:style w:type="character" w:default="on" w:styleId="a0">
			<w:name w:val="Default Paragraph Font"/>
			<wx:uiName wx:val="默认段落字体"/>
			<w:semiHidden/>
		</w:style>
		<w:style w:type="table" w:default="on" w:styleId="a1">
			<w:name w:val="Normal Table"/>
			<wx:uiName wx:val="普通表格"/>
			<w:semiHidden/>
			<w:rPr>
				<wx:font wx:val="Times New Roman"/>
			</w:rPr>
			<w:tblPr>
				<w:tblInd w:w="0" w:type="dxa"/>
				<w:tblCellMar>
					<w:top w:w="0" w:type="dxa"/>
					<w:left w:w="108" w:type="dxa"/>
					<w:bottom w:w="0" w:type="dxa"/>
					<w:right w:w="108" w:type="dxa"/>
				</w:tblCellMar>
			</w:tblPr>
		</w:style>
		<w:style w:type="list" w:default="on" w:styleId="a2">
			<w:name w:val="No List"/>
			<wx:uiName wx:val="无列表"/>
			<w:semiHidden/>
		</w:style>
		<w:style w:type="character" w:styleId="CharChar8">
			<w:name w:val=" Char Char8"/>
			<w:link w:val="a3"/>
			<w:locked/>
			<w:rsid w:val="00685977"/>
			<w:rPr>
				<w:rFonts w:ascii="宋体" w:fareast="宋体" w:h-ansi="Courier New" w:cs="Courier New"/>
				<w:kern w:val="2"/>
				<w:sz w:val="21"/>
				<w:sz-cs w:val="21"/>
				<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
			</w:rPr>
		</w:style>
		<w:style w:type="paragraph" w:styleId="a3">
			<w:name w:val="Plain Text"/>
			<wx:uiName wx:val="纯文本"/>
			<w:basedOn w:val="a"/>
			<w:link w:val="CharChar8"/>
			<w:rsid w:val="00685977"/>
			<w:pPr>
				<w:pStyle w:val="a3"/>
			</w:pPr>
			<w:rPr>
				<w:rFonts w:ascii="宋体" w:h-ansi="Courier New" w:cs="Courier New"/>
				<wx:font wx:val="Courier New"/>
				<w:sz-cs w:val="21"/>
			</w:rPr>
		</w:style>
		<w:style w:type="character" w:styleId="CharChar14">
			<w:name w:val=" Char Char14"/>
			<w:link w:val="1"/>
			<w:locked/>
			<w:rsid w:val="00685977"/>
			<w:rPr>
				<w:rFonts w:fareast="宋体"/>
				<w:b/>
				<w:b-cs/>
				<w:kern w:val="44"/>
				<w:sz w:val="44"/>
				<w:sz-cs w:val="44"/>
				<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
			</w:rPr>
		</w:style>
	</w:styles>
	<w:docPr>
		<w:view w:val="print"/>
		<w:zoom w:percent="100"/>
		<w:bordersDontSurroundHeader/>
		<w:bordersDontSurroundFooter/>
		<w:proofState w:spelling="clean" w:grammar="clean"/>
		<w:attachedTemplate w:val=""/>
		<w:defaultTabStop w:val="420"/>
		<w:drawingGridVerticalSpacing w:val="156"/>
		<w:displayHorizontalDrawingGridEvery w:val="0"/>
		<w:displayVerticalDrawingGridEvery w:val="2"/>
		<w:punctuationKerning/>
		<w:characterSpacingControl w:val="CompressPunctuation"/>
		<w:optimizeForBrowser/>
		<w:validateAgainstSchema/>
		<w:saveInvalidXML w:val="off"/>
		<w:ignoreMixedContent w:val="off"/>
		<w:alwaysShowPlaceholderText w:val="off"/>
		<w:compat>
			<w:spaceForUL/>
			<w:balanceSingleByteDoubleByteWidth/>
			<w:doNotLeaveBackslashAlone/>
			<w:ulTrailSpace/>
			<w:doNotExpandShiftReturn/>
			<w:adjustLineHeightInTable/>
			<w:breakWrappedTables/>
			<w:snapToGridInCell/>
			<w:wrapTextWithPunct/>
			<w:useAsianBreakRules/>
			<w:dontGrowAutofit/>
			<w:useFELayout/>
		</w:compat>
		<wsp:rsids>
			<wsp:rsidRoot wsp:val="00685977"/>
			<wsp:rsid wsp:val="00685977"/>
			<wsp:rsid wsp:val="006945CB"/>
			<wsp:rsid wsp:val="008A7D31"/>
			<wsp:rsid wsp:val="00947A9F"/>
			<wsp:rsid wsp:val="00985310"/>
		</wsp:rsids>
	</w:docPr>
	<w:body>
		<wx:sect>
			<wx:sub-section>
				<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
					<w:pPr>
						<w:pStyle w:val="1"/>
						<w:jc w:val="center"/>
						<w:rPr>
							<w:sz w:val="18"/>
							<w:sz-cs w:val="18"/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<w:sz w:val="32"/>
							<w:sz-cs w:val="32"/>
						</w:rPr>
						<w:t>${plan.zy?if_exists}本科专业培养方案</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
					<w:pPr>
						<w:pStyle w:val="a3"/>
						<w:spacing w:line="580" w:line-rule="exact"/>
						<w:ind w:first-line-chars="200"/>
						<w:rPr>
							<w:rFonts w:h-ansi="宋体"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
						<w:t>一、大类培养特色</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="00685977" wsp:rsidRPr="00BC2701" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
					<w:pPr>
						<w:spacing w:line="580" w:line-rule="exact"/>
						<w:ind w:first-line-chars="200"/>
						<w:rPr>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
						</w:rPr>
						<w:t>${plan.pymb?if_exists}</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
					<w:pPr>
						<w:pStyle w:val="a3"/>
						<w:spacing w:line="580" w:line-rule="exact"/>
						<w:ind w:first-line-chars="200"/>
						<w:rPr>
							<w:rFonts w:h-ansi="宋体"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
						<w:t>二、大类培养面向</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
					<w:pPr>
						<w:pStyle w:val="a3"/>
						<w:spacing w:line="580" w:line-rule="exact"/>
						<w:ind w:first-line-chars="200"/>
						<w:rPr>
							<wx:font wx:val="宋体"/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>${plan.zgxk?if_exists}</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
					<w:pPr>
						<w:pStyle w:val="a3"/>
						<w:spacing w:line="240" w:line-rule="at-least"/>
						<w:ind w:first-line-chars="200"/>
						<w:rPr>
							<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
					</w:pPr>
				</w:p>
				<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
					<w:pPr>
						<w:pStyle w:val="a3"/>
						<w:spacing w:line="240" w:line-rule="at-least"/>
						<w:ind w:first-line-chars="200"/>
						<w:rPr>
							<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
						<w:t>三、大类教育阶段课程指导性修读计划</w:t>
					</w:r>
				</w:p>
				<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
					<w:pPr>
						<w:pStyle w:val="a3"/>
						<w:spacing w:line="240" w:line-rule="at-least"/>
						<w:rPr>
							<w:rFonts w:h-ansi="宋体"/>
							<wx:font wx:val="宋体"/>
							<w:b/>
							<w:b-cs/>
						</w:rPr>
					</w:pPr>
				</w:p>
				<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
					<w:pPr>
						<w:spacing w:line="240" w:line-rule="at-least"/>
						<w:jc w:val="center"/>
					</w:pPr>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>表</w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
						</w:rPr>
						<w:t>1</w:t>
					</w:r>
					<w:r>
						<w:t> </w:t>
					</w:r>
					<w:r>
						<w:rPr>
							<w:rFonts w:hint="fareast"/>
							<wx:font wx:val="宋体"/>
						</w:rPr>
						<w:t>大类教育阶段课程指导性修读计划</w:t>
					</w:r>
					<w:r>
						<w:t> </w:t>
					</w:r>
				</w:p>
				<w:tbl>
					<w:tblPr>
						<w:tblW w:w="8748" w:type="dxa"/>
						<w:jc w:val="center"/>
						<w:tblBorders>
							<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
							<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
							<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
							<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
							<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
							<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
						</w:tblBorders>
						<w:tblLayout w:type="Fixed"/>
						<w:tblCellMar>
							<w:left w:w="0" w:type="dxa"/>
							<w:right w:w="0" w:type="dxa"/>
						</w:tblCellMar>
						<w:tblLook w:val="00A0"/>
					</w:tblPr>
					
					<w:tr wsp:rsidR="00685977" wsp:rsidTr="00685977">
						<w:trPr>
							<w:trHeight w:val="841"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="428" w:type="pct"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<aml:annotation aml:id="0" w:type="Word.Bookmark.Start" w:name="OLE_LINK1"/>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="260" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>课程类别</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="173" w:type="pct"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="260" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>课程性质</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="650" w:type="pct"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="260" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>课程代码</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="954" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="260" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>课程名称</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="261" w:type="pct"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="260" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>学分</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="213" w:type="pct"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="260" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>总学时</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="258" w:type="pct"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="260" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>讲课学时</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="10" w:type="pct"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="260" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>实验（实践）学时</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="200" w:type="pct"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="260" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>开课学期</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="348" w:type="pct"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="200" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>集中性实践环节</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1048" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="260" w:line-rule="at-least"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>修读说明</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="00685977" wsp:rsidTr="00685977">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="200" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:left="113" w:right="113"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>通识教育平台</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="228" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:left="113" w:right="113"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>公共基础必修课程</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="173" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:left="113" w:right="113"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>必修</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="641" w:type="pct"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="954" w:type="pct"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="261" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="213" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="258" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="356" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="320" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="348" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1048" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="00C3495C" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:color w:val="FF0000"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
					</w:tr>
					<#list courselist1 as c>
					<w:tr wsp:rsidR="00685977" wsp:rsidTr="00685977">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="200" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="228" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="173" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="641" w:type="pct"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kcdm?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="954" w:type="pct"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>${c.kcmc?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="261" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.xf?if_exists}</w:t>
									<#if c.qtxf?exists>
									<#if c.qtxf!=0>
									<w:t>(${c.qtxf?if_exists})</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="213" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.zxs?exists>
									<#if c.zxs!=0>
									<w:t>${c.zxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="258" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.llxs?exists>
									<#if c.llxs!=0>
									<w:t>${c.llxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="356" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.syxs?exists>
									<#if c.syxs!=0>
									<w:t>${c.syxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="320" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kkxq?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="348" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.sjhj?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1048" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="00985310" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r wsp:rsidRPr="00985310">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.xdsm?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					</#list>
					<w:tr wsp:rsidR="00685977" wsp:rsidTr="00685977">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="200" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="228" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r wsp:rsidRPr="0047499D">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="20"/>
									</w:rPr>
									<w:t>创新创业教育与素质拓展课程</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="173" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>必修</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="641" w:type="pct"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="954" w:type="pct"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="261" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="213" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="258" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="356" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="320" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="348" w:type="pct"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1048" w:type="pct"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
					</w:tr>
					<#list courselist2 as c>
					<w:tr wsp:rsidR="008A7D31" wsp:rsidTr="00685977">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="200" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="008A7D31" wsp:rsidRDefault="008A7D31" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="228" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="008A7D31" wsp:rsidRDefault="008A7D31" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="173" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="008A7D31" wsp:rsidRDefault="008A7D31" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="641" w:type="pct"/>
								<w:gridSpan w:val="2"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="008A7D31" wsp:rsidRDefault="008A7D31" wsp:rsidP="008A7D31">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kcdm?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="954" w:type="pct"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="008A7D31" wsp:rsidRDefault="008A7D31" wsp:rsidP="008A7D31">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>${c.kcmc?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="261" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="008A7D31" wsp:rsidRPr="005255AB" wsp:rsidRDefault="008A7D31" wsp:rsidP="008A7D31">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.xf?if_exists}</w:t>
									<#if c.qtxf?exists>
									<#if c.qtxf!=0>
									<w:t>(${c.qtxf?if_exists})</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="213" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.zxs?exists>
									<#if c.zxs!=0>
									<w:t>${c.zxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="258" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.llxs?exists>
									<#if c.llxs!=0>
									<w:t>${c.llxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="356" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.syxs?exists>
									<#if c.syxs!=0>
									<w:t>${c.syxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="320" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="008A7D31" wsp:rsidRDefault="008A7D31" wsp:rsidP="008A7D31">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kkxq?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="348" w:type="pct"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="008A7D31" wsp:rsidRDefault="008A7D31" wsp:rsidP="008A7D31">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.sjhj?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1048" w:type="pct"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="008A7D31" wsp:rsidRPr="00985310" wsp:rsidRDefault="008A7D31" wsp:rsidP="008A7D31">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r wsp:rsidRPr="00985310">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.xdsm?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					</#list>
					<w:tr wsp:rsidR="00685977" wsp:rsidTr="00685977">
						<w:trPr>
							<w:trHeight w:val="70"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="200" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="228" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="173" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:right="113"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>选修</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="641" w:type="pct"/>
								<w:gridSpan w:val="2"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>公共选修课程</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="954" w:type="pct"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>人文修养教育类</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="261" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>6</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2543" w:type="pct"/>
								<w:gridSpan w:val="7"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="00191FAC" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:rPr>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>  </w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>学生应至少在每类课程中选修</w:t>
								</w:r>
								<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>1</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>门，且此模块须修满</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>6</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>学分。</w:t>
</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="00685977" wsp:rsidTr="00685977">
						<w:trPr>
							<w:trHeight w:val="77"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="200" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="228" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="173" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:left="113" w:right="113"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="641" w:type="pct"/>
								<w:gridSpan w:val="2"/>
								<w:vmerge/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="954" w:type="pct"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>自然科技教育类</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="261" w:type="pct"/>
								<w:vmerge/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2543" w:type="pct"/>
								<w:gridSpan w:val="7"/>
								<w:vmerge/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="00685977" wsp:rsidTr="00685977">
						<w:trPr>
							<w:trHeight w:val="70"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="200" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="228" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="173" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:left="113" w:right="113"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="641" w:type="pct"/>
								<w:gridSpan w:val="2"/>
								<w:vmerge/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="00BB6AAC" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="954" w:type="pct"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>创新创业教育类</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="261" w:type="pct"/>
								<w:vmerge/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2543" w:type="pct"/>
								<w:gridSpan w:val="7"/>
								<w:vmerge/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="00685977" wsp:rsidTr="00685977">
						<w:trPr>
							<w:trHeight w:val="70"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="200" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="228" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="173" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="641" w:type="pct"/>
								<w:gridSpan w:val="2"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="00BB6AAC" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>素质拓展学分</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="954" w:type="pct"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="00BB6AAC" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>创新</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>创业实践（</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>A</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>类</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>）</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="261" w:type="pct"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="00860D50" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>【</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>4</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>】</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2543" w:type="pct"/>
								<w:gridSpan w:val="7"/>
								<w:vmerge w:val="restart"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:first-line-chars="50"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>根据《淮海工学院素质拓展学分认定实施办法》（淮工院发</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>[</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>2016</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>]</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>168</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>号）认定。</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="00685977" wsp:rsidRPr="00B43EFA" wsp:rsidTr="00685977">
						<w:trPr>
							<w:trHeight w:val="238"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="200" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="228" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="173" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="641" w:type="pct"/>
								<w:gridSpan w:val="2"/>
								<w:vmerge/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="000573A7" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:rPr>
										<w:sz w:val="20"/>
									</w:rPr>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="954" w:type="pct"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="00BB6AAC" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>社会实践活动（</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>B</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>类</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>）</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="261" w:type="pct"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="000573A7" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>【</w:t>
								</w:r>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>6</w:t>
								</w:r>
								<w:r wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>】</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2543" w:type="pct"/>
								<w:gridSpan w:val="7"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="000573A7" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="00685977" wsp:rsidTr="00685977">
						<w:trPr>
							<w:trHeight w:val="199"/>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2196" w:type="pct"/>
								<w:gridSpan w:val="6"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>平台应修学分合计</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2804" w:type="pct"/>
								<w:gridSpan w:val="8"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="00860D50" wsp:rsidRDefault="008A7D31" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${xfhj1?if_exists}</w:t>
								</w:r>
								<w:r wsp:rsidR="00685977">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>+</w:t>
								</w:r>
								<w:r wsp:rsidR="00685977" wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>【</w:t>
								</w:r>
								<w:r wsp:rsidR="00685977">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>10</w:t>
								</w:r>
								<w:r wsp:rsidR="00685977" wsp:rsidRPr="00BB6AAC">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz w:val="18"/>
										<w:sz-cs w:val="18"/>
									</w:rPr>
									<w:t>】</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					<w:tr wsp:rsidR="00685977" wsp:rsidTr="00685977">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="200" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:ind w:left="113" w:right="113"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>大类教育平台</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="228" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>大类基础必修课程</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="173" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>必修</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="638" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="957" w:type="pct"/>
								<w:gridSpan w:val="2"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="261" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="213" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="258" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="356" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="320" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="348" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1048" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
					</w:tr>
					<#list courselist3 as c>
					<w:tr wsp:rsidR="00947A9F" wsp:rsidTr="00685977">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="200" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="228" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="173" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="638" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00947A9F">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kcdm?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="957" w:type="pct"/>
								<w:gridSpan w:val="2"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00947A9F">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>${c.kcmc?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="261" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00947A9F" wsp:rsidP="00947A9F">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.xf?if_exists}</w:t>
									<#if c.qtxf?exists>
									<#if c.qtxf!=0>
									<w:t>(${c.qtxf?if_exists})</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="213" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.zxs?exists>
									<#if c.zxs!=0>
									<w:t>${c.zxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="258" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.llxs?exists>
									<#if c.llxs!=0>
									<w:t>${c.llxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="356" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.syxs?exists>
									<#if c.syxs!=0>
									<w:t>${c.syxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="320" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00947A9F">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kkxq?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="348" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00947A9F">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.sjhj?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1048" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRPr="00985310" wsp:rsidRDefault="00947A9F" wsp:rsidP="00947A9F">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r wsp:rsidRPr="00985310">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.xdsm?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					</#list>
					<w:tr wsp:rsidR="00947A9F" wsp:rsidTr="00685977">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="200" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="228" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>学科基础必修课程</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="173" w:type="pct"/>
								<w:vmerge w:val="restart"/>
								<w:noWrap/>
								<w:textFlow w:val="tb-rl-v"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>必修</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="638" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="957" w:type="pct"/>
								<w:gridSpan w:val="2"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00685977">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="261" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="213" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="258" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="356" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="320" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="348" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1048" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
							</w:p>
						</w:tc>
					</w:tr>
					<#list courselist4 as c>
					<w:tr wsp:rsidR="00947A9F" wsp:rsidTr="00685977">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="200" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="228" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="173" w:type="pct"/>
								<w:vmerge/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00685977">
								<w:pPr>
									<w:widowControl/>
									<w:jc w:val="left"/>
								</w:pPr>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="638" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00947A9F">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kcdm?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="957" w:type="pct"/>
								<w:gridSpan w:val="2"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00947A9F">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<w:sz-cs w:val="21"/>
									</w:rPr>
									<w:t>${c.kcmc?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="261" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00947A9F" wsp:rsidP="00947A9F">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.xf?if_exists}</w:t>
									<#if c.qtxf?exists>
									<#if c.qtxf!=0>
									<w:t>(${c.qtxf?if_exists})</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="213" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.zxs?exists>
									<#if c.zxs!=0>
									<w:t>${c.zxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="258" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.llxs?exists>
									<#if c.llxs!=0>
									<w:t>${c.llxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="356" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00685977" wsp:rsidRPr="005255AB" wsp:rsidRDefault="00985310" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<#if c.syxs?exists>
									<#if c.syxs!=0>
									<w:t>${c.syxs?if_exists}</w:t>
									</#if>
									</#if>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="320" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00947A9F">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.kkxq?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="348" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00947A9F">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.sjhj?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="1048" w:type="pct"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRPr="00985310" wsp:rsidRDefault="00947A9F" wsp:rsidP="00947A9F">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r wsp:rsidRPr="00985310">
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${c.xdsm?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					</#list>
					<w:tr wsp:rsidR="00947A9F" wsp:rsidTr="00947A9F">
						<w:trPr>
							<w:jc w:val="center"/>
						</w:trPr>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2196" w:type="pct"/>
								<w:gridSpan w:val="7"/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00685977">
								<w:pPr>
									<w:keepLines/>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
										<wx:font wx:val="宋体"/>
									</w:rPr>
									<w:t>平台应修学分合计</w:t>
								</w:r>
							</w:p>
						</w:tc>
						<w:tc>
							<w:tcPr>
								<w:tcW w:w="2804" w:type="pct"/>
								<w:gridSpan w:val="7"/>
								<w:noWrap/>
								<w:vAlign w:val="center"/>
							</w:tcPr>
							<w:p wsp:rsidR="00947A9F" wsp:rsidRDefault="00947A9F" wsp:rsidP="00685977">
								<w:pPr>
									<w:spacing w:line="280" w:line-rule="exact"/>
									<w:jc w:val="center"/>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
								</w:pPr>
								<w:r>
									<w:rPr>
										<w:rFonts w:hint="fareast"/>
									</w:rPr>
									<w:t>${xfhj2?if_exists}</w:t>
								</w:r>
							</w:p>
						</w:tc>
					</w:tr>
					<aml:annotation aml:id="0" w:type="Word.Bookmark.End"/>
				</w:tbl>
				<w:p wsp:rsidR="00685977" wsp:rsidRDefault="00685977"/>
				<w:sectPr wsp:rsidR="00685977">
					<w:pgSz w:w="11906" w:h="16838"/>
					<w:pgMar w:top="1440" w:right="1800" w:bottom="1440" w:left="1800" w:header="851" w:footer="992" w:gutter="0"/>
					<w:cols w:space="425"/>
					<w:docGrid w:type="lines" w:line-pitch="312"/>
				</w:sectPr>
			</wx:sub-section>
		</wx:sect>
	</w:body>
</w:wordDocument>
