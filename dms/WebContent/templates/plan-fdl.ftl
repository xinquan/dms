<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<?mso-application progid="Word.Document"?>
<w:wordDocument xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:sl="http://schemas.microsoft.com/schemaLibrary/2003/core" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882" w:macrosPresent="no" w:embeddedObjPresent="no" w:ocxPresent="no" xml:space="preserve">
<o:DocumentProperties>
<o:Title> </o:Title>
<o:Author>微软用户</o:Author>
<o:LastAuthor>Win</o:LastAuthor>
<o:Revision>16</o:Revision>
<o:TotalTime>29</o:TotalTime>
<o:LastPrinted>2016-03-23T07:46:00Z</o:LastPrinted>
<o:Created>2016-04-19T00:30:00Z</o:Created>
<o:LastSaved>2016-04-19T06:41:00Z</o:LastSaved>
<o:Pages>1</o:Pages>
<o:Words>232</o:Words>
<o:Characters>1327</o:Characters>
<o:Company>微软中国</o:Company>
<o:Lines>11</o:Lines>
<o:Paragraphs>3</o:Paragraphs>
<o:CharactersWithSpaces>1556</o:CharactersWithSpaces>
<o:Version>11.5604</o:Version>
</o:DocumentProperties>
<w:fonts>
<w:defaultFonts w:ascii="Times New Roman" w:fareast="宋体" w:h-ansi="Times New Roman" w:cs="Times New Roman"/>
<w:font w:name="Wingdings">
<w:panose-1 w:val="05000000000000000000"/>
<w:charset w:val="02"/>
<w:family w:val="Auto"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="00000000" w:usb-1="10000000" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="80000000" w:csb-1="00000000"/>
</w:font>
<w:font w:name="宋体">
<w:altName w:val="SimSun"/>
<w:panose-1 w:val="02010600030101010101"/>
<w:charset w:val="86"/>
<w:family w:val="Auto"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
</w:font>
<w:font w:name="黑体">
<w:altName w:val="SimHei"/>
<w:panose-1 w:val="02010609060101010101"/>
<w:charset w:val="86"/>
<w:family w:val="Modern"/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
</w:font>
<w:font w:name="Arial Unicode MS">
<w:panose-1 w:val="020B0604020202020204"/>
<w:charset w:val="00"/>
<w:family w:val="Roman"/>
<w:notTrueType/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="00000003" w:usb-1="00000000" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="00000001" w:csb-1="00000000"/>
</w:font>
<w:font w:name="方正小标宋简体">
<w:altName w:val="微软雅黑"/>
<w:charset w:val="86"/>
<w:family w:val="Script"/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="00000000" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
</w:font>
<w:font w:name="@宋体">
<w:panose-1 w:val="02010600030101010101"/>
<w:charset w:val="86"/>
<w:family w:val="Auto"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="00000003" w:usb-1="288F0000" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
</w:font>
<w:font w:name="Tahoma">
<w:panose-1 w:val="020B0604030504040204"/>
<w:charset w:val="00"/>
<w:family w:val="Swiss"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="E1003EFF" w:usb-1="C000605B" w:usb-2="00000029" w:usb-3="00000000" w:csb-0="000101FF" w:csb-1="00000000"/>
</w:font>
<w:font w:name="@黑体">
<w:panose-1 w:val="02010609060101010101"/>
<w:charset w:val="86"/>
<w:family w:val="Modern"/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="800002BF" w:usb-1="38CF7CFA" w:usb-2="00000016" w:usb-3="00000000" w:csb-0="00040001" w:csb-1="00000000"/>
</w:font>
<w:font w:name="Cambria">
<w:panose-1 w:val="02040503050406030204"/>
<w:charset w:val="00"/>
<w:family w:val="Roman"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="E00002FF" w:usb-1="400004FF" w:usb-2="00000000" w:usb-3="00000000" w:csb-0="0000019F" w:csb-1="00000000"/>
</w:font>
<w:font w:name="华文中宋">
<w:panose-1 w:val="02010600040101010101"/>
<w:charset w:val="86"/>
<w:family w:val="Auto"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="00000287" w:usb-1="080F0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="0004009F" w:csb-1="00000000"/>
</w:font>
<w:font w:name="@华文中宋">
<w:panose-1 w:val="02010600040101010101"/>
<w:charset w:val="86"/>
<w:family w:val="Auto"/>
<w:pitch w:val="variable"/>
<w:sig w:usb-0="00000287" w:usb-1="080F0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="0004009F" w:csb-1="00000000"/>
</w:font>
<w:font w:name="方正宋黑简体">
<w:altName w:val="SimSun-ExtB"/>
<w:panose-1 w:val="00000000000000000000"/>
<w:charset w:val="86"/>
<w:family w:val="Script"/>
<w:notTrueType/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
</w:font>
<w:font w:name="仿宋_GB2312">
<w:altName w:val="黑体"/>
<w:charset w:val="86"/>
<w:family w:val="Modern"/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
</w:font>
<w:font w:name="HiddenHorzOCl">
<w:altName w:val="Arial Unicode MS"/>
<w:panose-1 w:val="00000000000000000000"/>
<w:charset w:val="86"/>
<w:family w:val="Swiss"/>
<w:notTrueType/>
<w:pitch w:val="default"/>
<w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
</w:font>
<w:font w:name="@方正小标宋简体">
<w:charset w:val="86"/>
<w:family w:val="Script"/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="00000000" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
</w:font>
<w:font w:name="@方正宋黑简体">
<w:panose-1 w:val="00000000000000000000"/>
<w:charset w:val="86"/>
<w:family w:val="Script"/>
<w:notTrueType/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
</w:font>
<w:font w:name="@HiddenHorzOCl">
<w:panose-1 w:val="00000000000000000000"/>
<w:charset w:val="86"/>
<w:family w:val="Swiss"/>
<w:notTrueType/>
<w:pitch w:val="default"/>
<w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
</w:font>
<w:font w:name="@仿宋_GB2312">
<w:charset w:val="86"/>
<w:family w:val="Modern"/>
<w:pitch w:val="fixed"/>
<w:sig w:usb-0="00000001" w:usb-1="080E0000" w:usb-2="00000010" w:usb-3="00000000" w:csb-0="00040000" w:csb-1="00000000"/>
</w:font>
</w:fonts>
<w:lists>
<w:listDef w:listDefId="0">
<w:lsid w:val="03755594"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="AC78F622"/>
<w:lvl w:ilvl="0" w:tplc="49080A76">
<w:start w:val="2"/>
<w:lvlText w:val="%1、"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="360" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:cs="Times New Roman" w:hint="default"/>
<w:color w:val="FF0000"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="840" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="1">
<w:lsid w:val="06661F9B"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="42809ABA"/>
<w:lvl w:ilvl="0" w:tplc="FF528008">
<w:start w:val="1"/>
<w:nfc w:val="27"/>
<w:lvlText w:val="%1"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="360" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="840" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="2">
<w:lsid w:val="0D3C77B3"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="96D85364"/>
<w:lvl w:ilvl="0" w:tplc="F97821F2">
<w:start w:val="2"/>
<w:nfc w:val="27"/>
<w:lvlText w:val="%1"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="360" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="840" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="3">
<w:lsid w:val="0FE910D1"/>
<w:plt w:val="SingleLevel"/>
<w:tmpl w:val="94002D12"/>
<w:lvl w:ilvl="0">
<w:start w:val="1"/>
<w:pStyle w:val="2"/>
<w:lvlText w:val="%1."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="425"/>
</w:tabs>
<w:ind w:left="425" w:hanging="425"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="4">
<w:lsid w:val="13892608"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="0F80EF74"/>
<w:lvl w:ilvl="0" w:tplc="FA4CDA1C">
<w:start w:val="1"/>
<w:nfc w:val="27"/>
<w:lvlText w:val="%1"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="780" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="4200" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="5">
<w:lsid w:val="18D71FF8"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="643CB37C"/>
<w:lvl w:ilvl="0" w:tplc="D0A4A4EC">
<w:start w:val="1"/>
<w:lvlText w:val="%1、"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="780"/>
</w:tabs>
<w:ind w:left="780" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="1260"/>
</w:tabs>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="1680"/>
</w:tabs>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="2100"/>
</w:tabs>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="2520"/>
</w:tabs>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="2940"/>
</w:tabs>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="3360"/>
</w:tabs>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="3780"/>
</w:tabs>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="4200"/>
</w:tabs>
<w:ind w:left="4200" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="6">
<w:lsid w:val="3A534CC0"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="5A8E4C94"/>
<w:lvl w:ilvl="0" w:tplc="9DDEFBBE">
<w:start w:val="1"/>
<w:nfc w:val="11"/>
<w:lvlText w:val="%1、"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="450" w:hanging="450"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="840" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="7">
<w:lsid w:val="3B08507D"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="969EAC30"/>
<w:lvl w:ilvl="0" w:tplc="73A060E4">
<w:start w:val="1"/>
<w:nfc w:val="27"/>
<w:lvlText w:val="%1"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="780" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="4200" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="8">
<w:lsid w:val="4DA0488D"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="BF2C7698"/>
<w:lvl w:ilvl="0" w:tplc="A6DE11C2">
<w:start w:val="1"/>
<w:nfc w:val="27"/>
<w:lvlText w:val="%1"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="780" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="4200" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="9">
<w:lsid w:val="511E2DBE"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="1B362884"/>
<w:lvl w:ilvl="0" w:tplc="8B8C1620">
<w:start w:val="1"/>
<w:nfc w:val="27"/>
<w:lvlText w:val="%1"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="780" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="4200" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="10">
<w:lsid w:val="550E9704"/>
<w:plt w:val="SingleLevel"/>
<w:tmpl w:val="550E9704"/>
<w:lvl w:ilvl="0">
<w:start w:val="1"/>
<w:nfc w:val="37"/>
<w:suff w:val="Nothing"/>
<w:lvlText w:val="（%1）"/>
<w:lvlJc w:val="left"/>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="11">
<w:lsid w:val="550EA039"/>
<w:plt w:val="SingleLevel"/>
<w:tmpl w:val="550EA039"/>
<w:lvl w:ilvl="0">
<w:start w:val="3"/>
<w:nfc w:val="37"/>
<w:suff w:val="Nothing"/>
<w:lvlText w:val="（%1）"/>
<w:lvlJc w:val="left"/>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="12">
<w:lsid w:val="550ED18E"/>
<w:plt w:val="SingleLevel"/>
<w:tmpl w:val="550ED18E"/>
<w:lvl w:ilvl="0">
<w:start w:val="1"/>
<w:suff w:val="Nothing"/>
<w:lvlText w:val="（%1）"/>
<w:lvlJc w:val="left"/>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="13">
<w:lsid w:val="5F3A761C"/>
<w:plt w:val="Multilevel"/>
<w:tmpl w:val="0888A4E6"/>
<w:lvl w:ilvl="0">
<w:start w:val="1"/>
<w:lvlText w:val="%1."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="720"/>
</w:tabs>
<w:ind w:left="720" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%2."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="1440"/>
</w:tabs>
<w:ind w:left="1440" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="2160"/>
</w:tabs>
<w:ind w:left="2160" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="2880"/>
</w:tabs>
<w:ind w:left="2880" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%5."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="3600"/>
</w:tabs>
<w:ind w:left="3600" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="4320"/>
</w:tabs>
<w:ind w:left="4320" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="5040"/>
</w:tabs>
<w:ind w:left="5040" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%8."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="5760"/>
</w:tabs>
<w:ind w:left="5760" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="6480"/>
</w:tabs>
<w:ind w:left="6480" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="14">
<w:lsid w:val="6010007B"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="E4D2F5BC"/>
<w:lvl w:ilvl="0" w:tplc="26EEFD52">
<w:start w:val="1"/>
<w:nfc w:val="11"/>
<w:lvlText w:val="（%1）"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="1590"/>
</w:tabs>
<w:ind w:left="1590" w:hanging="1170"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="1260"/>
</w:tabs>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="1680"/>
</w:tabs>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="2100"/>
</w:tabs>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="2520"/>
</w:tabs>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="2940"/>
</w:tabs>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="3360"/>
</w:tabs>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="3780"/>
</w:tabs>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="4200"/>
</w:tabs>
<w:ind w:left="4200" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="15">
<w:lsid w:val="64CC31A8"/>
<w:plt w:val="Multilevel"/>
<w:tmpl w:val="4CB41698"/>
<w:lvl w:ilvl="0">
<w:start w:val="1"/>
<w:nfc w:val="23"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="720"/>
</w:tabs>
<w:ind w:left="720" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Symbol" w:h-ansi="Symbol" w:hint="default"/>
<w:sz w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1">
<w:start w:val="1"/>
<w:nfc w:val="23"/>
<w:lvlText w:val="o"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="1440"/>
</w:tabs>
<w:ind w:left="1440" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Courier New" w:h-ansi="Courier New" w:hint="default"/>
<w:sz w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="23"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="2160"/>
</w:tabs>
<w:ind w:left="2160" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
<w:sz w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="23"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="2880"/>
</w:tabs>
<w:ind w:left="2880" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
<w:sz w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="23"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="3600"/>
</w:tabs>
<w:ind w:left="3600" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
<w:sz w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="23"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="4320"/>
</w:tabs>
<w:ind w:left="4320" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
<w:sz w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="23"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="5040"/>
</w:tabs>
<w:ind w:left="5040" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
<w:sz w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="23"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="5760"/>
</w:tabs>
<w:ind w:left="5760" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
<w:sz w:val="20"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="23"/>
<w:lvlText w:val=""/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="6480"/>
</w:tabs>
<w:ind w:left="6480" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Wingdings" w:h-ansi="Wingdings" w:hint="default"/>
<w:sz w:val="20"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="16">
<w:lsid w:val="6B2A7994"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="EB84E6AC"/>
<w:lvl w:ilvl="0" w:tplc="BDFCE7D8">
<w:start w:val="2"/>
<w:lvlText w:val="%1、"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="780" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
<w:color w:val="FF0000"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="4200" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="17">
<w:lsid w:val="726E38D2"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="F57C3174"/>
<w:lvl w:ilvl="0" w:tplc="E1CCF266">
<w:start w:val="1"/>
<w:nfc w:val="27"/>
<w:lvlText w:val="%1"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="780" w:hanging="360"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="4200" w:hanging="420"/>
</w:pPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="18">
<w:lsid w:val="770C5A84"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="F19CAA64"/>
<w:lvl w:ilvl="0" w:tplc="1B481584">
<w:start w:val="2"/>
<w:lvlText w:val="%1、"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="872" w:hanging="450"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Times New Roman" w:h-ansi="Times New Roman" w:cs="Times New Roman" w:hint="default"/>
<w:b w:val="off"/>
<w:color w:val="FF0000"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1262" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1682" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2102" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2522" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2942" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3362" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3782" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="4202" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="19">
<w:lsid w:val="79521EBA"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="DE82BD18"/>
<w:lvl w:ilvl="0" w:tplc="DE96C674">
<w:start w:val="1"/>
<w:nfc w:val="11"/>
<w:lvlText w:val="（%1）"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="1590"/>
</w:tabs>
<w:ind w:left="1590" w:hanging="1170"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman" w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="1260"/>
</w:tabs>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="1680"/>
</w:tabs>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="2100"/>
</w:tabs>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="2520"/>
</w:tabs>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="2940"/>
</w:tabs>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="3360"/>
</w:tabs>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="3780"/>
</w:tabs>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:tabs>
<w:tab w:val="list" w:pos="4200"/>
</w:tabs>
<w:ind w:left="4200" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
</w:rPr>
</w:lvl>
</w:listDef>
<w:listDef w:listDefId="20">
<w:lsid w:val="7EB40CE9"/>
<w:plt w:val="HybridMultilevel"/>
<w:tmpl w:val="4830D0B0"/>
<w:lvl w:ilvl="0" w:tplc="986CD70A">
<w:start w:val="1"/>
<w:nfc w:val="11"/>
<w:lvlText w:val="%1、"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="840" w:hanging="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:hint="default"/>
</w:rPr>
</w:lvl>
<w:lvl w:ilvl="1" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%2)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="1260" w:hanging="420"/>
</w:pPr>
</w:lvl>
<w:lvl w:ilvl="2" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%3."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="1680" w:hanging="420"/>
</w:pPr>
</w:lvl>
<w:lvl w:ilvl="3" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%4."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2100" w:hanging="420"/>
</w:pPr>
</w:lvl>
<w:lvl w:ilvl="4" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%5)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="2520" w:hanging="420"/>
</w:pPr>
</w:lvl>
<w:lvl w:ilvl="5" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%6."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="2940" w:hanging="420"/>
</w:pPr>
</w:lvl>
<w:lvl w:ilvl="6" w:tplc="0409000F" w:tentative="on">
<w:start w:val="1"/>
<w:lvlText w:val="%7."/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3360" w:hanging="420"/>
</w:pPr>
</w:lvl>
<w:lvl w:ilvl="7" w:tplc="04090019" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="4"/>
<w:lvlText w:val="%8)"/>
<w:lvlJc w:val="left"/>
<w:pPr>
<w:ind w:left="3780" w:hanging="420"/>
</w:pPr>
</w:lvl>
<w:lvl w:ilvl="8" w:tplc="0409001B" w:tentative="on">
<w:start w:val="1"/>
<w:nfc w:val="2"/>
<w:lvlText w:val="%9."/>
<w:lvlJc w:val="right"/>
<w:pPr>
<w:ind w:left="4200" w:hanging="420"/>
</w:pPr>
</w:lvl>
</w:listDef>
<w:list w:ilfo="1">
<w:ilst w:val="15"/>
</w:list>
<w:list w:ilfo="2">
<w:ilst w:val="3"/>
</w:list>
<w:list w:ilfo="3">
<w:ilst w:val="3"/>
<w:lvlOverride w:ilvl="0">
<w:startOverride w:val="1"/>
</w:lvlOverride>
</w:list>
<w:list w:ilfo="4">
<w:ilst w:val="13"/>
</w:list>
<w:list w:ilfo="5">
<w:ilst w:val="14"/>
</w:list>
<w:list w:ilfo="6">
<w:ilst w:val="19"/>
</w:list>
<w:list w:ilfo="7">
<w:ilst w:val="5"/>
</w:list>
<w:list w:ilfo="8">
<w:ilst w:val="7"/>
</w:list>
<w:list w:ilfo="9">
<w:ilst w:val="2"/>
</w:list>
<w:list w:ilfo="10">
<w:ilst w:val="9"/>
</w:list>
<w:list w:ilfo="11">
<w:ilst w:val="4"/>
</w:list>
<w:list w:ilfo="12">
<w:ilst w:val="8"/>
</w:list>
<w:list w:ilfo="13">
<w:ilst w:val="0"/>
</w:list>
<w:list w:ilfo="14">
<w:ilst w:val="16"/>
</w:list>
<w:list w:ilfo="15">
<w:ilst w:val="18"/>
</w:list>
<w:list w:ilfo="16">
<w:ilst w:val="6"/>
</w:list>
<w:list w:ilfo="17">
<w:ilst w:val="1"/>
</w:list>
<w:list w:ilfo="18">
<w:ilst w:val="17"/>
</w:list>
<w:list w:ilfo="19">
<w:ilst w:val="20"/>
</w:list>
<w:list w:ilfo="20">
<w:ilst w:val="10"/>
</w:list>
<w:list w:ilfo="21">
<w:ilst w:val="11"/>
</w:list>
<w:list w:ilfo="22">
<w:ilst w:val="12"/>
</w:list>
</w:lists>
<w:styles>
<w:versionOfBuiltInStylenames w:val="4"/>
<w:latentStyles w:defLockedState="off" w:latentStyleCount="156"/>
<w:style w:type="paragraph" w:default="on" w:styleId="a">
<w:name w:val="Normal"/>
<wx:uiName wx:val="正文"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:widowControl w:val="off"/>
<w:jc w:val="both"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="2"/>
<w:sz w:val="21"/>
<w:sz-cs w:val="24"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="1">
<w:name w:val="heading 1"/>
<wx:uiName wx:val="标题 1"/>
<w:basedOn w:val="a"/>
<w:next w:val="a"/>
<w:link w:val="CharChar14"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="1"/>
<w:keepNext/>
<w:keepLines/>
<w:spacing w:before="340" w:after="330" w:line="576" w:line-rule="auto"/>
<w:outlineLvl w:val="0"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Times New Roman"/>
<w:b/>
<w:b-cs/>
<w:kern w:val="44"/>
<w:sz w:val="44"/>
<w:sz-cs w:val="44"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="2">
<w:name w:val="heading 2"/>
<wx:uiName wx:val="标题 2"/>
<w:basedOn w:val="a"/>
<w:next w:val="a0"/>
<w:link w:val="CharChar13"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="2"/>
<w:keepNext/>
<w:keepLines/>
<w:listPr>
<w:ilfo w:val="2"/>
</w:listPr>
<w:spacing w:before="120" w:after="120" w:line="360" w:line-rule="auto"/>
<w:outlineLvl w:val="1"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial" w:fareast="黑体" w:h-ansi="Arial"/>
<wx:font wx:val="Arial"/>
<w:b/>
<w:sz w:val="28"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:default="on" w:styleId="a1">
<w:name w:val="Default Paragraph Font"/>
<wx:uiName wx:val="默认段落字体"/>
<w:semiHidden/>
</w:style>
<w:style w:type="table" w:default="on" w:styleId="a2">
<w:name w:val="Normal Table"/>
<wx:uiName wx:val="普通表格"/>
<w:semiHidden/>
<w:rPr>
<wx:font wx:val="Times New Roman"/>
</w:rPr>
<w:tblPr>
<w:tblInd w:w="0" w:type="dxa"/>
<w:tblCellMar>
<w:top w:w="0" w:type="dxa"/>
<w:left w:w="108" w:type="dxa"/>
<w:bottom w:w="0" w:type="dxa"/>
<w:right w:w="108" w:type="dxa"/>
</w:tblCellMar>
</w:tblPr>
</w:style>
<w:style w:type="list" w:default="on" w:styleId="a3">
<w:name w:val="No List"/>
<wx:uiName wx:val="无列表"/>
<w:semiHidden/>
</w:style>
<w:style w:type="paragraph" w:styleId="a4">
<w:name w:val="footer"/>
<wx:uiName wx:val="页脚"/>
<w:basedOn w:val="a"/>
<w:link w:val="CharChar12"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="a4"/>
<w:tabs>
<w:tab w:val="center" w:pos="4153"/>
<w:tab w:val="right" w:pos="8306"/>
</w:tabs>
<w:snapToGrid w:val="off"/>
<w:jc w:val="left"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Times New Roman"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="a5">
<w:name w:val="page number"/>
<wx:uiName wx:val="页码"/>
<w:basedOn w:val="a1"/>
<w:rsid w:val="00201EA8"/>
</w:style>
<w:style w:type="paragraph" w:styleId="a6">
<w:name w:val="header"/>
<wx:uiName wx:val="页眉"/>
<w:basedOn w:val="a"/>
<w:link w:val="CharChar11"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="a6"/>
<w:pBdr>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="1" w:color="auto"/>
</w:pBdr>
<w:tabs>
<w:tab w:val="center" w:pos="4153"/>
<w:tab w:val="right" w:pos="8306"/>
</w:tabs>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Times New Roman"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="a7">
<w:name w:val="Normal (Web)"/>
<wx:uiName wx:val="普通(网站)"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="a7"/>
<w:widowControl/>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="left"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体"/>
<wx:font wx:val="宋体"/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="table" w:styleId="a8">
<w:name w:val="Table Grid"/>
<wx:uiName wx:val="网格型"/>
<w:basedOn w:val="a2"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:widowControl w:val="off"/>
<w:jc w:val="both"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Times New Roman"/>
</w:rPr>
<w:tblPr>
<w:tblInd w:w="0" w:type="dxa"/>
<w:tblBorders>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:tblBorders>
<w:tblCellMar>
<w:top w:w="0" w:type="dxa"/>
<w:left w:w="108" w:type="dxa"/>
<w:bottom w:w="0" w:type="dxa"/>
<w:right w:w="108" w:type="dxa"/>
</w:tblCellMar>
</w:tblPr>
</w:style>
<w:style w:type="character" w:styleId="a9">
<w:name w:val="Hyperlink"/>
<wx:uiName wx:val="超链接"/>
<w:rsid w:val="00201EA8"/>
<w:rPr>
<w:strike w:val="off"/>
<w:dstrike w:val="off"/>
<w:color w:val="0000FF"/>
<w:u w:val="none"/>
<w:effect w:val="none"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="uimr10ctx3">
<w:name w:val="ui_mr10 c_tx3"/>
<w:basedOn w:val="a1"/>
<w:rsid w:val="00201EA8"/>
</w:style>
<w:style w:type="paragraph" w:styleId="fdetailctx3">
<w:name w:val="f_detail c_tx3"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="fdetailctx3"/>
<w:widowControl/>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="left"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体"/>
<wx:font wx:val="宋体"/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="uimr101">
<w:name w:val="ui_mr101"/>
<w:basedOn w:val="a1"/>
<w:rsid w:val="00201EA8"/>
</w:style>
<w:style w:type="character" w:styleId="feedstpoperatev2fdetailop">
<w:name w:val="feeds_tp_operate_v2 f_detail_op"/>
<w:basedOn w:val="a1"/>
<w:rsid w:val="00201EA8"/>
</w:style>
<w:style w:type="character" w:styleId="uimr102">
<w:name w:val="ui_mr102"/>
<w:basedOn w:val="a1"/>
<w:rsid w:val="00201EA8"/>
</w:style>
<w:style w:type="paragraph" w:styleId="HTML">
<w:name w:val="HTML Preformatted"/>
<wx:uiName wx:val="HTML 预设格式"/>
<w:basedOn w:val="a"/>
<w:link w:val="CharChar10"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="HTML"/>
<w:widowControl/>
<w:tabs>
<w:tab w:val="left" w:pos="916"/>
<w:tab w:val="left" w:pos="1832"/>
<w:tab w:val="left" w:pos="2748"/>
<w:tab w:val="left" w:pos="3664"/>
<w:tab w:val="left" w:pos="4580"/>
<w:tab w:val="left" w:pos="5496"/>
<w:tab w:val="left" w:pos="6412"/>
<w:tab w:val="left" w:pos="7328"/>
<w:tab w:val="left" w:pos="8244"/>
<w:tab w:val="left" w:pos="9160"/>
<w:tab w:val="left" w:pos="10076"/>
<w:tab w:val="left" w:pos="10992"/>
<w:tab w:val="left" w:pos="11908"/>
<w:tab w:val="left" w:pos="12824"/>
<w:tab w:val="left" w:pos="13740"/>
<w:tab w:val="left" w:pos="14656"/>
</w:tabs>
<w:spacing w:line="360" w:line-rule="auto"/>
<w:jc w:val="left"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体"/>
<wx:font wx:val="宋体"/>
<w:spacing w:val="15"/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="aa">
<w:name w:val="Date"/>
<wx:uiName wx:val="日期"/>
<w:basedOn w:val="a"/>
<w:next w:val="a"/>
<w:link w:val="CharChar9"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="aa"/>
<w:ind w:left-chars="2500"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar8">
<w:name w:val="Char Char8"/>
<w:link w:val="ab"/>
<w:locked/>
<w:rsid w:val="00201EA8"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="宋体" w:h-ansi="Courier New" w:cs="Courier New"/>
<w:kern w:val="2"/>
<w:sz w:val="21"/>
<w:sz-cs w:val="21"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="ab">
<w:name w:val="Plain Text"/>
<wx:uiName wx:val="纯文本"/>
<w:basedOn w:val="a"/>
<w:link w:val="CharChar8"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="ab"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="Courier New" w:cs="Courier New"/>
<wx:font wx:val="Courier New"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="ac">
<w:name w:val="Body Text Indent"/>
<wx:uiName wx:val="正文文本缩进"/>
<w:basedOn w:val="a"/>
<w:link w:val="CharChar7"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="ac"/>
<w:ind w:left="99" w:first-line="645"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312"/>
<wx:font wx:val="Times New Roman"/>
<w:sz w:val="32"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="p0">
<w:name w:val="p0"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="p0"/>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体"/>
<wx:font wx:val="宋体"/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar14">
<w:name w:val="Char Char14"/>
<w:link w:val="1"/>
<w:locked/>
<w:rsid w:val="00201EA8"/>
<w:rPr>
<w:rFonts w:fareast="宋体"/>
<w:b/>
<w:b-cs/>
<w:kern w:val="44"/>
<w:sz w:val="44"/>
<w:sz-cs w:val="44"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar13">
<w:name w:val="Char Char13"/>
<w:link w:val="2"/>
<w:semiHidden/>
<w:rsid w:val="00201EA8"/>
<w:rPr>
<w:rFonts w:ascii="Arial" w:fareast="黑体" w:h-ansi="Arial"/>
<w:b/>
<w:kern w:val="2"/>
<w:sz w:val="28"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar12">
<w:name w:val="Char Char12"/>
<w:link w:val="a4"/>
<w:rsid w:val="00201EA8"/>
<w:rPr>
<w:rFonts w:fareast="宋体"/>
<w:kern w:val="2"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar11">
<w:name w:val="Char Char11"/>
<w:link w:val="a6"/>
<w:semiHidden/>
<w:rsid w:val="00201EA8"/>
<w:rPr>
<w:rFonts w:fareast="宋体"/>
<w:kern w:val="2"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="ad">
<w:name w:val="FollowedHyperlink"/>
<wx:uiName wx:val="已访问的超链接"/>
<w:rsid w:val="00201EA8"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:color w:val="800080"/>
<w:u w:val="single"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="ae">
<w:name w:val="Emphasis"/>
<wx:uiName wx:val="强调"/>
<w:rsid w:val="00201EA8"/>
<w:rPr>
<w:rFonts w:cs="Times New Roman"/>
<w:color w:val="CC0000"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="a0">
<w:name w:val="Normal Indent"/>
<wx:uiName wx:val="正文缩进"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="a0"/>
<w:ind w:first-line="420"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Times New Roman"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar10">
<w:name w:val="Char Char10"/>
<w:link w:val="HTML"/>
<w:semiHidden/>
<w:rsid w:val="00201EA8"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="宋体" w:h-ansi="宋体" w:cs="宋体"/>
<w:spacing w:val="15"/>
<w:sz w:val="24"/>
<w:sz-cs w:val="24"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar6">
<w:name w:val="Char Char6"/>
<w:link w:val="af"/>
<w:locked/>
<w:rsid w:val="00201EA8"/>
<w:rPr>
<w:rFonts w:ascii="Cambria" w:fareast="宋体" w:h-ansi="Cambria"/>
<w:b/>
<w:b-cs/>
<w:kern w:val="2"/>
<w:sz w:val="32"/>
<w:sz-cs w:val="32"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="af">
<w:name w:val="Title"/>
<wx:uiName wx:val="标题"/>
<w:basedOn w:val="a"/>
<w:next w:val="a"/>
<w:link w:val="CharChar6"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="af"/>
<w:spacing w:before="240" w:after="60"/>
<w:jc w:val="center"/>
<w:outlineLvl w:val="0"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria"/>
<wx:font wx:val="Cambria"/>
<w:b/>
<w:b-cs/>
<w:sz w:val="32"/>
<w:sz-cs w:val="32"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="TitleChar1">
<w:name w:val="Title Char1"/>
<w:rsid w:val="00201EA8"/>
<w:rPr>
<w:rFonts w:ascii="Cambria" w:h-ansi="Cambria" w:cs="Times New Roman"/>
<w:b/>
<w:b-cs/>
<w:sz w:val="32"/>
<w:sz-cs w:val="32"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="af0">
<w:name w:val="Body Text"/>
<wx:uiName wx:val="正文文本"/>
<w:basedOn w:val="a"/>
<w:link w:val="CharChar5"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="af0"/>
<w:spacing w:line="420" w:line-rule="exact"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="24"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar5">
<w:name w:val="Char Char5"/>
<w:link w:val="af0"/>
<w:semiHidden/>
<w:rsid w:val="00201EA8"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="宋体" w:h-ansi="宋体"/>
<w:kern w:val="2"/>
<w:sz w:val="24"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar7">
<w:name w:val="Char Char7"/>
<w:link w:val="ac"/>
<w:semiHidden/>
<w:rsid w:val="00201EA8"/>
<w:rPr>
<w:rFonts w:ascii="仿宋_GB2312" w:fareast="仿宋_GB2312"/>
<w:kern w:val="2"/>
<w:sz w:val="32"/>
<w:sz-cs w:val="24"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar9">
<w:name w:val="Char Char9"/>
<w:link w:val="aa"/>
<w:semiHidden/>
<w:rsid w:val="00201EA8"/>
<w:rPr>
<w:rFonts w:fareast="宋体"/>
<w:kern w:val="2"/>
<w:sz w:val="21"/>
<w:sz-cs w:val="24"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="20">
<w:name w:val="Body Text Indent 2"/>
<wx:uiName wx:val="正文文本缩进 2"/>
<w:basedOn w:val="a"/>
<w:link w:val="CharChar4"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="20"/>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:ind w:first-line="482"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar4">
<w:name w:val="Char Char4"/>
<w:link w:val="20"/>
<w:semiHidden/>
<w:rsid w:val="00201EA8"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="宋体" w:h-ansi="宋体"/>
<w:kern w:val="2"/>
<w:sz w:val="21"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="3">
<w:name w:val="Body Text Indent 3"/>
<wx:uiName wx:val="正文文本缩进 3"/>
<w:basedOn w:val="a"/>
<w:link w:val="CharChar3"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="3"/>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:ind w:first-line="420"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="24"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar3">
<w:name w:val="Char Char3"/>
<w:link w:val="3"/>
<w:semiHidden/>
<w:rsid w:val="00201EA8"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="宋体" w:h-ansi="宋体"/>
<w:kern w:val="2"/>
<w:sz w:val="24"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="af1">
<w:name w:val="Block Text"/>
<wx:uiName wx:val="文本块"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="af1"/>
<w:autoSpaceDE w:val="off"/>
<w:autoSpaceDN w:val="off"/>
<w:adjustRightInd w:val="off"/>
<w:spacing w:line="360" w:line-rule="auto"/>
<w:ind w:left="4" w:right="38"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="Times New Roman"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar1">
<w:name w:val="Char Char1"/>
<w:locked/>
<w:rsid w:val="00201EA8"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:fareast="宋体" w:h-ansi="Courier New"/>
<w:kern w:val="2"/>
<w:sz w:val="21"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="font5">
<w:name w:val="font5"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="font5"/>
<w:widowControl/>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="left"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="Arial Unicode MS"/>
<wx:font wx:val="宋体"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl24">
<w:name w:val="xl24"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl24"/>
<w:widowControl/>
<w:pBdr>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl25">
<w:name w:val="xl25"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl25"/>
<w:widowControl/>
<w:pBdr>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl26">
<w:name w:val="xl26"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl26"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl27">
<w:name w:val="xl27"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl27"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl28">
<w:name w:val="xl28"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl28"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl29">
<w:name w:val="xl29"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl29"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl30">
<w:name w:val="xl30"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl30"/>
<w:widowControl/>
<w:pBdr>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl31">
<w:name w:val="xl31"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl31"/>
<w:widowControl/>
<w:pBdr>
<w:left w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl32">
<w:name w:val="xl32"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl32"/>
<w:widowControl/>
<w:pBdr>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl33">
<w:name w:val="xl33"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl33"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl34">
<w:name w:val="xl34"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl34"/>
<w:widowControl/>
<w:pBdr>
<w:left w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl35">
<w:name w:val="xl35"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl35"/>
<w:widowControl/>
<w:pBdr>
<w:left w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl36">
<w:name w:val="xl36"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl36"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl37">
<w:name w:val="xl37"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl37"/>
<w:widowControl/>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl38">
<w:name w:val="xl38"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl38"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl39">
<w:name w:val="xl39"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl39"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl40">
<w:name w:val="xl40"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl40"/>
<w:widowControl/>
<w:pBdr>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl41">
<w:name w:val="xl41"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl41"/>
<w:widowControl/>
<w:pBdr>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl42">
<w:name w:val="xl42"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl42"/>
<w:widowControl/>
<w:pBdr>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl43">
<w:name w:val="xl43"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl43"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl44">
<w:name w:val="xl44"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl44"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl45">
<w:name w:val="xl45"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl45"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl46">
<w:name w:val="xl46"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl46"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl47">
<w:name w:val="xl47"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl47"/>
<w:widowControl/>
<w:pBdr>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl48">
<w:name w:val="xl48"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl48"/>
<w:widowControl/>
<w:pBdr>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl49">
<w:name w:val="xl49"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl49"/>
<w:widowControl/>
<w:pBdr>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl50">
<w:name w:val="xl50"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl50"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl51">
<w:name w:val="xl51"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl51"/>
<w:widowControl/>
<w:pBdr>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl52">
<w:name w:val="xl52"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl52"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl53">
<w:name w:val="xl53"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl53"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl54">
<w:name w:val="xl54"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl54"/>
<w:widowControl/>
<w:pBdr>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl55">
<w:name w:val="xl55"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl55"/>
<w:widowControl/>
<w:pBdr>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl56">
<w:name w:val="xl56"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl56"/>
<w:widowControl/>
<w:pBdr>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl57">
<w:name w:val="xl57"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl57"/>
<w:widowControl/>
<w:pBdr>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="12"/>
<w:sz-cs w:val="12"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl58">
<w:name w:val="xl58"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl58"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl59">
<w:name w:val="xl59"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl59"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl60">
<w:name w:val="xl60"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl60"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl61">
<w:name w:val="xl61"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl61"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl62">
<w:name w:val="xl62"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl62"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl63">
<w:name w:val="xl63"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl63"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl64">
<w:name w:val="xl64"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl64"/>
<w:widowControl/>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl65">
<w:name w:val="xl65"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl65"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl66">
<w:name w:val="xl66"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl66"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl67">
<w:name w:val="xl67"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl67"/>
<w:widowControl/>
<w:pBdr>
<w:left w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl68">
<w:name w:val="xl68"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl68"/>
<w:widowControl/>
<w:pBdr>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl69">
<w:name w:val="xl69"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl69"/>
<w:widowControl/>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:b/>
<w:b-cs/>
<w:kern w:val="0"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl70">
<w:name w:val="xl70"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl70"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl71">
<w:name w:val="xl71"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl71"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="right"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl72">
<w:name w:val="xl72"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl72"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="right"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl73">
<w:name w:val="xl73"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl73"/>
<w:widowControl/>
<w:pBdr>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl74">
<w:name w:val="xl74"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl74"/>
<w:widowControl/>
<w:pBdr>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="right"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl75">
<w:name w:val="xl75"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl75"/>
<w:widowControl/>
<w:pBdr>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="right"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl76">
<w:name w:val="xl76"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl76"/>
<w:widowControl/>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:color w:val="000000"/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="xl77">
<w:name w:val="xl77"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="xl77"/>
<w:widowControl/>
<w:pBdr>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
</w:pBdr>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="center"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Arial Unicode MS" w:h-ansi="Arial Unicode MS" w:cs="Arial Unicode MS"/>
<wx:font wx:val="Arial Unicode MS"/>
<w:kern w:val="0"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="Default">
<w:name w:val="Default"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="Default"/>
<w:widowControl w:val="off"/>
<w:autoSpaceDE w:val="off"/>
<w:autoSpaceDN w:val="off"/>
<w:adjustRightInd w:val="off"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="HiddenHorzOCl" w:fareast="HiddenHorzOCl" w:cs="HiddenHorzOCl"/>
<wx:font wx:val="Times New Roman"/>
<w:color w:val="000000"/>
<w:sz w:val="24"/>
<w:sz-cs w:val="24"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="CharCharCharChar">
<w:name w:val="Char Char Char Char"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="CharCharCharChar"/>
<w:widowControl/>
<w:spacing w:after="160" w:line="240" w:line-rule="exact"/>
<w:jc w:val="left"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="Char">
<w:name w:val="Char"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="Char"/>
<w:spacing w:line="360" w:line-rule="auto"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Times New Roman"/>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="Char1">
<w:name w:val="Char1"/>
<w:basedOn w:val="af2"/>
<w:autoRedefine/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="Char1"/>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="360" w:line-rule="auto"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="Tahoma" w:h-ansi="Tahoma"/>
<wx:font wx:val="Tahoma"/>
<w:sz w:val="24"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="af2">
<w:name w:val="Document Map"/>
<wx:uiName wx:val="文档结构图"/>
<w:basedOn w:val="a"/>
<w:link w:val="CharChar2"/>
<w:semiHidden/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="af2"/>
<w:shd w:val="clear" w:color="auto" w:fill="000080"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar2">
<w:name w:val="Char Char2"/>
<w:link w:val="af2"/>
<w:semiHidden/>
<w:rsid w:val="00201EA8"/>
<w:rPr>
<w:rFonts w:fareast="宋体"/>
<w:kern w:val="2"/>
<w:sz w:val="21"/>
<w:sz-cs w:val="24"/>
<w:lang w:val="EN-US" w:fareast="ZH-CN" w:bidi="AR-SA"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="af3">
<w:name w:val="列出段落"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="af3"/>
<w:ind w:first-line-chars="200"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Times New Roman"/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="af4">
<w:name w:val="Balloon Text"/>
<wx:uiName wx:val="批注框文本"/>
<w:basedOn w:val="a"/>
<w:link w:val="CharChar"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="af4"/>
</w:pPr>
<w:rPr>
<wx:font wx:val="Times New Roman"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="CharChar">
<w:name w:val="Char Char"/>
<w:link w:val="af4"/>
<w:rsid w:val="00201EA8"/>
<w:rPr>
<w:kern w:val="2"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:style>
<w:style w:type="character" w:styleId="af5">
<w:name w:val="Strong"/>
<wx:uiName wx:val="要点"/>
<w:rsid w:val="00201EA8"/>
<w:rPr>
<w:b/>
<w:b-cs/>
</w:rPr>
</w:style>
<w:style w:type="paragraph" w:styleId="reader-word-layer">
<w:name w:val="reader-word-layer"/>
<w:basedOn w:val="a"/>
<w:rsid w:val="00201EA8"/>
<w:pPr>
<w:pStyle w:val="reader-word-layer"/>
<w:widowControl/>
<w:spacing w:before="100" w:before-autospacing="on" w:after="100" w:after-autospacing="on"/>
<w:jc w:val="left"/>
</w:pPr>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:cs="宋体"/>
<wx:font wx:val="宋体"/>
<w:kern w:val="0"/>
<w:sz w:val="24"/>
</w:rPr>
</w:style>
</w:styles>
<w:divs>
<w:div w:id="342900595">
<w:bodyDiv w:val="on"/>
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="405340494">
<w:bodyDiv w:val="on"/>
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="517892199">
<w:bodyDiv w:val="on"/>
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="1024138735">
<w:bodyDiv w:val="on"/>
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="100"/>
<w:marBottom w:val="100"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="2134787799">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1817837781">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="2044673782">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="194317206">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="150"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1320843353">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1437411333">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1266426768">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1221286563">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1007757767">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1730617631">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1047337355">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="967784704">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1118380489">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="839127029">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="43212384">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1441602198">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="25836500">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1624265694">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
<w:div w:id="1124615143">
<w:bodyDiv w:val="on"/>
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1932081214">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="45"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1723750549">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1235119001">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
<w:div w:id="1376198828">
<w:bodyDiv w:val="on"/>
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="100"/>
<w:marBottom w:val="100"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="437413093">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="2134442328">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1094670142">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="847519598">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="150"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1104615497">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="960770593">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="452754752">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="527067584">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1774322669">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="961961780">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="702749693">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1250230720">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1691418696">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1723019453">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="163278581">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1430469920">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="841313443">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1881744905">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
<w:div w:id="1475099477">
<w:bodyDiv w:val="on"/>
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="1521890178">
<w:bodyDiv w:val="on"/>
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1521581147">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="17853630">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="150"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="131870987">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="162090033">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1317109078">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="107480387">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="217934664">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="219244189">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="303897694">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="535701494">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="586885875">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="634456227">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="711004161">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="728505327">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="768894924">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="827403767">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="834612306">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="1257791318">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="1272397969">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="1290356386">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="1415124011">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="1502740522">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="1551571090">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="1552494278">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="1636179691">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="1733652130">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="1865632172">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="2089570554">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="2090690894">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="312"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
<w:div w:id="2075155989">
<w:bodyDiv w:val="on"/>
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
<w:div w:id="2109886726">
<w:bodyDiv w:val="on"/>
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="100"/>
<w:marBottom w:val="100"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1782216972">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="382411194">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1851409233">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="398749219">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="150"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="422381516">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1932623157">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="512888499">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="599682170">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1264342013">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1368523968">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="252786824">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="467164874">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="725222010">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="169371049">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1862934283">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="1320034946">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="603028190">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
<w:divsChild>
<w:div w:id="358313699">
<w:marLeft w:val="0"/>
<w:marRight w:val="0"/>
<w:marTop w:val="0"/>
<w:marBottom w:val="0"/>
<w:divBdr>
<w:top w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:left w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:bottom w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
<w:right w:val="none" w:sz="0" wx:bdrwidth="0" w:space="0" w:color="auto"/>
</w:divBdr>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divsChild>
</w:div>
</w:divs>
<w:shapeDefaults>
<o:shapedefaults v:ext="edit" spidmax="9218"/>
<o:shapelayout v:ext="edit">
<o:idmap v:ext="edit" data="1"/>
</o:shapelayout>
</w:shapeDefaults>
<w:docPr>
<w:view w:val="print"/>
<w:zoom w:percent="130"/>
<w:doNotEmbedSystemFonts/>
<w:bordersDontSurroundHeader/>
<w:bordersDontSurroundFooter/>
<w:attachedTemplate w:val=""/>
<w:defaultTabStop w:val="420"/>
<w:drawingGridHorizontalSpacing w:val="105"/>
<w:drawingGridVerticalSpacing w:val="156"/>
<w:displayHorizontalDrawingGridEvery w:val="0"/>
<w:displayVerticalDrawingGridEvery w:val="2"/>
<w:punctuationKerning/>
<w:characterSpacingControl w:val="CompressPunctuation"/>
<w:optimizeForBrowser/>
<w:savePreviewPicture/>
<w:validateAgainstSchema/>
<w:saveInvalidXML w:val="off"/>
<w:ignoreMixedContent w:val="off"/>
<w:alwaysShowPlaceholderText w:val="off"/>
<w:hdrShapeDefaults>
<o:shapedefaults v:ext="edit" spidmax="9218"/>
</w:hdrShapeDefaults>
<w:footnotePr>
<w:footnote w:type="separator">
<w:p>
<w:r>
<w:separator/>
</w:r>
</w:p>
</w:footnote>
<w:footnote w:type="continuation-separator">
<w:p>
<w:r>
<w:continuationSeparator/>
</w:r>
</w:p>
</w:footnote>
</w:footnotePr>
<w:endnotePr>
<w:endnote w:type="separator">
<w:p>
<w:r>
<w:separator/>
</w:r>
</w:p>
</w:endnote>
<w:endnote w:type="continuation-separator">
<w:p>
<w:r>
<w:continuationSeparator/>
</w:r>
</w:p>
</w:endnote>
</w:endnotePr>
<w:compat>
<w:spaceForUL/>
<w:balanceSingleByteDoubleByteWidth/>
<w:doNotLeaveBackslashAlone/>
<w:ulTrailSpace/>
<w:doNotExpandShiftReturn/>
<w:adjustLineHeightInTable/>
<w:breakWrappedTables/>
<w:snapToGridInCell/>
<w:wrapTextWithPunct/>
<w:useAsianBreakRules/>
<w:dontGrowAutofit/>
<w:useFELayout/>
</w:compat>
</w:docPr>
<w:body>
<wx:sect>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:ind w:left="1247" w:hanging-chars="345"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:sz w:val="36"/>
<w:sz-cs w:val="36"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:sz w:val="36"/>
<w:sz-cs w:val="36"/>
</w:rPr>
<w:t>2016版本科人才培养方案</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="方正小标宋简体" w:fareast="方正小标宋简体" w:h-ansi="华文中宋"/>
<wx:font wx:val="方正小标宋简体"/>
<w:b/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="方正小标宋简体" w:fareast="方正小标宋简体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="方正小标宋简体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>（</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="方正小标宋简体" w:fareast="方正小标宋简体" w:hint="fareast"/>
<wx:font wx:val="方正小标宋简体"/>
<w:sz w:val="28"/>
<w:sz-cs w:val="28"/>
</w:rPr>
<w:t>非大类培养专业）</w:t>
</w:r>
</w:p>
<wx:sub-section>
<w:p>
<w:pPr>
<w:pStyle w:val="1"/>
<w:jc w:val="center"/>
<w:rPr>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="32"/>
<w:sz-cs w:val="32"/>
</w:rPr>
<w:t>${plan.zy}</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="32"/>
<w:sz-cs w:val="32"/>
</w:rPr>
<w:t>专业本科人才培养方案</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体"/>
<wx:font wx:val="方正宋黑简体"/>
<w:u w:val="single"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
<wx:font wx:val="方正宋黑简体"/>
</w:rPr>
<w:t>门类：${plan.ml}</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体"/>
<wx:font wx:val="方正宋黑简体"/>
</w:rPr>
<w:t>  </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
<wx:font wx:val="方正宋黑简体"/>
</w:rPr>
<w:t>专业代码：${plan.zydm}</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体"/>
<wx:font wx:val="方正宋黑简体"/>
</w:rPr>
<w:t>  </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
<wx:font wx:val="方正宋黑简体"/>
</w:rPr>
<w:t>标准学制：${plan.bzxz}</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体"/>
<wx:font wx:val="方正宋黑简体"/>
</w:rPr>
<w:t>  </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="方正宋黑简体" w:fareast="方正宋黑简体" w:hint="fareast"/>
<wx:font wx:val="方正宋黑简体"/>
</w:rPr>
<w:t>授予学位：${plan.syxw}学士</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
<w:t>一、培养目标</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
<w:t> </w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:ind w:first-line="420"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:b-cs/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b-cs/>
</w:rPr>
<w:t>${plan.pymb}</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
<w:t>二、毕业要求</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:ind w:first-line="420"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:b-cs/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b-cs/>
</w:rPr>
<w:t>${plan.jbyq}</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
<w:t>三、主干学科</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:ind w:first-line="420"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:b-cs/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b-cs/>
</w:rPr>
<w:t>${plan.zgxk}</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
<w:t>四、主干课程</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
<w:t>  </w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:ind w:first-line="420"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:b-cs/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b-cs/>
</w:rPr>
<w:t>${plan.zykc}</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
<w:t>五、主要实践环节</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:ind w:first-line="420"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b-cs/>
</w:rPr>
<w:t>${plan.zysjhj}</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
<w:t>六、相关职业资格证书</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:ind w:first-line="420"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:b/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b-cs/>
</w:rPr>
<w:t>${plan.zdxfyq}</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:pStyle w:val="ab"/>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:jc w:val="left"/>
<w:rPr>
<w:rFonts w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:b/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
</w:rPr>
<w:t>七、毕业及学位授予</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:ind w:first-line="420"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:b-cs/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b-cs/>
</w:rPr>
<w:t>${plan.ssfa}</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:pStyle w:val="ab"/>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:jc w:val="left"/>
<w:rPr>
<w:rFonts w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
</w:rPr>
<w:t>八、</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
<w:b-cs/>
</w:rPr>
<w:t>课程构成及学分分配汇总表</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>表</w:t>
</w:r>
<w:r>
<w:t>1</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>课程构成及学分分配汇总表</w:t>
</w:r>
</w:p>
<w:tbl>
<w:tblPr>
<w:tblW w:w="4783" w:type="pct"/>
<w:jc w:val="center"/>
<w:tblBorders>
<w:top w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:insideH w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
<w:insideV w:val="single" w:sz="8" wx:bdrwidth="20" w:space="0" w:color="auto"/>
</w:tblBorders>
<w:tblLayout w:type="Fixed"/>
<w:tblCellMar>
<w:left w:w="0" w:type="dxa"/>
<w:right w:w="0" w:type="dxa"/>
</w:tblCellMar>
</w:tblPr>
<w:tblGrid>
<w:gridCol w:w="1865"/>
<w:gridCol w:w="2937"/>
<w:gridCol w:w="1498"/>
<w:gridCol w:w="1363"/>
<w:gridCol w:w="1537"/>
<w:gridCol w:w="1518"/>
</w:tblGrid>
<w:tr>
<w:trPr>
<w:cantSplit/>
<w:trHeight w:val="459"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="2240" w:type="pct"/>
<w:gridSpan w:val="2"/>
<w:vmerge w:val="restart"/>
<w:tcBorders>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>课</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t> </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>程</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t> </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>类</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t> </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>别</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1335" w:type="pct"/>
<w:gridSpan w:val="2"/>
<w:tcBorders>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>学分</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1425" w:type="pct"/>
<w:gridSpan w:val="2"/>
<w:tcBorders>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>占总学分比例</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t> %</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:cantSplit/>
<w:trHeight w:val="524"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="2240" w:type="pct"/>
<w:gridSpan w:val="2"/>
<w:vmerge/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="699" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="636" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>其中：实践环节学分</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="717" w:type="pct"/>
<w:tcBorders>
<w:top w:val="nil"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
<w:t>其中：实践环节比例</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:cantSplit/>
<w:trHeight w:val="376"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="870" w:type="pct"/>
<w:vmerge w:val="restart"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>通识教育平台</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1370" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>公共基础必修课程</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="699" w:type="pct"/>
<w:vmerge w:val="restart"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${xfhj1}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="636" w:type="pct"/>
<w:vmerge w:val="restart"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${sjxf1}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="717" w:type="pct"/>
<w:vmerge w:val="restart"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${zzxfbl1}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="pct"/>
<w:vmerge w:val="restart"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${sjbl1}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:cantSplit/>
<w:trHeight w:val="365"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="870" w:type="pct"/>
<w:vmerge/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1370" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
<w:w w:val="90"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:w w:val="90"/>
<w:sz w:val="18"/>
</w:rPr>
<w:t>创新创业教育与素质拓展课程</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="699" w:type="pct"/>
<w:vmerge/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>

</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="636" w:type="pct"/>
<w:vmerge/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>

</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="717" w:type="pct"/>
<w:vmerge/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>

</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="pct"/>
<w:vmerge/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>

</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:cantSplit/>
<w:trHeight w:val="408"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="870" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>大类教育平台</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1370" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>学科基础课程</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="699" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${xfhj2}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="636" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${sjxf2}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="717" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${zzxfbl2}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${sjbl2}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:cantSplit/>
<w:trHeight w:val="335"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="870" w:type="pct"/>
<w:vmerge w:val="restart"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>专业教育平台</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1370" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>专业核心课程</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="699" w:type="pct"/>
<w:vmerge w:val="restart"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${xfhj3hz}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="636" w:type="pct"/>
<w:vmerge w:val="restart"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${sjxf3hz}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="717" w:type="pct"/>
<w:vmerge w:val="restart"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${zzxfbl3hz}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="pct"/>
<w:vmerge w:val="restart"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${sjbl3hz}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:cantSplit/>
<w:trHeight w:val="334"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="870" w:type="pct"/>
<w:vmerge/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1370" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>专业拓展课程</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="699" w:type="pct"/>
<w:vmerge/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${xfhj4}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="636" w:type="pct"/>
<w:vmerge/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${sjxf4}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="717" w:type="pct"/>
<w:vmerge/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${zzxfbl4}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="pct"/>
<w:vmerge/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${sjbl4}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:cantSplit/>
<w:trHeight w:val="447"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="2240" w:type="pct"/>
<w:gridSpan w:val="2"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>合</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>    </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:h-ansi="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>计</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="699" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${zxfhj}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="636" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${zsjxfhj}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="717" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${zzzxfbl}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="708" w:type="pct"/>
<w:tcBorders>
<w:top w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="6" wx:bdrwidth="15" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:tcBorders>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:adjustRightInd w:val="off"/>
<w:snapToGrid w:val="off"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:ascii="宋体"/>
<wx:font wx:val="宋体"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:ascii="宋体" w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>${zsjbl}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
</w:tbl>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:rPr>
<w:b/>
</w:rPr>
</w:pPr>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:rPr>
<w:b/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
</w:rPr>
<w:t>九、课程指导性修读计划</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:rPr>
<w:b/>
</w:rPr>
</w:pPr>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>表</w:t>
</w:r>
<w:r>
<w:t>2</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t> </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>课程指导性修读计划</w:t>
</w:r>
<w:r>
<w:t> </w:t>
</w:r>
</w:p>
<w:tbl>
<w:tblPr>
<w:tblW w:w="8747" w:type="dxa"/>
<w:jc w:val="center"/>
<w:tblBorders>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:tblBorders>
<w:tblLayout w:type="Fixed"/>
<w:tblCellMar>
<w:left w:w="0" w:type="dxa"/>
<w:right w:w="0" w:type="dxa"/>
</w:tblCellMar>
<w:tblLook w:val="00A0"/>
</w:tblPr>
<w:tblGrid>
<w:gridCol w:w="421"/>
<w:gridCol w:w="475"/>
<w:gridCol w:w="294"/>
<w:gridCol w:w="1369"/>
<w:gridCol w:w="18"/>
<w:gridCol w:w="2489"/>
<w:gridCol w:w="462"/>
<w:gridCol w:w="120"/>
<w:gridCol w:w="581"/>
<w:gridCol w:w="707"/>
<w:gridCol w:w="7"/>
<w:gridCol w:w="1804"/>
</w:tblGrid>
<w:tr>
<w:trPr>
<w:trHeight w:val="841"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="896" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="260" w:line-rule="at-least"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>课程类别</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="294" w:type="dxa"/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="260" w:line-rule="at-least"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>课程性质</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1387" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="260" w:line-rule="at-least"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>课程代码</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2489" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="260" w:line-rule="at-least"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>课程名称</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="582" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="260" w:line-rule="at-least"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>学分</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="581" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="260" w:line-rule="at-least"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>开课学期</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="707" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="200" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:w w:val="80"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:w w:val="80"/>
</w:rPr>
<w:t>集中性</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="200" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:w w:val="80"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:w w:val="80"/>
</w:rPr>
<w:t>实践环节</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1811" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="260" w:line-rule="at-least"/>
<w:jc w:val="center"/>
<w:rPr>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>修读说明</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="421" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>通识教育平台</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="475" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>公共基础必修课程</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="294" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>必修</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1387" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2489" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:rPr>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="582" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="581" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="707" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1811" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:color w:val="FF0000"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
</w:tr>



<#list planlist1 as c>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="421" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="475" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="294" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1387" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.kcdm}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2489" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:rPr>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>${c.kcmc}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="582" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>
<#if c.xf!=0>
${c.xf}
</#if>
<#if c.qtxf!=0>
(${c.qtxf})
</#if>
<#if c.zkhxf!=0>
【${c.qtxf}】
</#if>
</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="581" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.kkxq}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="707" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.sjhj}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1811" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:color w:val="FF0000"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:color w:val="FF0000"/>
</w:rPr>
<w:t>${c.xdsm}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
</#list>



<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="421" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="475" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz w:val="20"/>
</w:rPr>
<w:t>创新创业教育与素质拓展课程</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="294" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>必修</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1387" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2489" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:rPr>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="582" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="581" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="714" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1804" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>



<#list planlist2 as c>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="421" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="475" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="294" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1387" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.kcdm}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2489" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:rPr>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>${c.kcmc}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="582" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:jc w:val="center"/>
<w:rPr>
<w:sz w:val="20"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.xf}
<#if c.qtxf!=0>
(${c.qtxf})
</#if>
</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="581" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.kkxq}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="714" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.sjhj}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1804" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:color w:val="FF0000"/>
</w:rPr>
<w:t>${c.xdsm}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
</#list>



<w:tr>
<w:trPr>
<w:trHeight w:val="95"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="421" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="475" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="294" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>选修</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1387" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:rPr>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2489" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:rPr>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="582" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="3099" w:type="dxa"/>
<w:gridSpan w:val="4"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:first-line-chars="150"/>
<w:rPr>
<w:sz w:val="18"/>
<w:sz-cs w:val="18"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
</w:tr>



<#list planlist3 as c>
<w:tr>
<w:trPr>
<w:trHeight w:val="261"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="421" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="475" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="294" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1387" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.kcdm}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2489" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:rPr>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>${c.kcmc}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="582" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.xf}
<#if c.qtxf!=0>
(${c.qtxf})
</#if>
</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="3099" w:type="dxa"/>
<w:gridSpan w:val="4"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.xdsm}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
</#list>



<w:tr>
<w:trPr>
<w:trHeight w:val="333"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="5066" w:type="dxa"/>
<w:gridSpan w:val="6"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>平台应修学分合计</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="3681" w:type="dxa"/>
<w:gridSpan w:val="6"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${xfhj1}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="421" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>大类教育平台</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="475" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>学科基础课程</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="294" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>必修</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1369" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2507" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="462" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="701" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="707" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1811" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>


<#list planlist4 as c>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="421" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="475" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="294" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1369" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.kcdm}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2507" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>${c.kcmc}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="462" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.xf}
<#if c.qtxf!=0>
(${c.qtxf})
</#if>
</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="701" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.kkxq}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="707" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.sjhj}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1811" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:color w:val="FF0000"/>
</w:rPr>
<w:t>${c.xdsm}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
</#list>



<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="421" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="475" w:type="dxa"/>
<w:vmerge/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="294" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>必修</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1369" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2507" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="462" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="701" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="707" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1811" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>



<#list planlist5 as c>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="421" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="475" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="294" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1369" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.kcdm}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2507" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>${c.kcmc}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="462" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.xf}
<#if c.qtxf!=0>
(${c.qtxf})
</#if>
</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="701" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.kkxq}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="707" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.sjhj}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1811" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:color w:val="FF0000"/>
</w:rPr>
<w:t>${c.xdsm}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
</#list>



<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="421" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="4645" w:type="dxa"/>
<w:gridSpan w:val="5"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>平台应修学分合计</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="3681" w:type="dxa"/>
<w:gridSpan w:val="6"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${xfhj2}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="421" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>专业教育平台</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="475" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>专业核心课程</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="294" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>必修</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1369" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2507" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="462" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="701" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="707" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1811" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>



<#list planlist6 as c>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="421" w:type="dxa"/>
<w:vmerge/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="475" w:type="dxa"/>
<w:vmerge/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="294" w:type="dxa"/>
<w:vmerge/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1369" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.kcdm}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2507" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>${c.kcmc}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="462" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.xf}
<#if c.qtxf!=0>
(${c.qtxf})
</#if>
</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="701" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.kkxq}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="707" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.sjhj}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1811" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:color w:val="FF0000"/>
</w:rPr>
<w:t>${c.xdsm}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
</#list>



<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="421" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="475" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="294" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1369" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2507" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>毕业实习与设计</w:t>
</w:r>
<w:r>
<w:t>(</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>论文</w:t>
</w:r>
<w:r>
<w:t>)</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="462" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:t>12</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="701" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:t>8</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="707" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>√</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1811" w:type="dxa"/>
<w:gridSpan w:val="2"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:trHeight w:val="347"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="421" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="4645" w:type="dxa"/>
<w:gridSpan w:val="5"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>本模块应修学分小计</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="3681" w:type="dxa"/>
<w:gridSpan w:val="6"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${xfhj3}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
</w:tbl>
<w:p>
<w:r>
<w:br w:type="page"/>
</w:r>
</w:p>
<w:tbl>
<w:tblPr>
<w:tblW w:w="8747" w:type="dxa"/>
<w:jc w:val="center"/>
<w:tblBorders>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:tblBorders>
<w:tblLayout w:type="Fixed"/>
<w:tblCellMar>
<w:left w:w="0" w:type="dxa"/>
<w:right w:w="0" w:type="dxa"/>
</w:tblCellMar>
<w:tblLook w:val="00A0"/>
</w:tblPr>
<w:tblGrid>
<w:gridCol w:w="421"/>
<w:gridCol w:w="475"/>
<w:gridCol w:w="294"/>
<w:gridCol w:w="1369"/>
<w:gridCol w:w="2507"/>
<w:gridCol w:w="462"/>
<w:gridCol w:w="701"/>
<w:gridCol w:w="707"/>
<w:gridCol w:w="1811"/>
</w:tblGrid>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="421" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="475" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:ind w:left="113" w:right="113"/>
<w:jc w:val="center"/>
<w:rPr>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>专业拓展课程</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="294" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:noWrap/>
<w:textFlow w:val="tb-rl-v"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>选修</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1369" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2507" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="462" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="701" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="707" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1811" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:color w:val="FF0000"/>
</w:rPr>
<w:t>${c.xdsm}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>



<#list planlist7 as c>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="421" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="475" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="294" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1369" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.kcdm}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2507" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>${c.kcmc}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="462" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.xf}
<#if c.qtxf!=0>
(${c.qtxf})
</#if>
</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="701" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.kkxq}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="707" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${c.sjhj}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1811" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
</w:tr>
</#list>



<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="421" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="475" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="294" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1369" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>专业类创新创业实践</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="2507" w:type="dxa"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>与专业背景相关的“创新创业实践</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>（</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>A</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>类）”</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t> </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>学分</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="462" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="701" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="707" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="1811" w:type="dxa"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>经认定可冲抵本</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>模块最多</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>6</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>学分</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="421" w:type="dxa"/>
<w:vmerge/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="4645" w:type="dxa"/>
<w:gridSpan w:val="4"/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>本模块应修学分小计</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="3681" w:type="dxa"/>
<w:gridSpan w:val="4"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>${xfhj4}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="5066" w:type="dxa"/>
<w:gridSpan w:val="5"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:keepLines/>
<w:spacing w:line="280" w:line-rule="exact"/>
<w:jc w:val="center"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>平台应修学分合计</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="3681" w:type="dxa"/>
<w:gridSpan w:val="4"/>
<w:noWrap/>
<w:vAlign w:val="center"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:widowControl/>
<w:jc w:val="left"/>
<w:rPr>
<w:kern w:val="0"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:kern w:val="0"/>
<w:sz w:val="20"/>
<w:sz-cs w:val="20"/>
</w:rPr>
<w:t>${zxfhj}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
</w:tbl>
<w:p>
<w:pPr>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:rPr>
<w:rFonts w:ascii="黑体" w:fareast="黑体" w:h-ansi="宋体"/>
<wx:font wx:val="黑体"/>
<w:kern w:val="0"/>
<w:sz w:val="32"/>
<w:sz-cs w:val="32"/>
</w:rPr>
</w:pPr>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:rPr>
<w:b/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:b/>
</w:rPr>
<w:t>十、课程设置与毕业要求关系矩阵</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="420" w:line-rule="exact"/>
<w:ind w:first-line-chars="150"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>【拟参加工程教育认证专业填写此表】</w:t>
</w:r>
</w:p>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:ind w:first-line-chars="1150"/>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>表</w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
</w:rPr>
<w:t>3  </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
</w:rPr>
<w:t>课程设置与毕业要求关系矩阵表</w:t>
</w:r>
</w:p>
<w:tbl>
<w:tblPr>
<w:tblW w:w="0" w:type="auto"/>
<w:jc w:val="center"/>
<w:tblBorders>
<w:top w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:left w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:bottom w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:right w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:insideH w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
<w:insideV w:val="single" w:sz="4" wx:bdrwidth="10" w:space="0" w:color="auto"/>
</w:tblBorders>
<w:tblLook w:val="04A0"/>
</w:tblPr>
<w:tblGrid>
<w:gridCol w:w="876"/>
<w:gridCol w:w="711"/>
<w:gridCol w:w="711"/>
<w:gridCol w:w="711"/>
<w:gridCol w:w="711"/>
<w:gridCol w:w="711"/>
<w:gridCol w:w="711"/>
<w:gridCol w:w="711"/>
<w:gridCol w:w="711"/>
<w:gridCol w:w="711"/>
<w:gridCol w:w="786"/>
<w:gridCol w:w="786"/>
<w:gridCol w:w="786"/>
</w:tblGrid>
<w:tr>
<w:trPr>
<w:trHeight w:val="170"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="800" w:type="dxa"/>
<w:vmerge w:val="restart"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:ind w:first-line-chars="200"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>课程名称</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="7920" w:type="dxa"/>
<w:gridSpan w:val="12"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz-cs w:val="21"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>                               </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>    </w:t>
</w:r>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<wx:font wx:val="宋体"/>
<w:sz-cs w:val="21"/>
</w:rPr>
<w:t>培养要求</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:trHeight w:val="241"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="800" w:type="dxa"/>
<w:vmerge/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="644" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>1</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="644" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>2</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="644" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>3</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="643" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>4</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="643" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>5</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="643" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>6</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="643" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>7</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="643" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>8</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="643" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>9</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="710" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>10</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="710" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>11</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="710" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>12</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
<w:tr>
<w:trPr>
<w:trHeight w:val="349"/>
<w:jc w:val="center"/>
</w:trPr>
<w:tc>
<w:tcPr>
<w:tcW w:w="800" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>${k.kcmc}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="644" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>${k.k1}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="644" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>${k.k2}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="644" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>${k.k3}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="643" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>${k.k4}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="643" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>${k.k5}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="643" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>${k.k6}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="643" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>${k.k7}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="643" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>${k.k8}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="643" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>${k.k9}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="710" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>${k.k10}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="710" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>${k.k12}</w:t>
</w:r>
</w:p>
</w:tc>
<w:tc>
<w:tcPr>
<w:tcW w:w="710" w:type="dxa"/>
<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
</w:tcPr>
<w:p>
<w:pPr>
<w:spacing w:line="580" w:line-rule="exact"/>
<w:rPr>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rFonts w:hint="fareast"/>
<w:sz w:val="15"/>
<w:sz-cs w:val="15"/>
</w:rPr>
<w:t>${k.k12}</w:t>
</w:r>
</w:p>
</w:tc>
</w:tr>
</w:tbl>
<w:p>
<w:pPr>
<w:spacing w:line="240" w:line-rule="at-least"/>
<w:rPr>
<w:kern w:val="0"/>
</w:rPr>
</w:pPr>
</w:p>
<w:sectPr>
<w:ftr w:type="even">
<wx:pBdrGroup>
<wx:apo>
<wx:jc wx:val="center"/>
</wx:apo>
<w:p>
<w:pPr>
<w:pStyle w:val="a4"/>
<w:framePr w:wrap="around" w:vanchor="text" w:hanchor="margin" w:x-align="center" w:y="1"/>
<w:rPr>
<w:rStyle w:val="a5"/>
</w:rPr>
</w:pPr>
<w:r>
<w:rPr>
<w:rStyle w:val="a5"/>
</w:rPr>
<w:fldChar w:fldCharType="begin"/>
</w:r>
<w:r>
<w:rPr>
<w:rStyle w:val="a5"/>
</w:rPr>
<w:instrText>PAGE  </w:instrText>
</w:r>
<w:r>
<w:rPr>
<w:rStyle w:val="a5"/>
</w:rPr>
<w:fldChar w:fldCharType="end"/>
</w:r>
</w:p>
</wx:pBdrGroup>
<w:p>
<w:pPr>
<w:pStyle w:val="a4"/>
</w:pPr>
</w:p>
</w:ftr>
<w:ftr w:type="odd">
<w:p>
<w:pPr>
<w:pStyle w:val="a4"/>
<w:jc w:val="right"/>
<w:rPr>
<w:sz w:val="24"/>
<w:sz-cs w:val="24"/>
</w:rPr>
</w:pPr>
<w:fldSimple w:instr="PAGE   \* MERGEFORMAT">
<w:r>
<w:rPr>
<w:noProof/>
<w:sz w:val="24"/>
<w:sz-cs w:val="24"/>
<w:lang w:val="ZH-CN"/>
</w:rPr>
<w:t>-</w:t>
</w:r>
<w:r>
<w:rPr>
<w:noProof/>
<w:sz w:val="24"/>
<w:sz-cs w:val="24"/>
</w:rPr>
<w:t> 3 -</w:t>
</w:r>
</w:fldSimple>
</w:p>
<w:p>
<w:pPr>
<w:pStyle w:val="a4"/>
<w:jc w:val="right"/>
</w:pPr>
</w:p>
</w:ftr>
<w:pgSz w:w="11906" w:h="16838"/>
<w:pgMar w:top="1418" w:right="1701" w:bottom="1701" w:left="1701" w:header="851" w:footer="1418" w:gutter="0"/>
<w:pgNumType w:fmt="number-in-dash" w:start="1"/>
<w:cols w:space="720"/>
<w:docGrid w:type="lines" w:line-pitch="312"/>
</w:sectPr>
</wx:sub-section>
</wx:sect>
</w:body>
</w:wordDocument>
