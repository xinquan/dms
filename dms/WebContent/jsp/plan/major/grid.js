var _config
var delIds = []
var department
function fillGrid() {
	_config = {
		planMainGrid : {
			id : 'planMainGrid',
			region : 'center',
			fields : ['version', 'state', 'handledate', 'zy', 'zydm', 'ml', 'bzxz', 'syxw', 'pymb', 'jbyq', 'zgxk', 'zykc', 'zysjhj', 'zdxfyq', 'ssfa', 'username', 'email', 'password',
					'registerDate', 'id'],
			headers : ['版本（年份）', '审批状态', '编制日期', '专业', '专业<br>代码', '门类', '标准<br>学制', '授予<br>学位', '培养<br>目标', '基本<br>要求', '主干<br>学科', '主干<br>课程', '主要<br>实践环节', '职业资格<br>证书要求', '毕业及<br>学位授予'],
			widths : [80, 80, 80, 160, 60, 60, 60, 80, 80, 100, 100, 100, 100, 100, 100],
			renderers : [tip, stater, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, tip, tip, tip, tip, tip, tip, tip, tip, tip, tip, tip],
			aligns : [null, null, 'center', null, 'center', null, null, null, null, null, null, null, null, null, null],
			url : PATH + 'plan/plan!list.do',
			params : {
				'department' : department,
				deptId : _deptid,
				type : '专业'
			},
			autoLoad : false,
			// storeCallback : autoSelFirst,
			hasPage : true,
			tbar : [{
						id : 'addBtn',
						iconCls : 'add',
						text : '制定方案',
						tooltip : '制定方案',
						handler : function() {
							showWindow(PATH + 'plan/plan!editMajor.do?opt=add', "新增培养方案", 800, 600, "yes");
						}
					}, {
						id : 'editBtn',
						iconCls : 'edit',
						text : '维护方案',
						tooltip : '维护方案',
						handler : function() {
							var sel = Ext.getCmp('planMainGrid').getSelectionModel().getSelection()
							if (sel.length == 1) {
								showEditWin(sel, PATH + 'plan/plan!editMajor.do?id=' + sel[0].data.id + '&opt=edit', "维护培养方案");
							} else {
								msgShow("条件缺失", "请选择要维护的方案！");
							}
						}
					}, {
						id : 'importBtn',
						iconCls : 'import',
						text : '修读计划',
						tooltip : '制定/查看修读计划',
						handler : function() {
							var sel = Ext.getCmp('planMainGrid').getSelectionModel().getSelection()
							if (sel.length != 1) {
								Ext.Msg.alert('选择方案', '请在上表中选择培养方案！')
							} else {
								if (sel[0].data.state == 0 || sel[0].data.state == -1) {
									showWindow(PATH + 'plan/plan!importCourse.do?pid=' + sel[0].data.id, "导入修读计划", 1600, 630, "yes");
								} else {
									Ext.Msg.alert('导入修读计划', '方案已提交，无法再修改修读计划！')
								}
							}
						}
					}, {
						id : 'summaryBtn',
						iconCls : 'summary',
						text : '课程汇总',
						tooltip : '查看课程构成及学分分配汇总表',
						handler : function() {
							var records = Ext.getCmp('planMainGrid').getSelectionModel().getSelection();
							if (records.length == 1) {
								showWindow(PATH + 'plan/plan!showDistribution.do?planid=' + records[0].data.id + '&type=major&version=' + records[0].data.version, "新增培养方案", 790, 310, "yes");
							} else {
								Ext.Msg.alert('查看课程构成及学分分配汇总表', '您必须在上面的表格中选择一条培养方案！');
							}
						}
					}, {
						id : 'courseBtn',
						iconCls : 'matrix',
						text : '课程设置与毕业要求关系矩阵',
						tooltip : '课程设置与毕业要求关系矩阵（拟参加工程教育认证专业填写此表）',
						handler : function() {
							var records = Ext.getCmp('planMainGrid').getSelectionModel().getSelection();
							if (records.length == 1) {
								var opt = 'edit'
								if (records[0].data.state != 0 && records[0].data.state != -1) {
									opt = 'show'
								}
								showWindow(PATH + 'plan/plan!showMatrix.do?planid=' + records[0].data.id + '&opt=' + opt, "课程设置与毕业要求关系矩阵", 1000, 600, "yes");
							} else {
								Ext.Msg.alert('查看课程构成及学分分配汇总表', '您必须在上面的表格中选择一条培养方案！');
							}
						}
					}, {
						id : 'delBtn',
						iconCls : 'delete',
						text : '删除',
						tooltip : '删除选中的记录',
						handler : function() {
							var records = Ext.getCmp('planMainGrid').getSelectionModel().getSelection();
							if (records) {
								// 提示是否删除数据
								Ext.Msg.confirm("是否要删除？", "是否要删除这些被选择的数据？", function(btn) {
											if (btn == "yes") {
												delIds = []
												for (var i = 0; i < records.length; i++) {
													if (records[i].data.state == 0) {
														delIds.push(records[i].data.id);
													}
												}
												// 3发出AJAX请求删除相应的数据！
												Ext.Ajax.request({
															url : PATH + 'plan/plan!delMain.do',
															params : {
																'delIds' : delIds
															},
															success : function() {
																Ext.Msg.alert("删除信息成功", "您已经成功删除信息！");
																Ext.getCmp('planMainGrid').getStore().reload();
															},
															failure : function() {
																Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
															}
														});
											}
										});
							} else {
								Ext.Msg.alert('删除操作', '您必须选择一行数据以便删除！');
							}
						}
					}, {
						id : 'submitBtn',
						iconCls : 'submit',
						text : '提交审批',
						tooltip : '提交审批',
						handler : function() {
							var sel = Ext.getCmp('planMainGrid').getSelectionModel().getSelection()
							validThreshold(sel)
						}
					}, {
						id : 'seeBtn',
						iconCls : 'flow',
						text : '查看审批信息',
						tooltip : '查看审批信息',
						handler : function() {
							var sel = Ext.getCmp('planMainGrid').getSelectionModel().getSelection()
							showWorkFlow(sel)
						}
					}, {
						id : 'exportBtn',
						iconCls : 'word',
						text : '导出Word',
						tooltip : '导出Word',
						handler : function() {
							var sel = Ext.getCmp('planMainGrid').getSelectionModel().getSelection()
							if (sel.length == 1) {
								// window.location.href = 'down_plan?currentId='
								// + sel[0].data.id
								if (!Ext.fly('downForm')) {
									var downForm = document.createElement('form');
									downForm.id = 'downForm';
									downForm.name = 'downForm';
									downForm.className = 'x-hidden';
									downForm.action = PATH + 'plan/plan!exportToWord.do';
									downForm.method = 'post';
									// downForm .target = '_blank'; //打开新的下载页面
									var data = document.createElement('input');
									data.type = 'hidden';// 隐藏域
									data.name = 'currentId';// form表单参数
									data.value = sel[0].data.id;// form表单值
									downForm.appendChild(data);
									document.body.appendChild(downForm);
								}
								Ext.fly('downForm').dom.submit();
								if (Ext.fly('downForm')) {
									document.body.removeChild(downForm);
								}
							}
						}
					}]
		}
		// planGrid : {
		// id : 'planGrid',
		// region : 'center',
		// height : '50%',
		// title : '课程指导性修读计划',
		// split : true,
		// fields : ['jd', 'pt', 'mk', 'kcxz', 'kcdm', 'kcmc', 'xf', 'qtxf',
		// 'zkhxf', 'zxs', 'syxs', 'kkxq', 'sjhj', 'xdsm', 'newadd', 'id'],
		// headers : ['阶段', '平台', '模块', '课程性质', '课程代码', '课程名称', '学分', '（学分）',
		// '【学分】', '总学时', '实验（实践）<br>学时', '开课学期', '集中性实践环节', '修读说明'],
		// widths : [90, 100, 150, 60, 100, 140, 60, 60, 60, 80, 100, 80, 100],
		// renderers : [tip, tip, tip, tip, tip, tip, tip, tip, tip, tip, tip,
		// tip, tip, tip],
		// aligns : [null, 'center', null, null, null, null, null, null, null,
		// null, null, null, null, null],
		// url : 'plan/plan!courseList.do',
		// autoLoad : false,
		// hasPage : false,
		// viewConfig : {
		// forceFit : true,
		// getRowClass : function(record, rowIndex, rowParams, store) {
		// var type = record.get('newadd');
		// if ('newadd' == type) {
		// return 'x-grid-row-yellow';
		// }
		// }
		// },
		// tbar : [{
		// id : 'importBtn',
		// iconCls : 'import',
		// text : '修读计划',
		// tooltip : '制定/查看修读计划',
		// handler : function() {
		// var sel =
		// Ext.getCmp('planMainGrid').getSelectionModel().getSelection()
		// if (sel.length != 1) {
		// Ext.Msg.alert('选择方案', '请在上表中选择培养方案！')
		// } else {
		// if (sel[0].data.state == 0 || sel[0].data.state == -1) {
		// showWindow(PATH + 'plan/plan!importCourse.do?pid=' + sel[0].data.id,
		// "导入修读计划", 1600, 630, "yes");
		// } else {
		// Ext.Msg.alert('导入修读计划', '方案已提交，无法再修改修读计划！')
		// }
		// }
		// }
		// }, {
		// id : 'summaryBtn',
		// iconCls : 'summary',
		// text : '课程构成及学分分配汇总表',
		// tooltip : '查看课程构成及学分分配汇总表',
		// handler : function() {
		// var records =
		// Ext.getCmp('planMainGrid').getSelectionModel().getSelection();
		// if (records.length == 1) {
		// showWindow(PATH + 'plan/plan!showDistribution.do?planid=' +
		// records[0].data.id + '&type=major&version=' +
		// records[0].data.version, "新增培养方案", 790, 310, "yes");
		// } else {
		// Ext.Msg.alert('查看课程构成及学分分配汇总表', '您必须在上面的表格中选择一条培养方案！');
		// }
		// }
		// }, {
		// id : 'courseBtn',
		// iconCls : 'matrix',
		// text : '课程设置与毕业要求关系矩阵（拟参加工程教育认证专业填写此表）',
		// tooltip : '课程设置与毕业要求关系矩阵',
		// handler : function() {
		// var records =
		// Ext.getCmp('planMainGrid').getSelectionModel().getSelection();
		// if (records.length == 1) {
		// var opt = 'edit'
		// if (records[0].data.state != 0 && records[0].data.state != -1) {
		// opt = 'show'
		// }
		// showWindow(PATH + 'plan/plan!showMatrix.do?planid=' +
		// records[0].data.id + '&opt=' + opt, "课程设置与毕业要求关系矩阵", 1000, 600,
		// "yes");
		// } else {
		// Ext.Msg.alert('查看课程构成及学分分配汇总表', '您必须在上面的表格中选择一条培养方案！');
		// }
		// }
		// }]
		// }
	}
}
function autoSelFirst() {
	var model = Ext.getCmp('planMainGrid').getSelectionModel()
	model.select(0)
}

function reloadGrid(department) {
	Ext.getCmp('planMainGrid').getStore().load({
				params : {
					'department' : department,
					deptId : _deptid,
					type : '专业'
				}
			})
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('培养方案', 'border', [createGrid(_config.planMainGrid)])

			Ext.Ajax.request({
						url : PATH + 'system/menu!getRights.do',
						params : {
							action : this.location.pathname,
							roleids : _roleids
						},
						success : function(response) {
							perm = Ext.decode(response.responseText)
							if (!perm.edit) {
								Ext.getCmp('addBtn').setVisible(false)
								Ext.getCmp('editBtn').setVisible(false)
								Ext.getCmp('delBtn').setVisible(false)
								Ext.getCmp('submitBtn').setVisible(false)
								Ext.getCmp('seeBtn').setVisible(false)
								Ext.getCmp('importBtn').setVisible(false)
								Ext.getCmp('summaryBtn').setVisible(false)
								Ext.getCmp('courseBtn').setVisible(false)
							}
							if (perm.department) {
								reloadGrid(perm.department)
							} else {
								reloadGrid()
								autoSelFirst()
							}
						},
						failure : function() {
							msgShow('错误', '服务器出现错误请稍后再试！', Ext.Msg.ERROR);
						}
					});
		});