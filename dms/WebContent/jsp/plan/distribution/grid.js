var planid = getUrlParam('planid');
var type = getUrlParam('type');
var version = getUrlParam('version')
var distributeGrid = {
	id : 'distributeGrid',
	title : '课程构成及学分分配汇总表',
	hasCheck : false,
	features : [{
				ftype : 'summary'
			}],
	// selType : 'cellmodel',
	border : false,
	hasSeq : false,
	fields : ['pt', 'mk', 'xf', 'sjxf', 'zzxfbl', 'sjbl', 'qtxf', 'jxxf', 'id', 'sindex'],
	headers : [['平台', '模块'], ['总学分', '其中：实践<br>环节学分'], ['占总学分比例（%）', '其中实践<br>环节比例'], '（学分）'],
	groupHeaders : ['课程类别', '学分', '占总学分比例（%）'],
	widths : [110, 170, 80, 100, 130, 110, 80],
	renderers : [tip, function(value, meta, record) {
				meta.style = autoWarp;
				return value;
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}],
	// editors : [null, null, 'numberfield', 'numberfield', 'numberfield',
	// 'numberfield', 'numberfield'],
	aligns : ['center', 'center', 'center', 'center', 'center', 'center', 'center'],
	summarys : [null, null, 'sum', 'sum', 'sum', 'sum', 'sum'],
	srenderers : [function() {
				return '合'
			}, function() {
				return '计:'
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}],
	url : PATH + 'plan/plan!getDistributionData.do?planid=' + planid + '&type=' + type + '&version=' + version,
	autoLoad : true,
	hasPage : false
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			createApp('课程构成及学分分配汇总表', 'fit', [createGrid(distributeGrid)])
		});