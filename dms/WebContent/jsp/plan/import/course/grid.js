var _config
var kkxqWin
var pid = getUrlParam('pid')
var plantype = getUrlParam('plantype')
var kkxqForm = new Ext.FormPanel({
			border : false,
			waitMsgTarget : true,
			items : [{
						fieldLabel : '开课学期',
						xtype : 'combo',
						padding : '10 5 5 5',
						store : getDictStore(true, 'KKXQ'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : false,
						id : 'kkxq',
						triggerAction : 'all'
					}]
		});
var fillKkxq = function() {
	var courses = Ext.getCmp('courseGrid').getSelectionModel().getSelection();
	if (!kkxqWin) {
		kkxqWin = new Ext.Window({
					width : 300,
					height : 150,
					closable : false,
					autoScroll : true,
					title : '请选择开课学期',
					modal : true,
					border : false,
					layout : 'fit',
					items : kkxqForm,
					buttonAlign : 'center',
					buttons : [{
								text : '保存',
								scale : 'medium',
								disabled : false,
								handler : function() {
									addCourseToPlan()
									kkxqWin.hide();
								}
							}, {
								text : '取消',
								scale : 'medium',
								handler : function() {
									kkxqForm.form.reset();// 清空表单
									kkxqWin.hide();
								}
							}]
				});
	}
	if (courses[0].data.kkxq) {
		Ext.getCmp('kkxq').setValue(courses[0].data.kkxq)
		Ext.getCmp('kkxq').setDisabled(true)
	}
	kkxqWin.show('kkxq');// 显示此窗口
}

function addCourseToPlan() {
	var kkxq = Ext.getCmp('kkxq').getValue();
	var courses = Ext.getCmp('courseGrid').getSelectionModel().getSelection();
	Ext.Ajax.request({
				url : PATH + 'plan/plan!isAdded.do',
				params : {
					'planid' : pid,
					'plantype' : plantype,
					'currentId' : courses[0].data.id
				},
				success : function(response) {
					if (Ext.decode(response.responseText).success) {
						window.opener.addCourseToPlan(courses[0], kkxq)
						Ext.toast.msg('消息', '添加成功!');
					} else {
						Ext.toast.msg('消息', '您已经添加过该课程了!');
					}
				},
				failure : function() {
					msgShow('错误', '服务器出现错误！', Ext.Msg.ERROR);
				}
			});
}
function fillGrid() {
	_config = {
		courseGrid : {
			id : 'courseGrid',
			fields : ['kcdm', 'kcmc', 'department', 'kcssx', 'state', 'xf', 'qtxf', 'zkhxf', 'zxs', 'llxs', 'syxs', 'sjhj', 'major', 'newadd', 'id', 'llxs', 'zkhxf', 'syhjbj', 'kknf', 'kkxq'],
			headers : ['课程代码', '课程名称', '开课部门', '开课系', '状态', '学分', '(学分)', '【学分】', '总学时', '理论学时', '实验（实践）<br>学时', '集中性<br>实践环节', '面向专业'],
			widths : [90, 160, 150, 150, 60, 50, 50, 60, 60, 80, 80, 80, 100, 60],
			renderers : [tip, tip, tip, tip, function(v) {
						if (v == 0) {
							return '起草'
						} else if (v == 1) {
							return '提交'
						} else if (v == 2) {
							return '已入库'
						} else if (v == -1) {
							return '退回'
						} else if (v == 3) {
							return '退回提交'
						} else {
							return '已入库'
						}
					}, parse0, parse0, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								return t.record.data.zxs / 16 + '周'
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								if (t.record.data.syxs == 0) {
									return ''
								} else {
									return t.record.data.syxs / 16 + '周'
								}
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, tip, tip, tip],
			aligns : [null, null, null, null, null, null, null, null, null, null, null, null, null, null],
			filters : [true, true, true, true, false, false, false, false, false, false, false, false, false, false],
			url : PATH + 'plan/plan!courseListAll.do',
			filtersurl : [PATH + 'plan/plan!courseListAll.do', PATH + 'plan/plan!courseListAll.do'],
			features : [{
						ftype : 'filters',
						local : true
					}],
			split : true,
			autoLoad : true,
			hasPage : true,
			viewConfig : {
				forceFit : true,
				getRowClass : function(record, rowIndex, rowParams, store) {
					var type = record.get('newadd');
					if ('newadd' == type) {
						return 'x-grid-row-gray';
					}
				}
			},
			tbar : [{
						id : 'addBtn',
						iconCls : 'add',
						text : '确定添加',
						tooltip : '确定添加',
						handler : function() {
							var records = Ext.getCmp('courseGrid').getSelectionModel().getSelection();
							if (records.length != 1) {
								msgShow('提示', '请逐条添加');
							} else {
								fillKkxq()
							}
						}
					}]
		}
	}
}
function autoSelFirst() {
	var model = Ext.getCmp('courseGrid').getSelectionModel()
	model.select(0)
}
function reloadGrid() {
	Ext.getCmp('courseGrid').getStore().reload()
}
Ext.onReady(function() {
			Ext.BLANK_IMAGE_URL = 'extjs/resources/images/default/s.gif';
			Ext.QuickTips.init();
			fillGrid()
			createApp('课程设置', 'fit', [createGrid(_config.courseGrid)])
		});