var _config
var tabs
var plantype
var treePanel
var newFormWin
var pid = getUrlParam('pid')
function fillGrid() {
	_config = {
		planGrid : {
			id : 'planGrid',
			fields : ['sortfield', 'department', 'kcdm', 'kcmc', 'xf', 'qtxf', 'kkxq', 'zkhxf', 'zxs', 'llxs', 'syxs', 'sjhj', 'xdsm', 'major', 'newadd', 'id', 'planid', 'plantype', 'kcssx', 'kcxz',
					'kskc', 'pid', 'jd', 'pt', 'mk', 'cindex', 'state', 'syhjbj', 'kknf'],
			headers : ['序号', '归属学院', '课程代码', '课程名称', '学分', '(学分)', '开课学期', '【学分】', '总学时', '理论学时', '实验/实<br>践学时', '集中性<br>实践环节', '修读说明', '面向专业'],
			widths : [40, 120, 90, 160, 60, 60, 60, 60, 60, 60, 60, 60, 180, 150],
			renderers : [null, null, null, null, parse0, parse0, parse0, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								return t.record.data.zxs / 16 + '周'
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								if (t.record.data.syxs == 0) {
									return ''
								} else {
									return t.record.data.syxs / 16 + '周'
								}
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, null, tip, tip],
			summarys : ['', '', '', '', 'sum', 'sum', '', 'sum', 'sum', 'sum', 'sum', '', '', ''],
			srenderers : ['', function() {
						return '合'
					}, '', function() {
						return '计:'
					}],
			aligns : [null, null, null, null, null, null, null, null, null, null, null, null, null, null],
			url : PATH + 'plan/plan!queryCourseList.do',
			filters : [false, true, true, true, false, false, false, false, false, false, false, false, false, false],
			filtersurl : [PATH + 'plan/plan!queryCourseList.do', PATH + 'plan/plan!queryCourseList.do'],
			features : [{
						ftype : 'summary'
					}, {
						ftype : 'filters',
						local : true
					}],
			hasSeq : false,
			viewConfig : {
				plugins : {
					ptype : 'gridviewdragdrop',
					dragText : '为课程排序'
				}
			},
			tbar : [{
						iconCls : 'add',
						text : '添加修读课程',
						handler : function() {
							var selects = treePanel.getSelectionModel().getSelection();
							var select
							if (selects.length != 0) {
								select = selects[0]
								if (select.data.leaf) {
									showWindow(PATH + 'plan/plan!setCourses.do?pid=' + pid + '&plantype=' + plantype, "导入修读课程", 800, 630, "yes");
								} else {
									msgShow('提示', '请在左侧树选择课程类别。')
								}
							} else {
								msgShow('提示', '请在左侧树选择课程类别。')
							}
						}
					}, {
						id : 'removeBtn',
						iconCls : 'delete',
						text : '移除修读课程',
						tooltip : '从方案中移除选中的记录',
						handler : function() {
							var items = Ext.getCmp('planGrid').getSelectionModel().getSelection();
							if (items.length != 0) {
								// 提示是否删除数据
								Ext.Msg.confirm("是否要删除？", "是否要删除这些被选择的数据？", function(btn) {
											if (btn == "yes") {
												delIds = []
												for (var i = 0; i < items.length; i++) {
													delIds.push(items[i].data.id);
												}
												Ext.Ajax.request({
															url : PATH + "plan/plan!delCourseUnderPlan.do",
															params : {
																'delIds' : delIds
															},
															success : function() {
																Ext.Msg.alert("删除信息成功", "您已经成功删除信息！");
																Ext.getCmp('planGrid').getStore().reload();
															},
															failure : function() {
																Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
															}
														});
											}
										});
							} else {
								Ext.Msg.alert('删除操作', '您必须选择一行数据以便删除！');
							}
						}
					}, {
						iconCls : 'syschro',
						text : '同步新增课程的信息',
						handler : function() {
							Ext.Ajax.request({
										url : PATH + "plan/plan!syschroNewCourses.do",
										params : {
											'planid' : pid
										},
										success : function() {
											Ext.Msg.alert("同步成功", "您成功同步了该培养方案中新增课程的信息！");
											Ext.getCmp('planGrid').getStore().reload();
										},
										failure : function() {
											Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
										}
									});
						}
					}, {
						xtype : 'displayfield',
						labelWidth : 200,
						value : '双击课程可以进行编辑，合计行在列表的最后一行。'
					}],
			autoLoad : true,
			hasPage : false,
			border : false
		}
	}
}

Ext.define('TreeModel', {
			extend : 'Ext.data.Model',
			fields : [{
						name : "id",
						type : "string"
					}, {
						name : "text",
						type : "string"
					}, {
						name : "iconCls",
						type : "string"
					}, {
						name : "leaf",
						type : "boolean"
					}]
		})
function createTreeStore() {
	return Ext.create('Ext.data.TreeStore', {
				defaultRootId : '-1',
				model : 'TreeModel',
				proxy : {
					type : 'ajax',
					extraParams : {
						planid : pid
					},
					url : PATH + "system/dict!styleList.do",
					reader : {
						type : 'json',
						root : 'res'
					}
				},
				root : {
					expanded : true
				},
				nodeParam : "pid"
			})
}

function buildTree() {
	treePanel = Ext.create('Ext.tree.Panel', {
				title : '修读模块',
				iconCls : 'contract',
				rootVisible : false,
				border : false,
				store : createTreeStore(),
				listeners : {
					'itemclick' : function(view, record, item, index, e) {
						var item = record.raw
						if (item.leaf) {
							plantype = item.id
							Ext.getCmp('planGrid').store.load({
										params : {
											'planid' : pid,
											fname : 'plantype',
											fvalue : plantype
										}
									})
						}
					},
					scope : this
				}
			});
	treePanel.expandAll()
	return treePanel
}
function buildTabs() {
	return Ext.create("Ext.tab.Panel", {
				activeTab : 0,
				region : 'center',
				border : false,
				layout : 'fit',
				// split : true,
				autoScroll : true,
				items : [{
							title : '修读课程',
							layout : 'fit',
							border : false,
							padding : 1,
							items : [createGrid(_config.planGrid)]
						}]
			})
}
var planCourseForm = new Ext.FormPanel({
			border : false,
			waitMsgTarget : true,
			layout : {
				type : 'table',
				columns : 2
			},
			defaults : {
				width : 280,
				labelWidth : 110,
				xtype : 'textfield',
				labelAlign : 'right',
				anchor : "100%"
			},
			items : [{
						fieldLabel : '课程代码',
						name : 'course.kcdm',
						xtype : 'displayfield'
					}, {
						fieldLabel : '课程名称',
						name : 'course.kcmc',
						xtype : 'displayfield'
					}, {
						fieldLabel : '开课学期',
						xtype : 'combo',
						store : getDictStore(true, 'KKXQ'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : false,
						name : 'course.kkxq',
						id : 'kkxq',
						triggerAction : 'all'
					}, {
						fieldLabel : '修读说明',
						name : 'course.xdsm'
					}, {
						fieldLabel : '课程归属学院',
						name : 'course.department',
						xtype : 'displayfield'
					}, {
						fieldLabel : '课程归属系',
						name : 'course.kcssx',
						xtype : 'displayfield'
					}, {
						fieldLabel : '面向专业',
						name : 'course.major'
					}, {
						fieldLabel : '学分',
						name : 'course.xf',
						xtype : 'displayfield'
					}, {
						fieldLabel : '(学分)',
						xtype : 'displayfield',
						name : 'course.qtxf'
					}, {
						fieldLabel : '【学分】',
						xtype : 'displayfield',
						name : 'course.zkhxf'
					}, {
						fieldLabel : '总学时',
						name : 'course.zxs',
						xtype : 'displayfield'
					}, {
						fieldLabel : '理论学时',
						name : 'course.llxs',
						xtype : 'displayfield'
					}, {
						fieldLabel : '集中性实践环节',
						name : 'course.sjhj',
						xtype : 'displayfield'
					}, {
						name : 'course.kknf',
						fieldLabel : '开课年份',
						xtype : 'displayfield'
					}, {
						hidden : true,
						fieldLabel : '状态',
						name : 'course.state'
					}, {
						id : 'id',
						name : 'course.id',
						hidden : true
					}]
		});
function initGrid() {
	Ext.getCmp('planGrid').on('itemdblclick', function(model, record, index) {
				Ext.Ajax.request({
							url : PATH + 'plan/plan!findPlanCourse.do',
							params : {
								currentId : record.data.id
							},
							callback : function(opt, success, response) {
								var result = Ext.decode(response.responseText).result;
								var course = makeNewObjAddPrefix(result, 'course.')
								planCourseForm.form.setValues(course)
							}
						})

				if (!newFormWin) {
					newFormWin = new Ext.Window({
								width : 580,
								height : 300,
								autoScroll : true,
								closable : false,
								title : '修读计划',
								modal : true,
								layout : 'fit',
								border : false,
								buttonAlign : 'center',
								items : planCourseForm,
								buttons : [{
											text : '保存',
											scale : 'medium',
											disabled : false,
											handler : function() {
												planCourseForm.submit({
															url : PATH + 'plan/plan!editPlanCourse.do',
															success : function(f, action) {
																Ext.toast.msg('消息', '修读计划修改成功!');
																planCourseForm.form.reset();
																newFormWin.hide()
																Ext.getCmp('planGrid').store.reload();
															},
															failure : function(f, action) {
																planCourseForm.getForm().reset();
																msgShow('提示信息', '修改失败');
															}
														});
											}
										}, {
											text : '取消',
											scale : 'medium',
											handler : function() {
												planCourseForm.form.reset();// 清空表单
												newFormWin.hide();
											}
										}]
							});
				}
				newFormWin.show('planCourseForm');// 显示此窗口
			})
	Ext.getCmp('planGrid').getView().addListener('beforedrop', function(node, data, model) {
				Ext.getBody().mask('正在排序，请稍后。。。')
			})

	Ext.getCmp('planGrid').getView().addListener('drop', function(node, data, model) {
				var store = Ext.getCmp('planGrid').store;
				var planGridJsonArray = []
				for (var i = 0; i < store.totalCount; i++) {
					planGridJsonArray.push(store.getAt(i).data)
					store.getAt(i).set('sortfield', i + 1);
					store.commitChanges();
				}
				Ext.Ajax.request({
							url : PATH + 'plan/plan!updateSortfield.do',
							params : {
								'gridJson' : JSON.stringify(planGridJsonArray)
							},
							success : function(response) {
								Ext.getBody().unmask()
							},
							failure : function() {
								msgShow('错误', '服务器出现错误！', Ext.Msg.ERROR);
							}
						});
			})
}

function addCourseToPlan(course, kkxq) {
	Ext.Ajax.request({
				url : PATH + 'plan/plan!addCoursesToPlan.do',
				params : {
					'planid' : pid,
					'plantype' : plantype,
					'kkxq' : kkxq,
					'currentId' : course.data.id
				},
				success : function() {
					Ext.getCmp('planGrid').store.reload();
				},
				failure : function() {
					msgShow('错误', '服务器出现错误！', Ext.Msg.ERROR);
				}
			});
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('修读计划', 'border', [{
								region : 'west',
								width : 220,
								items : buildTree()
							}, buildTabs()])
			initGrid()

			Ext.getCmp('planGrid').getStore().on("beforeload", function() {
						Ext.apply(Ext.getCmp('planGrid').getStore().proxy.extraParams, {
									planid : pid
								});
					});
		});