var _config
var tabs
var plantype
var treePanel
var newFormWin

function fillGrid() {
	_config = {
		xq1Grid : {
			id : 'xq1Grid',
			title : '第一学期',
			fields : ['kcdm', 'kcmc', 'xf', 'qtxf', 'zkhxf', 'zxs', 'llxs', 'syxs', 'plantype', 'id', 'sjhj'],
			headers : ['课程代码', '课程名称', '学分', '(学分)', '【学分】', '总学时', '理论学时', '实验/实践学时', '类型'],
			widths : [100, 220, 60, 60, 60, 60, 60, 100, 200],
			renderers : [null, null, parse0, parse0, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								return t.record.data.zxs / 16 + '周'
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								if (t.record.data.syxs == 0) {
									return ''
								} else {
									return t.record.data.syxs / 16 + '周'
								}
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, plantype],
			summarys : ['', '', 'sum', 'sum', 'sum', 'sum', 'sum', 'sum', ''],
			srenderers : [function() {
						return '合'
					}, function() {
						return '计:'
					}],
			url : PATH + 'plan/plan!getSummaryCourses.do?kkxq=1',
			features : [{
						ftype : 'summary'
					}],
			height : GRID_HEIGHT,
			hasSeq : true,
			autoLoad : true,
			hasPage : false,
			border : false
		},
		xq2Grid : {
			id : 'xq2Grid',
			title : '第二学期',
			fields : ['kcdm', 'kcmc', 'xf', 'qtxf', 'zkhxf', 'zxs', 'llxs', 'syxs', 'plantype', 'id', 'sjhj'],
			headers : ['课程代码', '课程名称', '学分', '(学分)', '【学分】', '总学时', '理论学时', '实验/实践学时', '类型'],
			widths : [100, 220, 60, 60, 60, 60, 60, 100, 200],
			renderers : [null, null, parse0, parse0, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								return t.record.data.zxs / 16 + '周'
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								if (t.record.data.syxs == 0) {
									return ''
								} else {
									return t.record.data.syxs / 16 + '周'
								}
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, plantype],
			summarys : ['', '', 'sum', 'sum', 'sum', 'sum', 'sum', 'sum'],
			srenderers : [function() {
						return '合'
					}, function() {
						return '计:'
					}],
			url : PATH + 'plan/plan!getSummaryCourses.do?kkxq=2',
			features : [{
						ftype : 'summary'
					}],
			height : GRID_HEIGHT,
			hasSeq : true,
			autoLoad : true,
			hasPage : false,
			border : false
		},
		xq3Grid : {
			id : 'xq3Grid',
			title : '第三学期',
			fields : ['kcdm', 'kcmc', 'xf', 'qtxf', 'zkhxf', 'zxs', 'llxs', 'syxs', 'plantype', 'id', 'sjhj'],
			headers : ['课程代码', '课程名称', '学分', '(学分)', '【学分】', '总学时', '理论学时', '实验/实践学时', '类型'],
			widths : [100, 220, 60, 60, 60, 60, 60, 100, 200],
			renderers : [null, null, parse0, parse0, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								return t.record.data.zxs / 16 + '周'
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								if (t.record.data.syxs == 0) {
									return ''
								} else {
									return t.record.data.syxs / 16 + '周'
								}
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, plantype],
			summarys : ['', '', 'sum', 'sum', 'sum', 'sum', 'sum', 'sum'],
			srenderers : [function() {
						return '合'
					}, function() {
						return '计:'
					}],
			url : PATH + 'plan/plan!getSummaryCourses.do?kkxq=3',
			features : [{
						ftype : 'summary'
					}],
			height : GRID_HEIGHT,
			hasSeq : true,
			autoLoad : true,
			hasPage : false,
			border : false
		},
		xq4Grid : {
			id : 'xq4Grid',
			title : '第四学期',
			fields : ['kcdm', 'kcmc', 'xf', 'qtxf', 'zkhxf', 'zxs', 'llxs', 'syxs', 'plantype', 'id', 'sjhj'],
			headers : ['课程代码', '课程名称', '学分', '(学分)', '【学分】', '总学时', '理论学时', '实验/实践学时', '类型'],
			widths : [100, 220, 60, 60, 60, 60, 60, 100, 200],
			renderers : [null, null, parse0, parse0, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								return t.record.data.zxs / 16 + '周'
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								if (t.record.data.syxs == 0) {
									return ''
								} else {
									return t.record.data.syxs / 16 + '周'
								}
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, plantype],
			summarys : ['', '', 'sum', 'sum', 'sum', 'sum', 'sum', 'sum'],
			srenderers : [function() {
						return '合'
					}, function() {
						return '计:'
					}],
			url : PATH + 'plan/plan!getSummaryCourses.do?kkxq=4',
			features : [{
						ftype : 'summary'
					}],
			height : GRID_HEIGHT,
			hasSeq : true,
			autoLoad : true,
			hasPage : false,
			border : false
		},
		xq5Grid : {
			id : 'xq5Grid',
			title : '第五学期',
			fields : ['kcdm', 'kcmc', 'xf', 'qtxf', 'zkhxf', 'zxs', 'llxs', 'syxs', 'plantype', 'id', 'sjhj'],
			headers : ['课程代码', '课程名称', '学分', '(学分)', '【学分】', '总学时', '理论学时', '实验/实践学时', '类型'],
			widths : [100, 220, 60, 60, 60, 60, 60, 100, 200],
			renderers : [null, null, parse0, parse0, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								return t.record.data.zxs / 16 + '周'
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								if (t.record.data.syxs == 0) {
									return ''
								} else {
									return t.record.data.syxs / 16 + '周'
								}
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, plantype],
			summarys : ['', '', 'sum', 'sum', 'sum', 'sum', 'sum', 'sum'],
			srenderers : [function() {
						return '合'
					}, function() {
						return '计:'
					}],
			url : PATH + 'plan/plan!getSummaryCourses.do?kkxq=5',
			features : [{
						ftype : 'summary'
					}],
			height : GRID_HEIGHT,
			hasSeq : true,
			autoLoad : true,
			hasPage : false,
			border : false
		},
		xq6Grid : {
			id : 'xq6Grid',
			title : '第六学期',
			fields : ['kcdm', 'kcmc', 'xf', 'qtxf', 'zkhxf', 'zxs', 'llxs', 'syxs', 'plantype', 'id', 'sjhj'],
			headers : ['课程代码', '课程名称', '学分', '(学分)', '【学分】', '总学时', '理论学时', '实验/实践学时', '类型'],
			widths : [100, 220, 60, 60, 60, 60, 60, 100, 200],
			renderers : [null, null, parse0, parse0, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								return t.record.data.zxs / 16 + '周'
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								if (t.record.data.syxs == 0) {
									return ''
								} else {
									return t.record.data.syxs / 16 + '周'
								}
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, plantype],
			summarys : ['', '', 'sum', 'sum', 'sum', 'sum', 'sum', 'sum'],
			srenderers : [function() {
						return '合'
					}, function() {
						return '计:'
					}],
			url : PATH + 'plan/plan!getSummaryCourses.do?kkxq=6',
			features : [{
						ftype : 'summary'
					}],
			height : GRID_HEIGHT,
			hasSeq : true,
			autoLoad : true,
			hasPage : false,
			border : false
		},
		xq7Grid : {
			id : 'xq7Grid',
			title : '第七学期',
			fields : ['kcdm', 'kcmc', 'xf', 'qtxf', 'zkhxf', 'zxs', 'llxs', 'syxs', 'plantype', 'id', 'sjhj'],
			headers : ['课程代码', '课程名称', '学分', '(学分)', '【学分】', '总学时', '理论学时', '实验/实践学时', '类型'],
			widths : [100, 220, 60, 60, 60, 60, 60, 100, 200],
			renderers : [null, null, parse0, parse0, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								return t.record.data.zxs / 16 + '周'
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								if (t.record.data.syxs == 0) {
									return ''
								} else {
									return t.record.data.syxs / 16 + '周'
								}
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, plantype],
			summarys : ['', '', 'sum', 'sum', 'sum', 'sum', 'sum', 'sum'],
			srenderers : [function() {
						return '合'
					}, function() {
						return '计:'
					}],
			url : PATH + 'plan/plan!getSummaryCourses.do?kkxq=7',
			features : [{
						ftype : 'summary'
					}],
			height : GRID_HEIGHT,
			hasSeq : true,
			autoLoad : true,
			hasPage : false,
			border : false
		},
		xq8Grid : {
			id : 'xq8Grid',
			title : '第八学期',
			fields : ['kcdm', 'kcmc', 'xf', 'qtxf', 'zkhxf', 'zxs', 'llxs', 'syxs', 'plantype', 'id', 'sjhj'],
			headers : ['课程代码', '课程名称', '学分', '(学分)', '【学分】', '总学时', '理论学时', '实验/实践学时', '类型'],
			widths : [100, 220, 60, 60, 60, 60, 60, 100, 200],
			renderers : [null, null, parse0, parse0, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								return t.record.data.zxs / 16 + '周'
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								if (t.record.data.syxs == 0) {
									return ''
								} else {
									return t.record.data.syxs / 16 + '周'
								}
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, plantype],
			summarys : ['', '', 'sum', 'sum', 'sum', 'sum', 'sum', 'sum'],
			srenderers : [function() {
						return '合'
					}, function() {
						return '计:'
					}],
			height : GRID_HEIGHT,
			url : PATH + 'plan/plan!getSummaryCourses.do?kkxq=8',
			features : [{
						ftype : 'summary'
					}],
			hasSeq : true,
			autoLoad : true,
			hasPage : false,
			border : false
		},
		xq9Grid : {
			id : 'xq9Grid',
			title : '第九学期',
			fields : ['kcdm', 'kcmc', 'xf', 'qtxf', 'zkhxf', 'zxs', 'llxs', 'syxs', 'plantype', 'id', 'sjhj'],
			headers : ['课程代码', '课程名称', '学分', '(学分)', '【学分】', '总学时', '理论学时', '实验/实践学时', '类型'],
			widths : [100, 220, 60, 60, 60, 60, 60, 100, 200],
			renderers : [null, null, parse0, parse0, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								return t.record.data.zxs / 16 + '周'
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								if (t.record.data.syxs == 0) {
									return ''
								} else {
									return t.record.data.syxs / 16 + '周'
								}
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, plantype],
			summarys : ['', '', 'sum', 'sum', 'sum', 'sum', 'sum', 'sum'],
			srenderers : [function() {
						return '合'
					}, function() {
						return '计:'
					}],
			height : GRID_HEIGHT,
			url : PATH + 'plan/plan!getSummaryCourses.do?kkxq=9',
			features : [{
						ftype : 'summary'
					}],
			hasSeq : true,
			autoLoad : true,
			hasPage : false,
			border : false
		},
		xq10Grid : {
			id : 'xq10Grid',
			title : '第十学期',
			fields : ['kcdm', 'kcmc', 'xf', 'qtxf', 'zkhxf', 'zxs', 'llxs', 'syxs', 'plantype', 'id', 'sjhj'],
			headers : ['课程代码', '课程名称', '学分', '(学分)', '【学分】', '总学时', '理论学时', '实验/实践学时', '类型'],
			widths : [100, 220, 60, 60, 60, 60, 60, 100, 200],
			renderers : [null, null, parse0, parse0, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								return t.record.data.zxs / 16 + '周'
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								if (t.record.data.syxs == 0) {
									return ''
								} else {
									return t.record.data.syxs / 16 + '周'
								}
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, plantype],
			summarys : ['', '', 'sum', 'sum', 'sum', 'sum', 'sum', 'sum'],
			srenderers : [function() {
						return '合'
					}, function() {
						return '计:'
					}],
			height : GRID_HEIGHT,
			url : PATH + 'plan/plan!getSummaryCourses.do?kkxq=10',
			features : [{
						ftype : 'summary'
					}],
			hasSeq : true,
			autoLoad : true,
			hasPage : false,
			border : false
		}
	}
}

Ext.define('TreeModel', {
			extend : 'Ext.data.Model',
			fields : [{
						name : "id",
						type : "string"
					}, {
						name : "text",
						type : "string"
					}, {
						name : "iconCls",
						type : "string"
					}, {
						name : "leaf",
						type : "boolean"
					}]
		})
function createTreeStore() {
	return Ext.create('Ext.data.TreeStore', {
				defaultRootId : '-1',
				model : 'TreeModel',
				proxy : {
					type : 'ajax',
					url : PATH + "school/school!schoolMajors.do",
					reader : {
						type : 'json',
						root : 'res'
					}
				},
				root : {
					expanded : true
				},
				nodeParam : "deptId"
			})
}

function buildTree() {
	treePanel = Ext.create('Ext.tree.Panel', {
				title : '学院专业',
				iconCls : 'contract',
				rootVisible : false,
				layout : 'fit',
				border : false,
				autoScroll : true,
				store : createTreeStore(),
				listeners : {
					'itemclick' : function(view, record, item, index, e) {
						var item = record.raw
						if (item.leaf) {
							reloadGrid(item.text)
						}
					},
					scope : this
				}
			});
	treePanel.expandAll()
	return treePanel
}
function reloadGrid(major) {
	Ext.getCmp('xq1Grid').getStore().load({
				params : {
					major : major
				}
			})
	Ext.getCmp('xq2Grid').getStore().load({
				params : {
					major : major
				}
			})
	Ext.getCmp('xq3Grid').getStore().load({
				params : {
					major : major
				}
			})
	Ext.getCmp('xq4Grid').getStore().load({
				params : {
					major : major
				}
			})
	Ext.getCmp('xq5Grid').getStore().load({
				params : {
					major : major
				}
			})
	Ext.getCmp('xq6Grid').getStore().load({
				params : {
					major : major
				}
			})
	Ext.getCmp('xq7Grid').getStore().load({
				params : {
					major : major
				}
			})
	Ext.getCmp('xq8Grid').getStore().load({
				params : {
					major : major
				}
			})
	Ext.getCmp('xq9Grid').getStore().load({
				params : {
					major : major
				}
			})
	Ext.getCmp('xq10Grid').getStore().load({
				params : {
					major : major
				}
			})
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('修读计划', 'border', [{
								region : 'west',
								width : 230,
								layout : 'fit',
								autoScroll : true,
								items : buildTree()
							}, {
								region : 'center',
								autoScroll : true,
								split : true,
								items : [createGrid(_config.xq1Grid), createGrid(_config.xq2Grid), createGrid(_config.xq3Grid), createGrid(_config.xq4Grid), createGrid(_config.xq5Grid),
										createGrid(_config.xq6Grid), createGrid(_config.xq7Grid), createGrid(_config.xq8Grid), createGrid(_config.xq9Grid), createGrid(_config.xq10Grid)]
							}])
		});