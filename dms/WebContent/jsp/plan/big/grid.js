var _config
var delIds = []
var perm
var _deptid
var department = false
function fillGrid() {
	_config = {
		planMainGrid : {
			id : 'planMainGrid',
			region : 'center',
			fields : ['version', 'state', 'handledate', 'zy', 'pymb', 'zgxk', 'id'],
			headers : ['版本(年份)', '审批状态', '编制日期', '专业大类', '大类培养特色', '大类培养面向'],
			widths : [80, 60, 90, 180, 500, 200],
			renderers : [tip, stater, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, tip, tip, tip],
			aligns : [null, null, null, null, null],
			url : PATH + 'plan/plan!list.do',
			autoLoad : false,
			params : {
				'department' : department,
				deptId : _deptid,
				type : '大类'
			},
			// storeCallback : autoSelFirst,
			tbar : [{
						id : 'addBtn',
						iconCls : 'add',
						text : '制定方案',
						tooltip : '制定方案',
						handler : function() {
							showWindow(PATH + 'plan/plan!editBig.do?opt=add', "新增培养方案", 800, 600, "yes");
						}
					}, {
						id : 'editBtn',
						iconCls : 'edit',
						text : '维护方案',
						tooltip : '维护方案',
						handler : function() {
							var sel = Ext.getCmp('planMainGrid').getSelectionModel().getSelection()
							if (sel.length == 1) {
								showEditWin(sel, 'plan/plan!editBig.do?id=' + sel[0].data.id + '&opt=edit', "维护培养方案");
							} else {
								msgShow("条件缺失", "请选择要维护的方案！");
							}
						}
					}, {
						id : 'importBtn',
						iconCls : 'import',
						text : '修读计划',
						tooltip : '制定/查看修读计划',
						handler : function() {
							var sel = Ext.getCmp('planMainGrid').getSelectionModel().getSelection()
							if (sel.length != 1) {
								Ext.Msg.alert('选择方案', '请先选择培养方案！')
							} else {
								if (sel[0].data.state == 0 || sel[0].data.state == -1) {
									showWindow(PATH + 'plan/plan!importCourse.do?pid=' + sel[0].data.id, "导入修读计划", 1600, 630, "yes");
								} else {
									Ext.Msg.alert('导入修读计划', '方案已提交，无法再修改修读计划！')
								}
							}
						}
					},
//					{
//						id : 'summaryBtn',
//						iconCls : 'summary',
//						text : '课程汇总',
//						tooltip : '查看课程构成及学分分配汇总表',
//						handler : function() {
//							var records = Ext.getCmp('planMainGrid').getSelectionModel().getSelection();
//							if (records.length == 1) {
//								showWindow(PATH + 'plan/plan!showDistribution.do?planid=' + records[0].data.id + '&type=big&version=' + records[0].data.version, "新增培养方案", 790, 310, "yes");
//							} else {
//								Ext.Msg.alert('查看课程构成及学分分配汇总表', '您必须选择一条培养方案！');
//							}
//						}
//					},
					{
						id : 'delBtn',
						iconCls : 'delete',
						text : '删除',
						tooltip : '删除选中的记录',
						handler : function() {
							var _records = Ext.getCmp('planMainGrid').getSelectionModel().getSelection();
							if (_records) {
								// 提示是否删除数据
								Ext.Msg.confirm("是否要删除？", "是否要删除这些被选择的数据？", function(btn) {
											if (btn == "yes") {
												delIds = []
												Ext.Array.forEach(_records, function(record, index, _records) {
															if (record.data.state == 0) {
																delIds.push(record.data.id);
															}
														})
												// 3发出AJAX请求删除相应的数据！
												Ext.Ajax.request({
															url : PATH + 'plan/plan!delMain.do',
															params : {
																'delIds' : delIds
															},
															success : function() {
																Ext.Msg.alert("删除信息成功", "您已经成功删除信息！");
																Ext.getCmp('planMainGrid').getStore().reload();
															},
															failure : function() {
																Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
															}
														});
											}
										});
							} else {
								Ext.Msg.alert('删除操作', '您必须选择一行数据以便删除！');
							}
						}
					}, {
						id : 'submitBtn',
						iconCls : 'submit',
						text : '提交审批',
						tooltip : '提交审批',
						handler : function() {
							var sel = Ext.getCmp('planMainGrid').getSelectionModel().getSelection()
							validThreshold(sel)
						}
					}, {
						id : 'seeBtn',
						iconCls : 'flow',
						text : '查看审批信息',
						tooltip : '查看审批信息',
						handler : function() {
							var sel = Ext.getCmp('planMainGrid').getSelectionModel().getSelection()
							showWorkFlow(sel)
						}
					}, {
						id : 'exportBtn',
						iconCls : 'word',
						text : '导出Word',
						tooltip : '导出Word',
						handler : function() {
							var sel = Ext.getCmp('planMainGrid').getSelectionModel().getSelection()
							if (sel.length == 1) {
								// window.location.href = 'down_plan?currentId='
								// + sel[0].data.id
								if (!Ext.fly('downForm')) {
									var downForm = document.createElement('form');
									downForm.id = 'downForm';
									downForm.name = 'downForm';
									downForm.className = 'x-hidden';
									downForm.action = PATH + 'plan/plan!exportBigToWord.do';
									downForm.method = 'post';
									// downForm .target = '_blank'; //打开新的下载页面
									var data = document.createElement('input');
									data.type = 'hidden';// 隐藏域
									data.name = 'currentId';// form表单参数
									data.value = sel[0].data.id;// form表单值
									downForm.appendChild(data);
									document.body.appendChild(downForm);
								}
								Ext.fly('downForm').dom.submit();
								if (Ext.fly('downForm')) {
									document.body.removeChild(downForm);
								}
							}
						}
					}, '-', '提示：请先在专业设置里确定大类']
		}
	}
}
function autoSelFirst() {
	var model = Ext.getCmp('planMainGrid').getSelectionModel()
	model.select(0)
	var sel = model.getSelection()
	if (sel.length == 1) {
		Ext.getCmp('planGrid').getStore().load({
					params : {
						planid : sel[0].data.id,
						bigtag : 1
					}
				})
	}
}
function reloadGrid(department) {
	Ext.getCmp('planMainGrid').getStore().load({
				params : {
					'department' : department,
					deptId : _deptid,
					type : '大类'
				}
			})
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('培养方案', 'border', [createGrid(_config.planMainGrid)])
			Ext.Ajax.request({
						url : PATH + 'system/menu!getRights.do',
						params : {
							action : this.location.pathname,
							roleids : _roleids
						},
						success : function(response) {
							perm = Ext.decode(response.responseText)
							if (!perm.edit) {
								Ext.getCmp('addBtn').setVisible(false)
								Ext.getCmp('editBtn').setVisible(false)
								Ext.getCmp('delBtn').setVisible(false)
								Ext.getCmp('importBtn').setVisible(false)
								Ext.getCmp('submitBtn').setVisible(false)
								Ext.getCmp('seeBtn').setVisible(false)
								Ext.getCmp('exportBtn').setVisible(false)
							}
							if (perm.department) {
								reloadGrid(perm.department)
							} else {
								reloadGrid()
								autoSelFirst()
							}
						},
						failure : function() {
							Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
						}
					});
		});