var newFormWin
var step
var platform
var planCourseForm = new Ext.FormPanel({
			labelWidth : 75,
			frame : true,
			width : 400,
			padding : '5 20 5 5',
			waitMsgTarget : true,
			defaults : {
				labelAlign : 'right'
			},
			defaultType : 'textfield',
			items : [{
						id : 'id',
						name : 'course.id',
						hidden : true,
						hideLabel : true,
						allowBlank : true
					}, {
						fieldLabel : '阶段',
						xtype : 'combo',
						afterLabelTextTpl : required,
						allowBlank : false,
						store : getDictStore(true, 'KCJD'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : false,
						name : 'course.jd',
						id : 'jd',
						triggerAction : 'all',
						listeners : {
							select : {
								scope : this,
								fn : function(combo, record, index) {
									var value = combo.getValue()
									var rec = combo.findRecord(combo.valueField || combo.displayField, value);
									var recv = rec.data
									var JyptGrid = Ext.getCmp('jypt')
									JyptGrid.clearValue()
									Ext.getCmp('mk').clearValue()
									JyptGrid.getStore().proxy.extraParams = {
										dictStyle : 'JYPT',
										pid : recv.detailId
									}
									JyptGrid.getStore().load()
								}
							}
						},
						anchor : '100%'
					}, {
						fieldLabel : '平台',
						xtype : 'combo',
						afterLabelTextTpl : required,
						allowBlank : false,
						store : getDictStore(false),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : false,
						name : 'course.pt',
						id : 'jypt',
						triggerAction : 'all',
						anchor : '100%',
						listeners : {
							select : {
								scope : this,
								fn : function(combo, record, index) {
									var value = combo.getValue()
									var rec = combo.findRecord(combo.valueField || combo.displayField, value);
									var recv = rec.data
									var MkGrid = Ext.getCmp('mk')
									MkGrid.clearValue()
									MkGrid.getStore().proxy.extraParams = {
										dictStyle : 'KCMK',
										pid : recv.detailId
									}
									MkGrid.getStore().load()
								}
							}
						}
					}, {
						fieldLabel : '模块',
						xtype : 'combo',
						afterLabelTextTpl : required,
						allowBlank : false,
						store : getDictStore(false),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : false,
						name : 'course.mk',
						id : 'mk',
						triggerAction : 'all',
						anchor : '100%',
						listeners : {
							select : {}
						}
					}, {
						fieldLabel : '课程性质',
						xtype : 'combo',
						afterLabelTextTpl : required,
						allowBlank : false,
						store : getDictStore(true, 'KCXZ'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : false,
						name : 'course.kcxz',
						id : 'kcxz',
						triggerAction : 'all',
						anchor : '100%',
						listeners : {
							select : {

							}
						}
					}, {
						fieldLabel : '课程代码',
						name : 'course.kcdm',
						anchor : '100%'
					}, {
						fieldLabel : '课程名称',
						name : 'course.kcmc',
						anchor : '100%',
						allowBlank : false
					}, {
						fieldLabel : '学分',
						xtype : 'numberfield',
						name : 'course.xf',
						anchor : '100%',
						allowBlank : false
					}, {
						fieldLabel : '(学分)',
						xtype : 'numberfield',
						name : 'course.qtxf',
						anchor : '100%',
						allowBlank : false
					}, {
						fieldLabel : '总学时',
						name : 'course.zxs',
						xtype : 'numberfield',
						anchor : '100%',
						allowBlank : false
					}, {
						fieldLabel : '实验（实践）学时',
						name : 'course.syxs',
						xtype : 'numberfield',
						anchor : '100%',
						allowBlank : false
					}, {
						fieldLabel : '开课学期',
						name : 'course.kkxq',
						anchor : '100%',
						allowBlank : false
					}, {
						fieldLabel : '集中性实践环节',
						xtype : 'combo',
						store : getDictStore(true, 'SJHJ'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : false,
						name : 'course.sjhj',
						id : 'sjhj',
						triggerAction : 'all',
						anchor : '100%',
						listeners : {
							select : {

							}
						}
					}, {
						xtype : 'textarea',
						fieldLabel : '修读说明',
						name : 'course.xdsm',
						anchor : '100%'
					}]
		});
var addFormWin = function() {
	var selMain = Ext.getCmp('planMainGrid').getSelectionModel().getSelection()
	if (selMain.length == 1) {
		if (selMain[0].data.state == 0 || selMain[0].data.state == -1) {
			if (!newFormWin) {
				newFormWin = new Ext.Window({
							width : 430,
							height : 420,
							closable : true,
							autoScroll : true,
							title : '添加课程',
							modal : true,
							border : false,
							items : planCourseForm,
							buttons : [{
										text : '保存',
										scale : 'medium',
										disabled : false,
										handler : addBtnsHandler
									}, {
										text : '取消',
										scale : 'medium',
										handler : function() {
											planCourseForm.form.reset();// 清空表单
											newFormWin.hide();
										}
									}]
						});
			}
			newFormWin.show();// 显示此窗口
		} else {
			msgShow("无法操作", "方案正在审批！", Ext.Msg.WARNING);
		}
	} else {
		msgShow('提示', '请选择需要修改课程指导性计划的培养方案。');
	}
}
function addBtnsHandler() {
	var selMain = Ext.getCmp('planMainGrid').getSelectionModel().getSelection()
	if (planCourseForm.form.isValid()) {
		planCourseForm.form.submit({
					url : PATH + 'plan/plan!editCourseUnderPlan.do',
					waitMsg : '正在保存数据，稍后...',
					params : {
						'course.newadd' : 'newadd',
						'course.department' : _deptname,
						currentId : selMain[0].data.id,
						bigtag : 1
					},
					success : function(form, action) {
						msgShow("保存成功", "您已经成功保存信息！");
						Ext.getCmp('planGrid').getStore().reload();
						planCourseForm.form.reset();// 清空表单
						newFormWin.hide();
					},
					failure : function(form, response) {
						var errors = Ext.decode(response.response.responseText).res
						var errorStr = '<font color="red">'
						for (var i = 1; i < errors.length; i++) {
							errorStr += errors[i].defaultMessage + '<br>'
						}
						errorStr += '</font>'
						msgShow('保存失败', errorStr, Ext.Msg.ERROR)
					}
				});
	} else {
		msgShow('信息', '请填写完成再提交!', Ext.Msg.WARNING);
	}
}
function validThreshold(sel) {
	if (sel.length == 1) {
		Ext.Ajax.request({
					url : PATH + 'plan/plan!validThreshold.do',
					params : {
						currentId : sel[0].data.id,
						version : sel[0].data.version
					},
					success : function(response, action) {
						if (response.responseText != 'true') {
							msgShow('警告', response.responseText, Ext.Msg.WARNING, function() {
										startWorkFlow(sel, 'DL', 'PlanMainModel', true)
									})
						} else {
							startWorkFlow(sel, 'DL', 'PlanMainModel', true)
						}
					},
					failure : function(response, action) {
						msgShow('错误', '服务器出现错误请稍后再试！', Ext.Msg.ERROR);
					}
				});
	} else {
		msgShow("条件缺失", "请选择要审批的记录！", Ext.Msg.WARNING);
	}
}