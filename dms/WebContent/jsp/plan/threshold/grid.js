var planid = getUrlParam('planid');
var distributeGrid = {
	id : 'distributeGrid',
	title : '课程构成及学分分配汇总表',
	hasCheck : false,
	features : [{
				ftype : 'summary'
			}],
	selType : 'cellmodel',
	border : false,
	hasSeq : false,
	tbar : [{
				id : 'version',
				fieldLabel : '版本(年份)',
				xtype : 'numberfield',
				labelWidth : 80,
				afterLabelTextTpl : required,
				allowBlank : false,
				value : parseInt(new Date().getFullYear().toString())
			}, {
				text : '查看(选择版本/年份查看阈值)',
				iconCls : 'info',
				handler : function() {
					Ext.getCmp('distributeGrid').getStore().load({
								params : {
									version : Ext.getCmp('version').getValue()
								}
							})
				}
			}, {
				text : '保存',
				iconCls : 'save',
				handler : function() {
					var gridArray = []
					var itemp = Ext.getCmp('distributeGrid').store.data.items
					if (itemp.length != 0) {
						for (var i = 0; i < itemp.length; i++) {
							gridArray.push(itemp[i].data)
						}
					}
					var gridJson = JSON.stringify(gridArray)
					Ext.Ajax.request({
								url : PATH + 'school/school!saveThreshold.do',
								params : {
									'version' : Ext.getCmp('version').getValue(),
									'gridJson' : gridJson
								},
								method : 'POST',
								success : function(response, options) {
									msgShow('提示', '保存成功。')
								},
								failure : function(response, options) {
									Ext.MessageBox.alert('失败', '请求超时或网络故障,错误编号：' + response.status);
								}
							})
				}
			}],
	fields : ['pt', 'mk', 'xfmin', 'xf', 'sjxfmin', 'sjxf', 'zzxfblmin', 'zzxfbl', 'sjblmin', 'sjbl', 'qtxfmin', 'qtxf', 'jxxf', 'id', 'sindex'],
	headers : [['平台', '模块'], ['总学分<br>最小值', '总学分<br>最大值', '其中实践环节<br>学分最小值', '其中实践环节<br>学分最大值'], ['占总学分比例<br>最小值（%）', '占总学分比例<br>最大值（%）', '其中实践环节<br>比例最小值', '其中实践环节<br>比例最大值'], '（学分）<br>最小值',
			'（学分）<br>最大值'],
	groupHeaders : ['课程类别', '学分', '占总学分比例（%）'],
	widths : [100, 170, 70, 70, 100, 100, 100, 100, 100, 100, 80, 80],
	renderers : [tip, function(value, meta, record) {
				meta.style = autoWarp;
				return value;
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}],
	editors : [null, null, 'numberfield', 'numberfield', 'numberfield', 'numberfield', 'numberfield', 'numberfield', 'numberfield', 'numberfield', 'numberfield', 'numberfield'],
	aligns : ['center', 'center', 'center', 'center', 'center', 'center', 'center', 'center', 'center', 'center', 'center', 'center'],
	summarys : [null, null, 'sum', 'sum', 'sum', 'sum', 'sum', 'sum', 'sum', 'sum', 'sum', 'sum'],
	srenderers : [function() {
				return '合'
			}, function() {
				return '计:'
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}, function(v) {
				return Ext.util.Format.round(v, 2);
			}],
	url : PATH + 'plan/plan!getDistributionThreshold.do',
	autoLoad : true,
	hasPage : false
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			createApp('课程构成及学分分配汇总表', 'fit', [createGrid(distributeGrid)])
		});