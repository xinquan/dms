var planid = getUrlParam('planid');
var opt = getUrlParam('opt');
var gridArray = []
var gridJson
var matrixGrid = {
	id : 'matrixGrid',
	title : '课程设置与毕业要求关系矩阵',
	hasCheck : false,
	border : false,
	selType : 'cellmodel',
	hasSeq : true,
	fields : ['kcmc', 'k1', 'k2', 'k3', 'k4', 'k5', 'k6', 'k7', 'k8', 'k9', 'k10', 'k11', 'k12', 'planid', 'id'],
	headers : ['课程名称', ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']],
	groupHeaders : ['课程名称', '培养要求'],
	widths : [200, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80],
	renderers : [tip, tip, tip, tip, tip, tip, tip, tip, tip, tip, tip, tip, tip],
	editors : [null, 'textfield', 'textfield', 'textfield', 'textfield', 'textfield', 'textfield', 'textfield', 'textfield', 'textfield', 'textfield', 'textfield', 'textfield'],
	aligns : [null, null, null, null, null, null, null, null, null, null, null, null, null],
	url : PATH + 'plan/plan!getMatrixData.do?planid=' + planid,
	tbar : [{
				text : '保存',
				id : 'saveBtn',
				iconCls : 'save',
				handler : function() {
					if (getGridJson()) {
						Ext.getBody().mask('正在提交数据，请稍等....');
						Ext.Ajax.request({
									url : PATH + 'plan/plan!editMatrix.do',
									params : {
										planid : planid,
										'gridJson' : gridJson
									},
									method : 'POST',
									success : function(response, options) {
										Ext.getBody().unmask();
										Ext.Msg.alert('成功', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;操作成功！！！', function() {
													window.close()
												});
									},
									failure : function(response, options) {
										Ext.Msg.alert('失败', '请求超时或网络故障,错误编号：' + response.status);
									}
								})
					}
				}
			}, '-', '注：拟参加工程教育认证专业填写此表'],
	autoLoad : true,
	hasPage : false
}
function getGridJson() {
	var items = Ext.getCmp('matrixGrid').store.data.items
	if (items.length != 0) {
		for (var i = 0; i < items.length; i++) {
			gridArray.push(items[i].data)
		}
	}
	gridJson = JSON.stringify(gridArray)
	return true;
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			createApp('课程设置与毕业要求关系矩阵', 'fit', [createGrid(matrixGrid)])
			if (opt == 'show') {
				Ext.getCmp('saveBtn').setDisabled(true)
			}
		});