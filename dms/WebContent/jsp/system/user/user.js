var _config
var userWin
var uploadWin
var currentId
var department
// Ext.define('major', {
// extend : 'Ext.data.Model',
// fields : [{
// name : 'id',
// type : 'string'
// }, {
// name : 'majorId',
// type : 'string'
// }, {
// name : 'majorName',
// type : 'string'
// }, {
// name : 'majorYear',
// type : 'string'
// }, {
// name : 'category',
// type : 'string'
// }, {
// name : 'degree',
// type : 'string'
// }]
// });
// var majords = Ext.create('Ext.data.Store', {
// model : 'major',
// proxy : {
// type : 'ajax',
// url : PATH + "school/school!majorList.do",
// actionMethods : {
// read : 'POST'
// },
// extraParams : {
// deptId : _deptid
// },
// reader : {
// type : 'json',
// root : 'res'
// }
// },
// autoLoad : true
// });
Ext.define('depart', {
			extend : 'Ext.data.Model',
			fields : [{
						name : 'id',
						type : 'string'
					}, {
						name : 'deptid',
						type : 'string'
					}, {
						name : 'deptname',
						type : 'string'
					}]
		});
var departs = Ext.create('Ext.data.Store', {
			model : 'depart',
			proxy : {
				type : 'ajax',
				url : PATH + "system/system!departList.do",
				actionMethods : {
					read : 'POST'
				},
				reader : {
					type : 'json',
					root : 'res'
				}
			},
			autoLoad : true
		});
function fillGrid() {
	_config = {
		userGrid : {
			id : 'userGrid',
			fields : ['username', 'unum', 'name', 'sex', 'department', 'registerDate', 'email', 'uindex', 'id', 'password', 'editable'],
			headers : ['用户名', '工号', '姓名', '性别', '所属部门'],
			widths : [100, 100, 100, 100, 180, 180],
			filters : [false, true, true, false, true, true],
			filtersurl : [PATH + 'system/menu!queryUserListByParams.do', PATH + 'system/menu!userList.do'],
			url : PATH + 'system/menu!userList.do',
			features : [{
						ftype : 'filters',
						local : true
					}],
			autoLoad : false,
			hasPage : true,
			border : false,
			tbar : [{
						iconCls : 'add',
						id : 'addBtn',
						text : '新增',
						handler : function() {
							editUser('add')
						}
					}, {
						iconCls : 'edit',
						id : 'editBtn',
						text : '编辑',
						handler : function() {
							editUser('edit')
						}
					}, {
						iconCls : 'delete',
						id : 'deleteBtn',
						text : '删除',
						handler : function() {
							var records = Ext.getCmp('userGrid').getSelectionModel().getSelection();
							if (records) {
								// 提示是否删除数据
								Ext.Msg.confirm("是否要删除？", "是否要删除这些被选择的数据？", function(btn) {
											if (btn == "yes") {
												delIds = []
												for (var i = 0; i < records.length; i++) {
													delIds.push(records[i].data.id);
												}
												// 3发出AJAX请求删除相应的数据！
												Ext.Ajax.request({
															url : PATH + 'system/menu!delUser.do',
															params : {
																'delIds' : delIds
															},
															success : function() {
																Ext.Msg.alert("删除信息成功", "您已经成功删除信息！");
																Ext.getCmp('userGrid').getStore().reload();
															},
															failure : function() {
																Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
															}
														});
											}
										});
							} else {
								Ext.Msg.alert('删除操作', '您必须选择一行数据以便删除！');
							}
						}
					}
			// , {
			// id : 'importBtn',
			// iconCls : 'excel',
			// text : '导入用户',
			// handler : function() {
			// uploadUserFromExcel()
			// }
			// }
			]
		}
	}
}
var userForm = new Ext.FormPanel({
			labelWidth : 75,
			border : false,
			padding : '5 20 5 5',
			waitMsgTarget : true,
			defaults : {
				labelAlign : 'right'
			},
			defaultType : 'textfield',
			items : [{
						id : 'id',
						name : 'user.id',
						hidden : true,
						hideLabel : true,
						allowBlank : true
					}, {
						fieldLabel : '登录名',
						afterLabelTextTpl : required,
						allowBlank : false,
						name : 'user.username',
						id : 'username',
						anchor : '100%'
					}, {
						fieldLabel : '密码',
						afterLabelTextTpl : required,
						allowBlank : false,
						name : 'user.password',
						id : 'password',
						anchor : '100%'
					}, {
						fieldLabel : '工号',
						afterLabelTextTpl : required,
						allowBlank : false,
						name : 'user.unum',
						id : 'unum',
						anchor : '100%'
					}, {
						fieldLabel : '姓名',
						afterLabelTextTpl : required,
						allowBlank : false,
						name : 'user.name',
						id : 'name',
						anchor : '100%'
					}, {
						fieldLabel : '性別',
						xtype : 'combo',
						store : getDictStore(true, 'SEX'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : false,
						name : 'user.sex',
						id : 'sex',
						triggerAction : 'all',
						anchor : '100%'
					}, {
						xtype : 'combo',
						fieldLabel : '所属部门',
						store : departs,
						mode : 'remote',
						editable : false,
						valueField : 'deptname',
						displayField : 'deptname',
						value : _deptname,
						name : 'user.department',
						id : 'deptname',
						triggerAction : 'all',
						anchor : '100%'
						// listeners : {
					// select : {
					// scope : this,
					// fn : function(combo, record, index) {
					// var value = combo.getValue()
					// var rec = combo.findRecord(combo.valueField ||
					// combo.displayField, value);
					// var recv = rec.data
					// var major = Ext.getCmp('major')
					// major.clearValue()
					// major.getStore().proxy.extraParams = {
					// deptId : recv.id
					// }
					// major.getStore().load()
					// }
					// }
					// }
				}
					// , {
					// xtype : 'combo',
					// fieldLabel : '所属专业',
					// store : majords,
					// mode : 'remote',
					// editable : false,
					// valueField : 'majorName',
					// displayField : 'majorName',
					// name : 'user.major',
					// id : 'major',
					// triggerAction : 'all',
					// anchor : '100%'
					// }
					, {
						fieldLabel : '排序',
						xtype : 'numberfield',
						name : 'user.uindex',
						anchor : '100%'
					}]
		});
var editUser = function(opt) {
	if (opt == 'edit') {
		var sel = Ext.getCmp('userGrid').getSelectionModel().getSelection()
		if (sel.length == 1) {
			currentId = sel[0].data.id
			Ext.Ajax.request({
						url : PATH + 'system/menu!findUser.do',
						params : {
							id : sel[0].data.id
						},
						callback : function(opt, success, response) {
							var course = makeNewObjAddPrefix(Ext.decode(response.responseText).result, 'user.')
							userForm.form.setValues(course)
						}
					})
			if (!userWin) {
				userWin = new Ext.Window({
							width : 430,
							height : 340,
							closable : true,
							autoScroll : true,
							title : '编辑用户',
							modal : true,
							layout : 'fit',
							border : false,
							buttonAlign : 'center',
							items : userForm,
							buttons : [{
										text : '保存',
										scale : 'medium',
										disabled : false,
										handler : addBtnsHandler
									}, {
										text : '取消',
										scale : 'medium',
										handler : function() {
											userForm.form.reset();// 清空表单
											userWin.hide();
										}
									}]
						});
			} else {
				userWin.setTitle('编辑用户')
			}
			userWin.show();// 显示此窗口
		} else {
			Ext.Msg.alert('提示', '请选择需要修改的用户。');
		}
	} else {
		currentId = 0
		if (!userWin) {
			userWin = new Ext.Window({
						width : 430,
						height : 340,
						closable : true,
						autoScroll : true,
						title : '新增用户',
						modal : true,
						layout : 'fit',
						border : false,
						buttonAlign : 'center',
						items : userForm,
						buttons : [{
									text : '保存',
									scale : 'medium',
									disabled : false,
									handler : addBtnsHandler
								}, {
									text : '取消',
									scale : 'medium',
									handler : function() {
										userForm.form.reset();// 清空表单
										userWin.hide();
									}
								}]
					});
		} else {
			userWin.setTitle('新增用户')
		}
		userWin.show();// 显示此窗口
	}

}
function addBtnsHandler() {
	Ext.Ajax.request({
				url : PATH + 'system/menu!uniqueUser.do',
				params : {
					'id' : currentId,
					'username' : Ext.getCmp('username').getValue(),
					'unum' : Ext.getCmp('unum').getValue()
				},
				method : 'POST',
				success : function(response, options) {
					if ('true' == response.responseText) {
						if (userForm.form.isValid()) {
							userForm.form.submit({
										url : PATH + 'system/menu!editUser.do',
										waitMsg : '正在保存数据，稍后...',
										success : function(form, action) {
											Ext.Msg.alert('保存成功', '操作成功！');
											userForm.form.reset();// 清空表单
											Ext.getCmp('userGrid').getStore().reload();
											userWin.hide();
										},
										failure : function(form, response) {
											var errors = Ext.decode(response.response.responseText).res
											var errorStr = '<font color="red">'
											for (var i = 1; i < errors.length; i++) {
												errorStr += errors[i].defaultMessage + '<br>'
											}
											errorStr += '</font>'
											Ext.Msg.alert('保存失败', errorStr)
										}
									});
						} else {
							Ext.Msg.alert('信息', '请填写完成再提交!');
						}
					} else {
						Ext.Msg.alert('提示', response.responseText)
					}
				},
				failure : function(response, options) {
					Ext.MessageBox.alert('失败', '请求超时或网络故障,错误编号：' + response.status);
				}
			})
}
function reloadGrid(department) {
	Ext.getCmp('userGrid').getStore().load({
				params : {
					'overdepartment' : department,
					deptname : _deptname
				}
			})
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			getPerm()
		});
function getPerm() {
	Ext.Ajax.request({
				url : PATH + 'system/menu!getRights.do',
				params : {
					action : this.location.pathname,
					roleids : _roleids
				},
				success : function(response) {
					perm = Ext.decode(response.responseText)
					department = perm.department
					fillGrid()
					createApp('用户管理', 'fit', [createGrid(_config.userGrid)])
					if (!perm.edit) {
						Ext.getCmp('addBtn').setVisible(false)
						Ext.getCmp('editBtn').setVisible(false)
						Ext.getCmp('deleteBtn').setVisible(false)
						Ext.getCmp('importBtn').setVisible(false)
					}
					if (perm.department) {
						reloadGrid(perm.department)
					} else {
						reloadGrid('false')
					}
				},
				failure : function() {
					Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
				}
			});
}
var uploadUserForm = Ext.create('Ext.form.Panel', {
			bodyPadding : 10,
			border : false,
			items : [{
						xtype : 'filefield',
						id : 'userfile',
						name : 'file', // 服务端获取 “名称”
						afterLabelTextTpl : '<span style="color:red;font-weight:bold" data-qtip="必需填写">*</span>',
						fieldLabel : '文件',
						labelWidth : 50,
						msgTarget : 'side', // 提示 文字的位置
						allowBlank : false,
						anchor : '100%',
						buttonText : '选择文件'
					}],

			buttons : [{
						text : '下载模版',
						handler : function() {
							window.location.href = PATH + "system/common!downloadUserTemplateFile.do"
						}
					}, {
						text : '导入',
						handler : function() {
							var form = this.up('form').getForm();
							if (form.isValid()) { // form 验证
								var excel_reg = /\.([xX][lL][sS]){1}$|\.([xX][lL][sS][xX]){1}$/;
								if (!excel_reg.test(Ext.getCmp('userfile').getValue())) {
									msgShow('提示信息', '文件类型错误,请选择Excel文件(xls/xlsx)', Ext.Msg.WARNING);
								} else {
									// 取控件DOM对象
									var field = document.getElementById('userfile');
									// 取控件中的input元素
									var inputs = field.getElementsByTagName('input');
									var fileInput = null;
									var il = inputs.length;
									// 取出input 类型为file的元素
									for (var i = 0; i < il; i++) {
										if (inputs[i].type == 'file') {
											fileInput = inputs[i];
											break;
										}
									}
									if (fileInput != null) {
										var fileSize = getFileSize(fileInput);
										// 允许上传不大于1M的文件
										if (fileSize > 10485760) {
											msgShow('提示信息', '文件太大，请选择小于10M的文件！', Ext.Msg.WARNING);
										} else {
											form.submit({ // 提交
												url : PATH + 'system/menu!uploadUserFile.do',
												waitMsg : '正在上传和解析，请稍后....',
												success : function(fp, o) {
													msgShow('上传并解析成功', '您的文件 "' + o.result.fileName + '"成功上传并解析，已导入数据库....');
													Ext.getCmp('userGrid').getStore().reload()
												}
											});
										}
									}
								}

							}
						}
					}, {
						text : '取消',
						handler : function() {
							uploadWin.hide()
						}
					}]
		});
function uploadUserFromExcel() {
	if (!uploadWin) {
		uploadWin = new Ext.Window({
					width : 460,
					height : 120,
					layout : 'fit',
					closable : false,
					autoScroll : true,
					title : '从Excel导入用户信息。<font color="yellow">请先下载模版，根据模版整理数据。</font>',
					modal : true,
					border : false,
					items : uploadUserForm
				}).show();
	} else {
		uploadWin.show()
	}
}