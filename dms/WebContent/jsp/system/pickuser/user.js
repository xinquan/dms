var _config
var muti = getUrlParam('muti');
if (muti == 'undefined') {
	muti = true
}
function fillGrid() {
	_config = {
		userGrid : {
			id : 'userGrid',
			fields : ['username', 'unum', 'name', 'sex',  'department', 'uindex', 'email', 'registerdate', 'id', 'editable', 'password'],
			headers : ['用户名', '工号', '姓名', '性别',  '所属部门', '排序'],
			widths : [100, 100, 100, 100,  100, 100],
			filters : [false, true, true, false,  true, false],
			filtersurl : [PATH + 'system/menu!queryUserListByParams.do', PATH + 'system/menu!userList.do'],
			url : PATH + 'system/menu!userList.do',
			features : [{
						ftype : 'filters',
						local : true
					}],
			autoLoad : true,
			hasCheck : muti,
			hasPage : true,
			border : false,
			tbar : [{
						iconCls : 'add',
						text : '确定添加',
						handler : function() {
							var records = Ext.getCmp('userGrid').getSelectionModel().getSelection();
							if (muti == 'false' && records.length != 1) {
								msgShow('提示信息', '您选择了多个用户，请选择一位用户')
							} else {
								 window.opener.addUser(records)
								window.close()
							}
						}
					}]
		}
	}
}
function initGrid() {
	Ext.getCmp('userGrid').on('itemmousedown', function(view, record, item, index, e, opt) {
				e.ctrlKey = true;
			})
	Ext.getCmp('userGrid').on('itemclick', function(view, record, item, index, e, opt) {
				e.ctrlKey = true;
			})
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('用户管理', 'fit', [createGrid(_config.userGrid)])
			initGrid()
		});