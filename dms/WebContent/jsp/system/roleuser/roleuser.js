var _config
var currentRoleId
function fillGrid() {
	_config = {
		userGrid : {
			id : 'userGrid',
			fields : ['username', 'unum', 'name', 'sex', 'major', 'department', 'uindex', 'email', 'registerdate', 'id', 'editable', 'password'],
			headers : ['用户名',  '工号/学号', '姓名', '性别', '所属专业', '所属学院', '排序'],
			widths : [100,  100, 100, 100, 100, 100, 100],
			url : PATH + 'system/menu!queryUserListUnderRole.do',
			autoLoad : true,
			region : 'center',
			hasPage : true,
			border : false,
			tbar : [{
						iconCls : 'add',
						text : '添加用户',
						handler : function() {
							if (currentRoleId) {
								showWindow(PATH + 'system/menu!importUser.do', "为角色添加用户", 800, 600, "yes");
							} else {
								Ext.Msg.alert('添加操作', '您必须选择一个角色以便为该角色添加用户！');
							}
						}
					}, {
						iconCls : 'delete',
						text : '移除用户',
						handler : function() {
							if (currentRoleId) {
								var records = Ext.getCmp('userGrid').getSelectionModel().getSelection();
								if (records.length != 0) {
									// 提示是否删除数据
									Ext.Msg.confirm("是否要删除？", "是否要删除这些被选择的数据？", function(btn) {
												if (btn == "yes") {
													delIds = []
													for (var i = 0; i < records.length; i++) {
														delIds.push(records[i].data.id);
													}
													// 3发出AJAX请求删除相应的数据！
													Ext.Ajax.request({
																url : PATH + 'system/menu!removeUserFromRole.do',
																params : {
																	roleid : currentRoleId,
																	'delIds' : delIds
																},
																success : function() {
																	Ext.Msg.alert("移除成功", "您已经成功移除用户！");
																	Ext.getCmp('userGrid').getStore().reload();
																},
																failure : function() {
																	Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
																}
															});
												}
											});
								} else {
									Ext.Msg.alert('删除操作', '您必须选择一个用户以便删除！');
								}
							} else {
								Ext.Msg.alert('移除操作', '您必须选择一个角色以便移除该角色下的用户！');
							}
						}
					}]
		}
	}
}
Ext.define('TreeModel', {
			extend : 'Ext.data.Model',
			fields : [{
						name : "id",
						type : "string"
					}, {
						name : "text",
						type : "string"
					}, {
						name : "iconCls",
						type : "string"
					}, {
						name : "leaf",
						type : "boolean"
					}]
		})
function createTreeStore() {
	return Ext.create('Ext.data.TreeStore', {
				defaultRootId : '-1',
				model : 'TreeModel',
				proxy : {
					type : 'ajax',
					extraParams : {},
					url : PATH + "system/menu!roleTreeList.do",
					reader : {
						type : 'json',
						root : 'res'
					}
				},
				root : {
					expanded : true
				},
				nodeParam : "pid"
			})
}

function buildTree() {
	return Ext.create('Ext.tree.Panel', {
				title : '角色列表',
				iconCls : 'rolelist',
				rootVisible : false,
				border : false,
				store : createTreeStore(),
				listeners : {
					'itemclick' : function(view, record, item, index, e) {
						var item = record.raw
						currentRoleId = item.id
						Ext.getCmp('userGrid').store.load({
									params : {
										id : item.id
									}
								})
					},
					scope : this
				}
			});
}
function addUser(records) {
	Ext.Array.forEach(records, function(record, index, records) {
				Ext.Ajax.request({
							url : PATH + 'system/menu!addUserToRole.do',
							params : {
								'roleid' : currentRoleId,
								'userid' : record.data.id
							},
							success : function() {
							},
							failure : function() {
							}
						});
			})

	Ext.Msg.alert('提示', '您已经成功为用户分配角色！', function() {
				Ext.getCmp('userGrid').store.reload()
			})
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('角色分配', 'border', [{
								region : 'west',
								width : 210,
								items : buildTree()
							}, createGrid(_config.userGrid)])
		});