var _config
var roleWin
var roleForm = new Ext.FormPanel({
			labelWidth : 75,
			border : false,
			padding : '5 20 5 5',
			waitMsgTarget : true,
			defaults : {
				labelAlign : 'right'
			},
			defaultType : 'textfield',
			items : [{
						id : 'id',
						name : 'role.id',
						hidden : true,
						hideLabel : true,
						allowBlank : true
					}, {
						fieldLabel : '角色名',
						afterLabelTextTpl : required,
						allowBlank : false,
						name : 'role.name',
						id : 'rolename',
						anchor : '100%'
					}, {
						fieldLabel : '角色说明',
						name : 'role.explan',
						id : 'explan',
						anchor : '100%'
					}, {
						fieldLabel : '排序',
						xtype : 'numberfield',
						name : 'role.rindex',
						anchor : '100%'
					}]
		});
var editRole = function(opt) {
	if (opt == 'edit') {
		var sel = Ext.getCmp('roleGrid').getSelectionModel().getSelection()
		if (sel.length == 1) {
			Ext.Ajax.request({
						url : PATH + 'system/menu!findRole.do',
						params : {
							id : sel[0].data.id
						},
						callback : function(opt, success, response) {
							var role = makeNewObjAddPrefix(Ext.decode(response.responseText).result, 'role.')
							roleForm.form.setValues(role)
						}
					})
			if (!roleWin) {
				roleWin = new Ext.Window({
							width : 430,
							height : 180,
							closable : true,
							autoScroll : true,
							title : '编辑角色',
							layout : 'fit',
							modal : true,
							border : false,
							buttonAlign : 'center',
							items : roleForm,
							buttons : [{
										text : '保存',
										scale : 'medium',
										disabled : false,
										handler : addBtnsHandler
									}, {
										text : '取消',
										scale : 'medium',
										handler : function() {
											roleForm.form.reset();// 清空表单
											roleWin.hide();
										}
									}]
						});
			} else {
				roleWin.setTitle('编辑角色')
			}
			roleWin.show();// 显示此窗口
		} else {
			Ext.Msg.alert('提示', '请选择需要修改的角色。');
		}
	} else {
		if (!roleWin) {
			roleWin = new Ext.Window({
						width : 430,
						height : 180,
						closable : true,
						autoScroll : true,
						title : '新增角色',
						layout : 'fit',
						modal : true,
						border : false,
						buttonAlign : 'center',
						items : roleForm,
						buttons : [{
									text : '保存',
									scale : 'medium',
									disabled : false,
									handler : addBtnsHandler
								}, {
									text : '取消',
									scale : 'medium',
									handler : function() {
										roleForm.form.reset();// 清空表单
										roleWin.hide();
									}
								}]
					});
		} else {
			roleWin.setTitle('新增角色')
		}
		roleWin.show();// 显示此窗口
	}

}
function addBtnsHandler() {
	if (roleForm.form.isValid()) {
		roleForm.form.submit({
					url : PATH + 'system/menu!editRole.do',
					waitMsg : '正在保存数据，稍后...',
					success : function(form, action) {
						Ext.Msg.alert('保存成功', '操作成功！');
						roleForm.form.reset();// 清空表单
						Ext.getCmp('roleGrid').getStore().reload();
						roleWin.hide();
					},
					failure : function(form, response) {
						var errors = Ext.decode(response.response.responseText).res
						var errorStr = '<font color="red">'
						for (var i = 1; i < errors.length; i++) {
							errorStr += errors[i].defaultMessage + '<br>'
						}
						errorStr += '</font>'
						Ext.Msg.alert('保存失败', errorStr)
					}
				});
	} else {
		Ext.Msg.alert('信息', '请填写完成再提交!');
	}
}
function fillGrid() {
	_config = {
		roleGrid : {
			id : 'roleGrid',
			fields : ['name', 'explan', 'rindex', 'id', 'editable'],
			headers : ['角色', '说明'],
			widths : [100, 300],
			url : PATH + 'system/menu!roleList.do',
			autoLoad : true,
			hasPage : true,
			border : false,
			tbar : [{
						iconCls : 'add',
						text : '新增',
						handler : function() {
							editRole('add')
						}
					}, {
						iconCls : 'edit',
						text : '编辑',
						handler : function() {
							editRole('edit')
						}
					}, {
						iconCls : 'delete',
						text : '删除',
						handler : function() {
							var records = Ext.getCmp('roleGrid').getSelectionModel().getSelection();
							if (records) {
								// 提示是否删除数据
								Ext.Msg.confirm("是否要删除？", "是否要删除这些被选择的数据？", function(btn) {
											if (btn == "yes") {
												delIds = []
												for (var i = 0; i < records.length; i++) {
													delIds.push(records[i].data.id);
												}
												// 3发出AJAX请求删除相应的数据！
												Ext.Ajax.request({
															url : PATH + 'system/menu!delRole.do',
															params : {
																'delIds' : delIds
															},
															success : function() {
																Ext.Msg.alert("删除信息成功", "您已经成功删除信息！");
																Ext.getCmp('roleGrid').getStore().reload();
															},
															failure : function() {
																Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
															}
														});
											}
										});
							} else {
								Ext.Msg.alert('删除操作', '您必须选择一行数据以便删除！');
							}
						}
					}]
		}
	}
}

Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('用户管理', 'fit', [createGrid(_config.roleGrid)])
		});