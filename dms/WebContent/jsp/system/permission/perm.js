var currentRoleId
var role_menu = {}
var role_menu_array = []
var jsonToSave
Ext.define('TreeModel', {
			extend : 'Ext.data.Model',
			fields : [{
						name : "id",
						type : "string"
					}, {
						name : "text",
						type : "string"
					}, {
						name : "iconCls",
						type : "string"
					}, {
						name : "leaf",
						type : "boolean"
					}]
		})
function createTreeStore() {
	return Ext.create('Ext.data.TreeStore', {
				defaultRootId : '-1',
				model : 'TreeModel',
				proxy : {
					type : 'ajax',
					extraParams : {},
					url : PATH + "system/menu!roleTreeList.do",
					reader : {
						type : 'json',
						root : 'res'
					}
				},
				root : {
					expanded : true
				},
				nodeParam : "pid"
			})
}

function buildTree() {
	return Ext.create('Ext.tree.Panel', {
				title : '角色列表',
				iconCls : 'rolelist',
				rootVisible : false,
				border : false,
				store : createTreeStore(),
				listeners : {
					'itemclick' : function(view, record, item, index, e) {
						var item = record.raw
						currentRoleId = item.id
						TreeGrid.store.reload({
									callback : function() {
										setChecked()
									}
								})

					},
					scope : this
				}
			});
}

Ext.define('Menu', {
			extend : 'Ext.data.Model',
			fields : [{
						name : 'id',
						type : 'int'
					}, {
						name : 'text',
						type : 'string'
					}, {
						name : 'iconCls',
						type : 'string'
					}, {
						name : 'leaf',
						type : 'boolean'
					}, {
						name : 'checked',
						type : 'boolean'
					}, {
						name : 'cancheck',
						type : 'string'
					}]
		});

var store = Ext.create('Ext.data.TreeStore', {
			defaultRootId : -1,
			model : 'Menu',
			proxy : {
				type : 'ajax',
				extraParams : {
					action : 'treenode'
				},
				url : PATH + "system/menu!list.do",
				reader : {
					type : 'json',
					root : 'res'
				}
			},
			root : {
				expanded : true
			},
			nodeParam : "id"
		});
function setChecked() {
	Ext.getBody().mask('正在加载权限,请稍等...')
	Ext.Ajax.request({
				url : PATH + 'system/menu!listPermissionsByRole.do',
				params : {
					roleid : currentRoleId
				},
				method : 'GET',
				success : function(response, options) {
					var perms = Ext.decode(response.responseText).res
					var menus = TreeGrid.getView().getStore().data.items
					Ext.Array.each(menus, function(menu, index, menus) {
								var checked = false
								var editable
								var department
								Ext.Array.each(perms, function(perm, index, perms) {
											if (menu.data.id == perm.menuid) {
												checked = true
												editable = perm.editable
												department = perm.department
											}

										})
								var record = TreeGrid.store.getById(menu.data.id)
								if (record) {
									record.set('checked', checked)
									record.set('editable', editable == 'true' ? true : false)
									record.set('department', department == 'true' ? true : false)
								}
							})
					Ext.getBody().unmask()
				},
				failure : function(response, options) {
					Ext.MessageBox.alert('失败', '请求超时或网络故障,错误编号：' + response.status);
				}
			})
}
function getChecked() {
	role_menu_array = []
	var records = TreeGrid.getChecked();
	Ext.Array.forEach(records, function(record, index, records) { // 单纯的遍历数组
				item = record.data
				role_menu = {}
				if (item.id != -1) {
					role_menu.menuid = item.id
					role_menu.roleid = currentRoleId
					if (item.editable) {
						role_menu.editable = item.editable
					} else {
						role_menu.editable = false
					}
					if (item.department) {
						role_menu.department = item.department
					} else {
						role_menu.department = false
					}
					role_menu_array.push(role_menu)
				}
			});
	jsonToSave = JSON.stringify(role_menu_array)
	return true
}
var TreeGrid = Ext.create('Ext.tree.Panel', {
			title : '菜单列表',
			region : 'center',
			border : false,
			iconCls : 'contract',
			collapsible : true,
			useArrows : true,
			rootVisible : false,
			store : store,
			multiSelect : true,
			tbar : [{
						text : '保存',
						iconCls : 'save',
						handler : function() {
							if (currentRoleId) {
								if (getChecked()) {
									Ext.Ajax.request({
												url : PATH + 'system/menu!savePerms.do',
												params : {
													roleid : currentRoleId,
													'jsonToSave' : jsonToSave
												},
												method : 'GET',
												success : function(response, options) {
													Ext.Msg.alert('操作成功', '授权信息保存成功！')
												},
												failure : function(response, options) {
													Ext.MessageBox.alert('失败', '请求超时或网络故障,错误编号：' + response.status);
												}
											})
								}
							} else {
								Ext.Msg.alert('添加操作', '您必须选择一个角色以便为该角色授权！');
							}
						}
					}, '-', '请首先在角色列表选择将要授权的角色！'],
			viewConfig : checkCascadViewConfig,
			columns : [{
						xtype : 'treecolumn', // this is so we know which
						text : '菜单',
						width : 260,
						sortable : true,
						dataIndex : 'text'
					}, {
						xtype : 'checkcolumn',
						header : '编辑',
						dataIndex : 'editable',
						width : 40,
						listeners : {
							'checkchange' : function(record, index, checked, opt) {
							}
						},
						stopSelection : false
					}
//					, {
//						xtype : 'checkcolumn',
//						header : '跨学院',
//						dataIndex : 'department',
//						width : 60,
//						renderer : function(val, m, rec) {
//							if (rec.get('cancheck') == 'false')
//								return '';
//							else {
//								return (new Ext.grid.column.CheckColumn).renderer(val);
//							}
//						},
//						listeners : {
//							'checkchange' : function(record, index, checked, opt) {
//							}
//						},
//						stopSelection : false
//					}
					]
		});
Ext.onReady(function() {
			Ext.QuickTips.init();
			createApp('角色分配', 'border', [{
								region : 'west',
								width : 210,
								items : buildTree()
							}, TreeGrid])// createGrid(_config.userGrid)])
		});