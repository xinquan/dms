var _config
var roleWin
var roleForm = new Ext.FormPanel({
			labelWidth : 75,
			border : false,
			padding : '5 20 5 5',
			waitMsgTarget : true,
			defaults : {
				labelAlign : 'right'
			},
			defaultType : 'textfield',
			items : [{
						id : 'id',
						name : 'department.id',
						hidden : true,
						hideLabel : true,
						allowBlank : true
					}, {
						fieldLabel : '部门名',
						afterLabelTextTpl : required,
						allowBlank : false,
						name : 'department.deptname',
						id : 'rolename',
						anchor : '100%'
					}, {
						fieldLabel : '职能说明',
						name : 'department.mark',
						id : 'explan',
						anchor : '100%'
					}, {
						fieldLabel : '排序',
						xtype : 'numberfield',
						name : 'department.dindex',
						anchor : '100%'
					}]
		});
var editRole = function(opt) {
	if (opt == 'edit') {
		var sel = Ext.getCmp('roleGrid').getSelectionModel().getSelection()
		if (sel.length == 1) {
			Ext.Ajax.request({
						url : PATH + 'system/menu!findDepartment.do',
						params : {
							id : sel[0].data.id
						},
						callback : function(opt, success, response) {
							var role = makeNewObjAddPrefix(Ext.decode(response.responseText).result, 'department.')
							roleForm.form.setValues(role)
						}
					})
			if (!roleWin) {
				roleWin = new Ext.Window({
							width : 430,
							height : 180,
							closable : true,
							autoScroll : true,
							title : '编辑部门',
							layout : 'fit',
							modal : true,
							border : false,
							buttonAlign : 'center',
							items : roleForm,
							buttons : [{
										text : '保存',
										scale : 'medium',
										disabled : false,
										handler : addBtnsHandler
									}, {
										text : '取消',
										scale : 'medium',
										handler : function() {
											roleForm.form.reset();// 清空表单
											roleWin.hide();
										}
									}]
						});
			} else {
				roleWin.setTitle('编辑部门')
			}
			roleWin.show();// 显示此窗口
		} else {
			Ext.Msg.alert('提示', '请选择需要修改的部门。');
		}
	} else {
		if (!roleWin) {
			roleWin = new Ext.Window({
						width : 430,
						height : 180,
						closable : true,
						autoScroll : true,
						title : '新增部门',
						layout : 'fit',
						modal : true,
						border : false,
						buttonAlign : 'center',
						items : roleForm,
						buttons : [{
									text : '保存',
									scale : 'medium',
									disabled : false,
									handler : addBtnsHandler
								}, {
									text : '取消',
									scale : 'medium',
									handler : function() {
										roleForm.form.reset();// 清空表单
										roleWin.hide();
									}
								}]
					});
		} else {
			roleWin.setTitle('新增部门')
		}
		roleWin.show();// 显示此窗口
	}

}

function reloadGrid() {
	Ext.getCmp('roleGrid').getStore().reload();
}
function addBtnsHandler() {
	if (roleForm.form.isValid()) {
		roleForm.form.submit({
					url : PATH + 'system/menu!editDepartment.do',
					waitMsg : '正在保存数据，稍后...',
					success : function(form, action) {
						Ext.Msg.alert('保存成功', '操作成功！');
						roleForm.form.reset();// 清空表单
						Ext.getCmp('roleGrid').getStore().reload();
						roleWin.hide();
					},
					failure : function(form, response) {
						var errors = Ext.decode(response.response.responseText).res
						var errorStr = '<font color="red">'
						for (var i = 1; i < errors.length; i++) {
							errorStr += errors[i].defaultMessage + '<br>'
						}
						errorStr += '</font>'
						Ext.Msg.alert('保存失败', errorStr)
					}
				});
	} else {
		Ext.Msg.alert('信息', '请填写完成再提交!');
	}
}
function fillGrid() {
	_config = {
		roleGrid : {
			id : 'roleGrid',
			fields : ['deptname', 'mark', 'rindex', 'id'],
			headers : ['部门', '说明'],
			widths : [300, 500],
			url : PATH + 'system/menu!departmentList.do',
			autoLoad : true,
			border : false,
			tbar : [{
						iconCls : 'add',
						text : '新增',
						handler : function() {
							editRole('add')
						}
					}, {
						iconCls : 'edit',
						text : '编辑',
						handler : function() {
							editRole('edit')
						}
					}, {
						iconCls : 'delete',
						text : '删除',
						handler : function() {
							var records = Ext.getCmp('roleGrid').getSelectionModel().getSelection();
							if (records) {
								delRecords(records, 'Department', true)
							} else {
								Ext.Msg.alert('删除操作', '您必须选择一行数据以便删除！');
							}
						}
					}]
		}
	}
}

Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('部门管理', 'fit', [createGrid(_config.roleGrid)])
		});