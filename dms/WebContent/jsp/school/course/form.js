var newFormWin
var step
var platform
Ext.define('major', {
			extend : 'Ext.data.Model',
			fields : [{
						name : 'id',
						type : 'string'
					}, {
						name : 'majorId',
						type : 'string'
					}, {
						name : 'majorName',
						type : 'string'
					}, {
						name : 'majorYear',
						type : 'string'
					}, {
						name : 'category',
						type : 'string'
					}, {
						name : 'degree',
						type : 'string'
					}]
		});
var majords = Ext.create('Ext.data.Store', {
			model : 'major',
			proxy : {
				type : 'ajax',
				url : PATH + "school/school!majorList.do",
				actionMethods : {
					read : 'POST'
				},
				// extraParams : {
				// deptId : _deptid
				// },
				reader : {
					type : 'json',
					root : 'res'
				}
			},
			autoLoad : true
		});
Ext.define('depart', {
			extend : 'Ext.data.Model',
			fields : [{
						name : 'id',
						type : 'string'
					}, {
						name : 'deptid',
						type : 'string'
					}, {
						name : 'deptname',
						type : 'string'
					}]
		});
var departs1 = Ext.create('Ext.data.Store', {
			model : 'depart',
			proxy : {
				type : 'ajax',
				url : PATH + "school/school!departList.do",
				actionMethods : {
					read : 'POST'
				},
				reader : {
					type : 'json',
					root : 'res'
				}
			},
			autoLoad : true
		});
var departs2 = Ext.create('Ext.data.Store', {
			model : 'depart',
			proxy : {
				type : 'ajax',
				url : PATH + "school/school!departList.do",
				actionMethods : {
					read : 'POST'
				},
				reader : {
					type : 'json',
					root : 'res'
				}
			},
			autoLoad : true
		});
var planCourseForm = new Ext.FormPanel({
			border : false,
			waitMsgTarget : true,
			layout : {
				type : 'table',
				columns : 2
			},
			defaults : {
				width : 280,
				labelWidth : 110,
				xtype : 'textfield',
				labelAlign : 'right',
				anchor : "100%"
			},
			items : [{
						fieldLabel : '课程代码',
						id : 'kcdm',
						name : 'course.kcdm',
						emptyText : '审批通过后由教务处确定'
					}, {
						fieldLabel : '课程名称',
						name : 'course.kcmc',
						afterLabelTextTpl : required,
						allowBlank : false
					}, {
						xtype : 'combo',
						fieldLabel : '课程归属学院',
						store : departs1,
						mode : 'remote',
						afterLabelTextTpl : required,
						allowBlank : false,
						queryMode : 'local',
						forceSelection : true,
						valueField : 'deptname',
						displayField : 'deptname',
						name : 'course.department',
						value : _deptname,
						id : 'deptname',
						triggerAction : 'all'
					}, {
						xtype : 'combo',
						fieldLabel : '课程归属系',
						store : departs2,
						mode : 'remote',
						queryMode : 'local',
						forceSelection : true,
						valueField : 'deptname',
						displayField : 'deptname',
						name : 'course.kcssx',
						id : 'ssxname',
						triggerAction : 'all'
					}, {
						xtype : 'combo',
						fieldLabel : '面向专业',
						store : majords,
						mode : 'remote',
						queryMode : 'local',
						forceSelection : true,
						valueField : 'majorName',
						displayField : 'majorName',
						name : 'course.major',
						id : 'majorName',
						triggerAction : 'all'
					}, {
						fieldLabel : '课程性质',
						xtype : 'combo',
						store : getDictStore(true, 'KCXZ'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : true,
						name : 'course.kcxz',
						id : 'kcxz',
						triggerAction : 'all'
					}, {
						fieldLabel : '学分',
						xtype : 'numberfield',
						afterLabelTextTpl : required,
						allowBlank : false,
						name : 'course.xf'
					}, {
						fieldLabel : '(学分)',
						xtype : 'numberfield',
						name : 'course.qtxf'
					}, {
						fieldLabel : '【学分】',
						xtype : 'numberfield',
						name : 'course.zkhxf'
					}, {
						id : 'zxs',
						fieldLabel : '总学时<br>(1周=16学时)',
						name : 'course.zxs',
						afterLabelTextTpl : required,
						allowBlank : false,
						xtype : 'numberfield'
					}, {
						fieldLabel : '理论学时',
						name : 'course.llxs',
						afterLabelTextTpl : required,
						allowBlank : false,
						xtype : 'numberfield'
					}, {
						fieldLabel : '集中性实践环节',
						xtype : 'combo',
						store : getDictStore(true, 'SJHJ'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : true,
						name : 'course.sjhj',
						id : 'sjhj',
						triggerAction : 'all',
						listeners : {
							select : {
								scope : this,
								fn : function(combo, record, index) {
									var value = combo.getValue()
									Ext.getCmp('syhjbj').setValue(value)
								}
							}
						}
					}, {
						fieldLabel : '实践环节标记',
						xtype : 'combo',
						store : getDictStore(true, 'SJHJ'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : true,
						name : 'course.syhjbj',
						id : 'syhjbj',
						triggerAction : 'all'
					}, {
						fieldLabel : '公共基础必修课程',
						xtype : 'combo',
						store : getDictStore(true, 'SF'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : false,
						name : 'course.ggjcbj',
						id : 'ggjcbj',
						triggerAction : 'all'
					}, {
						colspan : 2,
						xtype : 'numberfield',
						name : 'course.kknf',
						fieldLabel : '开课年份',
						value : parseInt(new Date().getFullYear().toString()),
						anchor : '100%',
						afterLabelTextTpl : required,
						allowBlank : false
					}, {
						hidden : true,
						fieldLabel : '状态',
						name : 'course.state'
					}, {
						id : 'id',
						name : 'course.id',
						hidden : true
					}]
		});
var editFormWin = function(opt) {
	if (opt == 'edit') {
		var sel = Ext.getCmp('courseGrid').getSelectionModel().getSelection()
		if (sel.length == 1) {
			if (sel[0].data.state == 0 || sel[0].data.state == -1 || (_uid == 1 && _username == 'admin')) {
				Ext.Ajax.request({
							url : PATH + 'plan/plan!findCourse.do',
							params : {
								currentId : sel[0].data.id
							},
							callback : function(opt, success, response) {
								var result = Ext.decode(response.responseText).result;
								var course = makeNewObjAddPrefix(result, 'course.')
								planCourseForm.form.setValues(course)
							}
						})
				if (!newFormWin) {
					newFormWin = new Ext.Window({
								width : 580,
								height : 330,
								autoScroll : true,
								closable : false,
								title : '修改课程',
								modal : true,
								layout : 'fit',
								border : false,
								items : planCourseForm,
								buttons : [{
											text : '保存',
											scale : 'medium',
											disabled : false,
											handler : addBtnsHandler
										}, {
											text : '取消',
											scale : 'medium',
											handler : function() {
												planCourseForm.form.reset();// 清空表单
												newFormWin.hide();
											}
										}]
							});
				} else {
					newFormWin.setTitle('修改课程')
				}
				newFormWin.show('planCourseForm');// 显示此窗口

			} else {
				Ext.Msg.alert('提示', '该状态无法编辑')
			}
		} else {
			Ext.Msg.alert('提示', '请选择要修改的课程。');
		}
	} else if (opt == 'add') {
		if (!newFormWin) {
			newFormWin = new Ext.Window({
						width : 580,
						height : 330,
						closable : false,
						autoScroll : true,
						title : '新增课程',
						modal : true,
						border : false,
						layout : 'fit',
						items : planCourseForm,
						buttonAlign : 'center',
						buttons : [{
									text : '保存',
									scale : 'medium',
									disabled : false,
									handler : addBtnsHandler
								}, {
									text : '取消',
									scale : 'medium',
									handler : function() {
										planCourseForm.form.reset();// 清空表单
										newFormWin.hide();
									}
								}]
					});
		} else {
			newFormWin.setTitle('新增课程')
		}
		newFormWin.show('planCourseForm');// 显示此窗口
	}

}

function addBtnsHandler() {
	if (planCourseForm.form.isValid()) {
		planCourseForm.form.submit({
					url : PATH + 'plan/plan!editCourse.do',
					params : {
						'course.newadd' : 'newadd'
					},
					submitEmptyText : false,
					waitMsg : '正在保存数据，稍后...',
					success : function(form, action) {
						Ext.Msg.alert('保存成功', '操作成功！');
						planCourseForm.form.reset();// 清空表单
						Ext.getCmp('courseGrid').getStore().reload();
						newFormWin.hide();
					},
					failure : function(form, response) {
						var errors = Ext.decode(response.response.responseText).res
						var errorStr = '<font color="red">'
						for (var i = 1; i < errors.length; i++) {
							errorStr += errors[i].defaultMessage + '<br>'
						}
						errorStr += '</font>'
						Ext.Msg.alert('保存失败', errorStr)
					}
				});
	} else {
		Ext.Msg.alert('信息', '请填写完成再提交!');
	}
}

function downloadWord(selid) {
	// window.location.href = 'down_plan?currentId='
	// + sel[0].data.id
	if (!Ext.fly('downForm')) {
		var downForm = document.createElement('form');
		downForm.id = 'downForm';
		downForm.name = 'downForm';
		downForm.className = 'x-hidden';
		downForm.action = PATH + 'plan/plan!exportCourseToWord.do';
		downForm.method = 'post';
		// downForm .target = '_blank'; //打开新的下载页面
		var data = document.createElement('input');
		data.type = 'hidden';// 隐藏域
		data.name = 'currentId';// form表单参数
		data.value = selid;// form表单值
		downForm.appendChild(data);
		document.body.appendChild(downForm);
	}
	Ext.fly('downForm').dom.submit();
	if (Ext.fly('downForm')) {
		document.body.removeChild(downForm);
	}
}

function downloadExcel() {
	if (!Ext.fly('downForm')) {
		var downForm = document.createElement('form');
		downForm.id = 'downForm';
		downForm.name = 'downForm';
		downForm.className = 'x-hidden';
		downForm.action = PATH + 'plan/plan!exportCourseToExcel.do';
		downForm.method = 'post';
		// downForm .target = '_blank'; //打开新的下载页面
		var data = document.createElement('input');
		data.type = 'hidden';// 隐藏域
		downForm.appendChild(data);
		document.body.appendChild(downForm);
	}
	Ext.fly('downForm').dom.submit();
	if (Ext.fly('downForm')) {
		document.body.removeChild(downForm);
	}
}