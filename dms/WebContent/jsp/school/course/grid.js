var _config
var passWin
var passForm = new Ext.FormPanel({
			border : false,
			waitMsgTarget : true,
			items : [{
						fieldLabel : '是否通过',
						xtype : 'combo',
						padding : '10 5 5 5',
						store : getDictStore(true, 'SF'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : false,
						id : 'passed',
						triggerAction : 'all'
					}]
		});
var fillPass = function() {
	var courses = Ext.getCmp('courseGrid').getSelectionModel().getSelection();
	if (!passWin) {
		passWin = new Ext.Window({
					width : 300,
					height : 150,
					closable : false,
					autoScroll : true,
					title : '课程是否通过',
					modal : true,
					border : false,
					layout : 'fit',
					items : passForm,
					buttonAlign : 'center',
					buttons : [{
								text : '保存',
								scale : 'medium',
								disabled : false,
								handler : function() {
									var sel = Ext.getCmp('courseGrid').getSelectionModel().getSelection();
									var courseIds = []
									for (var i = 0; i < sel.length; i++) {
										courseIds.push(sel[i].data.id);
									}
									Ext.Ajax.request({
												url : PATH + 'plan/plan!passCourses.do',
												params : {
													passed : Ext.getCmp('passed').getValue(),
													courseIds : courseIds
												},
												success : function(response) {
													msgShow('提示', '标记成功')
													reloadGrid()
												},
												failure : function() {
													Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
												}
											});
									passWin.hide();
								}
							}, {
								text : '取消',
								scale : 'medium',
								handler : function() {
									passForm.form.reset();// 清空表单
									passWin.hide();
								}
							}]
				});
	}
	passWin.show();// 显示此窗口
}
function fillGrid() {
	_config = {
		courseGrid : {
			id : 'courseGrid',
			fields : ['kcdm', 'kcmc', 'state', 'passed', 'xf', 'qtxf', 'zkhxf', 'zxs', 'llxs', 'syxs', 'sjhj', 'department', 'kcssx', 'major', 'kcxz', 'newadd', 'id', 'llxs', 'zkhxf', 'syhjbj',
					'kknf'],
			headers : ['课程代码', '课程名称', '状态', '是否<br>通过', '学分', '(学分)', '【学分】', '总学时', '理论学时', '实验（实践）<br>学时', '集中性<br>实践环节', '开课部门', '开课系', '面向专业', '课程性质'],
			widths : [90, 160, 60, 50, 50, 50, 60, 60, 80, 80, 80, 150, 150, 100, 60],
			renderers : [tip, tip, function(v) {
						if (v == 0) {
							return '起草'
						} else if (v == 1) {
							return '提交'
						} else if (v == 2) {
							return '已入库'
						} else if (v == -1) {
							return '退回'
						} else if (v == 3) {
							return '退回提交'
						} else {
							return '已入库'
						}
					}, tip, parse0, parse0, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								return t.record.data.zxs / 16 + '周'
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, parse0, function(v, t) {
						if (t.record.data.llxs == 0) {
							if (t.record.data.sjhj == '√') {
								if (t.record.data.syxs == 0) {
									return ''
								} else {
									return t.record.data.syxs / 16 + '周'
								}
							} else {
								return parse0(v)
							}
						} else {
							return parse0(v)
						}
					}, tip, tip, tip, tip, tip],
			aligns : [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
			filters : [true, true, false, false, false, false, false, false, false, false, false, true, false, false, false],
			url : PATH + 'plan/plan!courseListAll.do',
			filtersurl : [PATH + 'plan/plan!courseListAll.do', PATH + 'plan/plan!courseListAll.do'],
			features : [{
						ftype : 'filters',
						local : true
					}],
			split : true,
			autoLoad : true,
			hasPage : true,
			viewConfig : {
				forceFit : true,
				getRowClass : function(record, rowIndex, rowParams, store) {
					var type = record.get('newadd');
					if ('newadd' == type) {
						return 'x-grid-row-gray';
					}
				}
			},
			tbar : [{
						id : 'addBtn',
						iconCls : 'add',
						text : ' 新增课程',
						tooltip : '新增课程',
						handler : function() {
							editFormWin('add')
						}
					}, {
						id : 'editBtn',
						iconCls : 'edit',
						text : '修改课程',
						tooltip : '修改课程',
						handler : function() {
							editFormWin('edit')
						}
					}, {
						id : 'delBtn',
						iconCls : 'delete',
						text : '删除',
						tooltip : '删除选中的记录',
						handler : function() {
							var _records = Ext.getCmp('courseGrid').getSelectionModel().getSelection();
							delRecords(_records, 'MainCourse', true)
						}
					}, {
						id : 'submitBtn',
						iconCls : 'submit',
						text : '提交审批',
						tooltip : '提交审批',
						handler : function() {
							var sel = Ext.getCmp('courseGrid').getSelectionModel().getSelection()
							startWorkFlow(sel, 'KC', 'PlanCourseModel', true)
						}
					}, {
						id : 'seeBtn',
						iconCls : 'flow',
						text : '查看审批信息',
						tooltip : '查看审批信息',
						handler : function() {
							var sel = Ext.getCmp('courseGrid').getSelectionModel().getSelection()
							showWorkFlow(sel)
						}
					}, {
						id : 'passedBtn',
						iconCls : 'reset',
						text : '是否通过',
						tooltip : '课程是否通过并入课程库',
						handler : function() {
							fillPass()
						}
					}, {
						id : 'delUnpassedBtn',
						iconCls : 'delete',
						text : '删除未通过的课程',
						tooltip : '删除未通过的课程',
						handler : function() {
							Ext.Msg.confirm('提示', '确认删除？', function(btn) {
										if (btn == 'yes') {
											Ext.Ajax.request({
														url : PATH + 'plan/plan!delUnpassedCourses.do',
														success : function(response) {
															msgShow('提示', '删除成功！')
														},
														failure : function() {
															msgShow('错误', '服务器出现错误请稍后再试！');
														}
													});
										}
									})
						}
					}, {
						id : 'exportWBtn',
						iconCls : 'word',
						text : '导出审批表',
						tooltip : '导出审批表',
						handler : function() {
							var sel = Ext.getCmp('courseGrid').getSelectionModel().getSelection()
							if (sel.length == 1) {
								downloadWord(sel[0].data.id)
							} else {
								Ext.Msg.alert("条件缺失", "请选择一门课程！");
							}
						}
					}, {
						id : 'importBtn',
						iconCls : 'excel',
						text : '课程导入',
						tooltip : '课程导入',
						handler : function() {
							uploadExcel()
						}
					}, {
						id : 'exportEBtn',
						iconCls : 'excel',
						text : '导出新增课程',
						tooltip : '导出所有新增课程',
						handler : function() {
							downloadExcel()
						}
					}]
		}
	}
}
function autoSelFirst() {
	var model = Ext.getCmp('courseGrid').getSelectionModel()
	model.select(0)
}
function reloadGrid() {
	Ext.getCmp('courseGrid').getStore().reload()
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('课程设置', 'fit', [createGrid(_config.courseGrid)])
			Ext.Ajax.request({
						url : PATH + 'system/menu!getRights.do',
						params : {
							action : this.location.pathname,
							roleids : _roleids
						},
						success : function(response) {
							perm = Ext.decode(response.responseText)
							if (!perm.edit) {
								Ext.getCmp('addBtn').setVisible(false)
								Ext.getCmp('editBtn').setVisible(false)
								Ext.getCmp('delBtn').setVisible(false)
								Ext.getCmp('seeBtn').setVisible(false)
								Ext.getCmp('submitBtn').setVisible(false)
								Ext.getCmp('exportWBtn').setVisible(false)
								Ext.getCmp('exportEBtn').setVisible(false)
								Ext.getCmp('importBtn').setVisible(false)
								Ext.getCmp('passedBtn').setVisible(false)
							}
							if (perm.department) {
								reloadGrid(perm.department)
							} else {
								reloadGrid()
								autoSelFirst()
							}
						},
						failure : function() {
							Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
						}
					});
		});