var _config
var delIds = []
var uploadWin
var department
function fillGrid() {
	_config = {
		majorGrid : {
			id : 'majorGrid',
			fields : ['deptname', 'majorname', 'majorid', 'majoryear', 'category', 'degree', 'type', 'bigmajor', 'createdate', 'mindex', 'id'],
			headers : ['学院', '专业名称', '专业代码', '标准学制', '门类', '授予学位', '是否大类', '所属大类', '开设年份'],
			widths : [100, 180, 100, 60, 80, 80, 80, 100, 60],
			renderers : [tip, tip, tip, tip, tip, tip, tip, tip, tip],
			aligns : [null, null, 'center', null, null, null, null, null, null],
			url : PATH + 'school/school!deptMajorList.do',
			split : true,
			params : {
				'overdepartment' : department,
				deptId : _deptid
			},
			viewConfig : {
				forceFit : true,
				getRowClass : function(record, rowIndex, rowParams, store) {
					var type = record.get('type');
					if ('是' == type) {
						return 'x-grid-row-blue';
					}
				}
			},
			autoLoad : true,
			hasPage : true,
			tbar : [{
						id : 'addBtn',
						iconCls : 'add',
						text : '添加专业',
						tooltip : '添加专业',
						handler : function() {
							editFormWin('add')
						}
					}, {
						id : 'editBtn',
						iconCls : 'edit',
						text : '修改专业',
						tooltip : '修改专业',
						handler : function() {
							editFormWin('edit')
						}
					},
					// {
					// id : 'importBtn',
					// iconCls : 'excel',
					// text : '专业导入',
					// tooltip : '专业导入',
					// handler : function() {
					// uploadMajorToExcel()
					// }
					// },
					{
						id : 'delBtn',
						iconCls : 'delete',
						text : '删除',
						tooltip : '删除选中的记录',
						handler : function() {
							var _records = Ext.getCmp('majorGrid').getSelectionModel().getSelection();
							if (_records) {
								// 提示是否删除数据
								Ext.Msg.confirm("是否要删除？", "是否要删除这些被选择的数据？", function(btn) {
											if (btn == "yes") {
												delIds = []
												for (var i = 0; i < _records.length; i++) {
													delIds.push(_records[i].data.id);
												}
												Ext.Ajax.request({
															url : PATH + 'school/school!delMajor.do',
															params : {
																'delIds' : delIds
															},
															success : function() {
																Ext.Msg.alert("删除信息成功", "您已经成功删除信息！");
																Ext.getCmp('majorGrid').getStore().reload();
															},
															failure : function() {
																Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
															}
														});
											}
										});
							} else {
								Ext.Msg.alert('删除操作', '您必须选择一行数据以便删除！');
							}
						}
					}, '-', '提示：请先维护大类']
		}
	}
}
function autoSelFirst() {
	var model = Ext.getCmp('majorGrid').getSelectionModel()
	model.select(0)
}
function reloadGrid() {
	Ext.getCmp('majorGrid').getStore().load()
}
function getPerm() {
	Ext.Ajax.request({
				url : PATH + 'system/menu!getRights.do',
				params : {
					action : this.location.pathname,
					roleids : _roleids
				},
				success : function(response) {
					perm = Ext.decode(response.responseText)
					department = perm.department
					fillGrid()
					createApp('专业设置', 'fit', [createGrid(_config.majorGrid)])
					if (!perm.edit) {
						Ext.getCmp('addBtn').setVisible(false)
						Ext.getCmp('editBtn').setVisible(false)
						Ext.getCmp('delBtn').setVisible(false)
					}
				},
				failure : function() {
					Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
				}
			});
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			getPerm()
		});
var uploadMajorForm = Ext.create('Ext.form.Panel', {
			bodyPadding : 10,
			border : false,
			items : [{
						xtype : 'filefield',
						id : 'majorfile',
						name : 'file', // 服务端获取 “名称”
						afterLabelTextTpl : '<span style="color:red;font-weight:bold" data-qtip="必需填写">*</span>',
						fieldLabel : '文件',
						labelWidth : 50,
						msgTarget : 'side', // 提示 文字的位置
						allowBlank : false,
						anchor : '100%',
						buttonText : '选择文件'
					}],

			buttons : [{
						text : '下载模版',
						handler : function() {
							window.location.href = PATH + "system/common!downloadMajorTemplateFile.do"
						}
					}, {
						text : '导入',
						handler : function() {
							var form = this.up('form').getForm();
							if (form.isValid()) { // form 验证
								var excel_reg = /\.([xX][lL][sS]){1}$|\.([xX][lL][sS][xX]){1}$/;
								if (!excel_reg.test(Ext.getCmp('majorfile').getValue())) {
									msgShow('提示信息', '文件类型错误,请选择Excel文件(xls/xlsx)', Ext.Msg.WARNING);
								} else {
									// 取控件DOM对象
									var field = document.getElementById('majorfile');
									// 取控件中的input元素
									var inputs = field.getElementsByTagName('input');
									var fileInput = null;
									var il = inputs.length;
									// 取出input 类型为file的元素
									for (var i = 0; i < il; i++) {
										if (inputs[i].type == 'file') {
											fileInput = inputs[i];
											break;
										}
									}
									if (fileInput != null) {
										var fileSize = getFileSize(fileInput);
										// 允许上传不大于1M的文件
										if (fileSize > 10485760) {
											msgShow('提示信息', '文件太大，请选择小于10M的文件！', Ext.Msg.WARNING);
										} else {
											form.submit({ // 提交
												url : PATH + 'school/school!uploadFile.do',
												waitMsg : '正在上传和解析，请稍后....',
												success : function(fp, o) {
													msgShow('上传并解析成功', '您的文件 "' + o.result.fileName + '"成功上传并解析，已导入数据库....');
													reloadGrid()
												}
											});
										}
									}
								}

							}
						}
					}, {
						text : '取消',
						handler : function() {
							uploadWin.hide()
						}
					}]
		});
function uploadMajorToExcel() {
	if (!uploadWin) {
		uploadWin = new Ext.Window({
					width : 460,
					height : 120,
					layout : 'fit',
					closable : false,
					autoScroll : true,
					title : '从Excel导入专业信息。<font color="yellow">请先下载模版，根据模版整理数据。</font>',
					modal : true,
					border : false,
					items : uploadMajorForm
				}).show();
	} else {
		uploadWin.show()
	}
}