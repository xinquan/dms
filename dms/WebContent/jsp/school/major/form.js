var newFormWin
var step
var platform
Ext.define('dlS', {
			extend : 'Ext.data.Model',
			fields : [{
						name : 'majorName',
						type : 'string'
					}, {
						name : 'majorId',
						type : 'string'
					}]
		});
var dlS = Ext.create('Ext.data.Store', {
			model : 'dlS',
			proxy : {
				type : 'ajax',
				url : PATH + 'school/school!bigMajorList.do',
				actionMethods : {
					read : 'POST'
				},
				extraParams : {
					'deptId' : _deptid,
					'type' : '大类'
				},
				reader : {
					type : 'json',
					root : 'res'
				}
			},
			autoLoad : true
		});
Ext.define('dptS', {
			extend : 'Ext.data.Model',
			fields : [{
						name : 'id',
						type : 'int'
					}, {
						name : 'deptname',
						type : 'string'
					}]
		});
var dptS = Ext.create('Ext.data.Store', {
			model : 'dptS',
			proxy : {
				type : 'ajax',
				url : PATH + 'school/school!departList.do',
				actionMethods : {
					read : 'POST'
				},
				reader : {
					type : 'json',
					root : 'res'
				}
			},
			autoLoad : true
		});
var majorForm = new Ext.FormPanel({
			labelWidth : 75,
			width : 400,
			border : false,
			padding : '5 20 5 5',
			waitMsgTarget : true,
			defaults : {
				labelAlign : 'right'
			},
			defaultType : 'textfield',
			items : [{
						id : 'id',
						name : 'major.id',
						hidden : true,
						hideLabel : true,
						allowBlank : true
					}, {
						fieldLabel : '所属学院',
						name : 'major.deptId',
						xtype : 'combo',
						store : dptS,
						mode : 'remote',
						valueField : 'id',
						displayField : 'deptname',
						editable : false,
						name : 'major.deptId',
						id : 'deptId',
						triggerAction : 'all',
						anchor : '100%',
						listeners : {
							select : {}
						}
					}, {
						xtype : 'numberfield',
						fieldLabel : '开设年份',
						name : 'major.createdate',
						value : parseInt(new Date().getFullYear().toString()),
						anchor : '100%',
						allowBlank : false
					}, {
						fieldLabel : '专业名称',
						name : 'major.majorName',
						anchor : '100%',
						allowBlank : false
					}, {
						fieldLabel : '专业代码',
						name : 'major.majorId',
						anchor : '100%'
					}, {
						fieldLabel : '标准学制',
						xtype : 'combo',
						store : getDictStore(true, 'BZXZ'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : false,
						name : 'major.majorYear',
						id : 'bzxz',
						triggerAction : 'all',
						anchor : '100%',
						listeners : {}
					}, {
						fieldLabel : '门类',
						xtype : 'combo',
						store : getDictStore(true, 'ML'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : false,
						name : 'major.category',
						id : 'ml',
						triggerAction : 'all',
						anchor : '100%',
						listeners : {
							select : {}
						}
					}, {
						fieldLabel : '授予学位',
						xtype : 'combo',
						store : getDictStore(true, 'SYXW'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : false,
						name : 'major.degree',
						id : 'syxw',
						triggerAction : 'all',
						anchor : '100%',
						listeners : {
							select : {}
						}
					}, {
						fieldLabel : '是否大类',
						xtype : 'combo',
						allowBlank : false,
						store : getDictStore(true, 'SF'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : false,
						name : 'major.type',
						id : 'sfdl',
						triggerAction : 'all',
						anchor : '100%',
						listeners : {
							select : {
								scope : this,
								fn : function(combo, record, index) {
									var value = combo.getValue()
									var rec = combo.findRecord(combo.valueField || combo.displayField, value);
									var recv = rec.data
									if (value == '是') {
										Ext.getCmp('ssdl').clearValue()
										Ext.getCmp('ssdl').setReadOnly(true)
									} else {
										Ext.getCmp('ssdl').setReadOnly(false)
									}
								}
							}
						}
					}, {
						fieldLabel : '所属大类',
						name : 'major.bigmajor',
						xtype : 'combo',
						store : dlS,
						mode : 'remote',
						valueField : 'majorName',
						displayField : 'majorName',
						editable : false,
						name : 'major.bigmajor',
						id : 'ssdl',
						triggerAction : 'all',
						anchor : '100%',
						listeners : {
							select : {}
						}
					}, {
						fieldLabel : '排序',
						xtype : 'numberfield',
						name : 'major.mindex',
						anchor : '100%'
					}]
		});
var editFormWin = function(opt) {
	if (opt == 'edit') {
		var sel = Ext.getCmp('majorGrid').getSelectionModel().getSelection()
		if (sel.length == 1) {
			Ext.Ajax.request({
						url : PATH + 'school/school!findMajor.do',
						params : {
							currentId : sel[0].data.id
						},
						callback : function(opt, success, response) {
							var course = makeNewObjAddPrefix(Ext.decode(response.responseText).result, 'major.')
							majorForm.form.setValues(course)
						}
					})
			if (!newFormWin) {
				newFormWin = new Ext.Window({
							width : 410,
							height : 380,
							autoScroll : true,
							closable : false,
							title : '修改专业',
							layout : 'fit',
							modal : true,
							border : false,
							items : majorForm,
							buttons : [{
										text : '保存',
										scale : 'medium',
										disabled : false,
										handler : addBtnsHandler
									}, {
										text : '取消',
										scale : 'medium',
										handler : function() {
											majorForm.form.reset();// 清空表单
											newFormWin.hide();
										}
									}]
						});
			} else {
				newFormWin.setTitle('修改专业')
			}
			newFormWin.show('majorForm');// 显示此窗口
		} else {
			Ext.Msg.alert('提示', '请选择要修改的专业。');
		}
	} else if (opt == 'add') {
		if (!newFormWin) {
			newFormWin = new Ext.Window({
						width : 430,
						height : 380,
						closable : false,
						autoScroll : true,
						title : '添加专业',
						layout : 'fit',
						modal : true,
						border : false,
						items : majorForm,
						buttons : [{
									text : '保存',
									scale : 'medium',
									disabled : false,
									handler : addBtnsHandler
								}, {
									text : '取消',
									scale : 'medium',
									handler : function() {
										majorForm.form.reset();// 清空表单
										newFormWin.hide();
									}
								}]
					});
		} else {
			newFormWin.setTitle('添加专业')
		}
		newFormWin.show('majorForm');// 显示此窗口
	}

}
function addBtnsHandler() {
	if (majorForm.form.isValid()) {
		majorForm.form.submit({
					url : PATH + 'school/school!editMajor.do',
					waitMsg : '正在保存数据，稍后...',
					success : function(form, action) {
						Ext.Msg.alert('提示', '操作成功！');
						majorForm.form.reset();// 清空表单
						Ext.getCmp('majorGrid').getStore().reload();
						newFormWin.hide();
					},
					failure : function(form, response) {
						var errors = Ext.decode(response.response.responseText).res
						var errorStr = '<font color="red">'
						for (var i = 1; i < errors.length; i++) {
							errorStr += errors[i].defaultMessage + '<br>'
						}
						errorStr += '</font>'
						Ext.Msg.alert('保存失败', errorStr)
					}
				});
	} else {
		Ext.Msg.alert('信息', '请填写完成再提交!');
	}
}