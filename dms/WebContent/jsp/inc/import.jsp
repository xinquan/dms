<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link
	href="<%=request.getContextPath()%>/js/ExtJS4.2/resources/neptune/ext-all.css"
	rel="stylesheet" type="text/css" />
<link
	href="<%=request.getContextPath()%>/js/ExtJS4.2/locale/ext-font-CN.css"
	rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/css/style.css"
	rel="stylesheet" type="text/css" />
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/ExtJS4.2/bootstrap.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/ExtJS4.2/locale/ext-lang-zh_CN.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/ExtJS4.2/ux/TipWindow.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/util/jquery-1.12.3.min.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/util/json2.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/util/boot.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/ExtJS4.2/ux/grid/FiltersFeature.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/util/Constants.js"></script>
<script type="text/javascript">
	var _username = '<%=session.getAttribute("username")%>'
	var _uid = '<%=session.getAttribute("uid")%>'
	var _userid = '<%=session.getAttribute("userid")%>'
	var _majorname = '<%=session.getAttribute("majorname")%>'
	var _deptid = '<%=session.getAttribute("deptid")%>'
	var _deptname = '<%=session.getAttribute("deptname")%>'
	var _roles = '<%=session.getAttribute("roles")%>'
	var _roleids = '<%=session.getAttribute("roleids")%>'
</script>