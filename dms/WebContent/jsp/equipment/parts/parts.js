var _config
var currentRoleId
function fillGrid() {
	_config = {
		partsGrid : {
			id : 'partsGrid',
			fields : ['pno', 'name', 'state', 'department', 'brand', 'model', 'standard', 'weight', 'lastinput', 'start', 'end', 'outputdate', 'damagedate', 'repairdate', 'scapdate', 'id'],
			headers : ['设备编号', '设备名称', '状态', '归属部门', '品牌', '型号', '规格', '重量', '入库时间', '使用年限（开始）', '使用年限（结束）', '出库时间', '损坏时间', '维护时间', '报废时间'],
			widths : [100, 120, 60, 100, 100, 80, 80, 80, 100, 120, 120, 100, 100, 100, 100],
			renderers : [tip, tip, tip, tip, tip, tip, tip, tip, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}],
			url : PATH + 'equipment/equipment!partsListAll.do',
			autoLoad : true,
			region : 'center',
			query :true,
			queryField : 'pno,name,state,department,brand,model,standard',
			border : false,
			tbar : [{
						iconCls : 'add',
						text : '添加配件',
						handler : function() {
							if (currentRoleId) {
								editFormWin('add')
							} else {
								Ext.Msg.alert('添加操作', '您必须选择一个设备以便为该设备添加配件！');
							}
						}
					}, {
						iconCls : 'edit',
						text : '修改配件',
						tooltip : '修改配件',
						handler : function() {
							if (currentRoleId) {
								editFormWin('edit')
							} else {
								Ext.Msg.alert('修改操作', '您必须先选择配件所属的设备！');
							}
						}
					}, {
						iconCls : 'delete',
						text : '移除配件',
						handler : function() {
							if (currentRoleId) {
								var records = Ext.getCmp('partsGrid').getSelectionModel().getSelection();
								if (records.length != 0) {
									// 提示是否删除数据
									Ext.Msg.confirm("是否要删除？", "是否要删除这些被选择的数据？", function(btn) {
												if (btn == "yes") {
													delIds = []
													for (var i = 0; i < records.length; i++) {
														delIds.push(records[i].data.id);
													}
													// 3发出AJAX请求删除相应的数据！
													Ext.Ajax.request({
																url : PATH + 'equipment/equipment!removePartsFromEuipment.do',
																params : {
																	eid : currentRoleId,
																	'delIds' : delIds
																},
																success : function() {
																	Ext.Msg.alert("移除成功", "您已经成功移除配件！");
																	Ext.getCmp('partsGrid').getStore().reload();
																},
																failure : function() {
																	Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
																}
															});
												}
											});
								} else {
									Ext.Msg.alert('删除操作', '您必须选择一个配件以便删除！');
								}
							} else {
								Ext.Msg.alert('移除操作', '您必须选择一个设备以便移除该设备下的配件！');
							}
						}
					}, {
						iconCls : 'borrow',
						text : '借用配件',
						tooltip : '借用配件',
						handler : function() {
							if (currentRoleId) {
								showWindow(PATH + 'equipment/equipment!borrowParts.do?eid=' + currentRoleId, "借用配件", 800, 600, "yes");
							} else {
								Ext.Msg.alert('借用配件', '您必须先选择配件所属的设备！');
							}
						}
					}]
		}
	}
}
function createTreeStore() {
	return Ext.create('Ext.data.TreeStore', {
				defaultRootId : '-1',
				model : 'TreeModel',
				proxy : {
					type : 'ajax',
					extraParams : {},
					url : PATH + "equipment/equipment!equipmentTreeList.do",
					reader : {
						type : 'json',
						root : 'res'
					}
				},
				root : {
					expanded : true
				},
				nodeParam : "deptId"
			})
}

function buildTree() {
	var tree = Ext.create('Ext.tree.Panel', {
				title : '设备列表',
				layout : 'fit',
				iconCls : 'rolelist',
				rootVisible : false,
				border : false,
				autoScroll : true,
				store : createTreeStore(),
				listeners : {
					'itemclick' : function(view, record, item, index, e) {
						var item = record.raw
						currentRoleId = item.sortIndex
						Ext.getCmp('partsGrid').store.load({
									params : {
										currentId : currentRoleId
									}
								})
					},
					scope : this
				}
			});
	tree.expandAll()
	return tree;
}
function reloadGrid() {
	Ext.getCmp('partsGrid').store.reload()
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('添加配件', 'border', [{
								region : 'west',
								layout : 'fit',
								width : 210,
								items : buildTree()
							}, createGrid(_config.partsGrid)])
		});