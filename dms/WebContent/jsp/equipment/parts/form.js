var newFormWin
var step
var platform
Ext.define('depart', {
			extend : 'Ext.data.Model',
			fields : [{
						name : 'id',
						type : 'string'
					}, {
						name : 'deptid',
						type : 'string'
					}, {
						name : 'deptname',
						type : 'string'
					}]
		});
var departs = Ext.create('Ext.data.Store', {
			model : 'depart',
			proxy : {
				type : 'ajax',
				url : PATH + "system/system!departList.do",
				actionMethods : {
					read : 'POST'
				},
				reader : {
					type : 'json',
					root : 'res'
				}
			},
			autoLoad : true
		});
var partsForm = new Ext.FormPanel({
			border : false,
			padding : '10 0 0 0',
			waitMsgTarget : true,
			layout : {
				type : 'table',
				columns : 2
			},
			defaults : {
				width : 280,
				labelWidth : 110,
				xtype : 'textfield',
				labelAlign : 'right',
				anchor : "100%"
			},
			items : [{
						fieldLabel : '配件编号',
						id : 'pno',
						name : 'parts.pno',
						afterLabelTextTpl : required,
						allowBlank : false
					}, {
						fieldLabel : '配件名称',
						name : 'parts.name',
						afterLabelTextTpl : required,
						allowBlank : false
					}, {
						xtype : 'combo',
						fieldLabel : '归属部门',
						store : departs,
						mode : 'remote',
//						afterLabelTextTpl : required,
//						allowBlank : false,
						queryMode : 'local',
						forceSelection : true,
						valueField : 'deptname',
						displayField : 'deptname',
						name : 'parts.department',
						value : _deptname,
						id : 'deptname',
						triggerAction : 'all'
					}, {
						fieldLabel : '品牌',
						name : 'parts.brand'
//						afterLabelTextTpl : required,
//						allowBlank : false
					}, {
						fieldLabel : '型号',
						name : 'parts.model'
//						afterLabelTextTpl : required,
//						allowBlank : false
					}, {
						fieldLabel : '规格',
						name : 'parts.standard'
//						afterLabelTextTpl : required,
//						allowBlank : false
					}, {
						fieldLabel : '重量',
						name : 'parts.weight'
//						afterLabelTextTpl : required,
//						allowBlank : false
					}, {
						fieldLabel : '入库时间',
						xtype : 'datefield',
						name : 'parts.lastinput',
						format : 'Y-m-d',
						value : new Date(),
						afterLabelTextTpl : required,
						allowBlank : false
					}, {
						fieldLabel : '使用年限：从',
						xtype : 'datefield',
						name : 'parts.start',
						format : 'Y-m-d'
//						afterLabelTextTpl : required,
//						allowBlank : false
					}, {
						fieldLabel : '到',
						xtype : 'datefield',
						name : 'parts.end',
						format : 'Y-m-d'
//						afterLabelTextTpl : required,
//						allowBlank : false
					}, 
//						{
//						fieldLabel : '出库时间',
//						xtype : 'datefield',
//						format : 'Y-m-d',
//						name : 'parts.outputdate'
//					}, {
//						fieldLabel : '损坏时间',
//						xtype : 'datefield',
//						format : 'Y-m-d',
//						name : 'parts.damagedate'
//					}, {
//						fieldLabel : '维护时间',
//						xtype : 'datefield',
//						format : 'Y-m-d',
//						name : 'parts.repairdate'
//					}, {
//						fieldLabel : '报废时间',
//						xtype : 'datefield',
//						format : 'Y-m-d',
//						name : 'parts.scapdate'
//					}, 
						{
						fieldLabel : '设备当前状态',
						xtype : 'combo',
						store : getDictStore(true, 'SBZT'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : false,
						hidden : true,
						name : 'parts.state',
						value : '在库',
						id : 'state',
						triggerAction : 'all'
					}, {
						hidden : true,
						fieldLabel : '部门id',
						name : 'parts.departmentid'
					}, {
						id : 'id',
						name : 'parts.id',
						hidden : true
					}]
		});
var editFormWin = function(opt) {
	if (opt == 'edit') {
		var sel = Ext.getCmp('partsGrid').getSelectionModel().getSelection()
		if (sel.length == 1) {
			if (sel[0].data.state == 0 || sel[0].data.state == -1 || (_uid == 1 && _username == 'admin')) {
				Ext.Ajax.request({
							url : PATH + 'equipment/equipment!findParts.do',
							params : {
								currentId : sel[0].data.id
							},
							callback : function(opt, success, response) {
								var result = Ext.decode(response.responseText).result;
								if (result) {
									if (result.lastinput) {
										result.lastinput = new Date(result.lastinput)
									}
									if (result.outputdate) {
										result.outputdate = new Date(result.outputdate)
									}
									if (result.damagedate) {
										result.damagedate = new Date(result.damagedate)
									}
									if (result.repairdate) {
										result.repairdate = new Date(result.repairdate)
									}
									if (result.scapdate) {
										result.scapdate = new Date(result.scapdate)
									}
									if (result.start) {
										result.start = new Date(result.start)
									}
									if (result.end) {
										result.end = new Date(result.end)
									}
								}
								var course = makeNewObjAddPrefix(result, 'parts.')
								partsForm.form.setValues(course)
							}
						})
				if (!newFormWin) {
					newFormWin = new Ext.Window({
								width : 580,
								height : 300,
								autoScroll : true,
								closable : false,
								title : '修改配件信息',
								modal : true,
								layout : 'fit',
								border : false,
								items : partsForm,
								buttons : [{
											text : '保存',
											scale : 'medium',
											disabled : false,
											handler : addBtnsHandler
										}, {
											text : '取消',
											scale : 'medium',
											handler : function() {
												partsForm.form.reset();// 清空表单
												newFormWin.hide();
											}
										}]
							});
				} else {
					newFormWin.setTitle('修改配件信息')
				}
				newFormWin.show('partsForm');// 显示此窗口

			} else {
				Ext.Msg.alert('提示', '该状态无法编辑')
			}
		} else {
			Ext.Msg.alert('提示', '请选择要修改的配件。');
		}
	} else if (opt == 'add') {
		if (!newFormWin) {
			newFormWin = new Ext.Window({
						width : 580,
						height : 300,
						closable : false,
						autoScroll : true,
						title : '添加配件',
						modal : true,
						border : false,
						layout : 'fit',
						items : partsForm,
						buttonAlign : 'center',
						buttons : [{
									text : '保存',
									scale : 'medium',
									disabled : false,
									handler : addBtnsHandler
								}, {
									text : '取消',
									scale : 'medium',
									handler : function() {
										partsForm.form.reset();// 清空表单
										newFormWin.hide();
									}
								}]
					});
		} else {
			newFormWin.setTitle('添加配件')
		}
		newFormWin.show('partsForm');// 显示此窗口
	}

}

function addBtnsHandler() {
	if (partsForm.form.isValid()) {
		partsForm.form.submit({
					url : PATH + 'equipment/equipment!editParts.do',
					params : {
						'parts.did' : currentRoleId
					},
					submitEmptyText : false,
					waitMsg : '正在保存数据，稍后...',
					success : function(form, action) {
						Ext.Msg.alert('保存成功', '操作成功！');
						partsForm.form.reset();// 清空表单
						Ext.getCmp('partsGrid').getStore().reload();
						newFormWin.hide();
					},
					failure : function(form, response) {
						var errors = Ext.decode(response.response.responseText).res
						var errorStr = '<font color="red">'
						for (var i = 1; i < errors.length; i++) {
							errorStr += errors[i].defaultMessage + '<br>'
						}
						errorStr += '</font>'
						Ext.Msg.alert('保存失败', errorStr)
					}
				});
	} else {
		Ext.Msg.alert('信息', '请填写完成再提交!');
	}
}

function downloadWord(selid) {
	// window.location.href = 'down_plan?currentId='
	// + sel[0].data.id
	if (!Ext.fly('downForm')) {
		var downForm = document.createElement('form');
		downForm.id = 'downForm';
		downForm.name = 'downForm';
		downForm.className = 'x-hidden';
		downForm.action = PATH + 'plan/plan!exportCourseToWord.do';
		downForm.method = 'post';
		// downForm .target = '_blank'; //打开新的下载页面
		var data = document.createElement('input');
		data.type = 'hidden';// 隐藏域
		data.name = 'currentId';// form表单参数
		data.value = selid;// form表单值
		downForm.appendChild(data);
		document.body.appendChild(downForm);
	}
	Ext.fly('downForm').dom.submit();
	if (Ext.fly('downForm')) {
		document.body.removeChild(downForm);
	}
}

function downloadExcel() {
	if (!Ext.fly('downForm')) {
		var downForm = document.createElement('form');
		downForm.id = 'downForm';
		downForm.name = 'downForm';
		downForm.className = 'x-hidden';
		downForm.action = PATH + 'plan/plan!exportCourseToExcel.do';
		downForm.method = 'post';
		// downForm .target = '_blank'; //打开新的下载页面
		var data = document.createElement('input');
		data.type = 'hidden';// 隐藏域
		downForm.appendChild(data);
		document.body.appendChild(downForm);
	}
	Ext.fly('downForm').dom.submit();
	if (Ext.fly('downForm')) {
		document.body.removeChild(downForm);
	}
}