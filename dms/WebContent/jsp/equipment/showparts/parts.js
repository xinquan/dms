var _config
var currentRoleId
currentRoleId = getUrlParam('eid')
function fillGrid() {
	_config = {
		partsGrid : {
			id : 'partsGrid',
			fields : ['pno', 'name', 'state', 'department', 'brand', 'model', 'standard', 'weight', 'lastinput', 'start', 'end', 'outputdate', 'damagedate', 'repairdate', 'scapdate', 'id'],
			headers : ['设备编号', '设备名称', '状态', '归属部门', '品牌', '型号', '规格', '重量', '入库时间', '使用年限（开始）', '使用年限（结束）', '出库时间', '损坏时间', '维护时间', '报废时间'],
			widths : [100, 120, 60, 100, 100, 80, 80, 80, 100, 120, 120, 100, 100, 100, 100],
			renderers : [tip, tip, tip, tip, tip, tip, tip, tip, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}],
			url : PATH + 'equipment/equipment!partsListAll.do',
			params : {
				currentId : currentRoleId
			},
			autoLoad : true,
			region : 'center',
			// hasPage : true,
			border : false
		}
	}
}

Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('添加配件', 'fit', createGrid(_config.partsGrid))
		});