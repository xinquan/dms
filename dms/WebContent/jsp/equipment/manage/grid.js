var _config
var passWin
function fillGrid() {
	_config = {
		courseGrid : {
			id : 'courseGrid',
			fields : ['dno', 'name', 'state', 'department', 'brand', 'model', 'standard', 'weight', 'lastinput', 'start', 'end', 'outputdate', 'repairdate', 'damagedate', 'id'],
			headers : ['设备编号', '设备名称', '状态', '归属部门', '品牌', '型号', '规格', '重量', '入库时间', '使用年限（开始）', '使用年限（结束）', '出库时间', '维护时间', '报废时间'],
			widths : [100, 120, 60, 100, 100, 80, 80, 80, 100, 120, 120, 100, 100, 100, 100],
			renderers : [tip, tip, tip, tip, tip, tip, tip, tip, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}],
			url : PATH + 'equipment/equipment!equipmentListAll.do',
			split : true,
			query : true,
			queryField : 'dno,name,state,department,brand,model,standard',
			autoLoad : true,
			hasPage : true,
			viewConfig : {
				forceFit : true,
				getRowClass : function(record, rowIndex, rowParams, store) {
					var type = record.get('state');
					if ('报废' == type) {
						return 'x-grid-row-gray';
					}
				}
			},
			tbar : [{
						id : 'addBtn',
						iconCls : 'add',
						text : ' 入库',
						tooltip : '入库',
						handler : function() {
							editFormWin('add')
						}
					}, {
						id : 'editBtn',
						iconCls : 'edit',
						text : '修改设备信息',
						tooltip : '修改设备信息',
						handler : function() {
							editFormWin('edit')
						}
					}, {
						id : 'delBtn',
						iconCls : 'delete',
						text : '删除',
						tooltip : '将选中的记录移到回收站',
						handler : function() {
							var _records = Ext.getCmp('courseGrid').getSelectionModel().getSelection();
							delRecords(_records, 'Equipment', true)
						}
					}, {
						id : 'twBtn',
						iconCls : 'barcode',
						text : '查看条形码',
						tooltip : '查看条形码',
						handler : function() {
							var _records = Ext.getCmp('courseGrid').getSelectionModel().getSelection();
							twWin.show()
							// $("#qrcodeTable").html("");
							var value = _records[0].data.dno
							$("#qrcodeTable").JsBarcode(value, {
								format : "CODE39",// 选择要使用的条形码类型
								width : 3,// 设置条之间的宽度
								height : 100,// 高度
								displayValue : true,// 是否在条形码下方显示文字
								text : "扬中广播电视设备",// 覆盖显示的文本
								fontOptions : "bold italic",// 使文字加粗体或变斜体
								font : "Microsoft YaHei",// 设置文本的字体
								textAlign : "left",// 设置文本的水平对齐方式
								textPosition : "top",// 设置文本的垂直位置
								textMargin : 5,// 设置条形码和文本之间的间距
								fontSize : 15,// 设置文本的大小
								background : "#eee",// 设置条形码的背景
								lineColor : "black",// 设置条和文本的颜色。
								margin : 15
									// 设置条形码周围的空白边距
								})
							// $("#qrcodeTable").qrcode({
							// render : "canvas",
							// text : utf16to8(value),
							// width : "200",
							// height : "200"
							// });
							// stamp()
						}
					}]
		}
	}
}
function autoSelFirst() {
	var model = Ext.getCmp('courseGrid').getSelectionModel()
	model.select(0)
}
function reloadGrid() {
	Ext.getCmp('courseGrid').getStore().reload()
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()

			createApp('课程设置', 'fit', [createGrid(_config.courseGrid)])
			Ext.Ajax.request({
						url : PATH + 'system/menu!getRights.do',
						params : {
							action : this.location.pathname,
							roleids : _roleids
						},
						success : function(response) {
							perm = Ext.decode(response.responseText)
							if (!perm.edit) {
								Ext.getCmp('addBtn').setVisible(false)
								Ext.getCmp('editBtn').setVisible(false)
								Ext.getCmp('delBtn').setVisible(false)
							}
							if (perm.department) {
								reloadGrid(perm.department)
							} else {
								reloadGrid()
								autoSelFirst()
							}
						},
						failure : function() {
							Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
						}
					});
		});