var newFormWin
var showWin
var returnWin
var step
var platform
var currentPress
Ext.define('TreeModel', {
			extend : 'Ext.data.Model',
			fields : [{
						name : "id",
						type : "string"
					}, {
						name : "text",
						type : "string"
					}, {
						name : "iconCls",
						type : "string"
					}, {
						name : "leaf",
						type : "boolean"
					}]
		})
var equipmentForm = new Ext.FormPanel({
			border : false,
			waitMsgTarget : true,
			padding : '10 0 0 0',
			layout : {
				type : 'table',
				columns : 2
			},
			defaults : {
				width : 280,
				labelWidth : 110,
				xtype : 'textfield',
				labelAlign : 'right',
				anchor : "100%"
			},
			items : [{
						fieldLabel : '设备编号',
						id : 'dno',
						xtype : 'triggerfield',
						name : 'repairRecordsView.dno',
						afterLabelTextTpl : required,
						triggerCls : 'ux-form-search-trigger',
						enableKeyEvents : true,
						onTriggerClick : function() {
							currentPress = this.id
							showWindow(PATH + 'equipment/equipment!importEquipment.do?belong=false', "选择设备", 800, 600, "yes");
						},
						listeners : {
							keyup : function() {
								Ext.Ajax.request({
											url : PATH + 'equipment/equipment!findEquipmentByHql.do',
											params : {
												dno : this.getValue()
											},
											callback : function(opt, success, response) {
												var result = Ext.decode(response.responseText).result;
												if (result) {
													Ext.getCmp('name').setValue(result.name)
													Ext.getCmp('state').setValue(result.state)
													Ext.getCmp('department').setValue(result.department)
													Ext.getCmp('lastinput').setValue(new Date(result.lastinput))
													Ext.getCmp('eid').setValue(result.id)
												}
											}
										})
							}
						},
						allowBlank : false
					}, {
						id : 'name',
						fieldLabel : '设备名称',
						readOnly : true,
						name : 'repairRecordsView.name'
					}, {
						id : 'state',
						fieldLabel : '状态',
						readOnly : true,
						name : 'repairRecordsView.state'
					}, {
						id : 'department',
						fieldLabel : '归属部门',
						readOnly : true,
						name : 'repairRecordsView.department'
					}, {
						id : 'lastinput',
						fieldLabel : '入库时间',
						xtype : 'datefield',
						readOnly : true,
						name : 'repairRecordsView.lastinput',
						format : 'Y-m-d'
					},
					// {
					// fieldLabel : '是否交给处理人',
					// xtype : 'combo',
					// store : getDictStore(true, 'SF'),
					// mode : 'remote',
					// valueField : 'detailName',
					// displayField : 'detailName',
					// editable : true,
					// name : 'repairRecordsView.assign',
					// id : 'assign',
					// // afterLabelTextTpl : required,
					// // allowBlank : false,
					// triggerAction : 'all'
					// },
					{
						fieldLabel : '维修时间',
						xtype : 'datefield',
						name : 'repairRecordsView.repairdate',
						format : 'Y-m-d',
						value : new Date()
					}, {
						id : 'repairman',
						fieldLabel : '维护人',
						xtype : 'triggerfield',
						triggerCls : 'ux-form-search-trigger',
						name : 'repairRecordsView.repairman',
						onTriggerClick : function() {
							currentPress = this.id
							showWindow(PATH + 'system/menu!importUser.do?muti=false', "选择用户", 800, 600, "yes");
						},
						editable : false
					},
					// {
					// id : 'repairmark',
					// fieldLabel : '处理说明',
					// colspan : 2,
					// width : 560,
					// xtype : 'textarea',
					// name : 'repairRecordsView.repairmark',
					// editable : false
					// },
					{
						xtype : 'panel',
						border : false
					},
					// {
					// fieldLabel : '修复时间',
					// xtype : 'datefield',
					// name : 'repairRecordsView.returndate',
					// format : 'Y-m-d'
					// },
					{
						fieldLabel : '备注',
						colspan : 2,
						width : 560,
						xtype : 'textarea',
						name : 'repairRecordsView.mark'
					}, {
						id : 'eid',
						hidden : true,
						fieldLabel : '设备id',
						name : 'repairRecordsView.eid'
					}, {
						id : 'id',
						xtype : 'numberfield',
						name : 'repairRecordsView.id',
						hidden : true
					}]
		});
var returnForm = new Ext.FormPanel({
			border : false,
			waitMsgTarget : true,
			padding : '10 0 0 0',
			layout : {
				type : 'table',
				columns : 2
			},
			defaults : {
				width : 280,
				labelWidth : 110,
				xtype : 'textfield',
				labelAlign : 'right',
				anchor : "100%"
			},
			items : [{
						fieldLabel : '维修结果',
						xtype : 'combo',
						store : getDictStore(true, 'SBZT'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : true,
						name : 'repairRecordsView.state',
						id : 'repairstate',
						afterLabelTextTpl : required,
						allowBlank : false,
						triggerAction : 'all'
					}, {
						fieldLabel : '归还时间',
						id : 'returndate',
						xtype : 'datefield',
						name : 'repairRecordsView.returndate',
						format : 'Y-m-d',
						value : new Date()
					}, {
						fieldLabel : '维修说明',
						colspan : 2,
						width : 560,
						id : 'repairmark',
						xtype : 'textarea',
						name : 'repairRecordsView.repairmark'
					}]
		});
var showForm = new Ext.FormPanel({
			border : false,
			waitMsgTarget : true,
			layout : {
				type : 'table',
				columns : 2
			},
			defaults : {
				width : 280,
				labelWidth : 110,
				xtype : 'displayfield',
				labelAlign : 'right',
				anchor : "100%"
			},
			items : [{
						fieldLabel : '设备编号',
						name : 'equipment.dno'
					}, {
						fieldLabel : '设备名称',
						name : 'equipment.name'
					}, {
						fieldLabel : '归属部门',
						name : 'equipment.department'
					}, {
						fieldLabel : '品牌',
						name : 'equipment.brand'
					}, {
						fieldLabel : '型号',
						name : 'equipment.model'
					}, {
						fieldLabel : '规格',
						name : 'equipment.standard'
					}, {
						fieldLabel : '重量',
						name : 'equipment.weight'
					}, {
						fieldLabel : '入库时间',
						xtype : 'datefield',
						readOnly : true,
						name : 'equipment.lastinput',
						format : 'Y-m-d'
					}, {
						fieldLabel : '使用年限：从',
						xtype : 'datefield',
						readOnly : true,
						format : 'Y-m-d',
						name : 'equipment.start'
					}, {
						fieldLabel : '到',
						xtype : 'datefield',
						readOnly : true,
						format : 'Y-m-d',
						name : 'equipment.end'
					}, {
						fieldLabel : '出库时间',
						xtype : 'datefield',
						readOnly : true,
						format : 'Y-m-d',
						name : 'equipment.outputdate'
					}, {
						fieldLabel : '维护时间',
						xtype : 'datefield',
						readOnly : true,
						format : 'Y-m-d',
						name : 'equipment.repairdate'
					}, {
						fieldLabel : '报废时间',
						xtype : 'datefield',
						readOnly : true,
						format : 'Y-m-d',
						name : 'equipment.scapdate'
					}, {
						fieldLabel : '设备当前状态',
						name : 'equipment.state'
					}, {
						fieldLabel : '备注',
						colspan : 2,
						width : 560,
						readOnly : true,
						xtype : 'textarea',
						name : 'equipment.mark'
					}]
		});
var editFormWin = function(opt) {
	if (opt == 'edit') {
		var sel = Ext.getCmp('courseGrid').getSelectionModel().getSelection()
		if (sel.length == 1) {
			if (sel[0].data.state == 0 || sel[0].data.state == -1 || (_uid == 1 && _username == 'admin')) {
				Ext.Ajax.request({
							url : PATH + 'equipment/equipment!findRepairRecordsView.do',
							params : {
								currentId : sel[0].data.id
							},
							callback : function(opt, success, response) {
								var result = Ext.decode(response.responseText).result;
								if (result) {
									if (result.repairdate) {
										result.repairdate = new Date(result.repairdate)
									}
									if (result.returndate) {
										result.returndate = new Date(result.returndate)
									}
								}
								var course = makeNewObjAddPrefix(result, 'repairRecordsView.')
								equipmentForm.form.setValues(course)
							}
						})
				if (!newFormWin) {
					newFormWin = new Ext.Window({
								width : 580,
								height : 360,
								autoScroll : true,
								closable : false,
								title : '修改维护记录',
								modal : true,
								layout : 'fit',
								border : false,
								items : equipmentForm,
								buttons : [{
											text : '保存',
											scale : 'medium',
											disabled : false,
											handler : addBtnsHandler
										}, {
											text : '取消',
											scale : 'medium',
											handler : function() {
												equipmentForm.form.reset();// 清空表单
												newFormWin.hide();
											}
										}]
							});
				} else {
					newFormWin.setTitle('修改维护记录')
				}
				newFormWin.show('equipmentForm');// 显示此窗口

			} else {
				Ext.Msg.alert('提示', '该状态无法编辑')
			}
		} else {
			Ext.Msg.alert('提示', '请选择要修改的设备。');
		}
	} else if (opt == 'add') {
		if (!newFormWin) {
			newFormWin = new Ext.Window({
						width : 580,
						height : 360,
						closable : false,
						autoScroll : true,
						title : '添加维护记录',
						modal : true,
						border : false,
						layout : 'fit',
						items : equipmentForm,
						buttonAlign : 'center',
						buttons : [{
									text : '保存',
									scale : 'medium',
									disabled : false,
									handler : addBtnsHandler
								}, {
									text : '取消',
									scale : 'medium',
									handler : function() {
										equipmentForm.form.reset();// 清空表单
										newFormWin.hide();
									}
								}]
					});
		} else {
			newFormWin.setTitle('添加维护记录')
		}
		newFormWin.show('equipmentForm');// 显示此窗口
	} else if (opt == 'show') {
		var sel = Ext.getCmp('courseGrid').getSelectionModel().getSelection()
		Ext.Ajax.request({
					url : PATH + 'equipment/equipment!findEquipment.do',
					params : {
						currentId : sel[0].data.eid
					},
					callback : function(opt, success, response) {
						var result = Ext.decode(response.responseText).result;
						if (result) {
							if (result.lastinput) {
								result.lastinput = new Date(result.lastinput)
							}
							if (result.outputdate) {
								result.outputdate = new Date(result.outputdate)
							}
							if (result.damagedate) {
								result.damagedate = new Date(result.damagedate)
							}
							if (result.repairdate) {
								result.repairdate = new Date(result.repairdate)
							}
							if (result.scapdate) {
								result.scapdate = new Date(result.scapdate)
							}
							if (result.start) {
								result.start = new Date(result.start)
							}
							if (result.end) {
								result.end = new Date(result.end)
							}
						}
						var course = makeNewObjAddPrefix(result, 'equipment.')
						showForm.form.setValues(course)
					}
				})
		if (!showWin) {
			showWin = new Ext.Window({
						width : 580,
						height : 430,
						autoScroll : true,
						closable : false,
						title : '查看设备信息',
						modal : true,
						layout : 'fit',
						border : false,
						items : showForm,
						buttonAlign : 'center',
						buttons : [{
									text : '关闭',
									scale : 'medium',
									handler : function() {
										showForm.form.reset();// 清空表单
										showWin.hide();
									}
								}]
					});
		} else {
			showWin.setTitle('查看设备信息')
		}
		showWin.show('showForm');
	} else if (opt == 'return') {
		var sel = Ext.getCmp('courseGrid').getSelectionModel().getSelection()
		if (sel.length == 1) {
			if (!returnWin) {
				returnWin = new Ext.Window({
							width : 580,
							height : 320,
							autoScroll : true,
							closable : false,
							title : '修改维护记录',
							modal : true,
							layout : 'fit',
							border : false,
							items : returnForm,
							buttons : [{
										text : '保存',
										scale : 'medium',
										disabled : false,
										handler : function() {
											if (returnForm.form.isValid()) {
												Ext.Ajax.request({
															url : PATH + 'equipment/equipment!returnBackRepairedEquipment.do',
															params : {
																currentId : sel[0].data.id,
																state : Ext.getCmp('repairstate').getValue(),
																mark : Ext.getCmp('repairmark').getValue(),
																returndate : Ext.getCmp('returndate').getValue()
															},
															callback : function(opt, success, response) {
																Ext.Msg.alert('提示', '反馈成功！')
																window.close()
																returnForm.form.reset();// 清空表单
																returnWin.hide();
																Ext.getCmp('courseGrid').getStore().reload();
															}
														})
											} else {
												Ext.Msg.alert('信息', '请填写完成再提交!');
											}
										}
									}, {
										text : '取消',
										scale : 'medium',
										disabled : false,
										handler : function() {
											returnForm.form.reset();// 清空表单
											returnWin.hide();
										}
									}]
						});
			}
			returnWin.show('returnForm');// 显示此窗口
		}
	}

}
function addBtnsHandler() {
	if (equipmentForm.form.isValid()) {
		equipmentForm.form.submit({
					url : PATH + 'equipment/equipment!editRepair.do',
					submitEmptyText : false,
					waitMsg : '正在保存数据，稍后...',
					success : function(form, action) {
						Ext.Msg.alert('保存成功', '操作成功！');
						equipmentForm.form.reset();// 清空表单
						Ext.getCmp('courseGrid').getStore().reload();
						newFormWin.hide();
					},
					failure : function(form, response) {
						var errors = Ext.decode(response.response.responseText).res
						var errorStr = '<font color="red">'
						for (var i = 1; i < errors.length; i++) {
							errorStr += errors[i].defaultMessage + '<br>'
						}
						errorStr += '</font>'
						Ext.Msg.alert('保存失败', errorStr)
					}
				});
	} else {
		Ext.Msg.alert('信息', '请填写完成再提交!');
	}
}

function addUser(records) {
	Ext.getCmp(currentPress).setValue(records[0].data.name)
}
function addEquipment(records) {
	Ext.getCmp(currentPress).setValue(records[0].data.dno);
	Ext.getCmp(currentPress).fireEvent('keyup')
}