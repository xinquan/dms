var _config
var passWin
var eid = getUrlParam('eid')
var passForm = new Ext.FormPanel({
			border : false,
			waitMsgTarget : true,
			items : [{
						fieldLabel : '是否通过',
						xtype : 'combo',
						padding : '10 5 5 5',
						store : getDictStore(true, 'SF'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : false,
						id : 'passed',
						triggerAction : 'all'
					}]
		});
var fillPass = function() {
	var courses = Ext.getCmp('courseGrid').getSelectionModel().getSelection();
	if (!passWin) {
		passWin = new Ext.Window({
					width : 300,
					height : 150,
					closable : false,
					autoScroll : true,
					title : '课程是否通过',
					modal : true,
					border : false,
					layout : 'fit',
					items : passForm,
					buttonAlign : 'center',
					buttons : [{
								text : '保存',
								scale : 'medium',
								disabled : false,
								handler : function() {
									var sel = Ext.getCmp('courseGrid').getSelectionModel().getSelection();
									var courseIds = []
									for (var i = 0; i < sel.length; i++) {
										courseIds.push(sel[i].data.id);
									}
									Ext.Ajax.request({
												url : PATH + 'plan/plan!passCourses.do',
												params : {
													passed : Ext.getCmp('passed').getValue(),
													courseIds : courseIds
												},
												success : function(response) {
													msgShow('提示', '标记成功')
													reloadGrid()
												},
												failure : function() {
													Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
												}
											});
									passWin.hide();
								}
							}, {
								text : '取消',
								scale : 'medium',
								handler : function() {
									passForm.form.reset();// 清空表单
									passWin.hide();
								}
							}]
				});
	}
	passWin.show();// 显示此窗口
}
function fillGrid() {
	_config = {
		courseGrid : {
			id : 'courseGrid',
			fields : ['dno', 'name', 'state', 'department', 'lastinput', 'repairman', 'repairmark', 'repairdate', 'returndate', 'mark', 'createdate', 'eid', 'id'],
			headers : ['设备编号', '设备名称', '状态', '归属部门', '入库时间', '维护人', '维护说明', '维护时间', '归还时间', '备注'],
			widths : [100, 120, 60, 100, 100, 80, 80, 120, 100, 100, 300, 120],
			renderers : [tip, tip, tip, tip, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, tip, tip, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, tip],
			url : PATH + 'equipment/equipment!repairRecordsListAll.do',
			query : true,
			queryField : 'dno,name,state,department,assign,repairman',
			split : true,
			autoLoad : true,
			hasPage : true,
			storeCallback : isQuery,
			viewConfig : {
				forceFit : true,
				getRowClass : function(record, rowIndex, rowParams, store) {
					var type = record.get('state');
					if ('报废' == type) {
						return 'x-grid-row-gray';
					}
				}
			},
			tbar : [{
						id : 'addBtn',
						iconCls : 'add',
						text : ' 维护',
						tooltip : '添加维护的设备记录',
						handler : function() {
							editFormWin('add')
						}
					},
					// {
					// id : 'editBtn',
					// iconCls : 'edit',
					// text : '修改记录',
					// tooltip : '修改记录',
					// handler : function() {
					// editFormWin('edit')
					// }
					// },
					{
						id : 'showBtn',
						iconCls : 'information',
						text : ' 查看设备信息',
						tooltip : '查看设备详细信息',
						handler : function() {
							editFormWin('show')
						}
					},
					// {
					// id : 'delBtn',
					// iconCls : 'delete',
					// text : '删除',
					// tooltip : '删除选中的记录',
					// handler : function() {
					// var _records =
					// Ext.getCmp('courseGrid').getSelectionModel().getSelection();
					// delRecords(_records, 'Repairs', true)
					// }
					// },
					{
						id : 'feedback',
						iconCls : 'feedback',
						text : '维修反馈',
						tooltip : '维护情况反馈',
						handler : function() {
							var _records = Ext.getCmp('courseGrid').getSelectionModel().getSelection();
							if (_records.length == 1 && _records[0].data.state == '维护中') {
								editFormWin('return')
							} else {
								Ext.Msg.alert('查看信息', '该设备的这条维护记录已反馈！');
							}
						}
					}]
		}
	}
}
function autoSelFirst() {
	var model = Ext.getCmp('courseGrid').getSelectionModel()
	model.select(0)
}
function reloadGrid() {
	Ext.getCmp('courseGrid').getStore().reload({
				callback : isQuery
			})
}
function isQuery() {
	if (eid) {
		Ext.getCmp('addBtn').setVisible(false)
		Ext.getCmp('showBtn').setVisible(false)
		Ext.getCmp('feedback').setVisible(false)
		Ext.getCmp('courseGrid').getStore().load({
					params : {
						eid : eid
					}
				})
	}
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('设备维护', 'fit', [createGrid(_config.courseGrid)])
			Ext.Ajax.request({
						url : PATH + 'system/menu!getRights.do',
						params : {
							action : this.location.pathname,
							roleids : _roleids
						},
						success : function(response) {
							perm = Ext.decode(response.responseText)
							if (!perm.edit) {
								Ext.getCmp('addBtn').setVisible(false)
								// Ext.getCmp('editBtn').setVisible(false)
								// Ext.getCmp('delBtn').setVisible(false)
								Ext.getCmp('feedback').setVisible(false)
								// Ext.getCmp('seeBtn').setVisible(false)
								// Ext.getCmp('submitBtn').setVisible(false)
								// Ext.getCmp('exportWBtn').setVisible(false)
								// Ext.getCmp('exportEBtn').setVisible(false)
								// Ext.getCmp('importBtn').setVisible(false)
								// Ext.getCmp('passedBtn').setVisible(false)
							}
							if (perm.department) {
								reloadGrid(perm.department)
							} else {
								reloadGrid()
								autoSelFirst()
							}
						},
						failure : function() {
							Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
						}
					});
		});