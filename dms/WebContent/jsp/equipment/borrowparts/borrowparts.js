var _config
var tabs
var plantype
var treePanel
var newFormWin
var eid = getUrlParam('eid')
var partsidArray = []
function fillGrid() {
	_config = {
		partsGrid : {
			id : 'partsGrid',
			fields : ['pno', 'name', 'state', 'department', 'brand', 'model', 'standard', 'weight', 'lastinput', 'start', 'end', 'outputdate', 'damagedate', 'repairdate', 'scapdate', 'id'],
			headers : ['设备编号', '设备名称', '状态', '归属部门', '品牌', '型号', '规格', '重量', '入库时间', '使用年限（开始）', '使用年限（结束）', '出库时间', '损坏时间', '维护时间', '报废时间'],
			widths : [100, 120, 60, 100, 100, 80, 80, 80, 100, 120, 120, 100, 100, 100, 100],
			renderers : [tip, tip, tip, tip, tip, tip, tip, tip, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}],
			url : PATH + 'equipment/equipment!partsListAll.do',
			autoLoad : true,
			region : 'center',
			border : false
		}
	}
}

function createTreeStore() {
	return Ext.create('Ext.data.TreeStore', {
				defaultRootId : '-1',
				model : 'TreeModel',
				proxy : {
					type : 'ajax',
					url : PATH + "equipment/equipment!equipmentTreeList.do",
					reader : {
						type : 'json',
						root : 'res'
					}
				},
				root : {
					expanded : true
				},
				nodeParam : "deptId"
			})
}

function buildTree() {
	treePanel = Ext.create('Ext.tree.Panel', {
				title : '设备列表',
				iconCls : 'rolelist',
				rootVisible : false,
				border : false,
				store : createTreeStore(),
				listeners : {
					'itemclick' : function(view, record, item, index, e) {
						var item = record.raw
						currentRoleId = item.sortIndex
						Ext.getCmp('partsGrid').store.load({
									params : {
										currentId : currentRoleId
									}
								})
					},
					scope : this
				}
			});
	treePanel.expandAll()
	return treePanel
}

Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('修读计划', 'border', [{
								region : 'north',
								buttonAlign : 'left',
								buttons : [{
											text : '确定借用',
											handler : function() {
												var parts = Ext.getCmp('partsGrid').getSelectionModel().getSelection()
												Ext.each(parts, function(item) {
															partsidArray.push(item.data.id)
														})
												Ext.Ajax.request({
															url : PATH + 'equipment/equipment!addPartsToEquipment.do',
															params : {
																eid : eid,
																jsonString : partsidArray
															},
															success : function() {
																window.opener.reloadGrid()
																window.close()
															},
															failure : function() {
																msgShow('错误', '服务器出现错误！', Ext.Msg.ERROR);
															}
														});
											}
										}]
							}, {
								region : 'west',
								width : 220,
								items : buildTree()
							}, createGrid(_config.partsGrid)])
		});