var editWin
var showWin
var returnWin
var step
var platform
var currentPress
Ext.define('TreeModel', {
			extend : 'Ext.data.Model',
			fields : [{
						name : "id",
						type : "string"
					}, {
						name : "text",
						type : "string"
					}, {
						name : "iconCls",
						type : "string"
					}, {
						name : "leaf",
						type : "boolean"
					}]
		})
var equipmentForm = new Ext.FormPanel({
			border : false,
			padding : '10 0 0 0',
			waitMsgTarget : true,
			layout : {
				type : 'table',
				columns : 2
			},
			defaults : {
				width : 280,
				labelWidth : 110,
				xtype : 'textfield',
				labelAlign : 'right',
				anchor : "100%"
			},
			items : [{
						fieldLabel : '设备编号',
						id : 'dno',
						xtype : 'triggerfield',
						name : 'outRecordsView.dno',
						afterLabelTextTpl : required,
						triggerCls : 'ux-form-search-trigger',
						enableKeyEvents : true,
						onTriggerClick : function() {
							currentPress = this.id
							showWindow(PATH + 'equipment/equipment!importEquipment.do?belong=false', "选择设备", 800, 600, "yes");
						},
						listeners : {
							keyup : function() {
								Ext.Ajax.request({
											url : PATH + 'equipment/equipment!findEquipmentByHql.do',
											params : {
												dno : this.getValue()
											},
											callback : function(opt, success, response) {
												var result = Ext.decode(response.responseText).result;
												if (result) {
													Ext.getCmp('name').setValue(result.name)
													Ext.getCmp('state').setValue(result.state)
													Ext.getCmp('department').setValue(result.department)
													Ext.getCmp('lastinput').setValue(new Date(result.lastinput))
													Ext.getCmp('eid').setValue(result.id)
												}
											}
										})
							}
						},
						allowBlank : false
					}, {
						id : 'name',
						fieldLabel : '设备名称',
						readOnly : true,
						name : 'outRecordsView.name'
					}, {
						id : 'state',
						fieldLabel : '状态',
						readOnly : true,
						name : 'outRecordsView.state'
					}, {
						id : 'department',
						fieldLabel : '归属部门',
						readOnly : true,
						name : 'outRecordsView.department'
					}, {
						id : 'lastinput',
						fieldLabel : '入库时间',
						xtype : 'datefield',
						readOnly : true,
						name : 'outRecordsView.lastinput',
						format : 'Y-m-d'
					}, {
						id : 'outpeople',
						fieldLabel : '经手人',
						xtype : 'triggerfield',
						triggerCls : 'ux-form-search-trigger',
						name : 'outRecordsView.outpeople',
						value : _username,
						editable : false
					}, {
						id : 'usepeople',
						fieldLabel : '使用人',
						name : 'outRecordsView.usepeople',
						xtype : 'triggerfield',
						triggerCls : 'ux-form-search-trigger',
						onTriggerClick : function() {
							currentPress = this.id
							showWindow(PATH + 'system/menu!importUser.do', "选择用户", 800, 600, "yes");
						},
						editable : false
					}, {
						fieldLabel : '借出时间',
						xtype : 'datefield',
						name : 'outRecordsView.outdate',
						format : 'Y-m-d',
						value : new Date(),
						afterLabelTextTpl : required,
						allowBlank : false
					}, {
						fieldLabel : '备注',
						colspan : 2,
						width : 560,
						xtype : 'textarea',
						name : 'outRecordsView.mark'
					}, {
						id : 'eid',
						hidden : true,
						fieldLabel : '设备id',
						name : 'outRecordsView.eid'
					}, {
						id : 'id',
						xtype : 'numberfield',
						name : 'outRecordsView.id',
						hidden : true
					}]
		});
var returnForm = new Ext.FormPanel({
			border : false,
			padding : '10 0 0 0',
			waitMsgTarget : true,
			layout : {
				type : 'table',
				columns : 2
			},
			defaults : {
				width : 280,
				labelWidth : 110,
				xtype : 'textfield',
				labelAlign : 'right',
				anchor : "100%"
			},
			items : [{
						fieldLabel : '设备编号',
						readOnly : true,
						name : 'outRecordsView.dno',
						allowBlank : false
					}, {
						fieldLabel : '设备名称',
						readOnly : true,
						name : 'outRecordsView.name'
					}, {
						fieldLabel : '状态',
						readOnly : true,
						name : 'outRecordsView.state'
					}, {
						fieldLabel : '归属部门',
						readOnly : true,
						name : 'outRecordsView.department'
					}, {
						fieldLabel : '入库时间',
						xtype : 'datefield',
						readOnly : true,
						name : 'outRecordsView.lastinput',
						format : 'Y-m-d'
					}, {
						fieldLabel : '经手人',
						readOnly : true,
						name : 'outRecordsView.outpeople'
					}, {
						fieldLabel : '使用人',
						name : 'outRecordsView.usepeople',
						readOnly : true
					}, {
						fieldLabel : '借出时间',
						name : 'outRecordsView.outdate',
						xtype : 'datefield',
						readOnly : true,
						format : 'Y-m-d'
					}, {
						fieldLabel : '归还时间',
						xtype : 'datefield',
						id : 'returndate',
						name : 'outRecordsView.returndate',
						format : 'Y-m-d',
						value : new Date()
					}, {
						xtype : 'displayfield'
					}, {
						fieldLabel : '出库备注',
						colspan : 2,
						id : 'outputmark',
						width : 560,
						xtype : 'textarea',
						name : 'outRecordsView.outputmark'
					}, {
						hidden : true,
						fieldLabel : '设备id',
						name : 'outRecordsView.eid'
					}, {
						xtype : 'numberfield',
						name : 'outRecordsView.id',
						hidden : true
					}]
		});
var showForm = new Ext.FormPanel({
			border : false,
			waitMsgTarget : true,
			layout : {
				type : 'table',
				columns : 2
			},
			defaults : {
				width : 280,
				labelWidth : 110,
				xtype : 'displayfield',
				labelAlign : 'right',
				anchor : "100%"
			},
			items : [{
						fieldLabel : '设备编号',
						name : 'equipment.dno'
					}, {
						fieldLabel : '设备名称',
						name : 'equipment.name'
					}, {
						fieldLabel : '归属部门',
						name : 'equipment.department'
					}, {
						fieldLabel : '品牌',
						name : 'equipment.brand'
					}, {
						fieldLabel : '型号',
						name : 'equipment.model'
					}, {
						fieldLabel : '规格',
						name : 'equipment.standard'
					}, {
						fieldLabel : '重量',
						name : 'equipment.weight'
					}, {
						fieldLabel : '入库时间',
						xtype : 'datefield',
						readOnly : true,
						name : 'equipment.lastinput',
						format : 'Y-m-d'
					}, {
						fieldLabel : '使用年限：从',
						xtype : 'datefield',
						readOnly : true,
						format : 'Y-m-d',
						name : 'equipment.start'
					}, {
						fieldLabel : '到',
						xtype : 'datefield',
						readOnly : true,
						format : 'Y-m-d',
						name : 'equipment.end'
					}, {
						fieldLabel : '借出时间',
						xtype : 'datefield',
						readOnly : true,
						format : 'Y-m-d',
						name : 'equipment.outputdate'
					}, {
						fieldLabel : '设备当前状态',
						name : 'equipment.state'
					}, {
						fieldLabel : '备注',
						colspan : 2,
						width : 560,
						readOnly : true,
						xtype : 'textarea',
						name : 'equipment.mark'
					}, {
						fieldLabel : '归还时间',
						xtype : 'datefield',
						readOnly : true,
						format : 'Y-m-d',
						name : 'equipment.returndate'
					}, {
						xtype : 'displayfield'
					}, {
						fieldLabel : '出库备注',
						colspan : 2,
						width : 560,
						readOnly : true,
						xtype : 'textarea',
						name : 'equipment.outputmark'
					}]
		});
var editFormWin = function(opt) {
	if (opt == 'edit') {
		var sel = Ext.getCmp('equipmentGrid').getSelectionModel().getSelection()
		if (sel.length == 1) {
			if (sel[0].data.state == 0 || sel[0].data.state == -1 || (_uid == 1 && _username == 'admin')) {
				Ext.Ajax.request({
							url : PATH + 'equipment/equipment!findOutRecordsView.do',
							params : {
								currentId : sel[0].data.id
							},
							callback : function(opt, success, response) {
								var result = Ext.decode(response.responseText).result;
								if (result) {
									if (result.lastinput) {
										result.lastinput = new Date(result.lastinput)
									}
									if (result.outdate) {
										result.outdate = new Date(result.outdate)
									}
									if (result.returndate) {
										result.returndate = new Date(result.returndate)
									}
								}
								var course = makeNewObjAddPrefix(result, 'outRecordsView.')
								equipmentForm.form.setValues(course)
							}
						})
				if (!editWin) {
					editWin = new Ext.Window({
								width : 580,
								height : 330,
								autoScroll : true,
								closable : false,
								title : '修改出库记录',
								modal : true,
								layout : 'fit',
								border : false,
								items : equipmentForm,
								buttons : [{
											text : '保存',
											scale : 'medium',
											disabled : false,
											handler : addBtnsHandler
										}, {
											text : '取消',
											scale : 'medium',
											handler : function() {
												equipmentForm.form.reset();// 清空表单
												editWin.hide();
											}
										}]
							});
				} else {
					editWin.setTitle('修改出库记录')
				}
				editWin.show('equipmentForm');// 显示此窗口

			} else {
				Ext.Msg.alert('提示', '该状态无法编辑')
			}
		} else {
			Ext.Msg.alert('提示', '请选择要修改的设备。');
		}
	} else if (opt == 'add') {
		if (!editWin) {
			editWin = new Ext.Window({
						width : 580,
						height : 330,
						closable : false,
						autoScroll : true,
						title : '添加出库记录',
						modal : true,
						border : false,
						layout : 'fit',
						items : equipmentForm,
						buttonAlign : 'center',
						buttons : [{
									text : '保存',
									scale : 'medium',
									disabled : false,
									handler : addBtnsHandler
								}, {
									text : '取消',
									scale : 'medium',
									handler : function() {
										equipmentForm.form.reset();// 清空表单
										editWin.hide();
									}
								}]
					});
		} else {
			editWin.setTitle('添加出库记录')
		}
		editWin.show('equipmentForm');// 显示此窗口
	} else if (opt == 'show') {
		var sel = Ext.getCmp('equipmentGrid').getSelectionModel().getSelection()
		Ext.Ajax.request({
					url : PATH + 'equipment/equipment!findEquipment.do',
					params : {
						currentId : sel[0].data.eid
					},
					callback : function(opt, success, response) {
						var result = Ext.decode(response.responseText).result;
						if (result) {
							if (result.lastinput) {
								result.lastinput = new Date(result.lastinput)
							}
							if (result.outputdate) {
								result.outputdate = new Date(result.outputdate)
							}
							if (result.damagedate) {
								result.damagedate = new Date(result.damagedate)
							}
							if (result.repairdate) {
								result.repairdate = new Date(result.repairdate)
							}
							if (result.scapdate) {
								result.scapdate = new Date(result.scapdate)
							}
							if (result.start) {
								result.start = new Date(result.start)
							}
							if (result.end) {
								result.end = new Date(result.end)
							}
						}
						var course = makeNewObjAddPrefix(result, 'equipment.')
						showForm.form.setValues(course)
					}
				})
		if (!showWin) {
			showWin = new Ext.Window({
						width : 580,
						height : 450,
						autoScroll : true,
						closable : false,
						title : '查看设备信息',
						modal : true,
						layout : 'fit',
						border : false,
						items : showForm,
						buttonAlign : 'center',
						buttons : [{
									text : '关闭',
									scale : 'medium',
									handler : function() {
										showForm.form.reset();// 清空表单
										showWin.hide();
									}
								}]
					});
		} else {
			showWin.setTitle('查看设备信息')
		}
		showWin.show('showForm');
	} else if (opt == 'return') {
		var sel = Ext.getCmp('equipmentGrid').getSelectionModel().getSelection()
		if (sel.length == 1) {
			if (sel[0].data.state == 0 || sel[0].data.state == -1 || (_uid == 1 && _username == 'admin')) {
				Ext.Ajax.request({
							url : PATH + 'equipment/equipment!findOutRecordsView.do',
							params : {
								currentId : sel[0].data.id
							},
							callback : function(opt, success, response) {
								var result = Ext.decode(response.responseText).result;
								if (result) {
									if (result.lastinput) {
										result.lastinput = new Date(result.lastinput)
									}
									if (result.outdate) {
										result.outdate = new Date(result.outdate)
									}
									if (result.returndate) {
										result.returndate = new Date(result.returndate)
									}
								}
								var course = makeNewObjAddPrefix(result, 'outRecordsView.')
								returnForm.form.setValues(course)
							}
						})
				if (!returnWin) {
					returnWin = new Ext.Window({
								width : 580,
								height : 330,
								autoScroll : true,
								closable : false,
								title : '设备归还',
								modal : true,
								layout : 'fit',
								border : false,
								items : returnForm,
								buttons : [{
											text : '保存',
											scale : 'medium',
											disabled : false,
											handler : function() {
												var sel = Ext.getCmp('equipmentGrid').getSelectionModel().getSelection()
												Ext.Ajax.request({
															url : PATH + 'equipment/equipment!returnBackEquipment.do',
															params : {
																currentId : sel[0].data.id,
																returndate : Ext.getCmp('returndate').getValue(),
																outputmark : Ext.getCmp('outputmark').getValue()
															},
															callback : function(opt, success, response) {
																Ext.Msg.alert('提示', '归还成功！')
																returnForm.form.reset();// 清空表单
																returnWin.hide();
																Ext.getCmp('equipmentGrid').getStore().reload();
															}
														})
											}
										}, {
											text : '取消',
											scale : 'medium',
											handler : function() {
												returnForm.form.reset();// 清空表单
												returnWin.hide();
											}
										}]
							});
				} else {
					returnWin.setTitle('设备归还')
				}
				returnWin.show('returnForm');// 显示此窗口

			} else {
				Ext.Msg.alert('提示', '该状态无法编辑')
			}
		} else {
			Ext.Msg.alert('提示', '请选择要归还的设备。');
		}
	}

}
function addBtnsHandler() {
	if (equipmentForm.form.isValid()) {
		equipmentForm.form.submit({
					url : PATH + 'equipment/equipment!editOutput.do',
					submitEmptyText : false,
					waitMsg : '正在保存数据，稍后...',
					success : function(form, action) {
						Ext.Msg.alert('保存成功', '操作成功！');
						equipmentForm.form.reset();// 清空表单
						Ext.getCmp('equipmentGrid').getStore().reload();
						newFormWin.hide();
					},
					failure : function(form, response) {
						var errors = Ext.decode(response.response.responseText).res
						var errorStr = '<font color="red">'
						for (var i = 1; i < errors.length; i++) {
							errorStr += errors[i].defaultMessage + '<br>'
						}
						errorStr += '</font>'
						Ext.Msg.alert('保存失败', errorStr)
					}
				});
	} else {
		Ext.Msg.alert('信息', '请填写完成再提交!');
	}
}

function addUser(records) {
	Ext.getCmp(currentPress).setValue(records[0].data.name)
}

function addEquipment(records) {
	Ext.getCmp(currentPress).setValue(records[0].data.dno);
	Ext.getCmp(currentPress).fireEvent('keyup')
}
