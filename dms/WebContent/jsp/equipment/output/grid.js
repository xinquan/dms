var _config
var eid = getUrlParam('eid')
function fillGrid() {
	_config = {
		equipmentGrid : {
			id : 'equipmentGrid',
			fields : ['dno', 'name', 'state', 'department', 'lastinput', 'outpeople', 'usepeople', 'outdate', 'returndate', 'mark', 'eid', 'id'],
			headers : ['设备编号', '设备名称', '状态', '归属部门', '入库时间', '经手人', '使用人', '借出时间', '归还时间', '备注'],
			widths : [100, 120, 60, 100, 100, 80, 80, 100, 100, 300],
			renderers : [tip, tip, tip, tip, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, tip, tip, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, tip],
			url : PATH + 'equipment/equipment!outRecordsListAll.do',
			split : true,
			query : true,
			queryField : 'dno,name,state,department,outpeople,usepeople',
			autoLoad : true,
			storeCallback : isQuery,
			hasPage : true,
			viewConfig : {
				forceFit : true,
				getRowClass : function(record, rowIndex, rowParams, store) {
					var type = record.get('state');
					if ('报废' == type) {
						return 'x-grid-row-gray';
					}
				}
			},
			tbar : [{
						id : 'addBtn',
						iconCls : 'add',
						text : '出库',
						tooltip : '添加出库记录',
						handler : function() {
							editFormWin('add')
						}
					},
					// {
					// id : 'editBtn',
					// iconCls : 'edit',
					// text : '修改记录',
					// tooltip : '修改记录',
					// handler : function() {
					// editFormWin('edit')
					// }
					// }, {
					// id : 'delBtn',
					// iconCls : 'delete',
					// text : '删除',
					// tooltip : '删除选中的记录',
					// handler : function() {
					// var _records =
					// Ext.getCmp('courseGrid').getSelectionModel().getSelection();
					// delRecords(_records, 'Output', true)
					// }
					// },
					{
						id : 'showBtn',
						iconCls : 'information',
						text : ' 查看设备信息',
						tooltip : '查看设备详细信息',
						handler : function() {
							editFormWin('show')
						}
					}, {
						id : 'showPartsBtn',
						iconCls : 'borrow',
						text : ' 查看配件信息',
						tooltip : '查看配件详细信息',
						handler : function() {
							var _records = Ext.getCmp('equipmentGrid').getSelectionModel().getSelection();
							if (_records.length == 1) {
								showWindow(PATH + 'equipment/equipment!showPartsOut.do?eid=' + _records[0].data.eid, "查看配件信息", 800, 600, "yes");
							} else {
								Ext.Msg.alert('查看信息', '您必须先选择一条记录！');
							}
						}
					}, {
						id : 'return',
						iconCls : 'return',
						text : '归还',
						tooltip : '归还设备',
						handler : function() {
							var _records = Ext.getCmp('equipmentGrid').getSelectionModel().getSelection();
							if (_records.length == 1 && _records[0].data.state != '已归还') {
								editFormWin('return')
							} else {
								Ext.Msg.alert('查看信息', '请选择一条状态为“出库”的记录！');
							}
						}
					}]
		}
	}
}
function autoSelFirst() {
	var model = Ext.getCmp('equipmentGrid').getSelectionModel()
	model.select(0)
}
function reloadGrid() {
	Ext.getCmp('equipmentGrid').getStore().reload({
				callback : isQuery
			})
}

function isQuery() {
	if (eid) {
		Ext.getCmp('addBtn').setVisible(false)
		Ext.getCmp('showBtn').setVisible(false)
		Ext.getCmp('showPartsBtn').setVisible(false)
		Ext.getCmp('return').setVisible(false)
		Ext.getCmp('equipmentGrid').getStore().load({
					params : {
						eid : eid
					}
				})
	}
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('课程设置', 'fit', [createGrid(_config.equipmentGrid)])
			Ext.Ajax.request({
						url : PATH + 'system/menu!getRights.do',
						params : {
							action : this.location.pathname,
							roleids : _roleids
						},
						success : function(response) {
							perm = Ext.decode(response.responseText)
							if (!perm.edit) {
								Ext.getCmp('addBtn').setVisible(false)
								Ext.getCmp('return').setVisible(false)
							}
							if (perm.department) {
								reloadGrid(perm.department)
							} else {
								reloadGrid()
								autoSelFirst()
							}
						},
						failure : function() {
							Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
						}
					});
		});