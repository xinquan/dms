var _config
var passWin
function fillGrid() {
	_config = {
		courseGrid : {
			id : 'courseGrid',
			fields : ['dno', 'name', 'state', 'department', 'brand', 'model', 'standard', 'weight', 'lastinput', 'start', 'end', 'outputdate', 'damagedate', 'repairdate', 'id'],
			headers : ['设备编号', '设备名称', '状态', '归属部门', '品牌', '型号', '规格', '重量', '入库时间', '使用年限（开始）', '使用年限（结束）', '出库时间', '报废时间', '维护时间'],
			widths : [100, 120, 60, 100, 100, 80, 80, 80, 100, 120, 120, 100, 100, 100, 100],
			renderers : [tip, tip, tip, tip, tip, tip, tip, tip, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}],
			url : PATH + 'equipment/equipment!equipmentListAll.do',
			params : {
				state : '报废'
			},
			split : true,
			autoLoad : true,
			hasPage : true,
			viewConfig : {
				forceFit : true,
				getRowClass : function(record, rowIndex, rowParams, store) {
					var type = record.get('state');
					if ('报废' == type) {
						return 'x-grid-row-gray';
					}
				}
			},
			tbar : [
//				{
//						id : 'editBtn',
//						iconCls : 'edit',
//						text : '还原到损坏状态',
//						tooltip : '还原到损坏状态',
//						handler : function() {
//							var sel = Ext.getCmp('courseGrid').getSelectionModel().getSelection();
//							var courseIds = []
//							for (var i = 0; i < sel.length; i++) {
//								courseIds.push(sel[i].data.id);
//							}
//							Ext.Ajax.request({
//										url : PATH + 'equipment/equipment!returnToDamaged.do',
//										params : {
//											state : '损坏',
//											jsonString : courseIds
//										},
//										success : function(response) {
//											msgShow('提示', '还原成功')
//											reloadGrid()
//										},
//										failure : function() {
//											Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
//										}
//									});
//						}
//					}, 
						{
						id : 'delBtn',
						iconCls : 'delete',
						text : '彻底删除',
						tooltip : '删除选中的记录',
						handler : function() {
							var _records = Ext.getCmp('courseGrid').getSelectionModel().getSelection();
							delRecords(_records, 'Equipment', true)
						}
					}, {
						id : 'clearBtn',
						iconCls : 'recycle',
						text : '清空回收站',
						tooltip : '清空回收站',
						handler : function() {
							Ext.Msg.confirm('系统提示', '确定要清空回收站吗？此操作无法撤回', function(btn) {
										if (btn == 'yes') {
											Ext.Ajax.request({
														url : PATH + 'equipment/equipment!clearEquipment.do',
														success : function(response) {
															msgShow('提示', '已清空回收站')
															reloadGrid()
														},
														failure : function() {
															Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
														}
													});
										}
									}, this);
						}
					}]
		}
	}
}
function autoSelFirst() {
	var model = Ext.getCmp('courseGrid').getSelectionModel()
	model.select(0)
}
function reloadGrid() {
	Ext.getCmp('courseGrid').getStore().reload()
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('课程设置', 'fit', [createGrid(_config.courseGrid)])
			Ext.Ajax.request({
						url : PATH + 'system/menu!getRights.do',
						params : {
							action : this.location.pathname,
							roleids : _roleids
						},
						success : function(response) {
							perm = Ext.decode(response.responseText)
							if (!perm.edit) {
								Ext.getCmp('addBtn').setVisible(false)
								Ext.getCmp('editBtn').setVisible(false)
								Ext.getCmp('delBtn').setVisible(false)
								// Ext.getCmp('seeBtn').setVisible(false)
								// Ext.getCmp('submitBtn').setVisible(false)
								// Ext.getCmp('exportWBtn').setVisible(false)
								// Ext.getCmp('exportEBtn').setVisible(false)
								// Ext.getCmp('importBtn').setVisible(false)
								// Ext.getCmp('passedBtn').setVisible(false)
							}
							if (perm.department) {
								reloadGrid(perm.department)
							} else {
								reloadGrid()
								autoSelFirst()
							}
						},
						failure : function() {
							Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
						}
					});
		});