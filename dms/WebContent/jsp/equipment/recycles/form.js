var newFormWin
var step
var platform
Ext.define('depart', {
			extend : 'Ext.data.Model',
			fields : [{
						name : 'id',
						type : 'string'
					}, {
						name : 'deptid',
						type : 'string'
					}, {
						name : 'deptname',
						type : 'string'
					}]
		});
var departs = Ext.create('Ext.data.Store', {
			model : 'depart',
			proxy : {
				type : 'ajax',
				url : PATH + "system/system!departList.do",
				actionMethods : {
					read : 'POST'
				},
				reader : {
					type : 'json',
					root : 'res'
				}
			},
			autoLoad : true
		});
var equipmentForm = new Ext.FormPanel({
			border : false,
			waitMsgTarget : true,
			layout : {
				type : 'table',
				columns : 2
			},
			defaults : {
				width : 280,
				labelWidth : 110,
				xtype : 'textfield',
				labelAlign : 'right',
				anchor : "100%"
			},
			items : [{
						fieldLabel : '设备编号',
						id : 'dno',
						name : 'equipment.dno',
						afterLabelTextTpl : required,
						allowBlank : false
					}, {
						fieldLabel : '设备名称',
						name : 'equipment.name',
						afterLabelTextTpl : required,
						allowBlank : false
					}, {
						xtype : 'combo',
						fieldLabel : '归属部门',
						store : departs,
						mode : 'remote',
						afterLabelTextTpl : required,
						allowBlank : false,
						queryMode : 'local',
						forceSelection : true,
						valueField : 'deptname',
						displayField : 'deptname',
						name : 'equipment.department',
						value : _deptname,
						id : 'deptname',
						triggerAction : 'all'
					}, {
						fieldLabel : '品牌',
						name : 'equipment.brand',
						afterLabelTextTpl : required,
						allowBlank : false
					}, {
						fieldLabel : '型号',
						name : 'equipment.model',
						afterLabelTextTpl : required,
						allowBlank : false
					}, {
						fieldLabel : '规格',
						name : 'equipment.standard',
						afterLabelTextTpl : required,
						allowBlank : false
					}, {
						fieldLabel : '重量',
						name : 'equipment.weight',
						afterLabelTextTpl : required,
						allowBlank : false
					}, {
						fieldLabel : '入库时间',
						xtype : 'datefield',
						name : 'equipment.lastinput',
						format : 'Y-m-d',
						afterLabelTextTpl : required,
						allowBlank : false
					}, {
						fieldLabel : '使用年限：从',
						xtype : 'datefield',
						name : 'equipment.start',
						format : 'Y-m-d',
						afterLabelTextTpl : required,
						allowBlank : false
					}, {
						fieldLabel : '到',
						xtype : 'datefield',
						name : 'equipment.end',
						format : 'Y-m-d',
						afterLabelTextTpl : required,
						allowBlank : false
					}, {
						fieldLabel : '出库时间',
						xtype : 'datefield',
						format : 'Y-m-d',
						name : 'equipment.outputdate'
					}, {
						fieldLabel : '损坏时间',
						xtype : 'datefield',
						format : 'Y-m-d',
						name : 'equipment.damagedate'
					}, {
						fieldLabel : '维护时间',
						xtype : 'datefield',
						format : 'Y-m-d',
						name : 'equipment.repairdate'
					}, {
						fieldLabel : '报废时间',
						xtype : 'datefield',
						format : 'Y-m-d',
						name : 'equipment.scapdate'
					},
					// , {
					// xtype : 'combo',
					// fieldLabel : '课程归属学院',
					// store : departs1,
					// mode : 'remote',
					// afterLabelTextTpl : required,
					// allowBlank : false,
					// queryMode : 'local',
					// forceSelection : true,
					// valueField : 'deptname',
					// displayField : 'deptname',
					// name : 'course.department',
					// value : _deptname,
					// id : 'deptname',
					// triggerAction : 'all'
					// }, {
					// xtype : 'combo',
					// fieldLabel : '面向专业',
					// store : majords,
					// mode : 'remote',
					// queryMode : 'local',
					// forceSelection : true,
					// valueField : 'majorName',
					// displayField : 'majorName',
					// name : 'course.major',
					// id : 'majorName',
					// triggerAction : 'all'
					// }, {
					// fieldLabel : '课程性质',
					// xtype : 'combo',
					// store : getDictStore(true, 'KCXZ'),
					// mode : 'remote',
					// valueField : 'detailName',
					// displayField : 'detailName',
					// editable : true,
					// name : 'course.kcxz',
					// id : 'kcxz',
					// triggerAction : 'all'
					// }, {
					// fieldLabel : '学分',
					// xtype : 'numberfield',
					// afterLabelTextTpl : required,
					// allowBlank : false,
					// name : 'course.xf'
					// }, {
					// fieldLabel : '(学分)',
					// xtype : 'numberfield',
					// name : 'course.qtxf'
					// }, {
					// fieldLabel : '【学分】',
					// xtype : 'numberfield',
					// name : 'course.zkhxf'
					// }, {
					// id : 'zxs',
					// fieldLabel : '总学时<br>(1周=16学时)',
					// name : 'course.zxs',
					// afterLabelTextTpl : required,
					// allowBlank : false,
					// xtype : 'numberfield'
					// }, {
					// fieldLabel : '理论学时',
					// name : 'course.llxs',
					// afterLabelTextTpl : required,
					// allowBlank : false,
					// xtype : 'numberfield'
					// }, {
					// fieldLabel : '集中性实践环节',
					// xtype : 'combo',
					// store : getDictStore(true, 'SJHJ'),
					// mode : 'remote',
					// valueField : 'detailName',
					// displayField : 'detailName',
					// editable : true,
					// name : 'course.sjhj',
					// id : 'sjhj',
					// triggerAction : 'all',
					// listeners : {
					// select : {
					// scope : this,
					// fn : function(combo, record, index) {
					// var value = combo.getValue()
					// Ext.getCmp('syhjbj').setValue(value)
					// }
					// }
					// }
					// }, {
					// fieldLabel : '实践环节标记',
					// xtype : 'combo',
					// store : getDictStore(true, 'SJHJ'),
					// mode : 'remote',
					// valueField : 'detailName',
					// displayField : 'detailName',
					// editable : true,
					// name : 'course.syhjbj',
					// id : 'syhjbj',
					// triggerAction : 'all'
					// }, {
					// fieldLabel : '公共基础必修课程',
					// xtype : 'combo',
					// store : getDictStore(true, 'SF'),
					// mode : 'remote',
					// valueField : 'detailName',
					// displayField : 'detailName',
					// editable : false,
					// name : 'course.ggjcbj',
					// id : 'ggjcbj',
					// triggerAction : 'all'
					// }, {
					// colspan : 2,
					// xtype : 'numberfield',
					// name : 'course.kknf',
					// fieldLabel : '开课年份',
					// value : parseInt(new Date().getFullYear().toString()),
					// anchor : '100%',
					// afterLabelTextTpl : required,
					// allowBlank : false
					// },
					{
						fieldLabel : '设备当前状态',
						xtype : 'combo',
						store : getDictStore(true, 'SBZT'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : true,
						name : 'equipment.state',
						id : 'state',
						afterLabelTextTpl : required,
						allowBlank : false,
						triggerAction : 'all'
					}, {
						xtype : 'panel',
						border : false
					}, {
						fieldLabel : '备注',
						colspan : 2,
						width : 560,
						xtype : 'textarea',
						name : 'equipment.mark'
					}, {
						hidden : true,
						fieldLabel : '部门id',
						name : 'equipment.departmentid'
					}, {
						id : 'id',
						name : 'equipment.id',
						hidden : true
					}]
		});
var editFormWin = function(opt) {
	if (opt == 'edit') {
		var sel = Ext.getCmp('courseGrid').getSelectionModel().getSelection()
		if (sel.length == 1) {
			if (sel[0].data.state == 0 || sel[0].data.state == -1 || (_uid == 1 && _username == 'admin')) {
				Ext.Ajax.request({
							url : PATH + 'equipment/equipment!findEquipment.do',
							params : {
								currentId : sel[0].data.id
							},
							callback : function(opt, success, response) {
								var result = Ext.decode(response.responseText).result;
								if (result) {
									if (result.lastinput) {
										result.lastinput = new Date(result.lastinput)
									}
									if (result.outputdate) {
										result.outputdate = new Date(result.outputdate)
									}
									if (result.damagedate) {
										result.damagedate = new Date(result.damagedate)
									}
									if (result.repairdate) {
										result.repairdate = new Date(result.repairdate)
									}
									if (result.scapdate) {
										result.scapdate = new Date(result.scapdate)
									}
									if (result.start) {
										result.start = new Date(result.start)
									}
									if (result.end) {
										result.end = new Date(result.end)
									}
								}
								var course = makeNewObjAddPrefix(result, 'equipment.')
								equipmentForm.form.setValues(course)
							}
						})
				if (!newFormWin) {
					newFormWin = new Ext.Window({
								width : 580,
								height : 430,
								autoScroll : true,
								closable : false,
								title : '修改设备信息',
								modal : true,
								layout : 'fit',
								border : false,
								items : equipmentForm,
								buttons : [{
											text : '保存',
											scale : 'medium',
											disabled : false,
											handler : addBtnsHandler
										}, {
											text : '取消',
											scale : 'medium',
											handler : function() {
												equipmentForm.form.reset();// 清空表单
												newFormWin.hide();
											}
										}]
							});
				} else {
					newFormWin.setTitle('修改设备信息')
				}
				newFormWin.show('equipmentForm');// 显示此窗口

			} else {
				Ext.Msg.alert('提示', '该状态无法编辑')
			}
		} else {
			Ext.Msg.alert('提示', '请选择要修改的设备。');
		}
	} else if (opt == 'add') {
		if (!newFormWin) {
			newFormWin = new Ext.Window({
						width : 580,
						height : 430,
						closable : false,
						autoScroll : true,
						title : '添加设备',
						modal : true,
						border : false,
						layout : 'fit',
						items : equipmentForm,
						buttonAlign : 'center',
						buttons : [{
									text : '保存',
									scale : 'medium',
									disabled : false,
									handler : addBtnsHandler
								}, {
									text : '取消',
									scale : 'medium',
									handler : function() {
										equipmentForm.form.reset();// 清空表单
										newFormWin.hide();
									}
								}]
					});
		} else {
			newFormWin.setTitle('添加设备')
		}
		newFormWin.show('equipmentForm');// 显示此窗口
	}

}

function addBtnsHandler() {
	if (equipmentForm.form.isValid()) {
		equipmentForm.form.submit({
					url : PATH + 'equipment/equipment!editEquipment.do',
					submitEmptyText : false,
					waitMsg : '正在保存数据，稍后...',
					success : function(form, action) {
						if (action.result.result.state == '损坏，待维护' || action.result.result.state == '维护中' || action.result.result.state == '已维护') {
							Ext.Msg.alert('保存成功', '设备已损坏/正在维护中，请在设备维护菜单下完善损坏/维修记录（已经生成）。');
						} else {
							Ext.Msg.alert('保存成功', '保存成功。');
						}
						equipmentForm.form.reset();// 清空表单
						Ext.getCmp('courseGrid').getStore().reload();
						newFormWin.hide();
					},
					failure : function(form, response) {
						var errors = Ext.decode(response.response.responseText).res
						var errorStr = '<font color="red">'
						for (var i = 1; i < errors.length; i++) {
							errorStr += errors[i].defaultMessage + '<br>'
						}
						errorStr += '</font>'
						Ext.Msg.alert('保存失败', errorStr)
					}
				});
	} else {
		Ext.Msg.alert('信息', '请填写完成再提交!');
	}
}

function downloadWord(selid) {
	// window.location.href = 'down_plan?currentId='
	// + sel[0].data.id
	if (!Ext.fly('downForm')) {
		var downForm = document.createElement('form');
		downForm.id = 'downForm';
		downForm.name = 'downForm';
		downForm.className = 'x-hidden';
		downForm.action = PATH + 'plan/plan!exportCourseToWord.do';
		downForm.method = 'post';
		// downForm .target = '_blank'; //打开新的下载页面
		var data = document.createElement('input');
		data.type = 'hidden';// 隐藏域
		data.name = 'currentId';// form表单参数
		data.value = selid;// form表单值
		downForm.appendChild(data);
		document.body.appendChild(downForm);
	}
	Ext.fly('downForm').dom.submit();
	if (Ext.fly('downForm')) {
		document.body.removeChild(downForm);
	}
}

function downloadExcel() {
	if (!Ext.fly('downForm')) {
		var downForm = document.createElement('form');
		downForm.id = 'downForm';
		downForm.name = 'downForm';
		downForm.className = 'x-hidden';
		downForm.action = PATH + 'plan/plan!exportCourseToExcel.do';
		downForm.method = 'post';
		// downForm .target = '_blank'; //打开新的下载页面
		var data = document.createElement('input');
		data.type = 'hidden';// 隐藏域
		downForm.appendChild(data);
		document.body.appendChild(downForm);
	}
	Ext.fly('downForm').dom.submit();
	if (Ext.fly('downForm')) {
		document.body.removeChild(downForm);
	}
}