var _config
var pid = getUrlParam('pid')
var belong = getUrlParam('belong')
if (belong == undefined) {
	belong = true
} else if (belong == 'false') {
	belong = false
}
function fillGrid() {
	_config = {
		equipmentGrid : {
			id : 'equipmentGrid',
			fields : ['dno', 'name', 'state', 'department', 'brand', 'model', 'standard', 'weight', 'lastinput', 'start', 'end', 'outputdate', 'damagedate', 'repairdate', 'scapdate', 'id'],
			headers : ['设备编号', '设备名称', '状态', '归属部门', '品牌', '型号', '规格', '重量', '入库时间', '使用年限（开始）', '使用年限（结束）', '出库时间', '损坏时间', '维护时间', '报废时间'],
			widths : [100, 120, 60, 100, 100, 80, 80, 80, 100, 120, 120, 100, 100, 100, 100],
			renderers : [tip, tip, tip, tip, tip, tip, tip, tip, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}],
			url : PATH + 'equipment/equipment!equipmentListAll.do',
			params : {
				belong : belong,
				pid : pid
			},
			tbar : [{
						id : 'addBtn',
						text : '确定',
						iconCls : 'add',
						handler : function() {
							var records = Ext.getCmp('equipmentGrid').getSelectionModel().getSelection();
							if (records.length != 1) {
								msgShow('提示信息', '您选择了多台设备，请选择一台设备')
							} else {
								if (records[0].data.state != '在库') {
									Ext.Msg.alert('提示', '当前设备不在库，请在该设备归还后再操作！')
								} else {
									window.opener.addEquipment(records)
									window.close()
								}
							}
						}
					}],
			split : true,
			autoLoad : true,
			hasPage : true,
			query : true,
			queryField : 'dno,name,state,department,brand,model,standard',
			viewConfig : {
				forceFit : true,
				getRowClass : function(record, rowIndex, rowParams, store) {
					var type = record.get('state');
					if ('报废' == type) {
						return 'x-grid-row-gray';
					}
				}
			}
		}
	}
}

Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('设备列表', 'fit', [createGrid(_config.equipmentGrid)])
			if (belong) {
				Ext.getCmp('addBtn').setVisible(false)
			} else {
				Ext.getCmp('addBtn').setVisible(true)
			}
		});