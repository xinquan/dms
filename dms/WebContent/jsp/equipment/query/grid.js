var _config
var passWin
function fillGrid() {
	_config = {
		courseGrid : {
			id : 'courseGrid',
			fields : ['dno', 'name', 'state', 'department', 'brand', 'model', 'standard', 'weight', 'lastinput', 'start', 'end', 'outputdate', 'repairdate', 'damagedate', 'id'],
			headers : ['设备编号', '设备名称', '状态', '归属部门', '品牌', '型号', '规格', '重量', '入库时间', '使用年限（开始）', '使用年限（结束）', '出库时间', '维护时间', '报废时间'],
			widths : [100, 120, 60, 100, 100, 80, 80, 80, 100, 120, 120, 100, 100, 100, 100],
			renderers : [tip, tip, tip, tip, tip, tip, tip, tip, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}],
			url : PATH + 'equipment/equipment!equipmentListAll.do',
			params : {
				equipquery : true
			},
			split : true,
			tbar : [{
						id : 'showBtn',
						iconCls : 'information',
						text : ' 查看设备信息',
						tooltip : '查看设备详细信息',
						handler : function() {
							editFormWin('show')
						}
					}, {
						id : 'outputBtn',
						iconCls : 'information',
						text : ' 查看出库信息',
						tooltip : '查看出库信息',
						handler : function() {
							var _records = Ext.getCmp('courseGrid').getSelectionModel().getSelection();
							if (_records.length == 1) {
								showWindow(PATH + 'equipment/equipment!showOutputs.do?eid=' + _records[0].data.id, "查看出库信息", 800, 600, "yes");
							} else {
								Ext.Msg.alert('查看信息', '您必须先选择一条记录！');
							}
						}
					}, {
						id : 'repairBtn',
						iconCls : 'information',
						text : ' 查看维护信息',
						tooltip : '查看维护信息',
						handler : function() {
							var _records = Ext.getCmp('courseGrid').getSelectionModel().getSelection();
							if (_records.length == 1) {
								showWindow(PATH + 'equipment/equipment!showMaintains.do?eid=' + _records[0].data.id, "查看维护信息", 800, 600, "yes");
							} else {
								Ext.Msg.alert('查看信息', '您必须先选择一条记录！');
							}
						}
					}, {
						iconCls : 'borrow',
						text : '查看配件',
						handler : function() {
							var _records = Ext.getCmp('courseGrid').getSelectionModel().getSelection();
							if (_records.length == 1) {
								showWindow(PATH + 'equipment/equipment!showPartsOut.do?eid=' + _records[0].data.id, "查看配件信息", 800, 600, "yes");
							} else {
								Ext.Msg.alert('查看信息', '您必须先选择一条记录！');
							}
						}
					}, {
						iconCls : 'excel',
						text : '导出Excel',
						handler : function() {
							window.location = PATH + 'equipment/equipment!downloadEquipmentsToExcel.do?fileFileName=' + '设备信息列表'
						}
					}],
			query : true,
			queryField : 'dno,name,state,department,brand,model,standard',
			autoLoad : true,
			hasPage : true,
			viewConfig : {
				forceFit : true,
				getRowClass : function(record, rowIndex, rowParams, store) {
					var type = record.get('state');
					if ('报废' == type) {
						return 'x-grid-row-gray';
					}
				}
			}
		},
		partsGrid : {
			id : 'partsGrid',
			fields : ['pno', 'name', 'state', 'department', 'brand', 'model', 'standard', 'weight', 'lastinput', 'start', 'end', 'outputdate', 'damagedate', 'repairdate', 'scapdate', 'id'],
			headers : ['设备编号', '设备名称', '状态', '归属部门', '品牌', '型号', '规格', '重量', '入库时间', '使用年限（开始）', '使用年限（结束）', '出库时间', '损坏时间', '维护时间', '报废时间'],
			widths : [100, 120, 60, 100, 100, 80, 80, 80, 100, 120, 120, 100, 100, 100, 100],
			renderers : [tip, tip, tip, tip, tip, tip, tip, tip, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}],
			url : PATH + 'equipment/equipment!partsListAll.do',
			autoLoad : true,
			region : 'center',
			query : true,
			queryField : 'pno,name,state,department,brand,model,standard',
			hasPage : true,
			border : false,
			tbar : [{
						iconCls : 'camera',
						text : '查看所属设备',
						tooltip : '查看所属设备',
						handler : function() {
							var records = Ext.getCmp('partsGrid').getSelectionModel().getSelection();
							if (records.length == 1) {
								showWindow(PATH + 'equipment/equipment!showEquipment.do?pid=' + records[0].data.id, "借用配件", 800, 600, "yes");
							} else {
								Ext.Msg.alert('查看所属设备', '您必须先选择配件！');
							}
						}
					}, {
						iconCls : 'excel',
						text : '导出Excel',
						tooltip : '导出Excel',
						handler : function() {
							window.location = PATH + 'equipment/equipment!downloadPartsToExcel.do?fileFileName=' + '配件信息列表'
						}
					}]
		}
	}
}
function autoSelFirst() {
	var model = Ext.getCmp('courseGrid').getSelectionModel()
	model.select(0)
}
function reloadGrid() {
	Ext.getCmp('courseGrid').getStore().reload()
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('课程设置', 'fit', [{
								xtype : 'tabpanel',
								items : [{
											title : '设备区',
											layout : 'fit',
											items : createGrid(_config.courseGrid)
										}, {
											title : '配件区',
											layout : 'fit',
											items : createGrid(_config.partsGrid)
										}]
							}])
			Ext.Ajax.request({
						url : PATH + 'system/menu!getRights.do',
						params : {
							action : this.location.pathname,
							roleids : _roleids
						},
						success : function(response) {
							perm = Ext.decode(response.responseText)
							if (!perm.edit) {
								Ext.getCmp('addBtn').setVisible(false)
								Ext.getCmp('editBtn').setVisible(false)
								Ext.getCmp('delBtn').setVisible(false)
							}
							if (perm.department) {
								reloadGrid(perm.department)
							} else {
								reloadGrid()
								autoSelFirst()
							}
						},
						failure : function() {
							Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
						}
					});
		});