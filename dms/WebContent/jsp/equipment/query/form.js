var showWin
var step
var platform
var showForm = new Ext.FormPanel({
			border : false,
			waitMsgTarget : true,
			layout : {
				type : 'table',
				columns : 2
			},
			defaults : {
				width : 280,
				labelWidth : 110,
				xtype : 'displayfield',
				labelAlign : 'right',
				anchor : "100%"
			},
			items : [{
						fieldLabel : '设备编号',
						name : 'equipment.dno'
					}, {
						fieldLabel : '设备名称',
						name : 'equipment.name'
					}, {
						fieldLabel : '归属部门',
						name : 'equipment.department'
					}, {
						fieldLabel : '品牌',
						name : 'equipment.brand'
					}, {
						fieldLabel : '型号',
						name : 'equipment.model'
					}, {
						fieldLabel : '规格',
						name : 'equipment.standard'
					}, {
						fieldLabel : '重量',
						name : 'equipment.weight'
					}, {
						fieldLabel : '入库时间',
						xtype : 'datefield',
						readOnly : true,
						name : 'equipment.lastinput',
						format : 'Y-m-d'
					}, {
						fieldLabel : '使用年限：从',
						xtype : 'datefield',
						readOnly : true,
						format : 'Y-m-d',
						name : 'equipment.start'
					}, {
						fieldLabel : '到',
						xtype : 'datefield',
						readOnly : true,
						format : 'Y-m-d',
						name : 'equipment.end'
					}, {
						fieldLabel : '借出时间',
						xtype : 'datefield',
						readOnly : true,
						format : 'Y-m-d',
						name : 'equipment.outputdate'
					},
					// {
					// fieldLabel : '损坏时间',
					// xtype : 'datefield',
					// readOnly : true,
					// format : 'Y-m-d',
					// name : 'equipment.damagedate'
					// }, {
					// fieldLabel : '维护时间',
					// xtype : 'datefield',
					// readOnly : true,
					// format : 'Y-m-d',
					// name : 'equipment.repairdate'
					// }, {
					// fieldLabel : '报废时间',
					// xtype : 'datefield',
					// readOnly : true,
					// format : 'Y-m-d',
					// name : 'equipment.scapdate'
					// },
					{
						fieldLabel : '设备当前状态',
						name : 'equipment.state'
					}, {
						fieldLabel : '备注',
						colspan : 2,
						width : 560,
						readOnly : true,
						xtype : 'textarea',
						name : 'equipment.mark'
					}]
		});
var editFormWin = function(opt) {
	if (opt == 'show') {
		var sel = Ext.getCmp('courseGrid').getSelectionModel().getSelection()
		Ext.Ajax.request({
					url : PATH + 'equipment/equipment!findEquipment.do',
					params : {
						currentId : sel[0].data.id
					},
					callback : function(opt, success, response) {
						var result = Ext.decode(response.responseText).result;
						if (result) {
							if (result.lastinput) {
								result.lastinput = new Date(result.lastinput)
							}
							if (result.outputdate) {
								result.outputdate = new Date(result.outputdate)
							}
							if (result.damagedate) {
								result.damagedate = new Date(result.damagedate)
							}
							if (result.repairdate) {
								result.repairdate = new Date(result.repairdate)
							}
							if (result.scapdate) {
								result.scapdate = new Date(result.scapdate)
							}
							if (result.start) {
								result.start = new Date(result.start)
							}
							if (result.end) {
								result.end = new Date(result.end)
							}
						}
						var course = makeNewObjAddPrefix(result, 'equipment.')
						showForm.form.setValues(course)
					}
				})
		if (!showWin) {
			showWin = new Ext.Window({
						width : 580,
						height : 430,
						autoScroll : true,
						closable : false,
						title : '查看设备信息',
						modal : true,
						layout : 'fit',
						border : false,
						items : showForm,
						buttonAlign : 'center',
						buttons : [{
									text : '关闭',
									scale : 'medium',
									handler : function() {
										showForm.form.reset();// 清空表单
										showWin.hide();
									}
								}]
					});
		} else {
			showWin.setTitle('查看设备信息')
		}
		showWin.show('showForm');
	}
}

function addBtnsHandler() {
	if (equipmentForm.form.isValid()) {
		equipmentForm.form.submit({
					url : PATH + 'equipment/equipment!editEquipment.do',
					submitEmptyText : false,
					waitMsg : '正在保存数据，稍后...',
					success : function(form, action) {
						if (action.result.result.state == '损坏，待维护' || action.result.result.state == '维护中' || action.result.result.state == '已维护') {
							Ext.Msg.alert('保存成功', '设备已损坏/正在维护中，请在设备维护菜单下完善损坏/维修记录（已经生成）。');
						} else {
							Ext.Msg.alert('保存成功', '保存成功。');
						}
						equipmentForm.form.reset();// 清空表单
						Ext.getCmp('courseGrid').getStore().reload();
						newFormWin.hide();
					},
					failure : function(form, response) {
						var errors = Ext.decode(response.response.responseText).res
						var errorStr = '<font color="red">'
						for (var i = 1; i < errors.length; i++) {
							errorStr += errors[i].defaultMessage + '<br>'
						}
						errorStr += '</font>'
						Ext.Msg.alert('保存失败', errorStr)
					}
				});
	} else {
		Ext.Msg.alert('信息', '请填写完成再提交!');
	}
}

function downloadWord(selid) {
	// window.location.href = 'down_plan?currentId='
	// + sel[0].data.id
	if (!Ext.fly('downForm')) {
		var downForm = document.createElement('form');
		downForm.id = 'downForm';
		downForm.name = 'downForm';
		downForm.className = 'x-hidden';
		downForm.action = PATH + 'plan/plan!exportCourseToWord.do';
		downForm.method = 'post';
		// downForm .target = '_blank'; //打开新的下载页面
		var data = document.createElement('input');
		data.type = 'hidden';// 隐藏域
		data.name = 'currentId';// form表单参数
		data.value = selid;// form表单值
		downForm.appendChild(data);
		document.body.appendChild(downForm);
	}
	Ext.fly('downForm').dom.submit();
	if (Ext.fly('downForm')) {
		document.body.removeChild(downForm);
	}
}

function downloadExcel() {
	if (!Ext.fly('downForm')) {
		var downForm = document.createElement('form');
		downForm.id = 'downForm';
		downForm.name = 'downForm';
		downForm.className = 'x-hidden';
		downForm.action = PATH + 'plan/plan!exportCourseToExcel.do';
		downForm.method = 'post';
		// downForm .target = '_blank'; //打开新的下载页面
		var data = document.createElement('input');
		data.type = 'hidden';// 隐藏域
		downForm.appendChild(data);
		document.body.appendChild(downForm);
	}
	Ext.fly('downForm').dom.submit();
	if (Ext.fly('downForm')) {
		document.body.removeChild(downForm);
	}
}