var actionWin
var actionForm = new Ext.FormPanel({
			labelWidth : 10,
			frame : true,
			padding : '5 20 5 0',
			waitMsgTarget : true,
			defaults : {
				labelAlign : 'right'
			},
			defaultType : 'textfield',
			items : [{
						name : 'actionis.id',
						hidden : true
					}, {
						name : 'actionis.instantid',
						hidden : true
					}, {
						name : 'actionis.step',
						hidden : true
					}, {
						name : 'actionis.oprator',
						hidden : true
					}, {
						name : 'actionis.description',
						hidden : true
					}, {
						name : 'actionis.handlertype',
						hidden : true
					}, {
						name : 'actionis.pre',
						hidden : true
					}, {
						name : 'actionis.next',
						hidden : true
					}, {
						name : 'actionis.name',
						hidden : true
					}, {
						name : 'actionis.opratorid',
						hidden : true
					}, {
						name : 'actionis.roleid',
						hidden : true
					}, {
						name : 'actionis.instantclass',
						hidden : true
					}, {
						name : 'actionis.flowid',
						hidden : true
					}, {
						fieldLabel : '审批意见',
						xtype : 'textarea',
						name : 'actionis.result',
						anchor : '100%'
					}, {
						fieldLabel : '是否通过',
						xtype : 'combo',
						afterLabelTextTpl : required,
						allowBlank : false,
						store : getDictStore(true, 'SF'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : false,
						name : 'actionis.pass',
						triggerAction : 'all',
						anchor : '100%'
					}]
		});
var editAction = function() {
	var sel = Ext.getCmp('actionGrid').getSelectionModel().getSelection()
	if (sel.length == 1) {
		if (!actionWin) {
			actionWin = new Ext.Window({
						width : 400,
						height : 195,
						closable : true,
						autoScroll : true,
						title : '审批',
						modal : true,
						border : false,
						buttonAlign : 'center',
						items : actionForm,
						buttons : [{
									text : '保存',
									scale : 'medium',
									disabled : false,
									handler : function() {
										addActions()
									}
								}, {
									text : '取消',
									scale : 'medium',
									handler : function() {
										actionForm.form.reset();// 清空表单
										actionWin.hide();
									}
								}]
					});
		}
		actionWin.show();// 显示此窗口
		fillDataToForm(sel[0].data.id)
	} else {
		Ext.Msg.alert('提示', '请选择一个要审批的节点')
	}
}
function fillDataToForm(selid) {
	Ext.Ajax.request({
				url : PATH + 'system/flow!findActionInstant.do',
				params : {
					'actionid' : selid
				},
				method : 'GET',
				success : function(response, options) {
					var re = Ext.decode(response.responseText).result
					var formrec = makeNewObjAddPrefix(re, 'actionis.')
					actionForm.form.setValues(formrec)
				},
				failure : function(response, options) {
					Ext.MessageBox.alert('失败', '请求超时或网络故障,错误编号：' + response.status);
				}
			})
}
function showSubmitInfo(instantid) {
	Ext.Ajax.request({
				url : PATH + 'system/flow!showSubmitInfo.do',
				params : {
					instantid : instantid
				},
				method : 'GET',
				success : function(response, options) {
					var url = response.responseText
					showWindow(PATH + url, "查看待审批信息", 800, 600, "yes");
				},
				failure : function(response, options) {
					Ext.MessageBox.alert('失败', '请求超时或网络故障,错误编号：' + response.status);
				}
			})
}
function addActions() {
	if (actionForm.form.isValid()) {
		actionForm.form.submit({
					url : PATH + 'system/flow!checkActionInstant.do',
					waitMsg : '正在保存数据，稍后...',
					success : function(form, action) {
						Ext.Msg.alert('保存成功', '操作成功！');
						actionForm.form.reset();// 清空表单
						Ext.getCmp('actionGrid').getStore().reload();
						actionWin.hide();
					},
					failure : function(form, response) {
						var errors = Ext.decode(response.response.responseText).res
						var errorStr = '<font color="red">'
						for (var i = 1; i < errors.length; i++) {
							errorStr += errors[i].defaultMessage + '<br>'
						}
						errorStr += '</font>'
						Ext.Msg.alert('保存失败', errorStr)
					}
				});
	}
}