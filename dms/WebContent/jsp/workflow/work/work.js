var _config
var instantid = getUrlParam('id')
var gridArray = []
var gridJson
function fillGrid() {
	_config = {
		actionGrid : {
			id : 'actionGrid',
			height : 200,
			title : '审批节点',
			split : true,
			fields : ['name', 'description', 'handlertype', 'oprator', 'result', 'pass', 'rollback', 'step', 'pre', 'next', 'id', 'opratorid', 'instantid', 'handledate', 'instantclass'],
			headers : ['节点名', '说明', '操作者类型', '审批人', '审批意见', '是否通过', '备注'],
			widths : [170, 200, 100, 100, 100, 100, 120],
			renderers : [tip, tip, tip, tip, tip, tip, function(v) {
						if (v == 'submit') {
							return '退回后又提交上来的'
						}
					}],
			url : PATH + 'system/flow!listMyWorks.do',
			viewConfig : {
				forceFit : true,
				getRowClass : function(record, rowIndex, rowParams, store) {
					var type = record.get('pass');
					if ('是' == type) {
						return 'x-grid-row-green';
					} else if ('否' == type) {
						return 'x-grid-row-red';
					} else {
						return 'x-grid-row-gray';
					}
				}
			},
			params : {
				instantid : instantid
			},
			autoLoad : true,
			hasPage : false,
			tbar : [{
						iconCls : 'check',
						text : '审批',
						handler : function() {
							editAction()
						}
					}, {
						iconCls : 'flow',
						text : '查看流程',
						handler : function() {
							var sel = Ext.getCmp('actionGrid').getSelectionModel().getSelection()
							if (sel.length == 1) {
								showWindow(PATH + 'system/flow!showActionInfos.do?id=' + sel[0].data.instantid, "查看审批信息", 800, 600, "yes");
							} else {
								Ext.Msg.alert("条件缺失", "请选择一条记录！");
							}
						}
					}, {
						iconCls : 'contract',
						text : '查看待审批内容',
						handler : function() {
							var sel = Ext.getCmp('actionGrid').getSelectionModel().getSelection()
							if (sel.length == 1) {
								showSubmitInfo(sel[0].data.instantid);
							} else {
								Ext.Msg.alert("条件缺失", "请选择一条记录！");
							}
						}
					}]
		}
	}
}
function getGridJson() {
	var items = Ext.getCmp('actionGrid').store.data.items
	if (items.length != 0) {
		for (var i = 0; i < items.length; i++) {
			if (items[i].data.oprator == '') {
				Ext.Msg.alert('条件缺失', '请为流程的各个节点选择用户！');
				return false;
			} else {
				gridArray.push(items[i].data)
			}
		}
	}
	gridJson = JSON.stringify(gridArray)
	return true;
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('流程定义', 'fit', [createGrid(_config.actionGrid)])
		});