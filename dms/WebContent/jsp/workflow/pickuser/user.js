var _config
var flow = getUrlParam('flowname')
flow = _flowname[flow]
var instantid = getUrlParam('id')
var instantClass = getUrlParam('instantClass')
var gridArray = []
var gridJson
var roleid = 0
function fillGrid() {
	_config = {
		actionGrid : {
			id : 'actionGrid',
			region : 'north',
			height : 200,
			title : '审批节点',
			split : true,
			fields : ['name', 'description', 'handlertype', 'oprator', 'step', 'result', 'pre', 'next', 'pass', 'id', 'opratorid', 'roleid'],
			headers : ['节点名', '说明', '操作者类型', '审批人'],
			widths : [150, 200, 100, 200],
			renderers : [tip, tip, tip, tip],
			url : PATH + 'system/flow!pickUserToActions.do',
			params : {
				flowname : flow
			},
			tbar : [{
						iconCls : 'submit',
						text : '保存并提交',
						handler : function() {
							if (getGridJson()) {
								Ext.Ajax.request({
											url : PATH + 'system/flow!submitApprove.do',
											params : {
												instantClass : instantClass,
												flowname : flow,
												instantid : instantid,
												gridJson : gridJson
											},
											success : function() {
												window.opener.reloadGrid()
												Ext.Msg.alert('提示', '信息已提交,请等待审批。', function() {
															window.close();
														})
											},
											failure : function() {

											}

										})
							}
						}
					}],
			autoLoad : true,
			hasPage : false
		},
		userGrid : {
			id : 'userGrid',
			title : '为节点选择用户',
			region : 'center',
			fields : ['unum', 'name', 'major', 'department', 'uindex', 'email', 'registerdate', 'id', 'username', 'password', 'editable', 'sex', 'userid', 'roleid'],
			headers : ['工号/学号', '姓名', '所属专业', '所属学院', '排序'],
			widths : [100, 100, 100, 100, 100],
			filters : [true, true, true, true, false],
			filtersurl : [PATH + 'system/menu!queryUserListByParamsUnderRole.do', roleid],
			url : PATH + 'system/menu!queryUserListByParamsUnderRole.do',
			features : [{
						ftype : 'filters',
						local : false
					}],
			autoLoad : true,
			hasPage : true,
			border : false,
			tbar : [{
						iconCls : 'add',
						text : '添加审批人到节点',
						handler : function() {
							var records = Ext.getCmp('actionGrid').getSelectionModel().getSelection();
							if (records.length == 1) {
								var users = Ext.getCmp('userGrid').getSelectionModel().getSelection();
								if (users.length == 1) {
									records[0].data.opratorid = users[0].data.userid
									records[0].data.oprator = users[0].data.name
									Ext.getCmp('actionGrid').getView().refresh(false)
								} else {
									Ext.Msg.alert('条件缺失', '您必须选择一个用户作为该节点的审批人！');
								}

							} else {
								Ext.Msg.alert('条件缺失', '您必须选择一个流程节点以便给该节点添加审批人！');
							}
						}
					}]
		}
	}
}
function getGridJson() {
	gridArray = []
	var items = Ext.getCmp('actionGrid').store.data.items
	if (items.length != 0) {
		for (var i = 0; i < items.length; i++) {
			if (items[i].data.oprator == '') {
				Ext.Msg.alert('条件缺失', '请为流程的各个节点选择用户！');
				return false;
			} else {
				gridArray.push(items[i].data)
			}
		}
	}
	gridJson = JSON.stringify(gridArray)
	return true;
}
function initGridlsner() {
	Ext.getCmp('actionGrid').on('itemclick', function(t, record, item, index, e, eOpts) {
				roleid = record.data.roleid
				n1 = record.data.roleid
				Ext.getCmp('userGrid').store.reload({
							params : {
								roleid : roleid
							}
						})
			})
}
function reloadUserGrid() {
	Ext.getCmp('userGrid').store.reload({
				params : {
					roleid : roleid
				}
			})
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('流程定义', 'border', [createGrid(_config.userGrid), createGrid(_config.actionGrid)])
			initGridlsner()
		});