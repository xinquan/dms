var _config
var instantid = getUrlParam('id')
var gridArray = []
var gridJson
function fillGrid() {
	_config = {
		actionGrid : {
			id : 'actionGrid',
			height : 200,
			title : '审批节点',
			split : true,
			fields : ['name', 'description', 'handlertype', 'oprator', 'result', 'pass', 'handledate', 'step', 'pre', 'next', 'id', 'opratorid', 'rollback'],
			headers : ['节点名', '说明', '操作者类型', '审批人', '审批意见', '是否通过', '操作日期'],
			widths : [160, 200, 100, 100, 100, 100, 100],
			renderers : [tip, tip, tip, tip, tip, tip, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}],
			url : PATH + 'system/flow!showActions.do',
			viewConfig : {
				forceFit : true,
				getRowClass : function(record, rowIndex, rowParams, store) {
					var type = record.get('pass');
					var rollback = record.get('rollback')
					if ('是' == type) {
						return 'x-grid-row-green';
					} else if ('否' == type && ('' == rollback || null == rollback)) {
						return 'x-grid-row-orange';
					} else if ('否' == type && 'rollback' == rollback) {
						return 'x-grid-row-red';
					} else if ('否' == type && 'submit' == rollback) {
						return 'x-grid-row-yellow';
					} else {
						return 'x-grid-row-gray';
					}
				}
			},
			params : {
				instantid : instantid
			},
			autoLoad : true,
			hasPage : false
		}
	}
}
function getGridJson() {
	var items = Ext.getCmp('actionGrid').store.data.items
	if (items.length != 0) {
		for (var i = 0; i < items.length; i++) {
			if (items[i].data.oprator == '') {
				Ext.Msg.alert('条件缺失', '请为流程的各个节点选择用户！');
				return false;
			} else {
				gridArray.push(items[i].data)
			}
		}
	}
	gridJson = JSON.stringify(gridArray)
	return true;
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('流程定义', 'fit', [createGrid(_config.actionGrid)])
		});