var flowWin
var actionWin
var step
Ext.define('oprater', {
			extend : 'Ext.data.Model',
			fields : [{
						name : 'id',
						type : 'string'
					}, {
						name : 'name',
						type : 'string'
					}, {
						name : 'rindex',
						type : 'string'
					}]
		});
var opraters = Ext.create('Ext.data.Store', {
			model : 'oprater',
			proxy : {
				type : 'ajax',
				url : PATH + "system/menu!roleList.do",
				actionMethods : {
					read : 'POST'
				},
				reader : {
					type : 'json',
					root : 'res'
				}
			},
			autoLoad : true
		});
var actionForm = new Ext.FormPanel({
			labelWidth : 10,
			border : false,
			padding : '5 20 5 0',
			waitMsgTarget : true,
			defaults : {
				labelAlign : 'right'
			},
			defaultType : 'textfield',
			items : [{
						id : 'actionid',
						name : 'action.id',
						hidden : true
					}, {
						fieldLabel : '节点名',
						afterLabelTextTpl : required,
						allowBlank : false,
						name : 'action.name',
						id : 'actionname',
						anchor : '100%'
					}, {
						fieldLabel : '节点序号',
						xtype : 'numberfield',
						allowDecimals : false, // 允许小数点
						maxValue : 50, // 最大值
						minValue : 1,
						afterLabelTextTpl : required,
						allowBlank : false,
						name : 'action.step',
						id : 'actionstep',
						anchor : '100%'
					}, {
						xtype : 'combo',
						fieldLabel : '操作者类型',
						store : opraters,
						editable : false,
						afterLabelTextTpl : required,
						allowBlank : false,
						mode : 'remote',
						valueField : 'name',
						displayField : 'name',
						name : 'action.handlertype',
						id : 'rolename',
						triggerAction : 'all',
						anchor : '100%',
						listeners : {
							select : {
								scope : this,
								fn : function(combo, record, index) {
									var value = combo.getValue()
									var rec = combo.findRecord(combo.valueField || combo.displayField, value);
									var recv = rec.data
									Ext.getCmp('roleid').setValue(recv.id)
								}
							}
						}
					}, {
						fieldLabel : '说明',
						name : 'action.description',
						xtype : 'textarea',
						id : 'actiondescription',
						anchor : '100%'
					}, {
						fieldLabel : '角色编号',
						name : 'action.roleid',
						hidden : true,
						id : 'roleid',
						anchor : '100%'
					}]
		});
var flowForm = new Ext.FormPanel({
			labelWidth : 10,
			border : false,
			padding : '5 20 5 0',
			waitMsgTarget : true,
			defaults : {
				labelAlign : 'right'
			},
			defaultType : 'textfield',
			items : [{
						id : 'id',
						name : 'flow.id',
						hidden : true,
						hideLabel : true,
						allowBlank : true
					}, {
						fieldLabel : '流程名',
						xtype : 'combo',
						afterLabelTextTpl : required,
						allowBlank : false,
						store : getDictStore(true, 'FLOW'),
						mode : 'remote',
						valueField : 'detailName',
						displayField : 'detailName',
						editable : false,
						name : 'flow.name',
						id : 'name',
						triggerAction : 'all',
						anchor : '100%'
					}, {
						fieldLabel : '说明',
						name : 'flow.description',
						xtype : 'textarea',
						id : 'description',
						anchor : '100%'
					}, {
						fieldLabel : '创建人',
						hidden : true,
						name : 'flow.createuser',
						id : 'createuser',
						anchor : '100%'
					}, {
						fieldLabel : '创建时间',
						hidden : true,
						// name : 'flow.createdate',
						id : 'createdate',
						anchor : '100%'
					}, {
						fieldLabel : '是否还在使用',
						hidden : true,
						name : 'flow.inuse',
						id : 'inuse',
						anchor : '100%'
					}]
		});
var editAction = function(sel) {
	if (!actionWin) {
		actionWin = new Ext.Window({
					width : 400,
					height : 280,
					closable : true,
					autoScroll : true,
					title : '新增审批节点信息',
					modal : true,
					border : false,
					layout : 'fit',
					buttonAlign : 'center',
					items : actionForm,
					buttons : [{
								text : '保存',
								scale : 'medium',
								disabled : false,
								handler : function() {
									if (actionForm.form.isValid()) {
										Ext.Ajax.request({
													url : PATH + 'system/flow!uniqueActionJudge.do',
													params : {
														flowid : sel.id,
														step : Ext.getCmp('actionstep').getValue()
													},
													success : function(result) {
														if (Ext.decode(result.responseText).success) {
															addActions(sel.id)
														} else {
															Ext.Msg.alert('提示', '节点序号重复!');
														}
													},
													failure : function() {
														Ext.Msg.alert('提示', '网络连接错误...');
													}
												})

									} else {
										Ext.Msg.alert('信息', '请填写完成再提交!');
									}
								}
							}, {
								text : '取消',
								scale : 'medium',
								handler : function() {
									actionForm.form.reset();// 清空表单
									actionWin.hide();
								}
							}]
				});
	} else {
		actionWin.setTitle('新增审批节点信息')
	}
	actionWin.show();
}
var editFlow = function(opt, sel) {
	if (opt == 'edit') {
		Ext.Ajax.request({
					url : PATH + 'system/flow!findFlowInfo.do',
					params : {
						flowid : sel.id
					},
					callback : function(opt, success, response) {
						var flow = makeNewObjAddPrefix(Ext.decode(response.responseText).result, 'flow.')
						flowForm.form.setValues(flow)
					}
				})
		if (!flowWin) {
			flowWin = new Ext.Window({
						width : 430,
						height : 220,
						closable : true,
						autoScroll : true,
						title : '编辑审批流程',
						modal : true,
						layout : 'fit',
						border : false,
						buttonAlign : 'center',
						items : flowForm,
						buttons : [{
									text : '保存',
									scale : 'medium',
									disabled : false,
									handler : addBtnsHandler
								}, {
									text : '取消',
									scale : 'medium',
									handler : function() {
										flowForm.form.reset();// 清空表单
										flowWin.hide();
									}
								}]
					});
		} else {
			flowWin.setTitle('编辑审批流程')
		}
		flowWin.show();// 显示此窗口
	} else {
		if (!flowWin) {
			flowWin = new Ext.Window({
						width : 400,
						height : 220,
						closable : true,
						autoScroll : true,
						title : '新增审批流程',
						modal : true,
						border : false,
						layout : 'fit',
						buttonAlign : 'center',
						items : flowForm,
						buttons : [{
									text : '保存',
									scale : 'medium',
									disabled : false,
									handler : function() {

										Ext.Ajax.request({
													url : PATH + 'system/flow!uniqueFlowJudge.do',
													params : {
														flowname : Ext.getCmp('name').getValue()
													},
													success : function(result) {
														if (Ext.decode(result.responseText).success) {
															addBtnsHandler()
														} else {
															Ext.Msg.alert('提示', '流程名重复!');
														}
													},
													failure : function() {
														Ext.Msg.alert('提示', '网络连接错误...');
													}
												})

									}
								}, {
									text : '取消',
									scale : 'medium',
									handler : function() {
										flowForm.form.reset();// 清空表单
										flowWin.hide();
									}
								}]
					});
		} else {
			flowWin.setTitle('新增审批流程')
		}
		flowWin.show();// 显示此窗口
	}

}
function addActions(flowid) {
	actionForm.form.submit({
				url : PATH + 'system/flow!editActionInfo.do',
				params : {
					flowid : flowid
				},
				waitMsg : '正在保存数据，稍后...',
				success : function(form, action) {
					Ext.Msg.alert('保存成功', '操作成功！');
					actionForm.form.reset();// 清空表单
					Ext.getCmp('actionGrid').getStore().reload();
					actionWin.hide();
				},
				failure : function(form, response) {
					var errors = Ext.decode(response.response.responseText).res
					var errorStr = '<font color="red">'
					for (var i = 1; i < errors.length; i++) {
						errorStr += errors[i].defaultMessage + '<br>'
					}
					errorStr += '</font>'
					Ext.Msg.alert('保存失败', errorStr)
				}
			});
}
function addBtnsHandler() {
	if (flowForm.form.isValid()) {
		flowForm.form.submit({
					url : PATH + 'system/flow!editFlowInfo.do',
					waitMsg : '正在保存数据，稍后...',
					success : function(form, action) {
						Ext.Msg.alert('保存成功', '操作成功！');
						flowForm.form.reset();// 清空表单
						reloadFlowGrid()
						flowWin.hide();
					},
					failure : function(form, response) {
						var errors = Ext.decode(response.response.responseText).res
						var errorStr = '<font color="red">'
						for (var i = 1; i < errors.length; i++) {
							errorStr += errors[i].defaultMessage + '<br>'
						}
						errorStr += '</font>'
						Ext.Msg.alert('保存失败', errorStr)
					}
				});
	} else {
		Ext.Msg.alert('信息', '请填写完成再提交!');
	}
}