var _config
var delIds = []
function fillGrid() {
	_config = {
		flowGrid : {
			id : 'flowGrid',
			region : 'north',
			height : 160,
			split : true,
			fields : ['name', 'description', 'createuser', 'createdate', 'state', 'inuse', 'id'],
			headers : ['流程名', '流程说明', '创建人', '创建日期', '是否保存', '是否还在使用'],
			renderers : [tip, tip, tip, function(v) {
						if (v) {
							return Ext.util.Format.date(new Date(v), 'Y-m-d')
						}
					}, function(v) {
						if (v == 'saved') {
							return '已保存'
						} else {
							return '未保存'
						}
					}, tip],
			widths : [160, 300, 100, 100, 100],
			url : PATH + 'system/flow!flowList.do',
			autoLoad : true,
			hasPage : false,
			border : false,
			tbar : [{
						iconCls : 'add',
						text : '新增审批流程',
						handler : function() {
							editFlow('add')
						}
					}, {
						iconCls : 'edit',
						text : '编辑审批流程',
						handler : function() {
							var sel = Ext.getCmp('flowGrid').getSelectionModel().getSelection()
							if (sel.length == 1) {
								if (sel[0].data.state == 'saved') {
									Ext.Msg.alert('提示', '该流程信息已保存无法修改！如果需要请停止使用该流程，再新建流程。')
								} else {
									editFlow('edit', sel[0].data)
								}
							} else {
								Ext.Msg.alert('提示', '请选择需要修改的流程信息。');
							}
						}
					}, {
						iconCls : 'save',
						text : '保存流程(节点信息将无法再修改)',
						handler : function() {
							var records = Ext.getCmp('flowGrid').getSelectionModel().getSelection();
							if (records) {
								Ext.Msg.confirm("是否要保存？", "保存后将无法修改(包括节点信息),是否要保存这些流程？", function(btn) {
											if (btn == "yes") {
												saveIds = []
												for (var i = 0; i < records.length; i++) {
													if (records[i].data.state != 'saved') {
														saveIds.push(records[i].data.id);
													}
												}
												Ext.Ajax.request({
															url : PATH + 'system/flow!saveFLowInfo.do',
															params : {
																'saveIds' : saveIds
															},
															success : function() {
																Ext.Msg.alert("保存流程信息成功", "您已经成功保存流程信息！", function() {
																			reloadFlowGrid()
																			Ext.getCmp('flowGrid').getSelectionModel().deselectAll()
																		});
															},
															failure : function() {
																Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
															}
														});
											}
										})
							}
						}
					}, {
						iconCls : 'delete',
						text : '停止使用',
						handler : function() {
							var records = Ext.getCmp('flowGrid').getSelectionModel().getSelection();
							if (records) {
								// 提示是否删除数据
								Ext.Msg.confirm("是否要停止使用该流程？", "停止使用后将无法修改(包括节点信息),是否要停止使用这些流程？", function(btn) {
											if (btn == "yes") {
												delIds = []
												for (var i = 0; i < records.length; i++) {
													delIds.push(records[i].data.id);
												}
												Ext.Ajax.request({
															url : PATH + 'system/flow!stopFLowInfo.do',
															params : {
																'delIds' : delIds
															},
															success : function() {
																Ext.Msg.alert("流程已停用", "您已经停止使用这些流程！请新建流程，以便审批。", function() {
																			reloadFlowGrid()
																			Ext.getCmp('flowGrid').getSelectionModel().deselectAll()
																		});
															},
															failure : function() {
																Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
															}
														});
											}
										})
							}
						}
					}]
		},
		actionGrid : {
			id : 'actionGrid',
			region : 'center',
			title : '流程节点定义',
			split : true,
			fields : ['name', 'description', 'handlertype', 'step', 'isfirst', 'islast', 'id'],
			headers : ['节点名', '说明', '操作者类型', '节点序号'],
			widths : [150, 200, 100, 100, 100, 100],
			renderers : [tip, tip, tip, tip, tip, tip],
			url : PATH + 'system/flow!actionList.do',
			autoLoad : false,
			hasPage : false,
			tbar : [{
						iconCls : 'add',
						text : '新增审批节点',
						tooltip : '新增审批节点',
						handler : function() {
							var sel = Ext.getCmp('flowGrid').getSelectionModel().getSelection()
							if (sel.length == 1) {
								if (sel[0].data.state == 'saved') {
									Ext.Msg.alert('提示', '该流程信息已保存无法修改！如果需要请停止使用该流程，再新建流程。')
								} else {
									editAction(sel[0].data)
								}
							} else {
								Ext.Msg.alert('信息', '请选择一条流程!');
							}
						}
					}, {
						iconCls : 'delete',
						text : '删除审批节点',
						tooltip : '删除选中的记录',
						handler : function() {
							var sel = Ext.getCmp('flowGrid').getSelectionModel().getSelection()
							if (sel.length == 1) {
								if (sel[0].data.state == 'saved') {
									Ext.Msg.alert('提示', '该流程信息已保存无法修改！如果需要请停止使用该流程，再新建流程。')
								} else {
									var items = Ext.getCmp('actionGrid').getSelectionModel().getSelection();
									if (items.length != 0) {
										// 提示是否删除数据
										Ext.Msg.confirm("是否要删除？", "是否要删除这些被选择的数据？", function(btn) {
													if (btn == "yes") {
														delIds = []
														for (var i = 0; i < items.length; i++) {
															delIds.push(items[i].data.id);
														}
														Ext.Ajax.request({
																	url : PATH + "system/flow!delActionInfo.do",
																	params : {
																		flowid : sel[0].data.id,
																		'delIds' : delIds
																	},
																	success : function() {
																		Ext.Msg.alert("删除信息成功", "您已经成功删除信息！");
																		Ext.getCmp('actionGrid').getStore().reload();
																	},
																	failure : function() {
																		Ext.Msg.alert('错误', '服务器出现错误请稍后再试！');
																	}
																});
													}
												});
									} else {
										Ext.Msg.alert('删除操作', '您必须选择一个要删除的节点！');
									}
								}
							} else {
								Ext.Msg.alert('信息', '请选择一条流程!');
							}
						}
					}]
		}
	}
}
function reloadFlowGrid() {
	Ext.getCmp('flowGrid').getStore().reload()
}
function reloadActionGrid() {
	Ext.getCmp('actionGrid').getStore().reload()
}
function initGridlsner() {
	Ext.getCmp('flowGrid').on('itemclick', function(t, record, item, index, e, eOpts) {
				Ext.getCmp('actionGrid').getStore().load({
							params : {
								flowid : record.data.id
							}
						})
			})
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('审批流程', 'border', [createGrid(_config.flowGrid), createGrid(_config.actionGrid)])
			initGridlsner()
		});