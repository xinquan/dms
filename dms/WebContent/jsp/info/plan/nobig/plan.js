var currentId = getUrlParam('id');
var gridArray = [];
var gridJson;
var majorid;
function fillDataToForm() {
	var form = Ext.getCmp('mainForm').getForm();
	Ext.Ajax.request({
				url : PATH + 'plan/plan!findMain.do',
				params : {
					'currentId' : currentId
				},
				method : 'GET',
				success : function(response, options) {
					var re = Ext.decode(response.responseText).result
					var form = Ext.getCmp('mainForm').getForm();
					var formrec = makeNewObjAddPrefix(re, 'planMain.')
					form.setValues(formrec)
				},
				failure : function(response, options) {
					Ext.MessageBox.alert('失败', '请求超时或网络故障,错误编号：' + response.status);
				}
			})
}

Ext.define('major', {
			extend : 'Ext.data.Model',
			fields : [{
						name : 'id',
						type : 'string'
					}, {
						name : 'majorId',
						type : 'string'
					}, {
						name : 'majorName',
						type : 'string'
					}, {
						name : 'majorYear',
						type : 'string'
					}, {
						name : 'category',
						type : 'string'
					}, {
						name : 'degree',
						type : 'string'
					}]
		});
var majords = Ext.create('Ext.data.Store', {
			model : 'major',
			proxy : {
				type : 'ajax',
				url : PATH + "school/school!bigMajorList.do",
				actionMethods : {
					read : 'POST'
				},
				extraParams : {
					deptId : _deptid,
					type : '大类'
				},
				reader : {
					type : 'json',
					root : 'res'
				}
			},
			autoLoad : true
		});

var form = Ext.create('Ext.form.Panel', {
			id : 'mainForm',
			frame : true,
			width : 770,
			region : 'center',
			autoScroll : true,
			title : '培养方案',
			titleAlign : 'center',
			buttonAlign : 'center',
			bodyPadding : '5 5 5',
			fieldDefaults : {
				labelAlign : 'left',
				msgTarget : 'side'
			},
			buttons : [{
						text : '查看课程指导性修读计划',
						scale : 'medium',
						disabled : false,
						handler : function() {
							showWindow(PATH + 'plan/plan!showCoursePlan.do?planid=' + currentId, "课程指导性修读计划", 800, 300, "yes");
						}
					}, {
						text : '查看课程构成及学分分配汇总表',
						scale : 'medium',
						disabled : false,
						handler : function() {
							showWindow(PATH + 'plan/plan!showDistribution.do?planid=' + currentId, "课程构成及学分分配汇总表", 800, 300, "yes");
						}
					}, {
						text : '取消',
						scale : 'medium',
						handler : function() {
							window.close();
						}
					}],
			items : [{
						xtype : 'container',
						anchor : '100%',
						layout : 'hbox',
						items : [{
									xtype : 'container',
									flex : 1,
									layout : 'anchor',
									items : [{
												xtype : 'combo',
												fieldLabel : '专业名称',
												afterLabelTextTpl : required,
												allowBlank : false,
												store : majords,
												mode : 'remote',
												valueField : 'majorName',
												displayField : 'majorName',
												editable : false,
												name : 'planMain.zy',
												id : 'majorName',
												triggerAction : 'all',
												listeners : {
													select : {
														scope : this,
														fn : function(combo, record, index) {
															var value = combo.getValue()
															var rec = combo.findRecord(combo.valueField || combo.displayField, value);
															var recv = rec.data
															Ext.getCmp('majorId').setValue(recv.majorId)
															Ext.getCmp('category').setValue(recv.category)
															Ext.getCmp('degree').setValue(recv.degree)
															Ext.getCmp('majorYear').setValue(recv.majorYear)
															Ext.getCmp('ssfa').setValue(_degree + recv.category + recv.degree + '学位')
														}
													}
												},
												anchor : '95%'
											}, {
												xtype : 'textfield',
												fieldLabel : '门类',
												readOnly : true,
												name : 'planMain.ml',
												id : 'category',
												anchor : '95%'
											}, {
												xtype : 'textfield',
												fieldLabel : '授予学位',
												readOnly : true,
												name : 'planMain.syxw',
												id : 'degree',
												anchor : '95%'
											}]
								}, {
									xtype : 'container',
									flex : 1,
									layout : 'anchor',
									items : [{
												xtype : 'combo',
												fieldLabel : '专业代码',
												afterLabelTextTpl : required,
												allowBlank : false,
												store : majords,
												mode : 'remote',
												valueField : 'majorId',
												displayField : 'majorId',
												editable : false,
												name : 'planMain.zydm',
												id : 'majorId',
												triggerAction : 'all',
												listeners : {
													select : {
														scope : this,
														fn : function(combo, record, index) {
															var value = combo.getValue()
															var rec = combo.findRecord(combo.valueField || combo.displayField, value);
															var recv = rec.data
															Ext.getCmp('majorName').setValue(recv.majorName)
															Ext.getCmp('category').setValue(recv.category)
															Ext.getCmp('degree').setValue(recv.degree)
															Ext.getCmp('majorYear').setValue(recv.majorYear)
															Ext.getCmp('ssfa').setValue(_degree + recv.category + recv.degree + '学位')
														}
													}
												},
												anchor : '95%'
											}, {
												xtype : 'textfield',
												fieldLabel : '标准学制',
												readOnly : true,
												name : 'planMain.bzxz',
												id : 'majorYear',
												anchor : '95%'
											}, {
												xtype : 'numberfield',
												name : 'planMain.version',
												fieldLabel : '版本（年份）',
												value : parseInt(new Date().getFullYear().toString()),
												anchor : '95%'
											}, {
												hidden : true,
												name : 'planMain.deptId',
												fieldLabel : '学院',
												value : parseInt(new Date().getFullYear().toString()),
												anchor : '95%'
											}, {
												hidden : true,
												name : 'planMain.type',
												fieldLabel : '培养方案类别',
												value : parseInt(new Date().getFullYear().toString()),
												anchor : '95%'
											}]
								}]
					}, {
						xtype : 'textarea',
						name : 'planMain.pymb',
						fieldLabel : '培养目标',
						height : 50,
						anchor : '100%'
					}, {
						xtype : 'textarea',
						name : 'planMain.jbyq',
						fieldLabel : '毕业要求',
						height : 160,
						anchor : '100%'
					}, {
						xtype : 'textarea',
						name : 'planMain.zgxk',
						fieldLabel : '主干学科',
						height : 30,
						anchor : '100%'
					}, {
						xtype : 'textarea',
						name : 'planMain.zykc',
						fieldLabel : '主干课程',
						height : 50,
						anchor : '100%'
					}, {
						xtype : 'textarea',
						name : 'planMain.zysjhj',
						fieldLabel : '主要实践环节',
						height : 50,
						anchor : '100%'
					}, {
						xtype : 'textarea',
						name : 'planMain.zdxfyq',
						fieldLabel : '相关职业资格证书',
						height : 40,
						anchor : '100%'
					}, {
						xtype : 'textarea',
						id : 'ssfa',
						name : 'planMain.ssfa',
						fieldLabel : '毕业及学位授予',
						readOnly : true,
						value : _degree,
						height : 120,
						anchor : '100%'
					}, {
						hidden : true,
						name : 'planMain.version',
						fieldLabel : '版本',
						value : new Date(),
						anchor : '100%'
					}, {
						hidden : true,
						name : 'planMain.state',
						fieldLabel : '状态',
						anchor : '100%'
					}]
		});
Ext.onReady(function() {
			new Ext.Viewport({
						layout : 'fit',
						items : [form]
					})
			fillDataToForm()
		})
