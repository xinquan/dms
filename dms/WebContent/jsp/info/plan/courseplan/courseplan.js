var _config
var id = getUrlParam('planid')
var bigtag = getUrlParam('bigtag')
function fillGrid() {
	_config = {
		planGrid : {
			id : 'planGrid',
			region : 'center',
			title : '课程指导性修读计划',
			split : true,
			fields : ['jd', 'pt', 'mk', 'kcxz', 'kcdm', 'kcmc', 'xf', 'qtxf', 'zxs', 'syxs', 'kkxq', 'sjhj', 'xdsm', 'newadd', 'id'],
			headers : ['阶段', '平台', '模块', '课程<br>性质', '课程代码', '课程名称', '学分', '（学分）', '总学时', '实验（实践）<br>学时', '开课学期', '集中性<br>实践环节', '修读说明'],
			widths : [85, 85, 150, 50, 90, 140, 50, 60, 60, 80, 60, 60, 160],
			renderers : [tip, tip, tip, tip, tip, tip, tip, tip, tip, tip, tip, tip, tip],
			aligns : [null, 'center', null, null, null, null, null, null, null, null, null, null, null],
			url : 'plan/plan!courseList.do',
			autoLoad : false,
			viewConfig : {
				forceFit : true,
				getRowClass : function(record, rowIndex, rowParams, store) {
					var type = record.get('newadd');
					if ('newadd' == type) {
						return 'x-grid-row-yellow';
					}
				}
			},
			hasPage : false
		}
	}
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			fillGrid()
			createApp('培养方案', 'border', [createGrid(_config.planGrid)])
			if (bigtag == 1) {
				Ext.getCmp('planGrid').getStore().load({
							params : {
								planid : id,
								bigtag : 1
							}
						})
			} else {
				Ext.getCmp('planGrid').getStore().load({
							params : {
								planid : id
							}
						})
			}
		});