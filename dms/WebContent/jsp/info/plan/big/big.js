var currentId = getUrlParam('id');
var gridArray = [];
var gridJson;
var majorid;
function fillDataToForm() {
	var form = Ext.getCmp('mainForm').getForm();
	Ext.Ajax.request({
				url : PATH + 'plan/plan!findMain.do',
				params : {
					'currentId' : currentId
				},
				method : 'GET',
				success : function(response, options) {
					var re = Ext.decode(response.responseText).result
					var form = Ext.getCmp('mainForm').getForm();
					var formrec = makeNewObjAddPrefix(re, 'planMain.')
					form.setValues(formrec)
				},
				failure : function(response, options) {
					Ext.MessageBox.alert('失败', '请求超时或网络故障,错误编号：' + response.status);
				}
			})
}

Ext.define('major', {
			extend : 'Ext.data.Model',
			fields : [{
						name : 'id',
						type : 'string'
					}, {
						name : 'majorId',
						type : 'string'
					}, {
						name : 'majorName',
						type : 'string'
					}, {
						name : 'majorYear',
						type : 'string'
					}, {
						name : 'category',
						type : 'string'
					}, {
						name : 'degree',
						type : 'string'
					}]
		});
var majords = Ext.create('Ext.data.Store', {
			model : 'major',
			proxy : {
				type : 'ajax',
				url : PATH + "school/school!bigMajorList.do",
				actionMethods : {
					read : 'POST'
				},
				extraParams : {
					deptId : _deptid,
					type : '大类'
				},
				reader : {
					type : 'json',
					root : 'res'
				}
			},
			autoLoad : true
		});

var form = Ext.create('Ext.form.Panel', {
			id : 'mainForm',
			frame : true,
			width : 770,
			region : 'center',
			autoScroll : true,
			title : '大类本科专业培养方案',
			titleAlign : 'center',
			buttonAlign : 'center',
			bodyPadding : '5 5 5',
			fieldDefaults : {
				labelAlign : 'left',
				msgTarget : 'side'
			},
			buttons : [{
						text : '查看课程指导性修读计划',
						scale : 'medium',
						disabled : false,
						handler : function() {
							showWindow(PATH + 'plan/plan!showCoursePlan.do?planid=' + currentId + '&bigtag=1', "课程指导性修读计划", 800, 300, "yes");
						}
					}, {
						text : '查看课程构成及学分分配汇总表',
						scale : 'medium',
						disabled : false,
						handler : function() {
							showWindow(PATH + 'plan/plan!showDistribution.do?planid=' + currentId, "课程构成及学分分配汇总表", 800, 300, "yes");
						}
					}, {
						text : '取消',
						scale : 'medium',
						handler : function() {
							window.close();
						}
					}],
			items : [{
						xtype : 'numberfield',
						name : 'planMain.version',
						fieldLabel : '版本（年份）',
						value : parseInt(new Date().getFullYear().toString()),
						anchor : '100%'
					}, {
						xtype : 'combo',
						fieldLabel : '大类',
						afterLabelTextTpl : required,
						allowBlank : false,
						store : majords,
						mode : 'remote',
						valueField : 'majorName',
						displayField : 'majorName',
						editable : false,
						name : 'planMain.zy',
						id : 'majorName',
						triggerAction : 'all',
						listeners : {
							select : {
								scope : this,
								fn : function(combo, record, index) {
									var value = combo.getValue()
									var rec = combo.findRecord(combo.valueField || combo.displayField, value);
									var recv = rec.data
									this.majorid = recv.majorId
									Ext.Ajax.request({
												url : PATH + 'school/school!findMajorsByBigType.do',
												params : {
													'type' : recv.majorName
												},
												method : 'POST',
												success : function(response, options) {
													var re = Ext.decode(response.responseText).res
													var faces = ''
													for (var i = 0; i < re.length; i++) {
														faces += re[i].majorName + '(' + re[i].majorId + ')\n'
													}
													Ext.getCmp('faces').setValue(faces)
												},
												failure : function(response, options) {
													Ext.MessageBox.alert('失败', '请求超时或网络故障,错误编号：' + response.status);
												}
											})
								}
							}
						},
						anchor : '100%'
					}, {
						xtype : 'textarea',
						name : 'planMain.pymb',
						fieldLabel : '大类培养特色',
						value : '【可以扼要介绍大类的一些特征、特色，办学背景，学科特色，专业荣誉，以及大类培养实践的意义、目标、培养方案基本思路等】',
						height : 300,
						anchor : '100%'
					}, {
						xtype : 'textarea',
						id : 'faces',
						name : 'planMain.zgxk',
						fieldLabel : '大类培养面向',
						value : '专业名称(专业代码)',
						height : 100,
						anchor : '100%'
					}, {
						xtype : 'hidden',
						id : 'majorId',
						name : 'planMain.zydm',
						fieldLabel : '专业代码',
						anchor : '100%'
					}, {
						xtype : 'hidden',
						id : 'state',
						name : 'planMain.state',
						fieldLabel : '状态不变',
						anchor : '100%'
					}, {
						xtype : 'displayfield',
						value : '说明：“课程指导性修读计划”请在主表下方的表格内编辑。'
					}]
		});
Ext.onReady(function() {
			new Ext.Viewport({
						layout : 'fit',
						items : [form]
					})
			fillDataToForm()
		})
