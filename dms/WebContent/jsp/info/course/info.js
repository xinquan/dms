var id = getUrlParam('id')
var courseForm = new Ext.FormPanel({
			frame : true,
			waitMsgTarget : true,
			layout : {
				type : 'table',
				columns : 2
			},
			defaults : {
				width : 320,
				labelWidth : 110,
				xtype : 'displayfield',
				labelAlign : 'right',
				anchor : "100%"
			},
			items : [{
						fieldLabel : '课程名称',
						name : 'course.kcmc',
						afterLabelTextTpl : required,
						allowBlank : false
					}, {
						fieldLabel : '课程代码',
						name : 'course.kcdm',
						value : '审批通过后由教务处确定'
					}, {
						fieldLabel : '课程归属学院',
						afterLabelTextTpl : required,
						allowBlank : false,
						name : 'course.department'
					}, {
						fieldLabel : '课程归属系',
						name : 'course.kcssx'
					}, {
						fieldLabel : '面向专业',
						afterLabelTextTpl : required,
						allowBlank : false,
						name : 'course.major'
					}, {
						fieldLabel : '课程性质',
						afterLabelTextTpl : required,
						allowBlank : false,
						name : 'course.kcxz'
					}, {
						fieldLabel : '学分',
						afterLabelTextTpl : required,
						allowBlank : false,
						name : 'course.xf'
					}, {
						fieldLabel : '(学分)',
						name : 'course.qtxf'
					}, {
						fieldLabel : '【学分】',
						name : 'course.zkhxf'
					}, {
						id : 'zxs',
						fieldLabel : '总学时',
						name : 'course.zxs',
						afterLabelTextTpl : required,
						allowBlank : false
					}, {
						fieldLabel : '理论学时',
						name : 'course.llxs',
						afterLabelTextTpl : required,
						allowBlank : false
					}, {
						fieldLabel : '实验（实践）学时',
						name : 'course.syxs'
					}, {
						fieldLabel : '阶段',
						afterLabelTextTpl : required,
						allowBlank : false,
						name : 'course.jd'
					}, {
						fieldLabel : '平台',
						afterLabelTextTpl : required,
						allowBlank : false,
						name : 'course.pt'
					}, {
						fieldLabel : '模块',
						afterLabelTextTpl : required,
						allowBlank : false,
						name : 'course.mk'
					}, {
						fieldLabel : '开课学期',
						name : 'course.kkxq'
					}, {
						fieldLabel : '集中性实践环节',
						name : 'course.sjhj'
					}, {
						colspan : 2,
						fieldLabel : '实践(实验)标记',
						name : 'course.syhjbj'
					}, {
						colspan : 2,
						name : 'course.kknf',
						fieldLabel : '开课年份',
						afterLabelTextTpl : required,
						allowBlank : false
					}, {
						colspan : 2,
						width : 640,
						height : 200,
						fieldLabel : '修读说明',
						name : 'course.xdsm'
					}, {
						hidden : true,
						fieldLabel : '状态',
						name : 'course.state'
					}, {
						id : 'id',
						name : 'course.id',
						hidden : true
					}]
		});
function fillForm() {
	Ext.Ajax.request({
				url : PATH + 'plan/plan!findCourse.do',
				params : {
					currentId : id
				},
				callback : function(opt, success, response) {
					var result = Ext.decode(response.responseText).result;
					var course = makeNewObjAddPrefix(result, 'course.')
					courseForm.form.setValues(course)
				}
			})
}
Ext.onReady(function() {
			Ext.QuickTips.init();
			Ext.create('Ext.container.Viewport', {
						layout : 'fit',
						items : [courseForm]
					})
			fillForm()
		});