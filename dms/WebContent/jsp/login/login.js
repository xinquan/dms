function login() {
	if (Ext.getCmp('loginForm').form.isValid()) {
		Ext.getCmp('loginForm').form.submit({
					url : PATH + "logon!logon.do",
					waitMsg : '正在登陆...',
					waitTitle : '提示',
					method : "POST",
					success : function(form, action) {
						document.location = PATH + "app.jsp";
					},
					failure : function(form, action) {
						Ext.Msg.show({
									title : "提示",
									msg : "请输入正确的用户名和密码!!!",
									buttons : Ext.MessageBox.OK,
									icon : Ext.Msg.WARNING
								})
					}
				})
	}
}
Ext.onReady(function() {
			var userLoginPanel = Ext.create('Ext.panel.Panel', {
						border : false,
						items : [{
									xtype : 'form',
									id : 'loginForm',
									defaults : {
										width : 260,
										margin : '10 0 0 20'
									},
									defaultType : 'textfield',
									labelWidth : 40,
									border : false,
									items : [{
												fieldLabel : '用户名',
												name : 'username',
												labelAlign : 'right',
												labelWidth : 65,
												maxLength : 30,
												emptyText : '用户名',
												value : 'admin',
												maxLengthText : '账号的最大长度为30个字符',
												blankText : "用户名不能为空，请填写！",// 错误提示信息，默认为This
												allowBlank : false,
												listeners : {
													specialkey : function(field, e) {
														if (e.getKey() == Ext.EventObject.ENTER) {
															login()
														}
													}
												}
											}, {
												fieldLabel : '密   码',
												cls : 'key',
												name : 'password',
												inputType : "password",
												labelWidth : 65,
												labelAlign : 'right',
												emptyText : '密码',
												value : 'admin',
												maxLengthText : '密码长度不能超过20',
												maxLength : 20,
												blankText : "密码不能为空，请填写！",// 错误提示信息，默认为This
												allowBlank : false,
												listeners : {
													specialkey : function(field, e) {
														if (e.getKey() == Ext.EventObject.ENTER) {
															login()
														}
													}
												}
											}, {
												id : 'id_reg_panel',
												xtype : 'panel',
												border : false,
												hidden : true,
												html : '<span id="messageTip" style="color:red"> </span>'
											}]
								}]
					});
			Ext.create('Ext.window.Window', {
						title : '扬中广播电视设备管理平台',
						width : 340,
						height : 180,
						layout : 'fit',
						plain : true,
						border : false,
						maximizable : false,
						draggable : false,
						closable : false,
						resizable : false,
						items : userLoginPanel,
						buttons : [{
									text : '登录',
									iconCls : 'login',
									handler : function() {
										login()
									}
								}]
					}).show();
		});