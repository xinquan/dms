Ext.Loader.setPath("Ext.ux", "./js/ExtJS4.2/ux");
/*
 * 创建应用，建议一个系统一个应用名
 */
function createApp(name, layout, items, buttons) {
	Ext.require('Ext.container.Viewport');
	Ext.application({
				name : name,
				launch : function() {
					Ext.create('Ext.container.Viewport', {
								layout : layout,
								items : items
							})
				}
			})
}

/*
 * 创建grid
 */
function createGrid(config) {
	var fields = Ext.decode("['" + config.fields.toString().replace(/,/g, "','") + "']")
	var storeConfig = {
		fields : config.fields
	}

	// 分页
	var reader = {
		type : 'json',
		root : 'res'
	}
	if (config.hasPage) {
		reader.totalProperty = 'totalCount';
		if (config.pageSize) {
			storeConfig.pageSize = config.pageSize;
		} else {
			storeConfig.pageSize = PAGE_SIZE
		}
	} else {
		storeConfig.pageSize = -1
	}
	var params = {}
	if (config.params) {
		params = config.params
	}
	storeConfig.proxy = {
		type : 'ajax',
		url : config.url,
		actionMethods : {
			read : 'POST'
		},
		extraParams : params,
		reader : reader
	}

	if (config.autoLoad || !config.autoLoad) {
		storeConfig.autoLoad = config.autoLoad
	} else {
		storeConfig.autoLoad = true
	}
	var store = Ext.create('Ext.data.Store', storeConfig);

	if (config.storeCallback) {
		store.load({
					params : params,
					callback : config.storeCallback
				})
	}

	// gridColumns
	var columns = []

	// 序号
	if (config.hasSeq != false) {
		columns.push({
					text : '序号',
					align : 'right',
					width : 40,
					xtype : 'rownumberer'
				})
	}
	// 列信息及（多）表头
	var width = GRID_COLUMN_WIDTH
	var headerAlign = HeaderAlignStyle
	var align = ColumnAlign
	if (config.groupHeaders) {// 多表头
		var headersLength = config.headers.length
		var index = 0;
		for (var s = 0; s < headersLength; s++) {
			var colHeader = config.headers[s];
			if (Ext.isString(colHeader)) {
				var col = {
					text : colHeader,
					dataIndex : config.fields[index],
					style : headerAlign,
					align : config.aligns[index],
					width : config.widths[index]
				}
				if (config.editors) {
					col.editor = config.editors[index]
				} else {
					col.editor = 'textfield'
				}
				if (config.summarys) {
					col.summaryType = config.summarys[index]
				} else {
					col.summaryType = 'sum'
				}
				if (config.srenderers) {
					col.summaryRenderer = config.srenderers[index]
				} else {
					col.summaryRenderer = null
				}

				if (config.renderers) {
					col.renderer = config.renderers[index]
				}
				index += 1;
				columns.push(col)
			} else {// 多
				var items = []
				for (var hl = 0; hl < colHeader.length; hl++) {
					if (config.widths && config.widths[index + hl]) {
						width = config.widths[index + hl]
					}
					if (config.aligns) {
						align = config.aligns[index + hl]
					}
					var itemCol = {
						text : colHeader[hl],
						dataIndex : config.fields[index + hl],
						style : headerAlign,
						align : align,
						width : width
					}
					if (config.editors) {
						itemCol.editor = config.editors[index + hl]
					} else {
						itemCol.editor = 'textfield'
					}
					if (config.summarys) {
						itemCol.summaryType = config.summarys[index + hl]
					} else {
						itemCol.summaryType = 'sum'
					}
					if (config.srenderers) {
						itemCol.summaryRenderer = config.srenderers[index + hl]
					} else {
						itemCol.summaryRenderer = null
					}
					if (config.renderers) {
						itemCol.renderer = config.renderers[index + hl]
					}
					items.push(itemCol)
				}
				var headerCol = {
					text : config.groupHeaders[s],
					columns : items
				}
				index += colHeader.length
				columns.push(headerCol)
			}
		}
	} else {// 简单表头
		for (var i = 0; i < config.headers.length; i++) {
			if (config.widths && config.widths[i]) {
				width = config.widths[i]
			}
			if (config.aligns) {
				align = config.aligns[i]
			}
			var col = {
				text : config.headers[i],
				dataIndex : config.fields[i],
				style : headerAlign,
				align : align,
				width : width
			}
			if (config.filters && config.filters[i]) {
				col.items = {
					xtype : 'textfield',
					flex : 1,
					margin : 2,
					enableKeyEvents : true,
					listeners : {
						keyup : function() {
							var store = this.up('tablepanel').store;
							if (this.value) {
								store.proxy.url = config.filtersurl[0];
								store.load({
											params : {
												'n1' : n1,
												's1' : s1,
												's2' : s2,
												'fname' : this.ownerCt.dataIndex,
												'fvalue' : this.value
											}
										})
							} else {
								store.load({
											params : {
												's1' : s1,
												's2' : s2,
												'n1' : n1
											}
										})
							}
						},
						buffer : 500
					}
				}
			}
			if (config.editors) {
				col.editor = config.editors[i] || 'textfield'
			}
			if (config.renderers && config.renderers[i]) {
				col.renderer = config.renderers[i]
			}
			if (config.summarys) {
				col.summaryType = config.summarys[i]
			} else {
				col.summaryType = 'sum'
			}
			if (config.srenderers) {
				col.summaryRenderer = config.srenderers[i]
			} else {
				col.summaryRenderer = null
			}
			columns.push(col)
		}
	}
	if (config.iconCls) {
		grid.iconCls = config.iconCls
	}

	// 选择模式，行/单元格，编辑模式
	var selType = 'rowmodel'
	if (config.selType) {
		selType = config.selType;
	}
	var plugins = []
	if (config.selType == 'cellmodel') {
		plugins.push(Ext.create('Ext.grid.plugin.CellEditing', {
					clicksToEdit : 1
				}))
	}
	var dockedItems = []
	if (config.hasPage) {
		dockedItems.push({
					xtype : 'pagingtoolbar',
					store : store,
					dock : 'bottom',
					displayInfo : true
				})
	}

	var gridConfig = {
		store : store,
		columns : columns,
		selType : selType,
		plugins : plugins,
		dockedItems : dockedItems
	};
	// 复选框
	var sm = Ext.create('Ext.selection.CheckboxModel', {
				model : 'SIMPLE'
			})
	if (config.hasCheck != false) {
		gridConfig.selModel = sm
	}

	if (config.region) {
		gridConfig.region = config.region
	}
	if (config.border) {
		gridConfig.border = config.border
	} else {
		gridConfig.border = false
	}

	if (config.tbar) {
		if (config.query) {
			config.tbar.push('->', {
						xtype : 'trigger',
						emptyText : '请输入关键字检索',
						triggerCls : 'ux-form-search-trigger',
						enableKeyEvents : true,
						listeners : {
							keyup : {
								fn : function(field, e) {
									if (Ext.EventObject.ENTER == e.getKey()) {
										field.onTriggerClick();
									}
								}
							}
						},
						onTriggerClick : function() {
							var store = this.up('tablepanel').store;
							store.load({
										params : {
											query : config.query,
											queryField : config.queryField,
											queryValue : this.getRawValue()
										}
									})
						}
					})
		}
		gridConfig.tbar = config.tbar
	}
	if (config.iconCls) {
		gridConfig.iconCls = config.iconCls
	}
	if (config.title) {
		gridConfig.title = config.title
	}
	if (config.id) {
		gridConfig.id = config.id
	} else {
		gridConfig.id = Ext.id()
	}
	if (config.renderTo) {
		gridConfig.renderTo = config.renderTo
	}
	if (config.split) {
		gridConfig.split = config.split
	} else {
		gridConfig.split = true
	}
	if (config.height) {
		gridConfig.height = config.height
	}
	if (config.width) {
		gridConfig.width = config.width
	}
	if (config.features) {
		gridConfig.features = config.features
	}
	if (config.viewConfig) {
		gridConfig.viewConfig = config.viewConfig
	} else {
		gridConfig.autoScroll = true;
		gridConfig.viewConfig = {
			forceFit : true
		}
	}
	// grid创建
	var grid = Ext.create('Ext.grid.Panel', gridConfig);

	return grid
}

function showWindow(url, name, iWidth, iHeight, resizeable) {
	resizeable = resizeable ? resizeable : "no"
	resizeable = 'yes';
	var url; // 转向网页的地址;
	var name; // 网页名称，可为空;
	var iWidth; // 弹出窗口的宽度;
	var iHeight; // 弹出窗口的高度;
	var iTop = (window.screen.availHeight - 30 - iHeight) / 2; // 获得窗口的垂直位置;
	var iLeft = (window.screen.availWidth - 10 - iWidth) / 2; // 获得窗口的水平位置;
	var w = window.open(url, name, 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft
					+ ',toolbar=no,menubar=no,scrollbars=auto,resizable=' + resizeable + ',location=no,status=no');
	return w;

}

function getUrlParam(param) {
	var params = Ext.urlDecode(location.search.substring(1));
	return param ? params[param] : params;
}

function tip(v, m) {
	if (v == null) {
		m.tdAttr = ''
	} else {
		m.tdAttr = 'data-qtip="' + v + '"'
	}
	return v
}

function parse0(v) {
	if (v != null && v != '') {
		if (v == 0) {
			return ''
		} else {
			return parseFloat(v)
		}
	}
}

function makeNewObjAddPrefix(obj, prefix) {
	var newObj = {}
	for (var field in obj) {
		newObj[prefix + field] = obj[field]
	}
	return newObj;
}

function getDictStore(autoLoad, dictStyle) {
	Ext.define('dict', {
				extend : 'Ext.data.Model',
				fields : [{
							name : 'id',
							type : 'string'
						}, {
							name : 'styleId',
							type : 'string'
						}, {
							name : 'detailId',
							type : 'string'
						}, {
							name : 'detailName',
							type : 'string'
						}, {
							name : 'detailSort',
							type : 'string'
						}, {
							name : 'pid',
							type : 'string'
						}]
			});
	var params = {}
	if (autoLoad) {
		params = {
			dictStyle : dictStyle
		}
	}
	var dictStore = Ext.create('Ext.data.Store', {
				model : 'dict',
				proxy : {
					type : 'ajax',
					url : PATH + "system/dict!list.do",
					extraParams : params,
					reader : {
						type : 'json',
						root : 'res'
					}
				},
				autoLoad : autoLoad
			});
	return dictStore
}