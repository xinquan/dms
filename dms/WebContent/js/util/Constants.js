var GRID_COLUMN_WIDTH = 160
var PAGE_SIZE = 40
var GRID_HEIGHT = 320
var delIds = []
var n1
var s1
var s2
var uploadWin
var _prompt = '温馨提示'
var _waitMsg = '请稍后....'
var _success = '成功'
var _failure = '失败'
var HeaderAlignStyle = "text-align:center"
var ColumnAlign = "left"
var required = '<span style="color:red;font-weight:bold" data-qtip="Required">*</span>';
var autoWarp = 'overflow:auto;padding: 3px 6px;text-overflow: ellipsis;white-space: nowrap;white-space:normal;line-height:20px;';
var _degree = '1.毕业标准\n' + '(1)具有良好的思想品德和身体素质，符合学校规定的德育和体育标准，《国家学生体质健康标准（2014年修订）》测试成绩达到50分（含50）以上；\n' + '(2)在规定的修业年限内，完成人才培养方案规定的所有课程和环节，取得规定的170个学业学分；\n' + '(3)取得规定的10个素质拓展学分（其中A类4个学分，B类6个学分）。\n'
		+ '2.学位授予\n' + '符合淮海工学院学士学位授予条例规定，可授予'
var IS_ROOT = false;
var _editable;

var _plantype = {
	GGJC : '公共基础必修课程',
	CXCY : '创新创业教育与素质拓展课程',
	DLBX : '大类基础必修课程',
	XKBX : '学科基础必修课程',
	HXKC : '专业核心课程',
	TZKC : '专业拓展课程'
}
var plantype = function(v) {
	return _plantype[v]
}

/*
 * 获得地址：http://localhost/app/
 */
var PATH = function() {
	if (IS_ROOT) {
		return "/";
	}
	var fullPath = window.location.pathname;
	var pos = fullPath.indexOf("/", 1);
	var path = fullPath.substring(0, pos + 1);
	return path;
}()
var _flowname = {
	DL : '大类培养方案',
	DLZY : '大类专业培养方案',
	FDL : '非大类专业培养方案',
	KC : '新增课程审批'
}

var checkCascadViewConfig = {
	onCheckboxChange : function(e, t) {
		var item = e.getTarget(this.getItemSelector(), this.getTargetEl()), record;
		if (item) {
			record = this.getRecord(item);
			var check = !record.get('checked');
			record.set('checked', check);
			if (check) {
				record.bubble(function(parentNode) {
							parentNode.set('checked', true);
						});
				record.cascadeBy(function(node) {
							node.set('checked', true);
						});
				record.expand();
				record.expandChildren();
			} else {
				record.collapse();
				record.collapseChildren();
				record.cascadeBy(function(node) {
							node.set('checked', false);
						});
				record.bubble(function(parentNode) {
							var childHasChecked = false;
							var childNodes = parentNode.childNodes;
							if (childNodes || childNodes.length > 0) {
								for (var i = 0; i < childNodes.length; i++) {
									if (childNodes[i].data.checked) {
										childHasChecked = true;
										break;
									}
								}
							}
							if (!childHasChecked) {
								parentNode.set('checked', false);
							}
						});

			}
		}
	}
}
function startWorkFlow(sel, flowname, instantClass, reload) {
	if (sel.length == 1) {
		var state = sel[0].data.state
		if (state == 0) {
			showWindow(PATH + 'system/flow!pickUserToFlow.do?id=' + sel[0].data.id + '&flowname=' + flowname + '&instantClass=' + instantClass, "选择审批人", 800, 600, "yes");
		} else if (state == 2) {
			msgShow("无法操作", "已通过审批！无法再提交！", Ext.Msg.WARNING);
		} else if (state == 3) {
			msgShow("无法操作", "已提交！请勿重复提交！", Ext.Msg.WARNING);
		} else if (state == 1) {
			msgShow("无法操作", "已提交！请勿重复提交！", Ext.Msg.WARNING);
		} else if (state == -1) {
			Ext.Ajax.request({
						url : PATH + 'system/flow!submitAgain.do',
						params : {
							instantid : sel[0].data.id,
							instantClass : instantClass
						},
						success : function() {
							msgShow("操作成功", "您已经成功提交信息,请等待审批！");
							if (reload != 'false') {
								reloadGrid();
							}
						},
						failure : function() {
							msgShow('错误', '服务器出现错误请稍后再试！', Ext.Msg.ERROR);
						}
					});
		}
	} else {
		msgShow("条件缺失", "请选择要审批的记录！", Ext.Msg.WARNING);
	}
}
function showWorkFlow(sel) {
	if (sel.length == 1) {
		showWindow(PATH + 'system/flow!showActionInfos.do?id=' + sel[0].data.id, "查看审批信息", 800, 600, "yes");
	} else {
		msgShow("条件缺失", "请选择一条记录！", Ext.Msg.WARNING);
	}
}
function delRecords(sel, instantClass, reload) {
	if (sel) {
		Ext.Msg.confirm("是否要删除？", "是否要删除这些被选择的数据？", function(btn) {
					if (btn == "yes") {
						delIds = []
						for (var i = 0; i < sel.length; i++) {
							if (sel[i].data.state == 0 || sel[i].data.kcdm == null || sel[i].data.kcdm == '' || _uid == 1) {
								delIds.push(sel[i].data.id);
							}
						}
						Ext.Ajax.request({
									url : PATH + 'system/menu!delRecords.do',
									params : {
										instantClass : instantClass,
										'delIds' : delIds
									},
									success : function() {
										msgShow("删除信息成功", "您已经成功删除信息！");
										if (reload) {
											reloadGrid();
										}
									},
									failure : function() {
										msgShow('错误', '服务器出现错误请稍后再试！', Ext.Msg.ERROR);
									}
								});
					}
				});
	} else {
		msgShow('删除操作', '您必须选择一行数据以便删除！', Ext.Msg.WARNING);
	}
}

var uploadForm = Ext.create('Ext.form.Panel', {
			bodyPadding : 10,
			border : false,
			items : [{
						xtype : 'filefield',
						id : 'filefield',
						name : 'file', // 服务端获取 “名称”
						afterLabelTextTpl : '<span style="color:red;font-weight:bold" data-qtip="必需填写">*</span>',
						fieldLabel : '文件',
						labelWidth : 50,
						msgTarget : 'side', // 提示 文字的位置
						allowBlank : false,
						anchor : '100%',
						buttonText : '选择文件'
					}],

			buttons : [{
						text : '下载模版',
						handler : function() {
							window.location.href = PATH + "system/common!downloadTemplateFile.do"
						}
					}, {
						text : '导入',
						handler : function() {
							var form = this.up('form').getForm();
							if (form.isValid()) { // form 验证
								var excel_reg = /\.([xX][lL][sS]){1}$|\.([xX][lL][sS][xX]){1}$/;
								if (!excel_reg.test(Ext.getCmp('filefield').getValue())) {
									msgShow('提示信息', '文件类型错误,请选择Excel文件(xls/xlsx)', Ext.Msg.WARNING);
								} else {
									// 取控件DOM对象
									var field = document.getElementById('filefield');
									// 取控件中的input元素
									var inputs = field.getElementsByTagName('input');
									var fileInput = null;
									var il = inputs.length;
									// 取出input 类型为file的元素
									for (var i = 0; i < il; i++) {
										if (inputs[i].type == 'file') {
											fileInput = inputs[i];
											break;
										}
									}
									if (fileInput != null) {
										var fileSize = getFileSize(fileInput);
										// 允许上传不大于1M的文件
										if (fileSize > 10485760) {
											msgShow('提示信息', '文件太大，请选择小于10M的文件！', Ext.Msg.WARNING);
										} else {
											form.submit({ // 提交
												url : PATH + 'system/common!uploadFile.do',
												waitMsg : '正在上传和解析，请稍后....',
												success : function(fp, o) {
													msgShow('上传并解析成功', '您的文件 "' + o.result.fileName + '"成功上传并解析，所有课程已导入数据库....');
													reloadGrid()
												}
											});
										}
									}
								}

							}
						}
					}, {
						text : '取消',
						handler : function() {
							uploadWin.hide()
						}
					}]
		});
function uploadExcel() {
	if (!uploadWin) {
		uploadWin = new Ext.Window({
					width : 460,
					height : 120,
					layout : 'fit',
					closable : false,
					autoScroll : true,
					title : '从Excel导入课程信息。<font color="yellow">请先下载模版，根据模版整理数据。</font>',
					modal : true,
					border : false,
					items : uploadForm
				}).show();
	} else {
		uploadWin.show()
	}
}
function getFileSize(target) {
	var isIE = /msie/i.test(navigator.userAgent) && !window.opera;
	var fs = 0;
	if (isIE && !target.files) {
		var filePath = target.value;
		var fileSystem = new ActiveXObject("Scripting.FileSystemObject");
		var file = fileSystem.GetFile(filePath);
		fs = file.Size;
	} else if (target.files && target.files.length > 0) {
		fs = target.files[0].size;
	} else {
		fs = 0;
	}
	return fs;
}
function stater(v) {
	if (v == 0) {
		return '起草'
	} else if (v == 1) {
		return '提交'
	} else if (v == 2) {
		return '通过'
	} else if (v == -1) {
		return '退回'
	} else if (v == 3) {
		return '退回后提交'
	}
}

function showEditWin(sel, url, title, width, height) {
	var w = 800, h = 600
	if (width) {
		w = width
	}
	if (height) {
		h = height
	}

	var state = sel[0].data.state
	if (state == 0 || state == -1) {
		showWindow(url, title, w, h, "yes");
	} else if (state == 2) {
		msgShow("无法操作", "已通过审批！无法再修改！");
	} else if (state == 1) {
		msgShow("无法操作", "已提交！暂时无法修改！");
	} else if (state == 3) {
		msgShow('无法操作', '该状态无法修改')
	}
}

var msgShow = function(title, info, icon, fn) {
	Ext.Msg.buttonText.yes = '确定';
	Ext.Msg.buttonText.no = "取消";
	var i;
	if (icon) {
		i = icon
	} else {
		i = Ext.Msg.INFO
	}
	var fnn = function() {
	}
	if (fn) {
		fnn = fn
	}
	Ext.Msg.show({
				title : title,
				modal : true,
				msg : info,
				icon : i,
				fn : fnn,
				buttons : Ext.Msg.YES
			});
}
Ext.toast = function() {
	var msgCt;
	function createBox(t, s) {
		return '<div class="msg"><h3>' + t + '</h3><p>' + s + '</p></div>';
	}
	return {
		msg : function(title, format) {
			if (!msgCt) {
				msgCt = Ext.DomHelper.append(document.body, {
							id : 'msg-div'
						}, true);
			}
			var s = Ext.String.format.apply(String, Array.prototype.slice.call(arguments, 1));
			var m = Ext.DomHelper.append(msgCt, createBox(title, s), true);
			m.hide();
			m.slideIn('t').ghost("t", {
						delay : 1000,
						remove : true
					});
		},

		init : function() {
			if (!msgCt) {
				// It's better to create the msg-div here in order to avoid
				// re-layouts
				// later that could interfere with the HtmlEditor and reset its
				// iFrame.
				msgCt = Ext.DomHelper.append(document.body, {
							id : 'msg-div'
						}, true);
			}
		}
	};
}();
Ext.define('TreeModel', {
			extend : 'Ext.data.Model',
			fields : [{
						name : "id",
						type : "string"
					}, {
						name : "text",
						type : "string"
					}, {
						name : "iconCls",
						type : "string"
					}, {
						name : "leaf",
						type : "boolean"
					}, {
						name : "sortIndex",
						type : "string"
					}]
		})
function utf16to8(str) {
	var out, i, len, c;
	out = "";
	len = str.length;
	for (i = 0; i < len; i++) {
		c = str.charCodeAt(i);
		if ((c >= 0x0001) && (c <= 0x007F)) {
			out += str.charAt(i);
		} else if (c > 0x07FF) {
			out += String.fromCharCode(0xE0 | ((c >> 12) & 0x0F));
			out += String.fromCharCode(0x80 | ((c >> 6) & 0x3F));
			out += String.fromCharCode(0x80 | ((c >> 0) & 0x3F));
		} else {
			out += String.fromCharCode(0xC0 | ((c >> 6) & 0x1F));
			out += String.fromCharCode(0x80 | ((c >> 0) & 0x3F));
		}
	}
	return out;
}
function stamp() {
	bdhtml = window.document.body.innerHTML;
	sprnstr = "<!--startprint-->";
	eprnstr = "<!--endprint-->";
	prnhtml = bdhtml.substr(bdhtml.indexOf(sprnstr) + 17);
	prnhtml = prnhtml.substring(0, prnhtml.indexOf(eprnstr));

	var newWindow = window.open("打印二维码", "_blank");
	newWindow.document.write(prnhtml);
	newWindow.document.close();
	setTimeout(function() {
				newWindow.print();
				newWindow.close();
			}, 100);
}