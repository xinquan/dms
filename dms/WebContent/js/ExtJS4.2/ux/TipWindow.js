Ext.namespace("Ext.ux");
Ext.ux.ToastWindowMgr = {
	positions : []
};
Ext.ux.ToastWindow = Ext.extend(Ext.Window, {
			initComponent : function() {
				Ext.apply(this, {
							iconCls : this.iconCls || Ext.Msg.INFO,
							width : 250,
							height : 150,
							autoScroll : true,
							closable : true,
							x : Ext.getBody().getWidth() - 250,
							y : Ext.getBody().getHeight() - 150,
							shadow : true
						});
				Ext.ux.ToastWindow.superclass.initComponent.call(this);
			},
			setMessage : function(msg) {
				this.body.update(msg);
			},
			setTitle : function(title, iconCls) {
				Ext.ux.ToastWindow.superclass.setTitle.call(this, title, iconCls || this.iconCls);
			},
			onRender : function(ct, position) {
				Ext.ux.ToastWindow.superclass.onRender.call(this, ct, position);
			},
			animShow : function() {
				this.pos = 0;
				while (Ext.ux.ToastWindowMgr.positions.indexOf(this.pos) > -1)
					this.pos++;
				Ext.ux.ToastWindowMgr.positions.push(this.pos);
				this.setSize(250, 150);
				this.el.alignTo(document, "br-br", [-20, -20 - ((this.getSize().height + 10) * this.pos)

						]);
				this.el.slideIn('b', {
							duration : 2,
							callback : this.afterShow,
							scope : this
						});
			}
		});