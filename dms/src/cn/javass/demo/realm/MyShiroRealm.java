package cn.javass.demo.realm;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import cn.hhit.system.model.UserModel;
import cn.hhit.system.service.PermissionService;
import cn.hhit.system.service.UserService;

public class MyShiroRealm extends AuthorizingRealm {
	@Autowired
	private UserService usermgr;
	@Autowired
	private PermissionService permgr;

	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
		return info;
	}

	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
		UserModel user = usermgr.findUserByUsername(token.getPrincipal().toString());
		if (user == null) {
			throw new UnknownAccountException();
		} else {
			return new SimpleAuthenticationInfo(user, user.getPassword(), getName());
		}
	}

	public UserService getUsermgr() {
		return usermgr;
	}

	public void setUsermgr(UserService usermgr) {
		this.usermgr = usermgr;
	}

	public PermissionService getPermgr() {
		return permgr;
	}

	public void setPermgr(PermissionService permgr) {
		this.permgr = permgr;
	}
}
