package cn.javass.demo.web.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;

import cn.hhit.common.pagination.Page;

public class ControllerUtil {
	public static void writeStringToClient(HttpServletResponse response, String lstrJson, boolean result) {
		try {
			response.setContentType("text/html");
			response.setCharacterEncoding("utf-8");
			response.getOutputStream().write(lstrJson.getBytes("utf-8"));
			response.getOutputStream().close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static void writePageListToClient(HttpServletResponse response, Page<?> page) {
		String json = JSON.toJSONString(page.getItems());
		json = "{" + "\"res\":" + json + ",\"totalCount\":" + page.getContext().getTotal() + "}";
		writeStringToClient(response, json, true);
	}

	public static void writeListToClient(HttpServletResponse response, List<?> list, boolean result) {
		String json = JSON.toJSONString(list);
		json = "{success:" + result + ",\"res\":" + json + "}";
		writeStringToClient(response, json, result);
	}
}
