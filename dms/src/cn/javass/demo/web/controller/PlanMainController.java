package cn.javass.demo.web.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.hhit.common.Constants;
import cn.hhit.common.pagination.Page;
import cn.hhit.school.model.PlanMainModel;
import cn.hhit.school.model.PlanMainQueryModel;
import cn.hhit.school.service.PlanCourseService;
import cn.hhit.school.service.PlanMainService;
import cn.javass.common.web.support.editor.DateEditor;

@Controller
public class PlanMainController extends ControllerUtil {

	@Autowired
	@Qualifier("PlanMainService")
	private PlanMainService planMainService;
	
	@Autowired
	@Qualifier("PlanCourseService")
	private PlanCourseService planCourseService;

	@RequestMapping(value = "/planMain", method = { RequestMethod.GET })
	public String list() {
		return "planMain/list";
	}

	@RequestMapping(value = "/planMain.list", method = { RequestMethod.GET })
	public void list(HttpServletRequest request, HttpServletResponse response) {
		int pn = ServletRequestUtils.getIntParameter(request, "page", 1);
		int ps = ServletRequestUtils.getIntParameter(request, "limit", Constants.DEFAULT_PAGE_SIZE);
		Page<PlanMainModel> page = planMainService.listAll(pn, ps);
		writePageListToClient(response, page);
	}

	@RequestMapping(value = "/planMain/query", method = { RequestMethod.GET })
	public String query(HttpServletRequest request, Model model, @ModelAttribute("command") PlanMainQueryModel command) {
		setCommonData(model);
		model.addAttribute(Constants.COMMAND, command);
		int pn = ServletRequestUtils.getIntParameter(request, "pn", 1);
		model.addAttribute("page", planMainService.query(pn, Constants.DEFAULT_PAGE_SIZE, command));

		return "planMain/list";
	}

	private void setCommonData(Model model) {
		// 设置通用属性
	}

	@RequestMapping(value = "/planMain/{userId}/view", method = { RequestMethod.GET })
	public String view(@PathVariable Integer topicId, HttpServletRequest request) {
		request.setAttribute(Constants.COMMAND, planMainService.get(topicId));
		return "planMain/view";
	}

	@RequestMapping(value = "/planMain/{id}/update", method = { RequestMethod.GET })
	public String toUpdate(Model model, @PathVariable Integer id) {
		if (!model.containsAttribute(Constants.COMMAND)) {
			model.addAttribute(Constants.COMMAND, planMainService.get(id));
		}
		setCommonData(model);
		return "planMain/update";
	}

	@RequestMapping(value = "/planMain/{id}/delete", method = { RequestMethod.GET })
	public String toDelete(@PathVariable Integer id) {
		return "planMain/delete";
	}

	@RequestMapping(value = "/planMain.add", method = { RequestMethod.POST })
	public void add(@Valid PlanMainModel planMain, BindingResult result, HttpServletResponse response) {

		// 如果有验证错误 返回到form页面
		if (result.hasErrors()) {
			writeListToClient(response, result.getAllErrors(), false);
		}
		planMainService.save(planMain);
	}

	@RequestMapping(value = "/planMain/{id}/update", method = { RequestMethod.PUT })
	public String update(Model model, @ModelAttribute("command") @Valid PlanMainModel command, BindingResult result) {
		if (result.hasErrors()) {
			model.addAttribute(Constants.COMMAND, command);
			return toUpdate(model, command.getId());
		}
		planMainService.update(command);
		return "redirect:/planMain/success";
	}

	@RequestMapping(value = "/planMain/{id}/delete", method = { RequestMethod.DELETE })
	public String delete(@PathVariable Integer id) {
		planMainService.delete(id);
		return "redirect:/planMain/success";
	}

	@RequestMapping(value = "/planMain/success", method = { RequestMethod.GET })
	public String success() {
		return "planMain/success";
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new DateEditor());
	}

}
