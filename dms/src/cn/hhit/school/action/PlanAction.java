package cn.hhit.school.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.alibaba.fastjson.JSON;

import cn.hhit.common.Constants;
import cn.hhit.common.action.CommonAction;
import cn.hhit.common.pagination.Page;
import cn.hhit.common.util.DocumentHandler;
import cn.hhit.common.util.ExcelHelper;
import cn.hhit.common.util.HssfExcelHelper;
import cn.hhit.common.util.ListUtil;
import cn.hhit.common.util.StringUtil;
import cn.hhit.school.model.PlanCourseModel;
import cn.hhit.school.model.PlanDistribute;
import cn.hhit.school.model.PlanMainModel;
import cn.hhit.school.model.PlanMatrix;
import cn.hhit.school.model.SchoolMajor;
import cn.hhit.school.service.PlanDistributeService;
import cn.hhit.school.service.PlanCourseService;
import cn.hhit.school.service.PlanMainService;
import cn.hhit.school.service.PlanMatrixService;
import cn.hhit.school.service.SchoolMajorService;

@SuppressWarnings("rawtypes")
public class PlanAction extends CommonAction {
	private static final long serialVersionUID = 1L;

	@Autowired
	@Qualifier("PlanMainService")
	private PlanMainService planMainService;

	@Autowired
	@Qualifier("PlanMatrixService")
	private PlanMatrixService planMatrixService;

	@Autowired
	@Qualifier("PlanDistributeService")
	private PlanDistributeService planDistributeService;
	@Autowired
	@Qualifier("PlanCourseService")
	private PlanCourseService planCourseService;

	@Autowired
	@Qualifier("SchoolMajorService")
	private SchoolMajorService schoolMajorService;

	private PlanMainModel planMain;
	private int currentId;
	private int pid;
	private int planid;
	private String plantype;
	private int bigtag;
	private String gridJson;
	public int[] delIds;
	public int[] courseIds;
	private String type;
	private int deptId;
	private String fname;
	private String fvalue;
	private String opterator;
	private Integer version;
	private String major;
	private boolean copy;
	private String kkxq;
	private String passed;

	public String showPlanInfo() {
		return "planinfo";
	}

	public String showBigPlanInfo() {
		return "bigplaninfo";
	}

	public String showCourseInfo() {
		return "courseinfo";
	}

	public String showCoursePlan() {
		return "courseplan";
	}

	public String showBig() {
		return "showbig";
	}

	public String editBig() {
		return "editbig";
	}

	public String editBigMajor() {
		return "editbigmajor";
	}

	public String editMajor() {
		return "editmajor";
	}

	public String showBigMajor() {
		return "showbigmajor";
	}

	public String showMajor() {
		return "showmajor";
	}

	public String importCourse() {
		return "importcourse";
	}

	public String setCourses() {
		return "setcourses";
	}

	public String showDistribution() {
		return "distribution";
	}

	public String showMatrix() {
		return "matrix";
	}

	public void getSummaryCourses() {
		if (major != null && !"".equals(major)) {
			PlanMainModel pmm = planMainService.getUnique(" where version=2016 and zy=?", major);
			PlanMainModel pmmd = null;
			if ("大类专业".equals(pmm.getType())) {
				SchoolMajor sm = schoolMajorService.getUnique(" where majorname=?", pmm.getZy());
				pmmd = planMainService.getUnique(" where version=2016 and zy=?", sm.getBigmajor());
			}
			if (pmm != null) {
				List<PlanCourseModel> pclist = planCourseService.listByParams(" where planid=? and kkxq=? and plantype!='TZKC' order by id", pmm.getId(), kkxq);
				List<PlanCourseModel> pcdlist = null;
				if (pmmd != null) {
					pcdlist = planCourseService.listByParams(" where planid=? and kkxq=? and plantype!='TZKC' order by id", pmmd.getId(), kkxq);
				}
				if (pcdlist != null) {
					pclist.addAll(pcdlist);
				}
				writeListToClient(response, pclist, true);
			}
		}
	}

	public void updateSortfield() {
		List<PlanCourseModel> pcmList = JSON.parseArray(gridJson, PlanCourseModel.class);
		for (PlanCourseModel pcm : pcmList) {
			planCourseService.update(pcm);
		}
	}

	public void validThreshold() {
		List<PlanDistribute> pdTList = planDistributeService.listByParams(" where version=?", version);
		List<PlanDistribute> distributeList = planDistributeService.listByParams(" where version is null order by sindex");
		List<PlanDistribute> pdRList = countDistribute(currentId, distributeList);
		int init = 0;
		if (ListUtil.filterList(pdTList)) {
			if (ListUtil.filterList(pdRList)) {
				for (PlanDistribute pd1 : pdTList) {
					for (PlanDistribute pd2 : pdRList) {
						if (pd2.getPlantype().equals(pd1.getPlantype())) {
							float pd1xf = pd1.getXf() == null ? 0 : pd1.getXf();
							float pd1xfmin = pd1.getXfmin() == null ? 0 : pd1.getXfmin();
							float pd2xf = pd2.getXf() == null ? 0 : pd2.getXf();
							float pd2xfmin = pd2.getXfmin() == null ? 0 : pd2.getXfmin();
							if (pd1xf != 0 || pd1xfmin != 0) {
								init += 1;
								if (pd2xf > pd1xf || pd2xfmin < pd1xfmin) {
									writeStringToClient(response, pd2.getMk() + "安排的总学分(" + pd2.getXf() + ")不符合要求（" + pd1xfmin + "-" + pd1xf + "）", false);
								}
							}
							float pd1sjxf = pd1.getSjxf() == null ? 0 : pd1.getSjxf();
							float pd1sjxfmin = pd1.getSjxfmin() == null ? 0 : pd1.getSjxfmin();
							float pd2sjxf = pd2.getSjxf() == null ? 0 : pd2.getSjxf();
							float pd2sjxfmin = pd2.getSjxfmin() == null ? 0 : pd2.getSjxfmin();
							if (pd1sjxf != 0 || pd1sjxfmin != 0) {
								init += 1;
								if (pd2sjxf > pd1sjxf || pd2sjxfmin < pd1sjxfmin) {
									writeStringToClient(response, pd2.getMk() + "安排的实践环节学分(" + pd2.getSjxf() + ")不符合要求（" + pd1sjxfmin + "-" + pd1sjxf + "）", false);
								}
							}
							float pd1zzxfbl = pd1.getZzxfbl() == null ? 0 : pd1.getZzxfbl();
							float pd1zzxfblmin = pd1.getZzxfblmin() == null ? 0 : pd1.getZzxfblmin();
							float pd2zzxfbl = pd2.getZzxfbl() == null ? 0 : pd2.getZzxfbl();
							float pd2zzxfblmin = pd2.getZzxfblmin() == null ? 0 : pd2.getZzxfblmin();
							if (pd1zzxfbl != 0 || pd1zzxfblmin != 0) {
								init += 1;
								if (pd2zzxfbl > pd1zzxfbl || pd2zzxfblmin < pd1zzxfblmin) {
									writeStringToClient(response, pd2.getMk() + "安排的占总学分比例(" + pd2.getZzxfbl() + ")不符合要求（" + pd1zzxfblmin + "-" + pd1zzxfbl + "）", false);
								}
							}
							float pd1sjbl = pd1.getSjbl() == null ? 0 : pd1.getSjbl();
							float pd1sjblmin = pd1.getSjblmin() == null ? 0 : pd1.getSjblmin();
							float pd2sjbl = pd2.getSjbl() == null ? 0 : pd2.getSjbl();
							float pd2sjblmin = pd2.getSjblmin() == null ? 0 : pd2.getSjblmin();
							if (pd1sjbl != 0 || pd1sjblmin != 0) {
								init += 1;
								if (pd2sjbl > pd1sjbl || pd2sjblmin < pd1sjblmin) {
									writeStringToClient(response, pd2.getMk() + "安排的实践环节占总学分比例(" + pd2.getSjbl() + ")不符合要求（" + pd1sjblmin + "-" + pd1sjbl + "）", false);
								}
							}
							float pd1qtxf = pd1.getQtxf() == null ? 0 : pd1.getQtxf();
							float pd1qtxfmin = pd1.getQtxfmin() == null ? 0 : pd1.getQtxfmin();
							float pd2qtxf = pd2.getQtxf() == null ? 0 : pd2.getQtxf();
							float pd2qtxfmin = pd2.getQtxfmin() == null ? 0 : pd2.getQtxfmin();
							if (pd1qtxf != 0 || pd1qtxfmin != 0) {
								init += 1;
								if (pd2qtxf > pd1qtxf || pd2qtxfmin < pd1qtxfmin) {
									writeStringToClient(response, pd2.getMk() + "安排的其他学分(" + pd2.getQtxf() + ")不符合要求（" + pd1qtxfmin + "-" + pd1qtxf + "）", false);
								}
							}
						}
					}
				}
				if (init == 0) {
					writeStringToClient(response, "该版本学分分配范围还没有划定，请联系教务处相关负责人。", false);
				} else {
					writeStringToClient(response, "true", true);
				}
			} else {
				writeStringToClient(response, "您还没有制定好本培养方案的修读计划。", false);
			}
		} else {
			writeStringToClient(response, "true", true);
		}
	}

	public void copyPlan() {
		PlanMainModel pmm = planMainService.getUnique(" where zy=? and version=(select max(version) from PlanMainModel where zy=?)", major, major);
		if (pmm != null) {
			pmm.setState(0);
			writeObjectToClient(response, pmm, true);
		} else {
			writeStringToClient(response, "true", true);
		}
	}

	public void uniquePlan() {
		PlanMainModel pmm = planMainService.getUnique(" where version=? and zy=?", version, major);
		if (pmm != null && pmm.getId() != currentId) {
			writeStringToClient(response, "已制定" + version + "版" + major + "培养方案", true);
		} else {
			writeStringToClient(response, "true", true);
		}
	}

	public void list() {
		Page<PlanMainModel> pagelist = null;
		if (department != null && "true".equals(department)) {
			pagelist = planMainService.listByParams(page, limit, " where type=?", type);
		} else {
			pagelist = planMainService.listByParams(page, limit, " where type=? and deptid=?", type, deptId);
		}
		writePageListToClient(response, pagelist);
	}

	public void editMatrix() {
		List<PlanMatrix> pmList = JSON.parseArray(gridJson, PlanMatrix.class);

		if (planid != 0) {
			planMatrixService.delByParams(" where planid=?", planid);

		}
		for (PlanMatrix pm : pmList) {
			pm.setId(0);
			planMatrixService.saveOrUpdate(pm);
		}
		writeResultToClient(response, true);
	}

	public void delCourseUnderPlan() {
		if (delIds != null) {
			for (int i = 0; i < delIds.length; i++) {
				planCourseService.delByParams(" where id=?", delIds[i]);
			}
		}
		writeResultToClient(response, true);
	}

	/*
	 * 课程管理页面使用
	 */

	public void queryCourseList() {
		Page<PlanCourseModel> pagelist = null;
		if (fname != null && !"".equals(fname)) {
			pagelist = planCourseService.listByParams(page, limit, " where planid=? and " + fname + " like ? order by sortfield", planid, "%" + fvalue + "%");
		} else {
			pagelist = planCourseService.listByParams(page, limit, " where planid=? order by sortfield", planid);
		}
		writePageListToClient(response, pagelist);
	}

	public void findMain() {
		PlanMainModel planMain = planMainService.get(currentId);
		writeObjectToClient(response, planMain, true);
	}

	public void findPlanCourse() {
		PlanCourseModel planCourse = planCourseService.get(currentId);
		writeObjectToClient(response, planCourse, true);
	}

	public void delMain() {
		if (delIds != null) {
			for (int i = 0; i < delIds.length; i++) {
				planCourseService.delByParams(" where planid=?", delIds[i]);
				planMatrixService.delByParams(" where planid=?", delIds[i]);
				planMainService.delete(delIds[i]);
			}
		}
		writeStringToClient(response, "", true);
	}

	public void getMatrixData() {
		List<PlanMatrix> pmList1 = planMatrixService.listByParams(" where planid=?", planid);
		List<PlanMatrix> pmList = new ArrayList<PlanMatrix>();
		if (pmList1 != null && !pmList1.isEmpty()) {
			pmList = pmList1;
		} else {
			List<PlanCourseModel> pdvList = planCourseService.listByParams(" where planid=?", planid);
			if (pdvList != null && !pdvList.isEmpty()) {
				for (PlanCourseModel pdv : pdvList) {
					PlanMatrix pm = new PlanMatrix();
					pm.setId(pdv.getId());
					pm.setKcmc(pdv.getKcmc());
					pm.setPlanid(planid);
					pmList.add(pm);
				}
			}
		}
		writeListToClient(response, pmList, true);
	}

	public void getDistributionData() {
		List<PlanDistribute> distributeList = planDistributeService.listByParams(" where version is null order by sindex");
		List<PlanDistribute> distributeList1 = planDistributeService.listByParams(" where version is null order by sindex");
		if (planid != 0) {
			countDistribute(planid, distributeList);
		}
		if ("bigmajor".equals(type)) {
			PlanMainModel pmm = planMainService.get(planid);
			if (pmm != null) {
				SchoolMajor sm = schoolMajorService.getUnique(" where majorname=?", pmm.getZy());
				if (sm != null) {
					// and state='2'"
					PlanMainModel pmm1 = planMainService.getUnique(" where zy=? and version=?", sm.getBigmajor(), version);
					if (pmm1 != null) {
						countDistribute(pmm1.getId(), distributeList1);
					}
				}
			}
			float bxxf = 0;
			if (ListUtil.filterList(distributeList)) {
				for (PlanDistribute pd : distributeList) {
					if (!"TZKC".equals(pd.getPlantype())) {
						bxxf += pd.getXf();
					}
				}
			}
			if (ListUtil.filterList(distributeList1)) {
				for (PlanDistribute pd1 : distributeList1) {
					bxxf += pd1.getXf();
				}
				for (PlanDistribute pd : distributeList) {
					for (PlanDistribute pd1 : distributeList1) {
						if (pd.getPlantype().equals(pd1.getPlantype()) && !"HXKC".equals(pd.getPlantype()) && !"TZKC".equals(pd.getPlantype())) {
							pd.setXf(pd1.getXf());
							pd.setSjxf(pd1.getSjxf());
							pd.setQtxf(pd1.getQtxf());
							continue;
						}
						if ("TZKC".equals(pd.getPlantype())) {
							pd.setXf(Constants.SUMMARY_XF - bxxf);
						}
					}
				}

			}
			summaryDistribute(distributeList);
		}

		writeListToClient(response, distributeList, true);
	}

	public List<PlanDistribute> summaryDistribute(List<PlanDistribute> distributeList) {
		for (PlanDistribute pd : distributeList) {
			pd.setZzxfbl(pd.getXf() == null ? 0 : pd.getXf() / Constants.SUMMARY_XF * 100);
			pd.setSjbl(pd.getSjxf() == null ? 0 : pd.getSjxf() / Constants.SUMMARY_XF * 100);
		}
		return distributeList;
	}

	public List<PlanDistribute> countDistribute(int planid, List<PlanDistribute> distributeList) {
		List<PlanCourseModel> pdvList = planCourseService.listByParams(" where planid=?", planid);
		// Set<String> xdsmSet = new HashSet<String>();
		float zxf = 0;
		if (pdvList != null && !pdvList.isEmpty()) {
			for (PlanCourseModel pdv : pdvList) {
				if ("major".equals(type) && "TZKC".equals(pdv.getPlantype())) {
				} else {
					zxf += pdv.getXf() == null ? 0 : pdv.getXf();
					// xdsmSet.add(pdv.getXdsm());
				}
			}
		}
		// float xxkc = 0;
		// for (String xdsm : xdsmSet) {
		// if (xdsm != null && !"".equals(xdsm)) {
		// List<PlanCourseModel> xdsmList = planCourseService.listByParams("
		// where planid=? and xdsm=?", planid, xdsm);
		// if (ListUtil.filterList(xdsmList)) {
		// float xxkc0 = xdsmList.get(0).getXf() == null ? 0 :
		// xdsmList.get(0).getXf();
		// xxkc += xxkc0 * (xdsmList.size() - 1);
		// }
		// }
		// }
		// zxf = zxf - xxkc;
		for (PlanDistribute pd : distributeList) {
			float mkxxkc = 0;
			String plantype = pd.getPlantype();
			float xf = 0;
			float sjxf = 0;
			float qtxf = 0;
			for (PlanCourseModel pdv : pdvList) {
				if (plantype.equals(pdv.getPlantype())) {
					if ("√".equals(pdv.getSyhjbj())) {
						sjxf += pdv.getXf() == null ? 0 : pdv.getXf();
					}
					qtxf += (pdv.getQtxf() == null ? 0 : pdv.getQtxf());
					xf += pdv.getXf() == null ? 0 : pdv.getXf();
				}
			}
			// for (String xdsm : xdsmSet) {
			// if (xdsm != null && !"".equals(xdsm)) {
			// List<PlanCourseModel> xdsmList = planCourseService.listByParams("
			// where planid=? and xdsm=? and plantype=?", planid, xdsm,
			// plantype);
			// if (ListUtil.filterList(xdsmList)) {
			// float xxkc0 = xdsmList.get(0).getXf() == null ? 0 :
			// xdsmList.get(0).getXf();
			// mkxxkc += xxkc0 * (xdsmList.size() - 1);
			// }
			// }
			// }
			xf = xf - mkxxkc;

			if ("major".equals(type) && "TZKC".equals(plantype)) {
				xf = Constants.SUMMARY_XF - zxf;
			}
			pd.setXf(xf);
			pd.setSjxf(sjxf);
			pd.setZzxfbl(xf / Constants.SUMMARY_XF * 100);
			pd.setSjbl(sjxf / Constants.SUMMARY_XF * 100);
			pd.setQtxf(qtxf);
		}
		return distributeList;
	}

	public void getDistributionThreshold() {
		List<PlanDistribute> distributeList = null;
		if (version != null && version != 0) {
			distributeList = planDistributeService.listByParams(" where version =? order by sindex", version);
		}
		if (!ListUtil.filterList(distributeList)) {
			distributeList = planDistributeService.listByParams(" where version is null order by sindex");
		}
		writeListToClient(response, distributeList, true);
	}

	public void distributeList() {
		List<PlanDistribute> distributeList = null;
		if (planid != 0) {
			distributeList = planDistributeService.listByParams(" where version=? order by sindex", planid);
		} else {
			distributeList = planDistributeService.listByParams(" where version is null order by sindex");
		}
		writeListToClient(response, distributeList, true);
	}

	public void exportBigToWord() throws IOException {
		String sortsql = " order by sortfield";
		DocumentHandler down = new DocumentHandler();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		PlanMainModel planMain = planMainService.get(currentId);
		List<PlanCourseModel> planlist1 = planCourseService.listByParams(" where planid=? and plantype='GGJC'" + sortsql, currentId);
		List<PlanCourseModel> planlist2 = planCourseService.listByParams(" where planid=? and plantype='CXCY'" + sortsql, currentId);
		float xf1 = 0;
		float qtxf1 = 0;
		// Set<String> xdsmSet = new HashSet<String>();
		if (ListUtil.filterList(planlist1)) {
			for (PlanCourseModel pdv : planlist1) {
				xf1 += pdv.getXf() == null ? 0 : pdv.getXf();
				qtxf1 += (pdv.getQtxf() == null ? 0 : pdv.getQtxf());
				// xdsmSet.add(pdv.getXdsm());
			}
		}
		if (ListUtil.filterList(planlist2)) {
			for (PlanCourseModel pdv : planlist2) {
				xf1 += pdv.getXf() == null ? 0 : pdv.getXf();
				qtxf1 += (pdv.getQtxf() == null ? 0 : pdv.getQtxf());
				// xdsmSet.add(pdv.getXdsm());
			}
		}

		// float xxkc = 0;
		// for (String xdsm : xdsmSet) {
		// if (xdsm != null && !"".equals(xdsm)) {
		// List<PlanCourseModel> xdsmList = new ArrayList<PlanCourseModel>();
		// List<PlanCourseModel> xdsmList1 = planCourseService.listByParams("
		// where planid=? and plantype='GGJC' and xdsm=?" + sortsql, currentId,
		// xdsm);
		// List<PlanCourseModel> xdsmList2 = planCourseService.listByParams("
		// where planid=? and plantype='CXCY' and xdsm=?" + sortsql, currentId,
		// xdsm);
		// if (ListUtil.filterList(xdsmList1)) {
		// xdsmList.addAll(xdsmList1);
		// }
		// if (ListUtil.filterList(xdsmList2)) {
		// xdsmList.addAll(xdsmList2);
		// }
		// if (ListUtil.filterList(xdsmList)) {
		// float xxkc0 = xdsmList.get(0).getXf() == null ? 0 :
		// xdsmList.get(0).getXf();
		// xxkc += xxkc0 * (xdsmList.size() - 1);
		// }
		// }
		// }
		// xf1 -= xxkc;

		String p1 = xf1 == 0 ? "" : getPrettyNumber(xf1) + "";
		// String p2 = qtxf1 == 0 ? "" : "(" + getPrettyNumber(qtxf1) + ")";
		String xfhj1 = p1;// + p2;

		List<PlanCourseModel> planlist3 = planCourseService.listByParams(" where planid=? and plantype='DLBX'" + sortsql, currentId);
		List<PlanCourseModel> planlist4 = planCourseService.listByParams(" where planid=? and plantype='XKBX'" + sortsql, currentId);

		// xdsmSet = new HashSet<String>();
		float xf2 = 0;
		float qtxf2 = 0;
		if (ListUtil.filterList(planlist3)) {
			for (PlanCourseModel pdv : planlist3) {
				xf2 += pdv.getXf() == null ? 0 : pdv.getXf();
				qtxf2 += (pdv.getQtxf() == null ? 0 : pdv.getQtxf());
				// xdsmSet.add(pdv.getXdsm());
			}
		}
		if (ListUtil.filterList(planlist4)) {
			for (PlanCourseModel pdv : planlist4) {
				xf2 += pdv.getXf() == null ? 0 : pdv.getXf();
				qtxf2 += (pdv.getQtxf() == null ? 0 : pdv.getQtxf());
				// xdsmSet.add(pdv.getXdsm());
			}
		}
		// xxkc = 0;
		// for (String xdsm : xdsmSet) {
		// if (xdsm != null && !"".equals(xdsm)) {
		// List<PlanCourseModel> xdsmList = new ArrayList<PlanCourseModel>();
		// List<PlanCourseModel> xdsmList1 = planCourseService.listByParams("
		// where planid=? and plantype='DLBX' and xdsm=?" + sortsql, currentId,
		// xdsm);
		// List<PlanCourseModel> xdsmList2 = planCourseService.listByParams("
		// where planid=? and plantype='XKBX' and xdsm=?" + sortsql, currentId,
		// xdsm);
		// if (ListUtil.filterList(xdsmList1)) {
		// xdsmList.addAll(xdsmList1);
		// }
		// if (ListUtil.filterList(xdsmList2)) {
		// xdsmList.addAll(xdsmList2);
		// }
		// if (ListUtil.filterList(xdsmList)) {
		// float xxkc0 = xdsmList.get(0).getXf() == null ? 0 :
		// xdsmList.get(0).getXf();
		// xxkc += xxkc0 * (xdsmList.size() - 1);
		// }
		// }
		// }
		// xf2 -= xxkc;

		p1 = xf2 == 0 ? "" : getPrettyNumber(xf2) + "";
		// String p2 = qtxf2 == 0 ? "" : "(" + getPrettyNumber(qtxf2) + ")";
		String xfhj2 = p1;// + p2;

		dataMap.put("xfhj1", xfhj1);
		dataMap.put("xfhj2", xfhj2);
		dataMap.put("plan", planMain);
		dataMap.put("courselist1", planlist1);
		dataMap.put("courselist2", planlist2);
		dataMap.put("courselist3", planlist3);
		dataMap.put("courselist4", planlist4);

		response.setContentType("application/x-msdownload");
		response.setHeader("Content-Disposition", "attachment; filename=" + java.net.URLEncoder.encode(planMain.getVersion() + " " + planMain.getZy() + "大类 培养方案.doc", "UTF-8").replaceAll("\\+", " "));
		down.createDoc("big.ftl", dataMap, response.getWriter());
	}

	public void exportBigMajorToWord() throws IOException {
		String sortsql = " order by sortfield";
		DocumentHandler down = new DocumentHandler();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		PlanMainModel planMain = planMainService.get(currentId);
		SchoolMajor sm = schoolMajorService.getUnique(" where majorid=?", planMain.getZydm());
		PlanMainModel main = null;
		if (sm != null && !"".equals(StringUtil.parseNULLtoString(sm.getBigmajor()))) {
			main = planMainService.getUnique(" where version=? and zy=?", planMain.getVersion(), sm.getBigmajor());
		}

		if (main != null) {
			List<PlanCourseModel> planlist1 = planCourseService.listByParams(" where planid=? and plantype='HXKC'" + sortsql, currentId);
			// Set<String> xdsmSet = new HashSet<String>();
			float xf3 = 0;
			float qtxf3 = 0;
			if (ListUtil.filterList(planlist1)) {
				for (PlanCourseModel pdv : planlist1) {
					xf3 += pdv.getXf() == null ? 0 : pdv.getXf();
					qtxf3 += (pdv.getQtxf() == null ? 0 : pdv.getQtxf());
					// xdsmSet.add(pdv.getXdsm());
				}
			}
			// float xxkc = 0;
			// for (String xdsm : xdsmSet) {
			// if (xdsm != null && !"".equals(xdsm)) {
			// List<PlanCourseModel> xdsmList = planCourseService.listByParams("
			// where planid=? and plantype='HXKC' and xdsm=?" + sortsql,
			// currentId, xdsm);
			// if (ListUtil.filterList(xdsmList)) {
			// float xxkc0 = xdsmList.get(0).getXf() == null ? 0 :
			// xdsmList.get(0).getXf();
			// xxkc += xxkc0 * (xdsmList.size() - 1);
			// }
			// }
			// }
			// xf3 -= xxkc;

			String p1 = xf3 == 0 ? "" : getPrettyNumber(xf3) + "";
			String p2 = qtxf3 == 0 ? "" : "(" + getPrettyNumber(qtxf3) + ")";
			String xfhj1 = p1 + p2;

			List<PlanCourseModel> planlist2 = planCourseService.listByParams(" where planid=? and plantype='TZKC'" + sortsql, currentId);
			// xdsmSet = new HashSet<String>();
			float xf4 = 0;
			float qtxf4 = 0;
			// if (ListUtil.filterList(planlist2)) {
			// for (PlanCourseModel pdv : planlist2) {
			// xf4 += pdv.getXf() == null ? 0 : pdv.getXf();
			// qtxf4 += (pdv.getQtxf() == null ? 0 : pdv.getQtxf());
			// xdsmSet.add(pdv.getXdsm());
			// }
			// }
			// xxkc = 0;
			// for (String xdsm : xdsmSet) {
			// if (xdsm != null && !"".equals(xdsm)) {
			// List<PlanCourseModel> xdsmList = planCourseService.listByParams("
			// where planid=? and plantype='TZKC' and xdsm=?" + sortsql,
			// currentId, xdsm);
			// if (ListUtil.filterList(xdsmList)) {
			// float xxkc0 = xdsmList.get(0).getXf() == null ? 0 :
			// xdsmList.get(0).getXf();
			// xxkc += xxkc0 * (xdsmList.size() - 1);
			// }
			// }
			// }
			// xf4 -= xxkc;

			p1 = xf4 == 0 ? "" : getPrettyNumber(xf4) + "";
			p2 = qtxf4 == 0 ? "" : "(" + getPrettyNumber(qtxf4) + ")";
			String xfhj2 = p1 + p2;

			p1 = (xf3 + xf4) == 0 ? "" : getPrettyNumber(xf3 + xf4) + "";
			p2 = (qtxf3 + qtxf4) == 0 ? "" : "(" + getPrettyNumber(qtxf3 + qtxf4) + ")";
			String xfhj3 = p1 + p2;

			dataMap.put("xfhj1", xfhj1);

			List<PlanMatrix> courselist = planMatrixService.listByParams(" where planid=?", currentId);

			dataMap.put("plan", planMain);
			dataMap.put("courselist1", planlist1);
			dataMap.put("courselist2", planlist2);
			if (ListUtil.filterList(courselist)) {
				dataMap.put("kcjz", "true");
				dataMap.put("courselist", courselist);
			}

			// 汇总表
			List<PlanCourseModel> planlist3 = planCourseService.listByParams(" where planid=? and plantype='GGJC'" + sortsql, main.getId());
			List<PlanCourseModel> planlist4 = planCourseService.listByParams(" where planid=? and plantype='CXCY'" + sortsql, main.getId());
			List<PlanCourseModel> planlist5 = planCourseService.listByParams(" where planid=? and plantype='DLBX'" + sortsql, main.getId());
			List<PlanCourseModel> planlist6 = planCourseService.listByParams(" where planid=? and plantype='XKBX'" + sortsql, main.getId());
			List<PlanCourseModel> list1 = new ArrayList<PlanCourseModel>();
			List<PlanCourseModel> list2 = new ArrayList<PlanCourseModel>();
			List<PlanCourseModel> list3 = new ArrayList<PlanCourseModel>();
			if (ListUtil.filterList(planlist3)) {
				list1.addAll(planlist3);
			}
			if (ListUtil.filterList(planlist4)) {
				list1.addAll(planlist4);
			}
			if (ListUtil.filterList(planlist5)) {
				list2.addAll(planlist5);
			}
			if (ListUtil.filterList(planlist6)) {
				list2.addAll(planlist6);
			}
			if (ListUtil.filterList(planlist1)) {
				list3.addAll(planlist1);
			}
			if (ListUtil.filterList(planlist2)) {
				list3.addAll(planlist2);
			}
			// float zxf = 0;
			float pt1 = 0;
			float pt2 = 0;
			float pt3 = 0;
			float pthj = 0;
			float zqtxf = 0;
			float qtxf1 = 0;
			float qtxf2 = 0;
			qtxf3 = 0;
			float sj1 = 0;
			float syxs1 = 0;
			float sj2 = 0;
			float syxs2 = 0;
			float sj3 = 0;
			float syxs3 = 0;
			float sjhj = 0;
			float bl1 = 0;
			float bl2 = 0;
			float bl3 = 0;
			float sjbl1 = 0;
			float sjbl2 = 0;
			float sjbl3 = 0;
			float sjblhj = 0;

			// xdsmSet = new HashSet<String>();
			for (PlanCourseModel pdv : list1) {
				pt1 += pdv.getXf() == null ? 0 : pdv.getXf();
				qtxf1 += pdv.getQtxf() == null ? 0 : pdv.getQtxf();
				if ("√".equals(pdv.getSjhj())) {
					sj1 += pdv.getXf();
				}
				syxs1 += pdv.getSyxs();
				// xdsmSet.add(pdv.getXdsm());
			}
			// xxkc = 0;
			// for (String xdsm : xdsmSet) {
			// if (xdsm != null && !"".equals(xdsm)) {
			// List<PlanCourseModel> xdsmList = new
			// ArrayList<PlanCourseModel>();
			// List<PlanCourseModel> xdsmList1 =
			// planCourseService.listByParams(" where planid=? and
			// plantype='GGJC' and xdsm=?" + sortsql, main.getId(), xdsm);
			// List<PlanCourseModel> xdsmList2 =
			// planCourseService.listByParams(" where planid=? and
			// plantype='CXCY' and xdsm=?" + sortsql, main.getId(), xdsm);
			// if (ListUtil.filterList(xdsmList1)) {
			// xdsmList.addAll(xdsmList1);
			// }
			// if (ListUtil.filterList(xdsmList2)) {
			// xdsmList.addAll(xdsmList2);
			// }
			// if (ListUtil.filterList(xdsmList)) {
			// float xxkc0 = xdsmList.get(0).getXf() == null ? 0 :
			// xdsmList.get(0).getXf();
			// xxkc += xxkc0 * (xdsmList.size() - 1);
			// }
			// }
			// }
			// pt1 -= xxkc;

			// xdsmSet = new HashSet<String>();
			for (PlanCourseModel pdv : list2) {
				pt2 += pdv.getXf() == null ? 0 : pdv.getXf();
				qtxf2 += pdv.getQtxf() == null ? 0 : pdv.getQtxf();
				if ("√".equals(pdv.getSjhj())) {
					sj2 += pdv.getXf();
				}
				syxs2 += pdv.getSyxs();
				// xdsmSet.add(pdv.getXdsm());
			}
			// xxkc = 0;
			// for (String xdsm : xdsmSet) {
			// if (xdsm != null && !"".equals(xdsm)) {
			// List<PlanCourseModel> xdsmList = new
			// ArrayList<PlanCourseModel>();
			// List<PlanCourseModel> xdsmList1 =
			// planCourseService.listByParams(" where planid=? and
			// plantype='DLBX' and xdsm=?" + sortsql, main.getId(), xdsm);
			// List<PlanCourseModel> xdsmList2 =
			// planCourseService.listByParams(" where planid=? and
			// plantype='XKBX' and xdsm=?" + sortsql, main.getId(), xdsm);
			// if (ListUtil.filterList(xdsmList1)) {
			// xdsmList.addAll(xdsmList1);
			// }
			// if (ListUtil.filterList(xdsmList2)) {
			// xdsmList.addAll(xdsmList2);
			// }
			// if (ListUtil.filterList(xdsmList)) {
			// float xxkc0 = xdsmList.get(0).getXf() == null ? 0 :
			// xdsmList.get(0).getXf();
			// xxkc += xxkc0 * (xdsmList.size() - 1);
			// }
			// }
			// }
			// pt2 -= xxkc;

			// xdsmSet = new HashSet<String>();
			for (PlanCourseModel pdv : list3) {
				// pt3 += pdv.getXf() == null ? 0 : pdv.getXf();
				qtxf3 += pdv.getQtxf() == null ? 0 : pdv.getQtxf();
				if ("√".equals(pdv.getSjhj())) {
					sj3 += pdv.getXf();
				}
				syxs3 += pdv.getSyxs();
				// xdsmSet.add(pdv.getXdsm());
			}
			// xxkc = 0;
			// for (String xdsm : xdsmSet) {
			// if (xdsm != null && !"".equals(xdsm)) {
			// List<PlanCourseModel> xdsmList = new
			// ArrayList<PlanCourseModel>();
			// List<PlanCourseModel> xdsmList1 =
			// planCourseService.listByParams(" where planid=? and
			// plantype='HXKC' and xdsm=?" + sortsql, currentId, xdsm);
			// List<PlanCourseModel> xdsmList2 =
			// planCourseService.listByParams(" where planid=? and
			// plantype='TZKC' and xdsm=?" + sortsql, currentId, xdsm);
			// if (ListUtil.filterList(xdsmList1)) {
			// xdsmList.addAll(xdsmList1);
			// }
			// if (ListUtil.filterList(xdsmList2)) {
			// xdsmList.addAll(xdsmList2);
			// }
			// if (ListUtil.filterList(xdsmList)) {
			// float xxkc0 = xdsmList.get(0).getXf() == null ? 0 :
			// xdsmList.get(0).getXf();
			// xxkc += xxkc0 * (xdsmList.size() - 1);
			// }
			// }
			// }
			// pt3 -= xxkc;

			// pthj = zxf = pt1 + pt2 + pt3;
			pt3 = Constants.SUMMARY_XF - pt1 - pt2 - 10;
			zqtxf = qtxf1 + qtxf2 + qtxf3;
			sjhj = sj1 + sj2 + sj3 + syxs1 / 16 + syxs2 / 16 + syxs3 / 16;
			// if (zxf != 0) {
			bl1 = (pt1 + 10) / Constants.SUMMARY_XF * 100;
			bl2 = pt2 / Constants.SUMMARY_XF * 100;
			bl3 = pt3 / Constants.SUMMARY_XF * 100;
			sjbl1 = (sj1 + syxs1 / 16 + 10) / Constants.SUMMARY_XF * 100;
			sjbl2 = (sj2 + syxs2 / 16) / Constants.SUMMARY_XF * 100;
			sjbl3 = (sj3 + syxs3 / 16) / Constants.SUMMARY_XF * 100;
			sjblhj = sjbl1 + sjbl2 + sjbl3;
			// }

			dataMap.put("pt1", getPrettyNumber(pt1));// + (qtxf1 == 0 ? "" : "("
														// +
														// getPrettyNumber(qtxf1)
														// + ")"));
			dataMap.put("pt2", getPrettyNumber(pt2));// + (qtxf2 == 0 ? "" : "("
														// +
														// getPrettyNumber(qtxf2)
														// + ")"));
			dataMap.put("pt3", getPrettyNumber(pt3));// + (qtxf3 == 0 ? "" : "("
														// +
														// getPrettyNumber(qtxf3)
														// + ")"));
			dataMap.put("pthj", Constants.SUMMARY_XF - 10);// + (zqtxf == 0 ? ""
															// :
															// "(" +
															// getPrettyNumber(zqtxf)
															// + ")"));
			dataMap.put("sj1", getPrettyNumber(sj1 + syxs1 / 16) + "+【10】");
			dataMap.put("sj2", getPrettyNumber(sj2 + syxs2 / 16));
			dataMap.put("sj3", getPrettyNumber(sj3 + syxs3 / 16));
			dataMap.put("sjhj", getPrettyNumber(sjhj));
			dataMap.put("bl1", getPrettyNumber(bl1));
			dataMap.put("bl2", getPrettyNumber(bl2));
			dataMap.put("bl3", getPrettyNumber(bl3));
			dataMap.put("sjbl1", getPrettyNumber(sjbl1));
			dataMap.put("sjbl2", getPrettyNumber(sjbl2));
			dataMap.put("sjbl3", getPrettyNumber(sjbl3));
			dataMap.put("sjblhj", getPrettyNumber(sjblhj));

			dataMap.put("xfhj2", pt3 - xf3);
			dataMap.put("ptxfhj", pt3);

			response.setContentType("application/x-msdownload");
			response.setHeader("Content-Disposition",
					"attachment; filename=" + java.net.URLEncoder.encode(planMain.getVersion() + " " + planMain.getZy() + "大类培养 专业培养方案.doc", "UTF-8").replaceAll("\\+", " "));
			down.createDoc("major-big.ftl", dataMap, response.getWriter());
		} else {
			writeStringToClient(response, "请先做大类培养方案", true);
		}
	}

	public void exportToWord() throws IOException {
		String sortsql = " order by sortfield";
		DocumentHandler down = new DocumentHandler();
		Map<String, Object> dataMap = new HashMap<String, Object>();
		PlanMainModel planMain = planMainService.get(currentId);
		List<PlanCourseModel> planlist1 = planCourseService.listByParams(" where planid=? and plantype='GGJC'" + sortsql, currentId);
		List<PlanCourseModel> planlist2 = planCourseService.listByParams(" where planid=? and plantype='CXCY'" + sortsql, currentId);
		List<PlanCourseModel> planlist3 = planCourseService.listByParams(" where planid=? and plantype='DLBX'" + sortsql, currentId);
		List<PlanCourseModel> planlist4 = planCourseService.listByParams(" where planid=? and plantype='XKBX'" + sortsql, currentId);
		List<PlanCourseModel> planlist5 = planCourseService.listByParams(" where planid=? and plantype='HXKC'" + sortsql, currentId);
		List<PlanCourseModel> planlist6 = planCourseService.listByParams(" where planid=? and plantype='TZKC'" + sortsql, currentId);

		List<PlanCourseModel> list1 = new ArrayList<PlanCourseModel>();
		list1.addAll(planlist1);
		list1.addAll(planlist2);
		List<PlanCourseModel> list2 = new ArrayList<PlanCourseModel>();
		list2.addAll(planlist3);
		list2.addAll(planlist4);
		// float zxf = 0;
		float pt1, pt2, pt3, pthj, xfhj3, xfhj4;
		pt1 = pt2 = pt3 = pthj = xfhj3 = xfhj4 = 0;
		float qtxf1, qtxf2, qtxf3, qtxf4, zqtxf;
		qtxf1 = qtxf2 = qtxf3 = qtxf4 = zqtxf = 0;
		float sj1, sj2, sj3, syxs1, syxs2, syxs3, sjhj = 0;
		sj1 = sj2 = sj3 = syxs1 = syxs2 = syxs3 = sjhj = 0;
		float bl1, bl2, bl3;
		bl1 = bl2 = bl3 = 0;
		float sjbl1, sjbl2, sjbl3, sjblhj;
		sjbl1 = sjbl2 = sjbl3 = sjblhj = 0;

		// Set<String> xdsmSet = new HashSet<String>();
		for (PlanCourseModel pdv : list1) {
			pt1 += pdv.getXf() == null ? 0 : pdv.getXf();
			qtxf1 += pdv.getQtxf() == null ? 0 : pdv.getQtxf();
			if ("√".equals(pdv.getSjhj())) {
				sj1 += pdv.getXf();
			}
			syxs1 += pdv.getSyxs();
			// xdsmSet.add(pdv.getXdsm());
		}

		// float xxkc = 0;
		// for (String xdsm : xdsmSet) {
		// if (xdsm != null && !"".equals(xdsm)) {
		// List<PlanCourseModel> xdsmList = new ArrayList<PlanCourseModel>();
		// List<PlanCourseModel> xdsmList1 = planCourseService.listByParams("
		// where planid=? and plantype='GGJC' and xdsm=?" + sortsql, currentId,
		// xdsm);
		// List<PlanCourseModel> xdsmList2 = planCourseService.listByParams("
		// where planid=? and plantype='CXCY' and xdsm=?" + sortsql, currentId,
		// xdsm);
		// if (ListUtil.filterList(xdsmList1)) {
		// xdsmList.addAll(xdsmList1);
		// }
		// if (ListUtil.filterList(xdsmList2)) {
		// xdsmList.addAll(xdsmList2);
		// }
		// if (ListUtil.filterList(xdsmList)) {
		// float xxkc0 = xdsmList.get(0).getXf() == null ? 0 :
		// xdsmList.get(0).getXf();
		// xxkc += xxkc0 * (xdsmList.size() - 1);
		// }
		// }
		// }
		// pt1 -= xxkc;

		// xdsmSet = new HashSet<String>();
		for (PlanCourseModel pdv : list2) {
			pt2 += pdv.getXf() == null ? 0 : pdv.getXf();
			qtxf2 += pdv.getQtxf() == null ? 0 : pdv.getQtxf();
			if ("√".equals(pdv.getSjhj())) {
				sj2 += pdv.getXf();
			}
			syxs2 += pdv.getSyxs();
			// xdsmSet.add(pdv.getXdsm());
		}
		// xxkc = 0;
		// for (String xdsm : xdsmSet) {
		// if (xdsm != null && !"".equals(xdsm)) {
		// List<PlanCourseModel> xdsmList = new ArrayList<PlanCourseModel>();
		// List<PlanCourseModel> xdsmList1 = planCourseService.listByParams("
		// where planid=? and plantype='DLBX' and xdsm=?" + sortsql, currentId,
		// xdsm);
		// List<PlanCourseModel> xdsmList2 = planCourseService.listByParams("
		// where planid=? and plantype='XKBX' and xdsm=?" + sortsql, currentId,
		// xdsm);
		// if (ListUtil.filterList(xdsmList1)) {
		// xdsmList.addAll(xdsmList1);
		// }
		// if (ListUtil.filterList(xdsmList2)) {
		// xdsmList.addAll(xdsmList2);
		// }
		// if (ListUtil.filterList(xdsmList)) {
		// float xxkc0 = xdsmList.get(0).getXf() == null ? 0 :
		// xdsmList.get(0).getXf();
		// xxkc += xxkc0 * (xdsmList.size() - 1);
		// }
		// }
		// }
		// pt2 -= xxkc;

		// xdsmSet = new HashSet<String>();
		for (PlanCourseModel pdv : planlist5) {
			pt3 += pdv.getXf() == null ? 0 : pdv.getXf();
			qtxf3 += pdv.getQtxf() == null ? 0 : pdv.getQtxf();
			if ("√".equals(pdv.getSjhj())) {
				sj3 += pdv.getXf();
			}
			syxs3 += pdv.getSyxs();
			// xdsmSet.add(pdv.getXdsm());
		}
		// xxkc = 0;
		// for (String xdsm : xdsmSet) {
		// if (xdsm != null && !"".equals(xdsm)) {
		// List<PlanCourseModel> xdsmList = planCourseService.listByParams("
		// where planid=? and plantype='HXKC' and xdsm=?" + sortsql, currentId,
		// xdsm);
		// if (ListUtil.filterList(xdsmList)) {
		// float xxkc0 = xdsmList.get(0).getXf() == null ? 0 :
		// xdsmList.get(0).getXf();
		// xxkc += xxkc0 * (xdsmList.size() - 1);
		// }
		// }
		// }
		// pt3 -= xxkc;
		xfhj3 = pt3;
		for (PlanCourseModel pdv : planlist6) {
			pt3 += pdv.getXf() == null ? 0 : pdv.getXf();
			qtxf4 += pdv.getQtxf() == null ? 0 : pdv.getQtxf();
			if ("√".equals(pdv.getSjhj())) {
				sj3 += pdv.getXf();
			}
			syxs3 += pdv.getSyxs();
			// xdsmSet.add(pdv.getXdsm());
		}
		// xxkc = 0;
		// for (String xdsm : xdsmSet) {
		// if (xdsm != null && !"".equals(xdsm)) {
		// List<PlanCourseModel> xdsmList = planCourseService.listByParams("
		// where planid=? and plantype='TZKC' and xdsm=?" + sortsql, currentId,
		// xdsm);
		// if (ListUtil.filterList(xdsmList)) {
		// float xxkc0 = xdsmList.get(0).getXf() == null ? 0 :
		// xdsmList.get(0).getXf();
		// xxkc += xxkc0 * (xdsmList.size() - 1);
		// }
		// }
		// }
		// pt3 -= xxkc;
		pt3 = Constants.SUMMARY_XF - pt1 - pt2 - 10;
		xfhj4 = pt3 - xfhj3;

		pthj = Constants.SUMMARY_XF;
		zqtxf = qtxf1 + qtxf2 + qtxf3 + qtxf4;
		sjhj = sj1 + sj2 + sj3 + syxs1 / 16 + syxs2 / 16 + syxs3 / 16;
		bl1 = (pt1 + 10) / Constants.SUMMARY_XF * 100;
		bl2 = pt2 / Constants.SUMMARY_XF * 100;
		bl3 = pt3 / Constants.SUMMARY_XF * 100;
		sjbl1 = (sj1 + syxs1 / 16 + 10) / Constants.SUMMARY_XF * 100;
		sjbl2 = (sj2 + syxs2 / 16) / Constants.SUMMARY_XF * 100;
		sjbl3 = (sj3 + syxs3 / 16) / Constants.SUMMARY_XF * 100;
		sjblhj = sjbl1 + sjbl2 + sjbl3;

		dataMap.put("pt1", getPrettyNumber(pt1));// + (qtxf1 == 0 ? "" : "(" +
													// getPrettyNumber(qtxf1) +
													// ")"));
		dataMap.put("pt2", getPrettyNumber(pt2));// + (qtxf2 == 0 ? "" : "(" +
													// getPrettyNumber(qtxf2) +
													// ")"));
		dataMap.put("pt3", getPrettyNumber(pt3));// + ((qtxf3 + qtxf4) == 0 ? ""
													// : "(" +
													// getPrettyNumber(qtxf3 +
													// qtxf4) + ")"));
		dataMap.put("pthj", getPrettyNumber(pthj - 10));// + (zqtxf == 0 ? "" :
														// "(" +
														// getPrettyNumber(zqtxf)
														// +
														// ")"));
		dataMap.put("xfhj3", getPrettyNumber(xfhj3));// + ((qtxf3) == 0 ? "" :
														// "(" +
														// getPrettyNumber(qtxf3)
														// + ")"));
		dataMap.put("xfhj4", getPrettyNumber(xfhj4));// + ((qtxf4) == 0 ? "" :
														// "(" +
														// getPrettyNumber(qtxf4)
														// + ")"));
		dataMap.put("sj1", getPrettyNumber(sj1 + syxs1 / 16) + "+【10】");
		dataMap.put("sj2", getPrettyNumber(sj2 + syxs2 / 16));
		dataMap.put("sj3", getPrettyNumber(sj3 + syxs3 / 16));
		dataMap.put("sjhj", getPrettyNumber(sjhj));
		dataMap.put("bl1", getPrettyNumber(bl1));
		dataMap.put("bl2", getPrettyNumber(bl2));
		dataMap.put("bl3", getPrettyNumber(bl3));
		dataMap.put("sjbl1", getPrettyNumber(sjbl1));
		dataMap.put("sjbl2", getPrettyNumber(sjbl2));
		dataMap.put("sjbl3", getPrettyNumber(sjbl3));
		dataMap.put("sjblhj", getPrettyNumber(sjblhj));

		List<PlanMatrix> courselist = planMatrixService.listByParams(" where planid=?", currentId);

		dataMap.put("plan", planMain);
		dataMap.put("planlist1", planlist1);
		dataMap.put("planlist2", planlist2);
		dataMap.put("planlist3", planlist3);
		dataMap.put("planlist4", planlist4);
		dataMap.put("planlist5", planlist5);
		dataMap.put("planlist6", planlist6);
		if (ListUtil.filterList(courselist)) {
			dataMap.put("kcjz", "true");
			dataMap.put("courselist", courselist);
		}

		response.setContentType("application/x-msdownload");
		response.setHeader("Content-Disposition", "attachment; filename=" + java.net.URLEncoder.encode(planMain.getVersion() + " " + planMain.getZy() + "专业 培养方案.doc", "UTF-8").replaceAll("\\+", " "));
		down.createDoc("major.ftl", dataMap, response.getWriter());
	}

	public int getPlanid() {
		return planid;
	}

	public void setPlanid(int planid) {
		this.planid = planid;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public int[] getDelIds() {
		return delIds;
	}

	public void setDelIds(int[] delIds) {
		this.delIds = delIds;
	}

	public int getCurrentId() {
		return currentId;
	}

	public void setCurrentId(int currentId) {
		this.currentId = currentId;
	}

	public PlanMainModel getPlanMain() {
		return planMain;
	}

	public void setPlanMain(PlanMainModel planMain) {
		this.planMain = planMain;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getBigtag() {
		return bigtag;
	}

	public void setBigtag(int bigtag) {
		this.bigtag = bigtag;
	}

	public String getGridJson() {
		return gridJson;
	}

	public void setGridJson(String gridJson) {
		this.gridJson = gridJson;
	}

	public int getDeptId() {
		return deptId;
	}

	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getFvalue() {
		return fvalue;
	}

	public void setFvalue(String fvalue) {
		this.fvalue = fvalue;
	}

	public String getOpterator() {
		return opterator;
	}

	public void setOpterator(String opterator) {
		this.opterator = opterator;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public boolean isCopy() {
		return copy;
	}

	public void setCopy(boolean copy) {
		this.copy = copy;
	}

	public int[] getCourseIds() {
		return courseIds;
	}

	public void setCourseIds(int[] courseIds) {
		this.courseIds = courseIds;
	}

	public String getPlantype() {
		return plantype;
	}

	public void setPlantype(String plantype) {
		this.plantype = plantype;
	}

	public String getKkxq() {
		return kkxq;
	}

	public void setKkxq(String kkxq) {
		this.kkxq = kkxq;
	}

	public String getPassed() {
		return passed;
	}

	public void setPassed(String passed) {
		this.passed = passed;
	}

}
