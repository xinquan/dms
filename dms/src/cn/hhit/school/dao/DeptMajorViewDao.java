package cn.hhit.school.dao;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.school.model.DeptMajorView;

public interface DeptMajorViewDao extends IBaseDao<DeptMajorView, Integer> {

}
