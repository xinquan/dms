package cn.hhit.school.dao;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.hhit.school.model.DeptMajorView;

@Repository("DeptMajorViewDao")
public class DeptMajorViewDaoImpl extends BaseHibernateDao<DeptMajorView, Integer> implements DeptMajorViewDao {
}
