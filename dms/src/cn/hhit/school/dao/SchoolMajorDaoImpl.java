package cn.hhit.school.dao;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.hhit.school.model.SchoolMajor;

@Repository("SchoolMajorDao")
public class SchoolMajorDaoImpl extends BaseHibernateDao<SchoolMajor, Integer> implements SchoolMajorDao {

	private static final String HQL_LIST = "from SchoolMajor ";
	private static final String HQL_COUNT = "select count(*) from SchoolMajor ";
	private static final String HQL_LIST_QUERY_CONDITION = " where username like ?";
	private static final String HQL_LIST_ORDER = "order by id desc";

	private static final String HQL_LIST_QUERY_ALL = HQL_LIST + HQL_LIST_QUERY_CONDITION + HQL_LIST_ORDER;
	private static final String HQL_COUNT_QUERY_ALL = HQL_COUNT + HQL_LIST_QUERY_CONDITION;

	private static final String HQL_LIST_LIST_PARAM = " where 1=1 and ";
}
