package cn.hhit.school.dao;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.hhit.school.model.PlanMatrix;

@Repository("PlanMatrixDao")
public class PlanMatrixDaoImpl extends BaseHibernateDao<PlanMatrix, Integer> implements PlanMatrixDao {

	private static final String HQL_LIST = "from PlanMatrix ";
	private static final String HQL_COUNT = "select count(*) from PlanMatrix ";

	private static final String HQL_LIST_QUERY_CONDITION = " where username like ?";
	private static final String HQL_LIST_QUERY_ALL = HQL_LIST + HQL_LIST_QUERY_CONDITION + "order by id desc";
	private static final String HQL_COUNT_QUERY_ALL = HQL_COUNT + HQL_LIST_QUERY_CONDITION;
}
