package cn.hhit.school.dao;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.school.model.PlanMatrix;

public interface PlanMatrixDao extends IBaseDao<PlanMatrix, Integer> {
    
}
