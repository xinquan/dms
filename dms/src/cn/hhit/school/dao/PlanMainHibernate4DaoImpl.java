package cn.hhit.school.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.hhit.school.model.PlanMainModel;
import cn.hhit.school.model.PlanMainQueryModel;

@Repository("PlanMainDao")
public class PlanMainHibernate4DaoImpl extends BaseHibernateDao<PlanMainModel, Integer> implements PlanMainDao {

    private static final String HQL_LIST = "from PlanMainModel ";
    private static final String HQL_COUNT = "select count(*) from PlanMainModel ";

    private static final String HQL_LIST_QUERY_CONDITION = " where username like ?";
    private static final String HQL_LIST_QUERY_ALL = HQL_LIST + HQL_LIST_QUERY_CONDITION + "order by id desc";
    private static final String HQL_COUNT_QUERY_ALL = HQL_COUNT + HQL_LIST_QUERY_CONDITION;

    @Override
    public List<PlanMainModel> query(int pn, int pageSize, PlanMainQueryModel command) {
        return listByHql(HQL_LIST_QUERY_ALL, pn, pageSize, getQueryParam(command));
    }

    @Override
    public int countQuery(PlanMainQueryModel command) {
        return this.<Number>aggregate(HQL_COUNT_QUERY_ALL, getQueryParam(command)).intValue();
    }
    

    private Object[] getQueryParam(PlanMainQueryModel command) {
        //TODO 改成全文索引
        String usernameLikeStr = "%" + command.getZy() + "%";
        return new Object[]{
            usernameLikeStr
        };
    }

}
