package cn.hhit.school.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.hhit.school.model.PlanDistribute;
import cn.hhit.school.model.PlanDistributeQueryModel;

@Repository("PlanDistributeDao")
public class PlanDistributeHibernate4DaoImpl extends BaseHibernateDao<PlanDistribute, Integer> implements PlanDistributeDao {

    private static final String HQL_LIST = "from PlanDistribute ";
    private static final String HQL_COUNT = "select count(*) from PlanDistribute ";

    private static final String HQL_LIST_QUERY_CONDITION = " where username like ?";
    private static final String HQL_LIST_QUERY_ALL = HQL_LIST + HQL_LIST_QUERY_CONDITION + "order by dindex desc";
    private static final String HQL_COUNT_QUERY_ALL = HQL_COUNT + HQL_LIST_QUERY_CONDITION;

    @Override
    public List<PlanDistribute> query(int pn, int pageSize, PlanDistributeQueryModel command) {
        return listByHql(HQL_LIST_QUERY_ALL, pn, pageSize, getQueryParam(command));
    }

    @Override
    public int countQuery(PlanDistributeQueryModel command) {
        return this.<Number>aggregate(HQL_COUNT_QUERY_ALL, getQueryParam(command)).intValue();
    }
    

    private Object[] getQueryParam(PlanDistributeQueryModel command) {
        //TODO 改成全文索引
        String usernameLikeStr = "%" + command.getPt() + "%";
        return new Object[]{
            usernameLikeStr
        };
    }
}
