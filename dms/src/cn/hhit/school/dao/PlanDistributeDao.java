package cn.hhit.school.dao;

import java.util.List;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.school.model.PlanDistribute;
import cn.hhit.school.model.PlanDistributeQueryModel;

public interface PlanDistributeDao extends IBaseDao<PlanDistribute, Integer> {

	List<PlanDistribute> query(int pn, int pageSize, PlanDistributeQueryModel command);

	int countQuery(PlanDistributeQueryModel command);

}
