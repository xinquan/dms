package cn.hhit.school.dao;

import java.util.List;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.school.model.PlanMainModel;
import cn.hhit.school.model.PlanMainQueryModel;


public interface PlanMainDao extends IBaseDao<PlanMainModel, Integer> {
    
    List<PlanMainModel> query(int pn, int pageSize, PlanMainQueryModel command);

    int countQuery(PlanMainQueryModel command);

}
