package cn.hhit.school.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.hhit.school.model.PlanCourseModel;
import cn.hhit.school.model.PlanCourseQueryModel;
import cn.hhit.school.model.PlanMainModel;
import cn.hhit.school.model.PlanMainQueryModel;

@Repository("PlanCourseDao")
public class PlanCourseHibernate4DaoImpl extends BaseHibernateDao<PlanCourseModel, Integer> implements PlanCourseDao {

	private static final String HQL_LIST = "from PlanCourseModel ";
	private static final String HQL_COUNT = "select count(*) from PlanCourseModel ";

	private static final String HQL_LIST_QUERY_CONDITION = " where username like ?";
	private static final String HQL_LIST_QUERY_ALL = HQL_LIST + HQL_LIST_QUERY_CONDITION + "order by id desc";
	private static final String HQL_COUNT_QUERY_ALL = HQL_COUNT + HQL_LIST_QUERY_CONDITION;

	@Override
	public List<PlanCourseModel> query(int pn, int pageSize, PlanCourseQueryModel command) {
		return listByHql(HQL_LIST_QUERY_ALL, pn, pageSize, getQueryParam(command));
	}

	@Override
	public int countQuery(PlanCourseQueryModel command) {
		return this.<Number> aggregate(HQL_COUNT_QUERY_ALL, getQueryParam(command)).intValue();
	}

	private Object[] getQueryParam(PlanCourseQueryModel command) {
		// TODO 改成全文索引
		String usernameLikeStr = "%" + command.getKcmc() + "%";
		return new Object[] { usernameLikeStr };
	}
}
