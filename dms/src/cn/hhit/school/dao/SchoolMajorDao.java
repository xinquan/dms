package cn.hhit.school.dao;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.school.model.SchoolMajor;

public interface SchoolMajorDao extends IBaseDao<SchoolMajor, Integer> {

}
