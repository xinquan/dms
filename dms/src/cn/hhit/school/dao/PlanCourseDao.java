package cn.hhit.school.dao;

import java.util.List;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.school.model.PlanCourseModel;
import cn.hhit.school.model.PlanCourseQueryModel;

public interface PlanCourseDao extends IBaseDao<PlanCourseModel, Integer> {
	List<PlanCourseModel> query(int pn, int pageSize, PlanCourseQueryModel command);

	int countQuery(PlanCourseQueryModel command);
}
