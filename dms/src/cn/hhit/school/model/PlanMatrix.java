package cn.hhit.school.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import cn.hhit.common.model.AbstractModel;

@Entity
@Table(name = "plan_matrix")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PlanMatrix extends AbstractModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private int id;
	private int planid;
	private String kcmc;
	private String k1;
	private String k2;
	private String k3;
	private String k4;
	private String k5;
	private String k6;
	private String k7;
	private String k8;
	private String k9;
	private String k10;
	private String k11;
	private String k12;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPlanid() {
		return planid;
	}

	public void setPlanid(int planid) {
		this.planid = planid;
	}

	public String getKcmc() {
		return kcmc;
	}

	public void setKcmc(String kcmc) {
		this.kcmc = kcmc;
	}

	public String getK1() {
		return k1;
	}

	public void setK1(String k1) {
		this.k1 = k1;
	}

	public String getK2() {
		return k2;
	}

	public void setK2(String k2) {
		this.k2 = k2;
	}

	public String getK3() {
		return k3;
	}

	public void setK3(String k3) {
		this.k3 = k3;
	}

	public String getK4() {
		return k4;
	}

	public void setK4(String k4) {
		this.k4 = k4;
	}

	public String getK5() {
		return k5;
	}

	public void setK5(String k5) {
		this.k5 = k5;
	}

	public String getK6() {
		return k6;
	}

	public void setK6(String k6) {
		this.k6 = k6;
	}

	public String getK7() {
		return k7;
	}

	public void setK7(String k7) {
		this.k7 = k7;
	}

	public String getK8() {
		return k8;
	}

	public void setK8(String k8) {
		this.k8 = k8;
	}

	public String getK9() {
		return k9;
	}

	public void setK9(String k9) {
		this.k9 = k9;
	}

	public String getK10() {
		return k10;
	}

	public void setK10(String k10) {
		this.k10 = k10;
	}

	public String getK11() {
		return k11;
	}

	public void setK11(String k11) {
		this.k11 = k11;
	}

	public String getK12() {
		return k12;
	}

	public void setK12(String k12) {
		this.k12 = k12;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanMatrix other = (PlanMatrix) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
