package cn.hhit.school.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import cn.hhit.common.model.AbstractModel;

@Entity
@Table(name = "plan_main")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PlanMainModel extends AbstractModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private int id;
	private String zy;
	private String zydm;
	private String ml;
	private String bzxz;
	private String syxw;
	private String pymb;
	private String jbyq;
	private String zgxk;
	private String zykc;
	private String zysjhj;
	private String zdxfyq;
	private String ssfa;
	private Integer version;
	private String type;
	private Integer deptid;
	private String operator;
	private Date handledate;
	private int state;
	private int operatorid;

	public String getZy() {
		return zy;
	}

	public void setZy(String zy) {
		this.zy = zy;
	}

	public String getZydm() {
		return zydm;
	}

	public void setZydm(String zydm) {
		this.zydm = zydm;
	}

	public String getMl() {
		return ml;
	}

	public void setMl(String ml) {
		this.ml = ml;
	}

	public String getBzxz() {
		return bzxz;
	}

	public void setBzxz(String bzxz) {
		this.bzxz = bzxz;
	}

	public String getSyxw() {
		return syxw;
	}

	public void setSyxw(String syxw) {
		this.syxw = syxw;
	}

	public String getPymb() {
		return pymb;
	}

	public void setPymb(String pymb) {
		this.pymb = pymb;
	}

	public String getJbyq() {
		return jbyq;
	}

	public void setJbyq(String jbyq) {
		this.jbyq = jbyq;
	}

	public String getZgxk() {
		return zgxk;
	}

	public void setZgxk(String zgxk) {
		this.zgxk = zgxk;
	}

	public String getZykc() {
		return zykc;
	}

	public void setZykc(String zykc) {
		this.zykc = zykc;
	}

	public String getZysjhj() {
		return zysjhj;
	}

	public void setZysjhj(String zysjhj) {
		this.zysjhj = zysjhj;
	}

	public String getZdxfyq() {
		return zdxfyq;
	}

	public void setZdxfyq(String zdxfyq) {
		this.zdxfyq = zdxfyq;
	}

	public String getSsfa() {
		return ssfa;
	}

	public void setSsfa(String ssfa) {
		this.ssfa = ssfa;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getDeptid() {
		return deptid;
	}

	public void setDeptid(Integer deptid) {
		this.deptid = deptid;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public Date getHandledate() {
		return handledate;
	}

	public void setHandledate(Date handledate) {
		this.handledate = handledate;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public int getOperatorid() {
		return operatorid;
	}

	public void setOperatorid(int operatorid) {
		this.operatorid = operatorid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanMainModel other = (PlanMainModel) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
