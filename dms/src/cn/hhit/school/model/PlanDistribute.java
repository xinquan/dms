package cn.hhit.school.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import cn.hhit.common.model.AbstractModel;

@SuppressWarnings("serial")
@Entity
@Table(name = "plan_distribute")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PlanDistribute extends AbstractModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Integer id;

	private Integer version;
	private String pt;
	private String mk;
	private Float xf;
	private Float xfmin;
	private Float sjxf;
	private Float sjxfmin;
	private Float qtxf;
	private Float qtxfmin;
	private String sm;
	private Float zzxfbl;
	private Float zzxfblmin;
	private Float sjbl;
	private Float sjblmin;
	private Float jxxf;
	private int sindex;
	private String plantype;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public String getPt() {
		return pt;
	}

	public void setPt(String pt) {
		this.pt = pt;
	}

	public String getMk() {
		return mk;
	}

	public void setMk(String mk) {
		this.mk = mk;
	}

	public Float getXf() {
		return xf;
	}

	public void setXf(Float xf) {
		this.xf = xf;
	}

	public Float getSjxf() {
		return sjxf;
	}

	public void setSjxf(Float sjxf) {
		this.sjxf = sjxf;
	}

	public Float getQtxf() {
		return qtxf;
	}

	public void setQtxf(Float qtxf) {
		this.qtxf = qtxf;
	}

	public String getSm() {
		return sm;
	}

	public void setSm(String sm) {
		this.sm = sm;
	}

	public Float getZzxfbl() {
		return zzxfbl;
	}

	public void setZzxfbl(Float zzxfbl) {
		this.zzxfbl = zzxfbl;
	}

	public Float getSjbl() {
		return sjbl;
	}

	public void setSjbl(Float sjbl) {
		this.sjbl = sjbl;
	}

	public Float getJxxf() {
		return jxxf;
	}

	public void setJxxf(Float jxxf) {
		this.jxxf = jxxf;
	}

	public int getSindex() {
		return sindex;
	}

	public void setSindex(int sindex) {
		this.sindex = sindex;
	}

	public Float getXfmin() {
		return xfmin;
	}

	public void setXfmin(Float xfmin) {
		this.xfmin = xfmin;
	}

	public Float getSjxfmin() {
		return sjxfmin;
	}

	public void setSjxfmin(Float sjxfmin) {
		this.sjxfmin = sjxfmin;
	}

	public Float getQtxfmin() {
		return qtxfmin;
	}

	public void setQtxfmin(Float qtxfmin) {
		this.qtxfmin = qtxfmin;
	}

	public Float getZzxfblmin() {
		return zzxfblmin;
	}

	public void setZzxfblmin(Float zzxfblmin) {
		this.zzxfblmin = zzxfblmin;
	}

	public Float getSjblmin() {
		return sjblmin;
	}

	public void setSjblmin(Float sjblmin) {
		this.sjblmin = sjblmin;
	}

	public String getPlantype() {
		return plantype;
	}

	public void setPlantype(String plantype) {
		this.plantype = plantype;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanDistribute other = (PlanDistribute) obj;
		if (id != other.id)
			return false;
		return true;
	}
}
