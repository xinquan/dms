package cn.hhit.school.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import cn.hhit.common.model.AbstractModel;

@Entity
@Table(name = "school_major")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SchoolMajor extends AbstractModel {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private int id;

	@Column(name = "majorid")
	private String majorId;

	@Column(name = "majorname")
	private String majorName;

	@Column(name = "dept_id")
	public Integer deptId;

	@Column(name = "majoryear")
	private String majorYear;

	private String category;
	private Integer mindex;
	private String degree;
	private String bigmajor;
	private Integer createdate;

	public String getBigmajor() {
		return bigmajor;
	}

	public void setBigmajor(String bigmajor) {
		this.bigmajor = bigmajor;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	private String type;

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMajorId() {
		return majorId;
	}

	public void setMajorId(String majorId) {
		this.majorId = majorId;
	}

	public String getMajorName() {
		return majorName;
	}

	public void setMajorName(String majorName) {
		this.majorName = majorName;
	}

	public String getMajorYear() {
		return majorYear;
	}

	public void setMajorYear(String majorYear) {
		this.majorYear = majorYear;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Integer getMindex() {
		return mindex;
	}

	public void setMindex(Integer mindex) {
		this.mindex = mindex;
	}

	public Integer getDeptId() {
		return deptId;
	}

	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}

	public Integer getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Integer createdate) {
		this.createdate = createdate;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SchoolMajor other = (SchoolMajor) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
