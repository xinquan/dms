package cn.hhit.school.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import cn.hhit.common.model.AbstractModel;

@Entity
@Table(name = "plan_course")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PlanCourseModel extends AbstractModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private int id;

	private int planid;
	private String plantype;

	// 课程归属系
	private String kcssx;
	// 课程性质
	private String kcxz;
	// 课程代码
	private String kcdm;
	// 课程名称
	private String kcmc;
	// 学分
	private Float xf;
	// 总学时
	private Float zxs;
	// 实验学时
	private Float syxs;
	// 开课学期
	private String kkxq;
	// 集中性实践环节
	private String sjhj;
	// 考试课程标记
	private String kskc;
	// 修读说明
	private String xdsm;
	private int pid;
	// 阶段
	private String jd;
	// 平台
	private String pt;
	// 模块
	private String mk;
	// 加括号学分
	private Float qtxf;
	// 课程排序
	private Date cindex;
	// 新增标记
	private String newadd;
	// 学院
	private String department;
	// 专业
	private String major;
	// 理论学时
	private Float llxs;
	// 审批流状态
	private Integer state;
	// 中括号学分
	private Float zkhxf;
	// 实验环节标记
	private String syhjbj;

	// 开课年份
	private Integer kknf;

	private Integer sortfield;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKcssx() {
		return kcssx;
	}

	public void setKcssx(String kcssx) {
		this.kcssx = kcssx;
	}

	public String getKcxz() {
		return kcxz;
	}

	public void setKcxz(String kcxz) {
		this.kcxz = kcxz;
	}

	public String getKcdm() {
		return kcdm;
	}

	public void setKcdm(String kcdm) {
		this.kcdm = kcdm;
	}

	public String getKcmc() {
		return kcmc;
	}

	public void setKcmc(String kcmc) {
		this.kcmc = kcmc;
	}

	public Float getXf() {
		return xf;
	}

	public void setXf(Float xf) {
		this.xf = xf;
	}

	public Float getZxs() {
		return zxs;
	}

	public void setZxs(Float zxs) {
		this.zxs = zxs;
	}

	public Float getSyxs() {
		return syxs;
	}

	public void setSyxs(Float syxs) {
		this.syxs = syxs;
	}

	public String getKkxq() {
		return kkxq;
	}

	public void setKkxq(String kkxq) {
		this.kkxq = kkxq;
	}

	public String getSjhj() {
		return sjhj;
	}

	public void setSjhj(String sjhj) {
		this.sjhj = sjhj;
	}

	public String getKskc() {
		return kskc;
	}

	public void setKskc(String kskc) {
		this.kskc = kskc;
	}

	public String getXdsm() {
		return xdsm;
	}

	public void setXdsm(String xdsm) {
		this.xdsm = xdsm;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getJd() {
		return jd;
	}

	public void setJd(String jd) {
		this.jd = jd;
	}

	public String getPt() {
		return pt;
	}

	public void setPt(String pt) {
		this.pt = pt;
	}

	public String getMk() {
		return mk;
	}

	public void setMk(String mk) {
		this.mk = mk;
	}

	public Float getQtxf() {
		return qtxf;
	}

	public void setQtxf(Float qtxf) {
		this.qtxf = qtxf;
	}

	public Date getCindex() {
		return cindex;
	}

	public void setCindex(Date cindex) {
		this.cindex = cindex;
	}

	public String getNewadd() {
		return newadd;
	}

	public void setNewadd(String newadd) {
		this.newadd = newadd;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public Float getLlxs() {
		return llxs;
	}

	public void setLlxs(Float llxs) {
		this.llxs = llxs;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Float getZkhxf() {
		return zkhxf;
	}

	public void setZkhxf(Float zkhxf) {
		this.zkhxf = zkhxf;
	}

	public String getSyhjbj() {
		return syhjbj;
	}

	public void setSyhjbj(String syhjbj) {
		this.syhjbj = syhjbj;
	}

	public Integer getKknf() {
		return kknf;
	}

	public void setKknf(Integer kknf) {
		this.kknf = kknf;
	}

	public int getPlanid() {
		return planid;
	}

	public void setPlanid(int planid) {
		this.planid = planid;
	}

	public String getPlantype() {
		return plantype;
	}

	public void setPlantype(String plantype) {
		this.plantype = plantype;
	}

	public Integer getSortfield() {
		return sortfield;
	}

	public void setSortfield(Integer sortfield) {
		this.sortfield = sortfield;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlanCourseModel other = (PlanCourseModel) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
