package cn.hhit.school.service;

import cn.hhit.common.pagination.Page;
import cn.hhit.common.service.IBaseService;
import cn.hhit.school.model.PlanMainModel;
import cn.hhit.school.model.PlanMainQueryModel;

public interface PlanMainService extends IBaseService<PlanMainModel, Integer> {

    Page<PlanMainModel> query(int pn, int pageSize, PlanMainQueryModel command);
}
