package cn.hhit.school.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.pagination.Page;
import cn.hhit.common.pagination.PageUtil;
import cn.hhit.common.service.BaseService;
import cn.hhit.school.dao.PlanMainDao;
import cn.hhit.school.model.PlanMainModel;
import cn.hhit.school.model.PlanMainQueryModel;

@Service("PlanMainService")
public class PlanMainServiceImpl extends BaseService<PlanMainModel, Integer> implements PlanMainService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlanMainServiceImpl.class);

    private PlanMainDao planMainDao;

    @Autowired
    @Qualifier("PlanMainDao")
    @Override
    public void setBaseDao(IBaseDao<PlanMainModel, Integer> planMainDao) {
        this.baseDao = planMainDao;
        this.planMainDao = (PlanMainDao) planMainDao;
    }
    


    @Override
    public Page<PlanMainModel> query(int pn, int pageSize, PlanMainQueryModel command) {
        return PageUtil.getPage(planMainDao.countQuery(command) ,pn, planMainDao.query(pn, pageSize, command), pageSize);
    }

   
}