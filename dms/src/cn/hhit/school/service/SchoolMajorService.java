package cn.hhit.school.service;

import cn.hhit.common.service.IBaseService;
import cn.hhit.school.model.SchoolMajor;

public interface SchoolMajorService extends IBaseService<SchoolMajor, Integer> {

}
