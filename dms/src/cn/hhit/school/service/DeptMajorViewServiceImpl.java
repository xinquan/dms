package cn.hhit.school.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.hhit.school.dao.DeptMajorViewDao;
import cn.hhit.school.model.DeptMajorView;

@Service("DeptMajorViewService")
public class DeptMajorViewServiceImpl extends BaseService<DeptMajorView, Integer> implements DeptMajorViewService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DeptMajorViewServiceImpl.class);

	private DeptMajorViewDao deptMajorViewDao;

	@Autowired
	@Qualifier("DeptMajorViewDao")
	@Override
	public void setBaseDao(IBaseDao<DeptMajorView, Integer> deptMajorViewDao) {
		this.baseDao = deptMajorViewDao;
		this.deptMajorViewDao = (DeptMajorViewDao) deptMajorViewDao;
	}

}