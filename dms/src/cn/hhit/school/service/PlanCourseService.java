package cn.hhit.school.service;

import cn.hhit.common.pagination.Page;
import cn.hhit.common.service.IBaseService;
import cn.hhit.school.model.PlanCourseModel;
import cn.hhit.school.model.PlanCourseQueryModel;

public interface PlanCourseService extends IBaseService<PlanCourseModel, Integer> {

    Page<PlanCourseModel> query(int pn, int pageSize, PlanCourseQueryModel command);
}
