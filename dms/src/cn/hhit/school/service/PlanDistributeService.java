package cn.hhit.school.service;

import cn.hhit.common.pagination.Page;
import cn.hhit.common.service.IBaseService;
import cn.hhit.school.model.PlanDistribute;
import cn.hhit.school.model.PlanDistributeQueryModel;

public interface PlanDistributeService extends IBaseService<PlanDistribute, Integer> {

    Page<PlanDistribute> query(int pn, int pageSize, PlanDistributeQueryModel command);
}
