package cn.hhit.school.service;

import cn.hhit.common.service.IBaseService;
import cn.hhit.school.model.DeptMajorView;

public interface DeptMajorViewService extends IBaseService<DeptMajorView, Integer> {

}
