package cn.hhit.school.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.hhit.school.dao.PlanMatrixDao;
import cn.hhit.school.model.PlanMatrix;

@Service("PlanMatrixService")
public class PlanMatrixServiceImpl extends BaseService<PlanMatrix, Integer> implements PlanMatrixService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlanMatrixServiceImpl.class);

	private PlanMatrixDao planMatrixDao;

	@Autowired
	@Qualifier("PlanMatrixDao")
	@Override
	public void setBaseDao(IBaseDao<PlanMatrix, Integer> planMatrixDao) {
		this.baseDao = planMatrixDao;
		this.planMatrixDao = (PlanMatrixDao) planMatrixDao;
	}
}