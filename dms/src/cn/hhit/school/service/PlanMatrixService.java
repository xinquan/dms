package cn.hhit.school.service;

import cn.hhit.common.service.IBaseService;
import cn.hhit.school.model.PlanMatrix;

public interface PlanMatrixService extends IBaseService<PlanMatrix, Integer> {

}
