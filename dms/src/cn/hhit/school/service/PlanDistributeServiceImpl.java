package cn.hhit.school.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.pagination.Page;
import cn.hhit.common.pagination.PageUtil;
import cn.hhit.common.service.BaseService;
import cn.hhit.school.dao.PlanDistributeDao;
import cn.hhit.school.model.PlanDistribute;
import cn.hhit.school.model.PlanDistributeQueryModel;

@Service("PlanDistributeService")
public class PlanDistributeServiceImpl extends BaseService<PlanDistribute, Integer> implements PlanDistributeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlanDistributeServiceImpl.class);

	private PlanDistributeDao planDistributeDao;

	@Autowired
	@Qualifier("PlanDistributeDao")
	@Override
	public void setBaseDao(IBaseDao<PlanDistribute, Integer> planDistributeDao) {
		this.baseDao = planDistributeDao;
		this.planDistributeDao = (PlanDistributeDao) planDistributeDao;
	}

	@Override
	public Page<PlanDistribute> query(int pn, int pageSize, PlanDistributeQueryModel command) {
		return PageUtil.getPage(planDistributeDao.countQuery(command), pn,
				planDistributeDao.query(pn, pageSize, command), pageSize);
	}
}