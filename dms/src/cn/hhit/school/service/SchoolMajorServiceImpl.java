package cn.hhit.school.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.hhit.school.dao.SchoolMajorDao;
import cn.hhit.school.model.SchoolMajor;

@Service("SchoolMajorService")
public class SchoolMajorServiceImpl extends BaseService<SchoolMajor, Integer> implements SchoolMajorService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SchoolMajorServiceImpl.class);

	private SchoolMajorDao schoolMajorDao;

	@Autowired
	@Qualifier("SchoolMajorDao")
	@Override
	public void setBaseDao(IBaseDao<SchoolMajor, Integer> schoolMajorDao) {
		this.baseDao = schoolMajorDao;
		this.schoolMajorDao = (SchoolMajorDao) schoolMajorDao;
	}
}