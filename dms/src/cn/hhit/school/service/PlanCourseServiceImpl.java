package cn.hhit.school.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.pagination.Page;
import cn.hhit.common.pagination.PageUtil;
import cn.hhit.common.service.BaseService;
import cn.hhit.school.dao.PlanCourseDao;
import cn.hhit.school.model.PlanCourseModel;
import cn.hhit.school.model.PlanCourseQueryModel;

@Service("PlanCourseService")
public class PlanCourseServiceImpl extends BaseService<PlanCourseModel, Integer> implements PlanCourseService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlanCourseServiceImpl.class);

    private PlanCourseDao planCourseDao;

    @Autowired
    @Qualifier("PlanCourseDao")
    @Override
    public void setBaseDao(IBaseDao<PlanCourseModel, Integer> planCourseDao) {
        this.baseDao = planCourseDao;
        this.planCourseDao = (PlanCourseDao) planCourseDao;
    }
    


    @Override
    public Page<PlanCourseModel> query(int pn, int pageSize, PlanCourseQueryModel command) {
        return PageUtil.getPage(planCourseDao.countQuery(command) ,pn, planCourseDao.query(pn, pageSize, command), pageSize);
    }

   
}