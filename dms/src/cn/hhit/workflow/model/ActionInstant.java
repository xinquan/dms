package cn.hhit.workflow.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import cn.hhit.common.model.AbstractModel;

@Entity
@Table(name = "wf_instant_action")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ActionInstant extends AbstractModel {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private int id;

	private int flowid;
	private int instantid;
	private int step;
	private String name;
	private String result;
	private String pass;
	private String oprator;
	private String description;
	private String handlertype;
	private Integer roleid;
	private Date handledate;
	private String opratorid;
	private int pre;
	private int next;
	private String instantclass;
	private String rollback;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getInstantid() {
		return instantid;
	}

	public void setInstantid(int instantid) {
		this.instantid = instantid;
	}

	public int getStep() {
		return step;
	}

	public void setStep(int step) {
		this.step = step;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getOprator() {
		return oprator;
	}

	public void setOprator(String oprator) {
		this.oprator = oprator;
	}

	public Date getHandledate() {
		return handledate;
	}

	public void setHandledate(Date handledate) {
		this.handledate = handledate;
	}

	public String getHandlertype() {
		return handlertype;
	}

	public void setHandlertype(String handlertype) {
		this.handlertype = handlertype;
	}

	public int getPre() {
		return pre;
	}

	public void setPre(int pre) {
		this.pre = pre;
	}

	public int getNext() {
		return next;
	}

	public void setNext(int next) {
		this.next = next;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOpratorid() {
		return opratorid;
	}

	public void setOpratorid(String opratorid) {
		this.opratorid = opratorid;
	}

	public Integer getRoleid() {
		return roleid;
	}

	public void setRoleid(Integer roleid) {
		this.roleid = roleid;
	}

	public String getInstantclass() {
		return instantclass;
	}

	public void setInstantclass(String instantclass) {
		this.instantclass = instantclass;
	}

	public int getFlowid() {
		return flowid;
	}

	public void setFlowid(int flowid) {
		this.flowid = flowid;
	}

	public String getRollback() {
		return rollback;
	}

	public void setRollback(String rollback) {
		this.rollback = rollback;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ActionInstant other = (ActionInstant) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
