package cn.hhit.workflow.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import cn.hhit.common.model.AbstractModel;

@Entity
@Table(name = "FLOW_WORKS")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class FlowWorks extends AbstractModel {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private int id;
	private int currentstep;
	private String pass;
	private String opratorid;
	private String rollback;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public int getCurrentstep() {
		return currentstep;
	}

	public void setCurrentstep(int currentstep) {
		this.currentstep = currentstep;
	}

	public String getOpratorid() {
		return opratorid;
	}

	public void setOpratorid(String opratorid) {
		this.opratorid = opratorid;
	}

	public String getRollback() {
		return rollback;
	}

	public void setRollback(String rollback) {
		this.rollback = rollback;
	}
}
