package cn.hhit.workflow.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import cn.hhit.common.model.AbstractModel;

@Entity
@Table(name = "wf_instant_flow")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class FlowInstant extends AbstractModel {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private int id;

	private int instantid;
	private int flowid;
	private int currentstep;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getInstantid() {
		return instantid;
	}

	public void setInstantid(int instantid) {
		this.instantid = instantid;
	}

	public int getFlowid() {
		return flowid;
	}

	public void setFlowid(int flowid) {
		this.flowid = flowid;
	}

	public int getCurrentstep() {
		return currentstep;
	}

	public void setCurrentstep(int currentstep) {
		this.currentstep = currentstep;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FlowInstant other = (FlowInstant) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
