package cn.hhit.workflow.service;

import cn.hhit.common.service.IBaseService;
import cn.hhit.workflow.model.FlowInstant;

public interface FlowInstantService extends IBaseService<FlowInstant, Integer> {

}
