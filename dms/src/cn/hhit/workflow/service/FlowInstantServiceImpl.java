package cn.hhit.workflow.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.hhit.workflow.dao.FlowInstantDao;
import cn.hhit.workflow.model.FlowInstant;

@Service("FlowInstantService")
public class FlowInstantServiceImpl extends BaseService<FlowInstant, Integer> implements FlowInstantService {

	@SuppressWarnings("unused")
	private FlowInstantDao flowInstantDao;

	@Autowired
	@Qualifier("FlowInstantDao")
	@Override
	public void setBaseDao(IBaseDao<FlowInstant, Integer> flowInstantDao) {
		this.baseDao = flowInstantDao;
		this.flowInstantDao = (FlowInstantDao) flowInstantDao;
	}
}