package cn.hhit.workflow.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.hhit.workflow.dao.FlowInfoDao;
import cn.hhit.workflow.model.FlowInfo;

@Service("FlowInfoService")
public class FlowInfoServiceImpl extends BaseService<FlowInfo, Integer> implements FlowInfoService {

    @SuppressWarnings("unused")
	private FlowInfoDao flowInfoDao;

    @Autowired
    @Qualifier("FlowInfoDao")
    @Override
    public void setBaseDao(IBaseDao<FlowInfo, Integer> flowInfoDao) {
        this.baseDao = flowInfoDao;
        this.flowInfoDao = (FlowInfoDao) flowInfoDao;
    }
    
}