package cn.hhit.workflow.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.hhit.workflow.dao.ActionInfoDao;
import cn.hhit.workflow.model.ActionInfo;

@Service("ActionInfoService")
public class ActionInfoServiceImpl extends BaseService<ActionInfo, Integer> implements ActionInfoService {

	@SuppressWarnings("unused")
	private ActionInfoDao actionInfoDao;

	@Autowired
	@Qualifier("ActionInfoDao")
	@Override
	public void setBaseDao(IBaseDao<ActionInfo, Integer> actionInfoDao) {
		this.baseDao = actionInfoDao;
		this.actionInfoDao = (ActionInfoDao) actionInfoDao;
	}
}