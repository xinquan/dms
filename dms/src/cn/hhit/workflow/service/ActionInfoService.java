package cn.hhit.workflow.service;

import cn.hhit.common.service.IBaseService;
import cn.hhit.workflow.model.ActionInfo;

public interface ActionInfoService extends IBaseService<ActionInfo, Integer> {

}
