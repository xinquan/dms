package cn.hhit.workflow.service;

import cn.hhit.common.service.IBaseService;
import cn.hhit.workflow.model.FlowInfo;

public interface FlowInfoService extends IBaseService<FlowInfo, Integer> {
}
