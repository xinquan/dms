package cn.hhit.workflow.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.hhit.workflow.dao.ActionInstantDao;
import cn.hhit.workflow.model.ActionInstant;

@Service("ActionInstantService")
public class ActionInstantServiceImpl extends BaseService<ActionInstant, Integer> implements ActionInstantService {

    @SuppressWarnings("unused")
	private ActionInstantDao actionInstantDao;

    @Autowired
    @Qualifier("ActionInstantDao")
    @Override
    public void setBaseDao(IBaseDao<ActionInstant, Integer> actionInstantDao) {
        this.baseDao = actionInstantDao;
        this.actionInstantDao = (ActionInstantDao) actionInstantDao;
    }

}