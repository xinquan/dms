package cn.hhit.workflow.service;

import cn.hhit.common.service.IBaseService;
import cn.hhit.workflow.model.FlowWorks;

public interface FlowWorksService extends IBaseService<FlowWorks, Integer> {
}
