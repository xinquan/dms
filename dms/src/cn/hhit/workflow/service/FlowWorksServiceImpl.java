package cn.hhit.workflow.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.hhit.workflow.dao.FlowWorksDao;
import cn.hhit.workflow.model.FlowWorks;

@Service("FlowWorksService")
public class FlowWorksServiceImpl extends BaseService<FlowWorks, Integer> implements FlowWorksService {

	@SuppressWarnings("unused")
	private FlowWorksDao flowWorksDao;

	@Autowired
	@Qualifier("FlowWorksDao")
	@Override
	public void setBaseDao(IBaseDao<FlowWorks, Integer> flowWorksDao) {
		this.baseDao = flowWorksDao;
		this.flowWorksDao = (FlowWorksDao) flowWorksDao;
	}

}