package cn.hhit.workflow.service;

import cn.hhit.common.service.IBaseService;
import cn.hhit.workflow.model.ActionInstant;

public interface ActionInstantService extends IBaseService<ActionInstant, Integer> {
}
