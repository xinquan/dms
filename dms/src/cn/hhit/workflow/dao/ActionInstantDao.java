package cn.hhit.workflow.dao;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.workflow.model.ActionInstant;

public interface ActionInstantDao extends IBaseDao<ActionInstant, Integer> {

}
