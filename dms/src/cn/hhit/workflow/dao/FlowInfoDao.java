package cn.hhit.workflow.dao;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.workflow.model.FlowInfo;

public interface FlowInfoDao extends IBaseDao<FlowInfo, Integer> {

}
