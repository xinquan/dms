package cn.hhit.workflow.dao;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.workflow.model.ActionInfo;

public interface ActionInfoDao extends IBaseDao<ActionInfo, Integer> {

}
