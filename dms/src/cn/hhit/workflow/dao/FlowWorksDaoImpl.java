package cn.hhit.workflow.dao;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.hhit.workflow.model.FlowWorks;

@Repository("FlowWorksDao")
public class FlowWorksDaoImpl extends BaseHibernateDao<FlowWorks, Integer> implements FlowWorksDao {

}
