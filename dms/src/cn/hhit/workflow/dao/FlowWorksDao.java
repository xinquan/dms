package cn.hhit.workflow.dao;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.workflow.model.FlowWorks;

public interface FlowWorksDao extends IBaseDao<FlowWorks, Integer> {

}
