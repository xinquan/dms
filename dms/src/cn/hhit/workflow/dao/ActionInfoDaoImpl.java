package cn.hhit.workflow.dao;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.hhit.workflow.model.ActionInfo;

@Repository("ActionInfoDao")
public class ActionInfoDaoImpl extends BaseHibernateDao<ActionInfo, Integer> implements ActionInfoDao {

}
