package cn.hhit.workflow.dao;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.hhit.workflow.model.ActionInstant;

@Repository("ActionInstantDao")
public class ActionInstantDaoImpl extends BaseHibernateDao<ActionInstant, Integer> implements ActionInstantDao {

}
