package cn.hhit.workflow.dao;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.hhit.workflow.model.FlowInstant;

@Repository("FlowInstantDao")
public class FlowInstantDaoImpl extends BaseHibernateDao<FlowInstant, Integer> implements FlowInstantDao {

}
