package cn.hhit.workflow.dao;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.workflow.model.FlowInstant;

public interface FlowInstantDao extends IBaseDao<FlowInstant, Integer> {

}
