package cn.hhit.workflow.dao;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.hhit.workflow.model.FlowInfo;

@Repository("FlowInfoDao")
public class FlowInfoDaoImpl extends BaseHibernateDao<FlowInfo, Integer> implements FlowInfoDao {

}
