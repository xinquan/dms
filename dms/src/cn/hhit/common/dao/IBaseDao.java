package cn.hhit.common.dao;

import java.util.List;

public interface IBaseDao<M extends java.io.Serializable, PK extends java.io.Serializable> {

	public PK save(M model);

	public void saveOrUpdate(M model);

	public void update(M model);

	public void merge(M model);

	public void delete(PK id);

	public void deleteObject(M model);

	public M get(PK id);

	public M getUnique(String hql, Object... params);

	public int countAll();

	public List<M> listAll();

	public List<M> listAll(int pn, int pageSize);

	public List<M> listByParams(String hql, Object... objects);

	public List<M> listByParams(int pn, int pageSize, String hql, Object... objects);

	public List<M> pre(PK pk, int pn, int pageSize);

	public List<M> next(PK pk, int pn, int pageSize);

	boolean exists(PK id);

	public void flush();

	public void clear();

	public int countByParams(String hql, Object... params);

	void delByParams(String hql, Object... objects);

	public void update(String hql, Object... params);

	public void updateState(String hql, Object... params);

	public void delRecordsByParams(String hql, Object... params);

}
