package cn.hhit.common.service;

import java.util.List;

import cn.hhit.common.pagination.Page;

public interface IBaseService<M extends java.io.Serializable, PK extends java.io.Serializable> {

	public M save(M model);

	public void saveOrUpdate(M model);

	public void update(M model);

	public void merge(M model);

	public void delete(PK id);

	public void deleteObject(M model);

	public M get(PK id);

	public M getUnique(String hql, Object... params);

	public int countAll();

	public List<M> listAll();

	public Page<M> listAll(int pn);

	public Page<M> listAll(int pn, int pageSize);

	public List<M> listByParams(String hql, Object... params);

	public Page<M> pre(PK pk, int pn, int pageSize);

	public Page<M> next(PK pk, int pn, int pageSize);

	public Page<M> pre(PK pk, int pn);

	public Page<M> next(PK pk, int pn);

	public Page<M> listByParams(int pn, int pageSize, String hql, Object... params);

	void delByParams(String hql, Object... params);

	void update(String hql, Object... params);

	void updateState(String hql, Object... params);

	void delRecordsByParams(String hql, Object... params);
}
