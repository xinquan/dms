package cn.hhit.common.service;

import cn.hhit.common.Constants;
import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.pagination.Page;
import cn.hhit.common.pagination.PageUtil;

import java.util.List;

public abstract class BaseService<M extends java.io.Serializable, PK extends java.io.Serializable>
		implements IBaseService<M, PK> {

	protected IBaseDao<M, PK> baseDao;

	public abstract void setBaseDao(IBaseDao<M, PK> baseDao);

	@Override
	public M save(M model) {
		baseDao.save(model);
		return model;
	}

	@Override
	public void merge(M model) {
		baseDao.merge(model);
	}

	@Override
	public void saveOrUpdate(M model) {
		baseDao.saveOrUpdate(model);
	}

	@Override
	public void update(M model) {
		baseDao.update(model);
	}

	@Override
	public void delete(PK id) {
		baseDao.delete(id);
	}

	@Override
	public void deleteObject(M model) {
		baseDao.deleteObject(model);
	}

	@Override
	public M get(PK id) {
		return baseDao.get(id);
	}

	@Override
	public M getUnique(String hql, Object... params) {
		return baseDao.getUnique(hql, params);
	}

	@Override
	public void update(String hql, Object... params) {
		baseDao.update(hql, params);
	}

	@Override
	public void updateState(String hql, Object... params) {
		baseDao.updateState(hql, params);
	}

	@Override
	public int countAll() {
		return baseDao.countAll();
	}

	@Override
	public List<M> listAll() {
		return baseDao.listAll();
	}

	@Override
	public Page<M> listAll(int pn) {

		return this.listAll(pn, Constants.DEFAULT_PAGE_SIZE);
	}

	public Page<M> listAllWithOptimize(int pn) {
		return this.listAllWithOptimize(pn, Constants.DEFAULT_PAGE_SIZE);
	}

	@Override
	public Page<M> listAll(int pn, int pageSize) {
		Integer count = countAll();
		List<M> items = baseDao.listAll(pn, pageSize);
		return PageUtil.getPage(count, pn, items, pageSize);
	}

	@Override
	public List<M> listByParams(String hql, Object... params) {
		return baseDao.listByParams(hql, params);
	}

	@Override
	public void delByParams(String hql, Object... params) {
		baseDao.delByParams(hql, params);
	}

	@Override
	public void delRecordsByParams(String hql, Object... params) {
		baseDao.delRecordsByParams(hql, params);
	}

	@Override
	public Page<M> listByParams(int pn, int pageSize, String hql, Object... params) {
		Integer count = countByParams(hql, params);
		List<M> items = baseDao.listByParams(pn, pageSize, hql, params);
		return PageUtil.getPage(count, pn, items, pageSize);
	}

	private Integer countByParams(String hql, Object... params) {
		return baseDao.countByParams(hql, params);
	}

	public Page<M> listAllWithOptimize(int pn, int pageSize) {
		Integer count = countAll();
		List<M> items = baseDao.listAll(pn, pageSize);
		return PageUtil.getPage(count, pn, items, pageSize);
	}

	@Override
	public Page<M> pre(PK pk, int pn, int pageSize) {
		Integer count = countAll();
		List<M> items = baseDao.pre(pk, pn, pageSize);
		return PageUtil.getPage(count, pn, items, pageSize);
	}

	@Override
	public Page<M> next(PK pk, int pn, int pageSize) {
		Integer count = countAll();
		List<M> items = baseDao.next(pk, pn, pageSize);
		return PageUtil.getPage(count, pn, items, pageSize);
	}

	@Override
	public Page<M> pre(PK pk, int pn) {
		return pre(pk, pn, Constants.DEFAULT_PAGE_SIZE);
	}

	@Override
	public Page<M> next(PK pk, int pn) {
		return next(pk, pn, Constants.DEFAULT_PAGE_SIZE);
	}

}
