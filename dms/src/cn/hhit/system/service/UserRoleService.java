package cn.hhit.system.service;

import cn.hhit.common.service.IBaseService;
import cn.hhit.system.model.UserRole;

public interface UserRoleService extends IBaseService<UserRole, Integer> {

}
