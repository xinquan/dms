package cn.hhit.system.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.hhit.system.dao.PermissionDao;
import cn.hhit.system.model.PermissionModel;

@Service("PermissionService")
public class PermissionServiceImpl extends BaseService<PermissionModel, Integer> implements PermissionService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PermissionServiceImpl.class);

	private PermissionDao permissionDao;

	@Autowired
	@Qualifier("PermissionDao")
	@Override
	public void setBaseDao(IBaseDao<PermissionModel, Integer> permissionDao) {
		this.baseDao = permissionDao;
		this.permissionDao = (PermissionDao) permissionDao;
	}
}