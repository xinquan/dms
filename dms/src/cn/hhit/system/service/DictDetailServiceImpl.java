package cn.hhit.system.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.hhit.system.dao.DictDetailDao;
import cn.hhit.system.model.DictDetail;

@Service("DictDetailService")
public class DictDetailServiceImpl extends BaseService<DictDetail,Integer> implements DictDetailService {

	@SuppressWarnings("unused")
	private DictDetailDao dictDetailDao;
	
	@Autowired
	@Qualifier("DictDetailDao")
	@Override
	public void setBaseDao(IBaseDao<DictDetail, Integer> dictDetailDao) {
		this.baseDao = dictDetailDao;
		this.dictDetailDao = (DictDetailDao)dictDetailDao;
	}

}
