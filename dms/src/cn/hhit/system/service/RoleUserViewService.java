package cn.hhit.system.service;

import cn.hhit.common.service.IBaseService;
import cn.hhit.system.model.RoleUserViewModel;

public interface RoleUserViewService extends IBaseService<RoleUserViewModel, Integer> {

}
