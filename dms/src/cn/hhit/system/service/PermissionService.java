package cn.hhit.system.service;

import cn.hhit.common.service.IBaseService;
import cn.hhit.system.model.PermissionModel;

public interface PermissionService extends IBaseService<PermissionModel, Integer> {
}
