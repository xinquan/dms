package cn.hhit.system.service;

import cn.hhit.common.service.IBaseService;
import cn.hhit.system.model.RoleModel;

public interface RoleService extends IBaseService<RoleModel, Integer> {

}
