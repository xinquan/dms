package cn.hhit.system.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.hhit.system.dao.MenuDao;
import cn.hhit.system.model.Menu;

@Service("MenuService")
public class MenuServiceImpl extends BaseService<Menu,Integer> implements MenuService {

	@SuppressWarnings("unused")
	private MenuDao menuDao;
	
	@Autowired
	@Qualifier("MenuDao")
	@Override
	public void setBaseDao(IBaseDao<Menu, Integer> menuDao) {
		this.baseDao = menuDao;
		this.menuDao = (MenuDao)menuDao;
	}

}
