package cn.hhit.system.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.pagination.Page;
import cn.hhit.common.pagination.PageUtil;
import cn.hhit.common.service.BaseService;
import cn.hhit.system.dao.UserDao;
import cn.hhit.system.model.UserModel;
import cn.hhit.system.model.UserQueryModel;

@Service("UserService")
public class UserServiceImpl extends BaseService<UserModel, Integer> implements UserService {

	private UserDao userDao;

	@Autowired
	@Qualifier("UserDao")
	@Override
	public void setBaseDao(IBaseDao<UserModel, Integer> userDao) {
		this.baseDao = userDao;
		this.userDao = (UserDao) userDao;
	}

	@Override
	public Page<UserModel> query(int pn, int pageSize, UserQueryModel command) {
		return PageUtil.getPage(userDao.countQuery(command), pn, userDao.query(pn, pageSize, command), pageSize);
	}

	@Override
	public UserModel findUserByUsername(String username) {
		return userDao.findUserByUsername(username);
	}

	@Override
	public Page<UserModel> findUserByParams(String hql, String... value) {
		return null;
	}

}