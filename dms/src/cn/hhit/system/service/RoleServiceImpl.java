package cn.hhit.system.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.hhit.system.dao.RoleDao;
import cn.hhit.system.model.RoleModel;

@Service("RoleService")
public class RoleServiceImpl extends BaseService<RoleModel, Integer> implements RoleService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RoleServiceImpl.class);

	private RoleDao roleDao;

	@Autowired
	@Qualifier("RoleDao")
	@Override
	public void setBaseDao(IBaseDao<RoleModel, Integer> roleDao) {
		this.baseDao = roleDao;
		this.roleDao = (RoleDao) roleDao;
	}
}