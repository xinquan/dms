package cn.hhit.system.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.hhit.system.dao.UserRoleDao;
import cn.hhit.system.model.UserRole;

@Service("UserRoleService")
public class UserRoleServiceImpl extends BaseService<UserRole, Integer> implements UserRoleService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserRoleServiceImpl.class);

	private UserRoleDao userRoleDao;

	@Autowired
	@Qualifier("UserRoleDao")
	@Override
	public void setBaseDao(IBaseDao<UserRole, Integer> userRoleDao) {
		this.baseDao = userRoleDao;
		this.userRoleDao = (UserRoleDao) userRoleDao;
	}

}