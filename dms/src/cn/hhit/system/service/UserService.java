package cn.hhit.system.service;

import cn.hhit.common.pagination.Page;
import cn.hhit.common.service.IBaseService;
import cn.hhit.system.model.UserModel;
import cn.hhit.system.model.UserQueryModel;

public interface UserService extends IBaseService<UserModel, Integer> {

	Page<UserModel> query(int pn, int pageSize, UserQueryModel command);

	UserModel findUserByUsername(String username);

	Page<UserModel> findUserByParams(String hql, String... value);
}
