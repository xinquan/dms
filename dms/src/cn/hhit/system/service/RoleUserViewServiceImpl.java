package cn.hhit.system.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.hhit.system.dao.RoleUserViewDao;
import cn.hhit.system.model.RoleUserViewModel;

@Service("RoleUserViewService")
public class RoleUserViewServiceImpl extends BaseService<RoleUserViewModel, Integer> implements RoleUserViewService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RoleUserViewServiceImpl.class);

	private RoleUserViewDao roleUserViewDao;

	@Autowired
	@Qualifier("RoleUserViewDao")
	@Override
	public void setBaseDao(IBaseDao<RoleUserViewModel, Integer> roleUserViewDao) {
		this.baseDao = roleUserViewDao;
		this.roleUserViewDao = (RoleUserViewDao) roleUserViewDao;
	}
}