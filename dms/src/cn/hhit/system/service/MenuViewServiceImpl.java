package cn.hhit.system.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.hhit.system.dao.MenuViewDao;
import cn.hhit.system.model.MenuView;

@Service("MenuViewService")
public class MenuViewServiceImpl extends BaseService<MenuView,Integer> implements MenuViewService {

	private MenuViewDao menuViewDao;
	
	@Autowired
	@Qualifier("MenuViewDao")
	@Override
	public void setBaseDao(IBaseDao<MenuView, Integer> menuViewDao) {
		this.baseDao = menuViewDao;
		this.menuViewDao = (MenuViewDao)menuViewDao;
	}

}
