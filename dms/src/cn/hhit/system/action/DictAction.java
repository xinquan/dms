package cn.hhit.system.action;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import cn.hhit.common.action.CommonAction;
import cn.hhit.school.model.PlanMainModel;
import cn.hhit.school.service.PlanMainService;
import cn.hhit.system.model.DictDetail;
import cn.hhit.system.model.TreeModel;
import cn.hhit.system.service.DictDetailService;

public class DictAction extends CommonAction {
	private static final long serialVersionUID = 1L;

	@Autowired
	@Qualifier("DictDetailService")
	private DictDetailService dictDetailService;

	@Autowired
	@Qualifier("PlanMainService")
	private PlanMainService planMainService;

	private String action;
	private int id;
	private String dictStyle;
	private String pid;
	private int planid;

	public void list() {
		List<DictDetail> pagelist = new ArrayList<DictDetail>();
		if (pid != null && !"".equals(pid)) {
			pagelist = dictDetailService.listByParams("where styleId=? and pid=? order by detailSort", dictStyle, pid);
		} else {
			pagelist = dictDetailService.listByParams("where styleId=? order by detailSort", dictStyle);
		}
		writeListToClient(response, pagelist, true);
	}

	public void styleList() {
		PlanMainModel pmm = planMainService.get(planid);
		List<DictDetail> dictlist = new ArrayList<DictDetail>();
		dictlist = dictDetailService.listByParams("where pid=? order by detailSort", pid);
		List<TreeModel> treelist = new ArrayList<TreeModel>();
		if (!dictlist.isEmpty()) {
			for (DictDetail dictDetail : dictlist) {
				if (pmm != null && "大类".equals(pmm.getType())) {
					if (!dictDetail.getDetailName().contains("专业")) {
						TreeModel node = new TreeModel();
						node.setId(dictDetail.getDetailId());
						node.setSortIndex(dictDetail.getDetailSort());
						node.setText(dictDetail.getDetailName());
						node.setIconCls("contract");
						if ("true".equals(dictDetail.getHasChild())) {
							node.setLeaf(false);
						} else {
							node.setLeaf(true);
						}
						treelist.add(node);
					}

				} else if (pmm != null && "大类专业".equals(pmm.getType())) {
					if (dictDetail.getDetailName().contains("专业")) {
						TreeModel node = new TreeModel();
						node.setId(dictDetail.getDetailId());
						node.setSortIndex(dictDetail.getDetailSort());
						node.setText(dictDetail.getDetailName());
						node.setIconCls("contract");
						if ("true".equals(dictDetail.getHasChild())) {
							node.setLeaf(false);
						} else {
							node.setLeaf(true);
						}
						treelist.add(node);
					}

				} else {
					TreeModel node = new TreeModel();
					node.setId(dictDetail.getDetailId());
					node.setSortIndex(dictDetail.getDetailSort());
					node.setText(dictDetail.getDetailName());
					node.setIconCls("contract");
					if ("true".equals(dictDetail.getHasChild())) {
						node.setLeaf(false);
					} else {
						node.setLeaf(true);
					}
					treelist.add(node);
				}

			}
		}
		writeListToClient(response, treelist, true);
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getDictStyle() {
		return dictStyle;
	}

	public void setDictStyle(String dictStyle) {
		this.dictStyle = dictStyle;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPlanid() {
		return planid;
	}

	public void setPlanid(int planid) {
		this.planid = planid;
	}
}
