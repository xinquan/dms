package cn.hhit.system.dao;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.system.model.RoleUserViewModel;

public interface RoleUserViewDao extends IBaseDao<RoleUserViewModel, Integer> {

}
