package cn.hhit.system.dao;

import java.util.List;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.school.model.PlanMainModel;
import cn.hhit.school.model.PlanMainQueryModel;
import cn.hhit.system.model.UserModel;
import cn.hhit.system.model.UserQueryModel;

public interface UserDao extends IBaseDao<UserModel, Integer> {

	List<UserModel> query(int pn, int pageSize, UserQueryModel command);

	int countQuery(UserQueryModel command);

	UserModel findUserByUsername(String username);
}
