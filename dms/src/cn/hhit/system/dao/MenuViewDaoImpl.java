package cn.hhit.system.dao;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.hhit.system.model.MenuView;

@Repository("MenuViewDao")
public class MenuViewDaoImpl  extends BaseHibernateDao<MenuView, Integer>  implements MenuViewDao {

}
