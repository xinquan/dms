package cn.hhit.system.dao;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.system.model.PermissionModel;

public interface PermissionDao extends IBaseDao<PermissionModel, Integer> {
}
