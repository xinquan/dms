package cn.hhit.system.dao;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.hhit.system.model.Menu;

@Repository("MenuDao")
public class MenuDaoImpl  extends BaseHibernateDao<Menu, Integer>  implements MenuDao {

}
