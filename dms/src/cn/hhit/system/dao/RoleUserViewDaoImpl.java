package cn.hhit.system.dao;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.hhit.system.model.RoleUserViewModel;

@Repository("RoleUserViewDao")
public class RoleUserViewDaoImpl  extends BaseHibernateDao<RoleUserViewModel, Integer>  implements RoleUserViewDao {

}
