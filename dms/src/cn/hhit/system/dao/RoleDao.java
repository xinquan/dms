package cn.hhit.system.dao;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.system.model.RoleModel;

public interface RoleDao extends IBaseDao<RoleModel, Integer> {

}
