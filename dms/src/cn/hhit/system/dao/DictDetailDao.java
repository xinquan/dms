package cn.hhit.system.dao;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.system.model.DictDetail;

public interface DictDetailDao extends IBaseDao<DictDetail, Integer> {

}
