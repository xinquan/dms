package cn.hhit.system.dao;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.hhit.system.model.DictDetail;

@Repository("DictDetailDao")
public class DictDetailDaoImpl  extends BaseHibernateDao<DictDetail, Integer>  implements DictDetailDao {

}
