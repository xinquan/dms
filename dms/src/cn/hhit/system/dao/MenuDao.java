package cn.hhit.system.dao;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.system.model.Menu;

public interface MenuDao extends IBaseDao<Menu, Integer> {

}
