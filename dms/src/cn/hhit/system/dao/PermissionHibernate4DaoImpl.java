package cn.hhit.system.dao;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.hhit.system.model.PermissionModel;

@Repository("PermissionDao")
public class PermissionHibernate4DaoImpl extends BaseHibernateDao<PermissionModel, Integer> implements PermissionDao {

	private static final String HQL_LIST = "from PermissionModel ";
	private static final String HQL_COUNT = "select count(*) from PermissionModel ";
	private static final String HQL_LIST_QUERY_CONDITION = " where Permissionname like ?";
	private static final String HQL_LIST_ORDER = "order by id desc";

	private static final String HQL_LIST_QUERY_ALL = HQL_LIST + HQL_LIST_QUERY_CONDITION + HQL_LIST_ORDER;
	private static final String HQL_COUNT_QUERY_ALL = HQL_COUNT + HQL_LIST_QUERY_CONDITION;

	private static final String HQL_LIST_LIST_PARAM = " where 1=1 and ";
}
