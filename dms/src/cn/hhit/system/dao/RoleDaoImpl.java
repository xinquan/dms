package cn.hhit.system.dao;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.hhit.system.model.RoleModel;

@Repository("RoleDao")
public class RoleDaoImpl  extends BaseHibernateDao<RoleModel, Integer>  implements RoleDao {

}
