package cn.hhit.system.dao;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.hhit.system.model.UserRole;

@Repository("UserRoleDao")
public class UserRoleDaoImpl  extends BaseHibernateDao<UserRole, Integer>  implements UserRoleDao {

}
