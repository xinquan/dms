package cn.hhit.system.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.hhit.school.dao.PlanMainDao;
import cn.hhit.school.model.PlanMainModel;
import cn.hhit.school.model.PlanMainQueryModel;
import cn.hhit.system.model.UserModel;
import cn.hhit.system.model.UserQueryModel;

@Repository("UserDao")
public class UserHibernate4DaoImpl extends BaseHibernateDao<UserModel, Integer> implements UserDao {

	private static final String HQL_LIST = "from UserModel ";
	private static final String HQL_COUNT = "select count(*) from UserModel ";
	private static final String HQL_LIST_QUERY_CONDITION = " where username like ?";
	private static final String HQL_LIST_ORDER = "order by id desc";

	private static final String HQL_LIST_QUERY_ALL = HQL_LIST + HQL_LIST_QUERY_CONDITION + HQL_LIST_ORDER;
	private static final String HQL_COUNT_QUERY_ALL = HQL_COUNT + HQL_LIST_QUERY_CONDITION;

	private static final String HQL_LIST_LIST_PARAM = " where 1=1 and ";

	@Override
	public List<UserModel> query(int pn, int pageSize, UserQueryModel command) {
		return listByHql(HQL_LIST_QUERY_ALL, pn, pageSize, getQueryParam(command));
	}

	@Override
	public int countQuery(UserQueryModel command) {
		return this.<Number> aggregate(HQL_COUNT_QUERY_ALL, getQueryParam(command)).intValue();
	}

	private Object[] getQueryParam(UserQueryModel command) {
		// TODO 改成全文索引
		String usernameLikeStr = "%" + command.getUsername() + "%";
		return new Object[] { usernameLikeStr };
	}

	@Override
	public UserModel findUserByUsername(String username) {
		return unique("from UserModel where username=? ", username);
	}
}
