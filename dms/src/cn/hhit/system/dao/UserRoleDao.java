package cn.hhit.system.dao;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.system.model.UserRole;

public interface UserRoleDao extends IBaseDao<UserRole, Integer> {

}
