package cn.hhit.system.dao;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.system.model.MenuView;

public interface MenuViewDao extends IBaseDao<MenuView, Integer> {

}
