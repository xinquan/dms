package cn.hhit.system.filter;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class LoginInterceptor extends AbstractInterceptor {

	private static final long serialVersionUID = 4956767125951165062L;

	// 拦截Action处理的拦截方法
	public String intercept(ActionInvocation invocation) throws Exception {

		// 取得请求相关的ActionContext实例
		ActionContext ctx = invocation.getInvocationContext();
		String action = invocation.getInvocationContext().getName();

		Map<String, Object> session = ctx.getSession();

		if ("logon".equals(action)) {
			invocation.invoke();
		} else {
			if (session == null) {
				return "login";
			} else {
				String username = (String) session.get("username");
				if (username != null) {
					invocation.invoke();
				} else {
					return "login";
				}
			}
		}
		return "login";
	}
}