package cn.dms.system.action;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import cn.dms.system.model.Department;
import cn.dms.system.service.DepartmentService;
import cn.hhit.common.action.CommonAction;
import cn.hhit.system.service.RoleService;
import cn.hhit.system.service.UserRoleService;
import cn.hhit.system.service.UserService;

public class SystemAction extends CommonAction {

	private static final long serialVersionUID = 8013816027944871760L;
	@Autowired
	@Qualifier("UserService")
	private UserService userService;
	@Autowired
	@Qualifier("RoleService")
	private RoleService roleService;
	@Autowired
	@Qualifier("UserRoleService")
	private UserRoleService userRoleService;
	@Autowired
	@Qualifier("DepartmentService")
	private DepartmentService departmentService;

	private String username;
	private String password;

	public void departList() {
		List<Department> departments = departmentService.listAll();
		writeListToClient(response, departments, true);
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsername() {
		return this.username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return this.password;
	}
}