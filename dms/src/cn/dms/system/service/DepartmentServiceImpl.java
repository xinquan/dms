package cn.dms.system.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.dms.system.dao.DepartmentDao;
import cn.dms.system.model.Department;
import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;

@Service("DepartmentService")
public class DepartmentServiceImpl extends BaseService<Department, Integer> implements DepartmentService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentServiceImpl.class);

	private DepartmentDao departmentDao;

	@Autowired
	@Qualifier("DepartmentDao")
	@Override
	public void setBaseDao(IBaseDao<Department, Integer> departmentDao) {
		this.baseDao = departmentDao;
		this.departmentDao = (DepartmentDao) departmentDao;
	}
}