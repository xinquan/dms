package cn.dms.system.service;

import cn.dms.system.model.Department;
import cn.hhit.common.service.IBaseService;

public interface DepartmentService extends IBaseService<Department, Integer> {

}
