package cn.dms.system.dao;

import org.springframework.stereotype.Repository;

import cn.dms.system.model.Department;
import cn.hhit.common.dao.BaseHibernateDao;

@Repository("DepartmentDao")
public class DepartmentDaoImpl extends BaseHibernateDao<Department, Integer> implements DepartmentDao {

	private static final String HQL_LIST = "from Department ";
	private static final String HQL_COUNT = "select count(*) from Department ";
	private static final String HQL_LIST_QUERY_CONDITION = " where username like ?";
	private static final String HQL_LIST_ORDER = "order by id desc";

	private static final String HQL_LIST_QUERY_ALL = HQL_LIST + HQL_LIST_QUERY_CONDITION + HQL_LIST_ORDER;
	private static final String HQL_COUNT_QUERY_ALL = HQL_COUNT + HQL_LIST_QUERY_CONDITION;

	private static final String HQL_LIST_LIST_PARAM = " where 1=1 and ";
}
