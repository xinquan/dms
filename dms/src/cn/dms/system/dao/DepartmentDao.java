package cn.dms.system.dao;

import cn.dms.system.model.Department;
import cn.hhit.common.dao.IBaseDao;

public interface DepartmentDao extends IBaseDao<Department, Integer> {

}
