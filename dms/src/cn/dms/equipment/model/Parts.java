package cn.dms.equipment.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import cn.hhit.common.model.AbstractModel;

@Entity
@Table(name = "parts")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Parts extends AbstractModel {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private int id;
	private int did;
	private String inputdate;
	private String department;
	private String name;
	private String pno;
	private String model;
	private String standard;
	private String state;
	private Date outputdate;
	private Date lastinput;
	private Date repairdate;
	private Date damagedate;
	private Date scapdate;
	private int departmentid;
	private String brand;
	private String weight;
	private Date start;
	private Date end;

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDid() {
		return this.did;
	}

	public void setDid(int did) {
		this.did = did;
	}

	public String getInputdate() {
		return this.inputdate;
	}

	public void setInputdate(String inputdate) {
		this.inputdate = inputdate;
	}

	public String getDepartment() {
		return this.department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPno() {
		return this.pno;
	}

	public void setPno(String pno) {
		this.pno = pno;
	}

	public String getModel() {
		return this.model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getStandard() {
		return this.standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getOutputdate() {
		return outputdate;
	}

	public void setOutputdate(Date outputdate) {
		this.outputdate = outputdate;
	}

	public Date getLastinput() {
		return lastinput;
	}

	public void setLastinput(Date lastinput) {
		this.lastinput = lastinput;
	}

	public Date getRepairdate() {
		return repairdate;
	}

	public void setRepairdate(Date repairdate) {
		this.repairdate = repairdate;
	}

	public Date getDamagedate() {
		return damagedate;
	}

	public void setDamagedate(Date damagedate) {
		this.damagedate = damagedate;
	}

	public Date getScapdate() {
		return scapdate;
	}

	public void setScapdate(Date scapdate) {
		this.scapdate = scapdate;
	}

	public int getDepartmentid() {
		return this.departmentid;
	}

	public void setDepartmentid(int departmentid) {
		this.departmentid = departmentid;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

}