package cn.dms.equipment.dao;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.dms.equipment.model.Equipment;

@Repository("EquipmentDao")
public class EquipmentDaoImpl extends BaseHibernateDao<Equipment, Integer> implements EquipmentDao {

	private static final String HQL_LIST = "from Equipment ";
	private static final String HQL_COUNT = "select count(*) from Equipment ";

	private static final String HQL_LIST_QUERY_CONDITION = " where username like ?";
	private static final String HQL_LIST_QUERY_ALL = HQL_LIST + HQL_LIST_QUERY_CONDITION + "order by id desc";
	private static final String HQL_COUNT_QUERY_ALL = HQL_COUNT + HQL_LIST_QUERY_CONDITION;
}
