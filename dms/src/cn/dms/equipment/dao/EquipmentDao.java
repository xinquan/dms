package cn.dms.equipment.dao;

import cn.hhit.common.dao.IBaseDao;
import cn.dms.equipment.model.Equipment;

public interface EquipmentDao extends IBaseDao<Equipment, Integer> {

}
