package cn.dms.equipment.dao;

import cn.hhit.common.dao.IBaseDao;
import cn.dms.equipment.model.Parts;

public interface PartsDao extends IBaseDao<Parts, Integer> {

}
