package cn.dms.equipment.dao;

import org.springframework.stereotype.Repository;

import cn.dms.equipment.model.OutRecordsView;
import cn.hhit.common.dao.BaseHibernateDao;

@Repository("OutRecordsViewDao")
public class OutRecordsViewDaoImpl  extends BaseHibernateDao<OutRecordsView, Integer>  implements OutRecordsViewDao {

}
