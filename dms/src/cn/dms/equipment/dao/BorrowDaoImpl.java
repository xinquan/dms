package cn.dms.equipment.dao;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.dms.equipment.model.Borrow;

@Repository("BorrowDao")
public class BorrowDaoImpl extends BaseHibernateDao<Borrow, Integer> implements BorrowDao {

	private static final String HQL_LIST = "from Borrow ";
	private static final String HQL_COUNT = "select count(*) from Parts ";

	private static final String HQL_LIST_QUERY_CONDITION = " where username like ?";
	private static final String HQL_LIST_QUERY_ALL = HQL_LIST + HQL_LIST_QUERY_CONDITION + "order by id desc";
	private static final String HQL_COUNT_QUERY_ALL = HQL_COUNT + HQL_LIST_QUERY_CONDITION;
}
