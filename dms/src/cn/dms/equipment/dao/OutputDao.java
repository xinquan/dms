package cn.dms.equipment.dao;

import cn.dms.equipment.model.Output;
import cn.hhit.common.dao.IBaseDao;

public interface OutputDao extends IBaseDao<Output, Integer> {

}
