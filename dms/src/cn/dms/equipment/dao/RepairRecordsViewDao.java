package cn.dms.equipment.dao;

import cn.dms.equipment.model.RepairRecordsView;
import cn.hhit.common.dao.IBaseDao;

public interface RepairRecordsViewDao extends IBaseDao<RepairRecordsView, Integer> {

}
