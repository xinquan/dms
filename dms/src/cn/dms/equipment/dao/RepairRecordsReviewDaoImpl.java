package cn.dms.equipment.dao;

import org.springframework.stereotype.Repository;

import cn.dms.equipment.model.RepairRecordsView;
import cn.hhit.common.dao.BaseHibernateDao;

@Repository("RepairRecordsViewDao")
public class RepairRecordsReviewDaoImpl  extends BaseHibernateDao<RepairRecordsView, Integer>  implements RepairRecordsViewDao {

}
