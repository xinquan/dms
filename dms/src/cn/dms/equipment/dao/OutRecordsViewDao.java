package cn.dms.equipment.dao;

import cn.dms.equipment.model.OutRecordsView;
import cn.hhit.common.dao.IBaseDao;

public interface OutRecordsViewDao extends IBaseDao<OutRecordsView, Integer> {

}
