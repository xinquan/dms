package cn.dms.equipment.dao;

import cn.dms.equipment.model.Repairs;
import cn.hhit.common.dao.IBaseDao;

public interface RepairsDao extends IBaseDao<Repairs, Integer> {

}
