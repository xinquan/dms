package cn.dms.equipment.dao;

import cn.hhit.common.dao.IBaseDao;
import cn.dms.equipment.model.Borrow;

public interface BorrowDao extends IBaseDao<Borrow, Integer> {

}
