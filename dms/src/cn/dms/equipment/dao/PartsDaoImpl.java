package cn.dms.equipment.dao;

import org.springframework.stereotype.Repository;

import cn.hhit.common.dao.BaseHibernateDao;
import cn.dms.equipment.model.Parts;

@Repository("PartsDao")
public class PartsDaoImpl extends BaseHibernateDao<Parts, Integer> implements PartsDao {

	private static final String HQL_LIST = "from Parts ";
	private static final String HQL_COUNT = "select count(*) from Parts ";

	private static final String HQL_LIST_QUERY_CONDITION = " where username like ?";
	private static final String HQL_LIST_QUERY_ALL = HQL_LIST + HQL_LIST_QUERY_CONDITION + "order by id desc";
	private static final String HQL_COUNT_QUERY_ALL = HQL_COUNT + HQL_LIST_QUERY_CONDITION;
}
