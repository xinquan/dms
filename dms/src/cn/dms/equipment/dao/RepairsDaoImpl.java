package cn.dms.equipment.dao;

import org.springframework.stereotype.Repository;

import cn.dms.equipment.model.Repairs;
import cn.hhit.common.dao.BaseHibernateDao;

@Repository("RepairsDao")
public class RepairsDaoImpl  extends BaseHibernateDao<Repairs, Integer>  implements RepairsDao {

}
