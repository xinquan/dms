package cn.dms.equipment.dao;

import org.springframework.stereotype.Repository;

import cn.dms.equipment.model.Output;
import cn.hhit.common.dao.BaseHibernateDao;

@Repository("OutputDao")
public class OutputDaoImpl  extends BaseHibernateDao<Output, Integer>  implements OutputDao {

}
