package cn.dms.equipment.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.dms.equipment.dao.PartsDao;
import cn.dms.equipment.model.Parts;

@Service("PartsService")
public class PartsServiceImpl extends BaseService<Parts, Integer> implements PartsService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PartsServiceImpl.class);

	private PartsDao partsDao;

	@Autowired
	@Qualifier("PartsDao")
	@Override
	public void setBaseDao(IBaseDao<Parts, Integer> partsDao) {
		this.baseDao = partsDao;
		this.partsDao = (PartsDao) partsDao;
	}
}