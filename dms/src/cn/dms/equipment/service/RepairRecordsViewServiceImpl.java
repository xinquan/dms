package cn.dms.equipment.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.dms.equipment.dao.RepairRecordsViewDao;
import cn.dms.equipment.model.RepairRecordsView;

@Service("RepairRecordsViewService")
public class RepairRecordsViewServiceImpl extends BaseService<RepairRecordsView, Integer> implements RepairRecordsViewService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RepairRecordsViewServiceImpl.class);

	private RepairRecordsViewDao repairRecordsViewDao;

	@Autowired
	@Qualifier("RepairRecordsViewDao")
	@Override
	public void setBaseDao(IBaseDao<RepairRecordsView, Integer> repairRecordsViewDao) {
		this.baseDao = repairRecordsViewDao;
		this.repairRecordsViewDao = (RepairRecordsViewDao) repairRecordsViewDao;
	}
}