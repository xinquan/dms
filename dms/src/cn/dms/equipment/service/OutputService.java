package cn.dms.equipment.service;

import cn.dms.equipment.model.Output;
import cn.hhit.common.service.IBaseService;

public interface OutputService extends IBaseService<Output, Integer> {

}
