package cn.dms.equipment.service;

import cn.hhit.common.service.IBaseService;
import cn.dms.equipment.model.RepairRecordsView;

public interface RepairRecordsViewService extends IBaseService<RepairRecordsView, Integer> {

}
