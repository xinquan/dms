package cn.dms.equipment.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.dms.equipment.dao.OutputDao;
import cn.dms.equipment.model.Output;

@Service("OutputService")
public class OutputServiceImpl extends BaseService<Output, Integer> implements OutputService {

	private static final Logger LOGGER = LoggerFactory.getLogger(OutputServiceImpl.class);

	private OutputDao outputDao;

	@Autowired
	@Qualifier("OutputDao")
	@Override
	public void setBaseDao(IBaseDao<Output, Integer> outputDao) {
		this.baseDao = outputDao;
		this.outputDao = (OutputDao) outputDao;
	}
}