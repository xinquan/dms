package cn.dms.equipment.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.dms.equipment.dao.BorrowDao;
import cn.dms.equipment.model.Borrow;

@Service("BorrowService")
public class BorrowServiceImpl extends BaseService<Borrow, Integer> implements BorrowService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BorrowServiceImpl.class);

	private BorrowDao borrowDao;

	@Autowired
	@Qualifier("BorrowDao")
	@Override
	public void setBaseDao(IBaseDao<Borrow, Integer> borrowDao) {
		this.baseDao = borrowDao;
		this.borrowDao = (BorrowDao) borrowDao;
	}
}