package cn.dms.equipment.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.dms.equipment.dao.OutRecordsViewDao;
import cn.dms.equipment.model.OutRecordsView;

@Service("OutRecordsViewService")
public class OutRecordsViewServiceImpl extends BaseService<OutRecordsView, Integer> implements OutRecordsViewService {

	private static final Logger LOGGER = LoggerFactory.getLogger(OutRecordsViewServiceImpl.class);

	private OutRecordsViewDao outRecordsViewDao;

	@Autowired
	@Qualifier("OutRecordsViewDao")
	@Override
	public void setBaseDao(IBaseDao<OutRecordsView, Integer> outRecordsViewDao) {
		this.baseDao = outRecordsViewDao;
		this.outRecordsViewDao = (OutRecordsViewDao) outRecordsViewDao;
	}
}