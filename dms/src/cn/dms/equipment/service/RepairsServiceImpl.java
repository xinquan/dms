package cn.dms.equipment.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.dms.equipment.dao.RepairsDao;
import cn.dms.equipment.model.Repairs;

@Service("RepairsService")
public class RepairsServiceImpl extends BaseService<Repairs, Integer> implements RepairsService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RepairsServiceImpl.class);

	private RepairsDao repairsDao;

	@Autowired
	@Qualifier("RepairsDao")
	@Override
	public void setBaseDao(IBaseDao<Repairs, Integer> repairsDao) {
		this.baseDao = repairsDao;
		this.repairsDao = (RepairsDao) repairsDao;
	}
}