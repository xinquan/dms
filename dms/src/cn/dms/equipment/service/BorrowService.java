package cn.dms.equipment.service;

import cn.hhit.common.service.IBaseService;
import cn.dms.equipment.model.Borrow;

public interface BorrowService extends IBaseService<Borrow, Integer> {

}
