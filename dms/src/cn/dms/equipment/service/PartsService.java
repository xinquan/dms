package cn.dms.equipment.service;

import cn.hhit.common.service.IBaseService;
import cn.dms.equipment.model.Parts;

public interface PartsService extends IBaseService<Parts, Integer> {

}
