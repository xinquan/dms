package cn.dms.equipment.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import cn.hhit.common.dao.IBaseDao;
import cn.hhit.common.service.BaseService;
import cn.dms.equipment.dao.EquipmentDao;
import cn.dms.equipment.model.Equipment;

@Service("EquipmentService")
public class EquipmentServiceImpl extends BaseService<Equipment, Integer> implements EquipmentService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EquipmentServiceImpl.class);

	private EquipmentDao equipmentDao;

	@Autowired
	@Qualifier("EquipmentDao")
	@Override
	public void setBaseDao(IBaseDao<Equipment, Integer> equipmentDao) {
		this.baseDao = equipmentDao;
		this.equipmentDao = (EquipmentDao) equipmentDao;
	}
}