package cn.dms.equipment.service;

import cn.hhit.common.service.IBaseService;
import cn.dms.equipment.model.Equipment;

public interface EquipmentService extends IBaseService<Equipment, Integer> {

}
