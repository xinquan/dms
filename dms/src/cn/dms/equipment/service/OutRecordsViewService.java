package cn.dms.equipment.service;

import cn.hhit.common.service.IBaseService;
import cn.dms.equipment.model.OutRecordsView;

public interface OutRecordsViewService extends IBaseService<OutRecordsView, Integer> {

}
