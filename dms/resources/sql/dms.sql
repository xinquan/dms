/*
Navicat MySQL Data Transfer

Source Server         : Mysql
Source Server Version : 50550
Source Host           : localhost:3306
Source Database       : dms

Target Server Type    : MYSQL
Target Server Version : 50550
File Encoding         : 65001

Date: 2016-10-31 00:51:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `borrow`
-- ----------------------------
DROP TABLE IF EXISTS `borrow`;
CREATE TABLE `borrow` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `eid` int(20) DEFAULT NULL,
  `pid` int(20) DEFAULT NULL,
  `borrowdate` date DEFAULT NULL,
  `mark` varchar(255) DEFAULT NULL,
  `returndate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of borrow
-- ----------------------------
INSERT INTO `borrow` VALUES ('5', '6', '3', '2016-10-25', null, null);
INSERT INTO `borrow` VALUES ('6', '6', '4', '2016-10-25', null, null);
INSERT INTO `borrow` VALUES ('7', '3', '3', '2016-10-29', null, null);
INSERT INTO `borrow` VALUES ('8', '3', '4', '2016-10-29', null, null);
INSERT INTO `borrow` VALUES ('9', '4', '5', '2016-10-29', null, null);
INSERT INTO `borrow` VALUES ('10', '4', '4', '2016-10-29', null, null);

-- ----------------------------
-- Table structure for `dict_detail`
-- ----------------------------
DROP TABLE IF EXISTS `dict_detail`;
CREATE TABLE `dict_detail` (
  `id` int(20) NOT NULL,
  `styleid` varchar(20) DEFAULT NULL,
  `detailid` varchar(50) DEFAULT NULL,
  `detailname` varchar(50) DEFAULT NULL,
  `detailsort` int(20) DEFAULT NULL,
  `pid` varchar(50) DEFAULT NULL,
  `haschild` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dict_detail
-- ----------------------------
INSERT INTO `dict_detail` VALUES ('1', 'KCJD', 'DLJD', '大类教育阶段', '1', '-1', 'true');
INSERT INTO `dict_detail` VALUES ('2', 'KCJD', 'ZYJD', '专业教育阶段', '2', '-1', 'true');
INSERT INTO `dict_detail` VALUES ('3', 'JYPT', 'TSJY', '通识教育平台', '1', 'DLJD', 'true');
INSERT INTO `dict_detail` VALUES ('4', 'JYPT', 'DLPT', '大类教育平台', '2', 'DLJD', 'true');
INSERT INTO `dict_detail` VALUES ('5', 'JYPT', 'ZYPT', '专业教育平台', '3', 'ZYJD', 'true');
INSERT INTO `dict_detail` VALUES ('6', 'KCMK', 'GGJC', '公共基础必修课程', '1', 'TSJY', 'false');
INSERT INTO `dict_detail` VALUES ('7', 'KCMK', 'CXCY', '创新创业教育与素质拓展课程', '2', 'TSJY', 'false');
INSERT INTO `dict_detail` VALUES ('8', 'KCMK', 'DLBX', '大类基础必修课程', '3', 'DLPT', 'false');
INSERT INTO `dict_detail` VALUES ('9', 'KCMK', 'XKBX', '学科基础必修课程', '4', 'DLPT', 'false');
INSERT INTO `dict_detail` VALUES ('10', 'KCMK', 'HXKC', '专业核心课程', '5', 'ZYPT', 'false');
INSERT INTO `dict_detail` VALUES ('11', 'KCMK', 'TZKC', '专业拓展课程', '6', 'ZYPT', 'false');
INSERT INTO `dict_detail` VALUES ('12', 'KCXZ', '必修', '必修', '1', null, 'false');
INSERT INTO `dict_detail` VALUES ('13', 'KCXZ', '选修', '选修', '2', null, 'false');
INSERT INTO `dict_detail` VALUES ('14', 'SJHJ', '√', '√', '1', null, 'false');
INSERT INTO `dict_detail` VALUES ('15', 'SJHJ', '否', ' 否', '2', null, 'false');
INSERT INTO `dict_detail` VALUES ('16', 'BZXZ', '两年', '两年', '1', null, 'false');
INSERT INTO `dict_detail` VALUES ('17', 'BZXZ', '三年', '三年', '2', null, 'false');
INSERT INTO `dict_detail` VALUES ('18', 'BZXZ', '四年', '四年', '3', null, 'false');
INSERT INTO `dict_detail` VALUES ('19', 'BZXZ', '五年', '五年', '4', null, 'false');
INSERT INTO `dict_detail` VALUES ('20', 'ML', '工学', '工学', '1', null, 'false');
INSERT INTO `dict_detail` VALUES ('21', 'ML', '理学', '理学', '2', null, 'false');
INSERT INTO `dict_detail` VALUES ('22', 'ML', '管理学', '管理学', '3', null, 'false');
INSERT INTO `dict_detail` VALUES ('23', 'ML', '文学', '文学', '4', null, 'false');
INSERT INTO `dict_detail` VALUES ('24', 'ML', '艺术学', '艺术学', '5', null, 'false');
INSERT INTO `dict_detail` VALUES ('25', 'ML', '医学', '医学', '6', null, 'false');
INSERT INTO `dict_detail` VALUES ('26', 'ML', '农学', '农学', '7', null, 'false');
INSERT INTO `dict_detail` VALUES ('27', 'ML', '经济学', '经济学', '8', null, 'false');
INSERT INTO `dict_detail` VALUES ('28', 'ML', '法学', '法学', '9', null, 'false');
INSERT INTO `dict_detail` VALUES ('29', 'ML', '教育学', '教育学', '10', null, 'false');
INSERT INTO `dict_detail` VALUES ('30', 'SF', '是', '是', '1', null, 'false');
INSERT INTO `dict_detail` VALUES ('31', 'SF', '否', '否', '2', null, 'false');
INSERT INTO `dict_detail` VALUES ('32', 'SYXW', '学士', '学士', '1', null, 'false');
INSERT INTO `dict_detail` VALUES ('33', 'SYXW', '硕士', '硕士', '2', null, 'false');
INSERT INTO `dict_detail` VALUES ('34', 'SYXW', '博士', '博士', '3', null, 'false');
INSERT INTO `dict_detail` VALUES ('35', 'SEX', '男', '男', '1', null, 'false');
INSERT INTO `dict_detail` VALUES ('36', 'SEX', '女', '女', '2', null, 'false');
INSERT INTO `dict_detail` VALUES ('37', 'OPTTYPE', '用户', '用户', '1', null, 'false');
INSERT INTO `dict_detail` VALUES ('38', 'OPTTYPE', '角色', '角色', '2', null, 'false');
INSERT INTO `dict_detail` VALUES ('39', 'FLOW', '大类培养方案', '大类培养方案', '1', null, 'plan/plan!showBigPlanInfo.do?id=');
INSERT INTO `dict_detail` VALUES ('40', 'FLOW', '大类专业培养方案', '大类专业培养方案', '2', null, 'plan/plan!showPlanInfo.do?id=');
INSERT INTO `dict_detail` VALUES ('41', 'FLOW', '非大类专业培养方案', '非大类专业培养方案', '3', null, 'plan/plan!showPlanInfo.do?id=');
INSERT INTO `dict_detail` VALUES ('42', 'FLOW', '新增课程审批', '新增课程审批', '4', null, 'plan/plan!showCourseInfo.do?id=');
INSERT INTO `dict_detail` VALUES ('43', 'KKXQ', '1', '1', '1', null, 'false');
INSERT INTO `dict_detail` VALUES ('44', 'KKXQ', '2', '2', '2', null, 'false');
INSERT INTO `dict_detail` VALUES ('45', 'KKXQ', '3', '3', '3', null, 'false');
INSERT INTO `dict_detail` VALUES ('46', 'KKXQ', '4', '4', '4', null, 'false');
INSERT INTO `dict_detail` VALUES ('47', 'KKXQ', '5', '5', '5', null, 'false');
INSERT INTO `dict_detail` VALUES ('48', 'KKXQ', '6', '6', '6', null, 'false');
INSERT INTO `dict_detail` VALUES ('49', 'KKXQ', '7', '7', '7', null, 'false');
INSERT INTO `dict_detail` VALUES ('50', 'KKXQ', '8', '8', '8', null, 'false');
INSERT INTO `dict_detail` VALUES ('51', 'SBZT', '1', '在库', '1', null, 'false');
INSERT INTO `dict_detail` VALUES ('52', 'SBZT', '2', '出库', '2', null, 'false');
INSERT INTO `dict_detail` VALUES ('53', 'SBZT', '3', '损坏，待维护', '3', null, 'false');
INSERT INTO `dict_detail` VALUES ('54', 'SBZT', '4', '维护中', '4', null, 'false');
INSERT INTO `dict_detail` VALUES ('55', 'SBZT', '5', '报废', '5', null, 'false');
INSERT INTO `dict_detail` VALUES ('56', 'SBZT', '6', '已维护', '6', null, 'false');

-- ----------------------------
-- Table structure for `dict_style`
-- ----------------------------
DROP TABLE IF EXISTS `dict_style`;
CREATE TABLE `dict_style` (
  `id` int(20) NOT NULL,
  `dictid` varchar(20) DEFAULT NULL,
  `dictname` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dict_style
-- ----------------------------
INSERT INTO `dict_style` VALUES ('1', 'KCJD', '阶段');
INSERT INTO `dict_style` VALUES ('2', 'JYPT', '平台');
INSERT INTO `dict_style` VALUES ('3', 'KCMK', '模块');
INSERT INTO `dict_style` VALUES ('4', 'KCXZ', '课程性质');
INSERT INTO `dict_style` VALUES ('5', 'SJHJ', '集中性实践环节');

-- ----------------------------
-- Table structure for `equipment`
-- ----------------------------
DROP TABLE IF EXISTS `equipment`;
CREATE TABLE `equipment` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `dno` varchar(20) DEFAULT NULL,
  `model` varchar(10) DEFAULT NULL,
  `brand` varchar(20) DEFAULT NULL,
  `standard` varchar(20) DEFAULT NULL,
  `weight` varchar(20) DEFAULT NULL,
  `start` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  `qrcode` varchar(20) DEFAULT NULL,
  `state` varchar(10) DEFAULT NULL,
  `department` varchar(20) DEFAULT NULL,
  `lastinput` date DEFAULT NULL,
  `outputdate` date DEFAULT NULL,
  `damagedate` date DEFAULT NULL,
  `repairdate` date DEFAULT NULL,
  `scapdate` date DEFAULT NULL,
  `departmentid` int(10) DEFAULT NULL,
  `mark` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of equipment
-- ----------------------------

-- ----------------------------
-- Table structure for `output`
-- ----------------------------
DROP TABLE IF EXISTS `output`;
CREATE TABLE `output` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `eid` int(20) DEFAULT NULL,
  `outpeople` varchar(20) DEFAULT NULL,
  `usepeople` varchar(20) DEFAULT NULL,
  `outdate` date DEFAULT NULL,
  `returndate` date DEFAULT NULL,
  `mark` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of output
-- ----------------------------

-- ----------------------------
-- Table structure for `parts`
-- ----------------------------
DROP TABLE IF EXISTS `parts`;
CREATE TABLE `parts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `did` int(10) DEFAULT NULL,
  `inputdate` date DEFAULT NULL,
  `department` varchar(20) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `pno` varchar(20) DEFAULT NULL,
  `model` varchar(20) DEFAULT NULL,
  `standard` varchar(20) DEFAULT NULL,
  `state` varchar(10) DEFAULT NULL,
  `outputdate` date DEFAULT NULL,
  `lastinput` date DEFAULT NULL,
  `repairdate` date DEFAULT NULL,
  `damagedate` date DEFAULT NULL,
  `scapdate` date DEFAULT NULL,
  `departmentid` int(10) DEFAULT NULL,
  `brand` varchar(20) DEFAULT NULL,
  `weight` varchar(10) DEFAULT NULL,
  `start` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of parts
-- ----------------------------

-- ----------------------------
-- Table structure for `repairs`
-- ----------------------------
DROP TABLE IF EXISTS `repairs`;
CREATE TABLE `repairs` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `eid` int(20) DEFAULT NULL,
  `mark` varchar(1000) DEFAULT NULL,
  `repairman` varchar(20) DEFAULT NULL,
  `repairmark` varchar(1000) DEFAULT NULL,
  `assign` varchar(20) DEFAULT NULL,
  `repairdate` date DEFAULT NULL,
  `returndate` date DEFAULT NULL,
  `createdate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of repairs
-- ----------------------------
INSERT INTO `repairs` VALUES ('2', '9', '需要入库人完善维护信息', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for `sys_department`
-- ----------------------------
DROP TABLE IF EXISTS `sys_department`;
CREATE TABLE `sys_department` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `deptid` varchar(50) DEFAULT NULL,
  `deptname` varchar(50) DEFAULT NULL,
  `english` varchar(100) DEFAULT NULL,
  `mark` varchar(200) DEFAULT NULL,
  `dindex` int(20) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_department
-- ----------------------------
INSERT INTO `sys_department` VALUES ('16', null, '部门A', null, '部门A', '0');
INSERT INTO `sys_department` VALUES ('17', null, '部门B', null, '部门B', '0');
INSERT INTO `sys_department` VALUES ('18', null, '部门C', null, '部门C', '0');

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` int(11) NOT NULL,
  `pid` int(11) DEFAULT NULL,
  `text` varchar(40) DEFAULT NULL,
  `iconCls` varchar(30) DEFAULT NULL,
  `leaf` tinyint(1) DEFAULT '0',
  `expanded` tinyint(1) DEFAULT '0',
  `disabled` tinyint(1) DEFAULT '0',
  `sortIndex` int(11) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `cancheck` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '-2', '培养方案', 'contract', '0', '1', '0', '4', null, 'false');
INSERT INTO `sys_menu` VALUES ('2', '1', '专业大类培养方案', 'contract', '1', '0', '0', '1', 'plan/plan!showBig.do', 'true');
INSERT INTO `sys_menu` VALUES ('3', '1', '非大类专业培养方案', 'contract', '1', '0', '0', '3', 'plan/plan!showMajor.do', 'true');
INSERT INTO `sys_menu` VALUES ('5', '-1', '系统管理', 'user', '0', '1', '0', '1', null, 'false');
INSERT INTO `sys_menu` VALUES ('6', '5', '角色管理', 'contract', '1', '0', '0', '2', 'system/menu!showRoles.do', 'false');
INSERT INTO `sys_menu` VALUES ('8', '-2', '专业课程', 'course', '0', '1', '0', '3', null, 'false');
INSERT INTO `sys_menu` VALUES ('9', '5', '角色分配', 'contract', '1', '0', '0', '3', 'system/menu!roleUsers.do', 'false');
INSERT INTO `sys_menu` VALUES ('10', '13', '出库管理', 'contract', '1', '0', '0', '3', 'equipment/equipment!showOutputs.do', 'true');
INSERT INTO `sys_menu` VALUES ('11', '5', '部门设置', 'contract', '1', '0', '0', '2', 'system/menu!showDepartments.do', 'false');
INSERT INTO `sys_menu` VALUES ('12', '5', '用户管理', 'contract', '1', '0', '0', '1', 'system/menu!showUsers.do', 'false');
INSERT INTO `sys_menu` VALUES ('13', '-1', '设备管理', 'check', '0', '1', '0', '2', null, 'false');
INSERT INTO `sys_menu` VALUES ('14', '13', '设备信息', 'contract', '1', '0', '0', '1', 'equipment/equipment!showEquipments.do', 'false');
INSERT INTO `sys_menu` VALUES ('15', '13', '回收站', 'contract', '1', '0', '0', '5', 'equipment/equipment!showRecycles.do', 'false');
INSERT INTO `sys_menu` VALUES ('16', '5', '授权管理', 'contract', '1', '0', '0', '4', 'system/menu!permsDistuibute.do', 'false');
INSERT INTO `sys_menu` VALUES ('17', '13', '配件信息', 'contract', '1', '0', '0', '2', 'equipment/equipment!showParts.do', 'false');
INSERT INTO `sys_menu` VALUES ('18', '13', '设备维护', 'contract', '1', '0', '0', '3', 'equipment/equipment!showMaintains.do', 'false');
INSERT INTO `sys_menu` VALUES ('19', '13', '台账检索', 'contract', '1', '0', '0', '4', 'equipment/equipment!showQuery.do', 'false');

-- ----------------------------
-- Table structure for `sys_permission`
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
  `id` int(11) NOT NULL,
  `pname` varchar(20) DEFAULT NULL,
  `perm` varchar(40) DEFAULT NULL,
  `menuid` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `explan` varchar(500) DEFAULT NULL,
  `rindex` int(20) DEFAULT NULL,
  `editable` varchar(20) DEFAULT 'true',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '管理员', null, '0', 'false');
INSERT INTO `sys_role` VALUES ('2', '主任', '', '2', 'true');
INSERT INTO `sys_role` VALUES ('6', '设备管理员', '', '1', 'true');
INSERT INTO `sys_role` VALUES ('9', '普通用户', null, '100', 'true');

-- ----------------------------
-- Table structure for `sys_role_permission`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `roleid` int(20) DEFAULT NULL,
  `menuid` int(20) DEFAULT NULL,
  `editable` varchar(20) DEFAULT NULL,
  `department` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=345 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
INSERT INTO `sys_role_permission` VALUES ('4', '1', '1', 'true', 'true');
INSERT INTO `sys_role_permission` VALUES ('5', '1', '2', 'true', 'true');
INSERT INTO `sys_role_permission` VALUES ('6', '1', '3', 'true', 'true');
INSERT INTO `sys_role_permission` VALUES ('8', '1', '5', 'true', 'true');
INSERT INTO `sys_role_permission` VALUES ('9', '1', '6', 'true', 'true');
INSERT INTO `sys_role_permission` VALUES ('10', '1', '8', 'true', 'true');
INSERT INTO `sys_role_permission` VALUES ('11', '1', '9', 'true', 'true');
INSERT INTO `sys_role_permission` VALUES ('12', '1', '10', 'true', 'true');
INSERT INTO `sys_role_permission` VALUES ('13', '1', '11', 'true', 'true');
INSERT INTO `sys_role_permission` VALUES ('14', '1', '12', 'true', 'true');
INSERT INTO `sys_role_permission` VALUES ('15', '1', '13', 'true', 'true');
INSERT INTO `sys_role_permission` VALUES ('16', '1', '14', 'true', 'true');
INSERT INTO `sys_role_permission` VALUES ('17', '1', '15', 'true', 'true');
INSERT INTO `sys_role_permission` VALUES ('18', '1', '16', 'true', 'true');
INSERT INTO `sys_role_permission` VALUES ('19', '1', '17', 'true', 'true');
INSERT INTO `sys_role_permission` VALUES ('20', '1', '18', 'true', 'true');
INSERT INTO `sys_role_permission` VALUES ('21', '1', '19', 'true', 'true');
INSERT INTO `sys_role_permission` VALUES ('124', '8', '5', 'false', null);
INSERT INTO `sys_role_permission` VALUES ('125', '8', '12', 'false', '');
INSERT INTO `sys_role_permission` VALUES ('126', '8', '16', 'false', null);
INSERT INTO `sys_role_permission` VALUES ('127', '8', '13', 'false', null);
INSERT INTO `sys_role_permission` VALUES ('128', '8', '14', 'false', null);
INSERT INTO `sys_role_permission` VALUES ('129', '8', '8', 'false', null);
INSERT INTO `sys_role_permission` VALUES ('130', '8', '10', 'false', null);
INSERT INTO `sys_role_permission` VALUES ('295', '3', '13', 'false', 'false');
INSERT INTO `sys_role_permission` VALUES ('296', '3', '17', 'true', 'false');
INSERT INTO `sys_role_permission` VALUES ('297', '3', '5', 'false', 'false');
INSERT INTO `sys_role_permission` VALUES ('298', '3', '12', 'true', 'false');
INSERT INTO `sys_role_permission` VALUES ('299', '3', '8', 'false', 'false');
INSERT INTO `sys_role_permission` VALUES ('300', '3', '10', 'true', 'true');
INSERT INTO `sys_role_permission` VALUES ('301', '3', '11', 'true', 'false');
INSERT INTO `sys_role_permission` VALUES ('302', '3', '1', 'false', 'false');
INSERT INTO `sys_role_permission` VALUES ('303', '3', '2', 'true', 'false');
INSERT INTO `sys_role_permission` VALUES ('304', '3', '15', 'true', 'false');
INSERT INTO `sys_role_permission` VALUES ('305', '3', '3', 'true', 'false');
INSERT INTO `sys_role_permission` VALUES ('324', '9', '13', 'false', 'false');
INSERT INTO `sys_role_permission` VALUES ('325', '9', '17', 'true', 'false');
INSERT INTO `sys_role_permission` VALUES ('326', '9', '8', 'false', 'false');
INSERT INTO `sys_role_permission` VALUES ('327', '9', '10', 'false', 'false');
INSERT INTO `sys_role_permission` VALUES ('328', '9', '11', 'true', 'false');
INSERT INTO `sys_role_permission` VALUES ('329', '9', '1', 'false', 'false');
INSERT INTO `sys_role_permission` VALUES ('330', '9', '2', 'true', 'false');
INSERT INTO `sys_role_permission` VALUES ('331', '9', '15', 'true', 'false');
INSERT INTO `sys_role_permission` VALUES ('332', '9', '3', 'true', 'false');
INSERT INTO `sys_role_permission` VALUES ('333', '2', '5', 'false', 'false');
INSERT INTO `sys_role_permission` VALUES ('334', '2', '12', 'false', 'false');
INSERT INTO `sys_role_permission` VALUES ('335', '2', '6', 'false', 'false');
INSERT INTO `sys_role_permission` VALUES ('336', '2', '11', 'false', 'false');
INSERT INTO `sys_role_permission` VALUES ('337', '2', '9', 'false', 'false');
INSERT INTO `sys_role_permission` VALUES ('338', '2', '16', 'false', 'false');
INSERT INTO `sys_role_permission` VALUES ('339', '2', '13', 'false', 'false');
INSERT INTO `sys_role_permission` VALUES ('340', '2', '14', 'false', 'false');
INSERT INTO `sys_role_permission` VALUES ('341', '2', '17', 'false', 'false');
INSERT INTO `sys_role_permission` VALUES ('342', '2', '10', 'false', 'false');
INSERT INTO `sys_role_permission` VALUES ('343', '2', '18', 'true', 'false');
INSERT INTO `sys_role_permission` VALUES ('344', '2', '15', 'true', 'true');

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `registerDate` datetime DEFAULT NULL,
  `editable` varchar(10) DEFAULT 'true',
  `uindex` int(10) DEFAULT NULL COMMENT '1',
  `major` varchar(20) DEFAULT NULL,
  `unum` varchar(20) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `sex` varchar(20) DEFAULT NULL,
  `department` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', '123456', '', null, 'false', '0', null, '001', '管理者', null, '管理部门');
INSERT INTO `sys_user` VALUES ('10', 'test', 'test', null, '2016-10-31 00:47:37', 'true', '0', null, 'test', 'test', '男', '部门A');

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `userid` int(20) DEFAULT NULL,
  `roleid` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1', '1');
INSERT INTO `sys_user_role` VALUES ('18', '10', '9');
INSERT INTO `sys_user_role` VALUES ('19', '10', '9');

-- ----------------------------
-- Table structure for `wf_action_info`
-- ----------------------------
DROP TABLE IF EXISTS `wf_action_info`;
CREATE TABLE `wf_action_info` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `flowid` int(20) DEFAULT NULL,
  `step` int(20) DEFAULT NULL,
  `isfirst` varchar(20) DEFAULT NULL,
  `islast` varchar(20) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `handlertype` varchar(100) DEFAULT NULL,
  `pre` int(11) DEFAULT '0',
  `next` int(11) DEFAULT '0',
  `roleid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_action_info
-- ----------------------------
INSERT INTO `wf_action_info` VALUES ('18', '课程归属系意见：', '10', '1', null, null, '系主任（签字）：', '系主任', '-1', '19', '3');
INSERT INTO `wf_action_info` VALUES ('19', '课程归属单位意见：', '10', '2', null, null, '教学院长（签字）：', '副院长', '18', '20', '2');
INSERT INTO `wf_action_info` VALUES ('20', '教务处意见：', '10', '3', null, null, '负责人签字：', '教务处', '19', '-1', '6');
INSERT INTO `wf_action_info` VALUES ('21', '系主任', '8', '1', null, null, '系主任', '系主任', '-1', '22', '3');
INSERT INTO `wf_action_info` VALUES ('22', '教学副院长', '8', '2', null, null, '教学副院长', '副院长', '21', '23', '2');
INSERT INTO `wf_action_info` VALUES ('23', '教务处', '8', '3', null, null, '教务处', '教务处', '22', '-1', '6');
INSERT INTO `wf_action_info` VALUES ('25', '系主任', '11', '1', null, null, '系主任', '系主任', '-1', '26', '3');
INSERT INTO `wf_action_info` VALUES ('26', '教学副院长', '11', '2', null, null, '教学副院长', '副院长', '25', '27', '2');
INSERT INTO `wf_action_info` VALUES ('27', '教务处', '11', '3', null, null, '教务处', '教务处', '26', '-1', '6');
INSERT INTO `wf_action_info` VALUES ('28', '系主任', '12', '1', null, null, '系主任', '系主任', '-1', '29', '3');
INSERT INTO `wf_action_info` VALUES ('29', '教学副院长', '12', '2', null, null, '教学副院长', '副院长', '28', '30', '2');
INSERT INTO `wf_action_info` VALUES ('30', '教务处', '12', '3', null, null, '教务处', '教务处', '29', '-1', '6');

-- ----------------------------
-- Table structure for `wf_flow_info`
-- ----------------------------
DROP TABLE IF EXISTS `wf_flow_info`;
CREATE TABLE `wf_flow_info` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `createuser` varchar(100) DEFAULT NULL,
  `createdate` date DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `inuse` varchar(20) DEFAULT NULL,
  `state` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_flow_info
-- ----------------------------
INSERT INTO `wf_flow_info` VALUES ('8', '大类培养方案', '管理者', '2016-04-07', '大类培养方案审批流程', '是', null);
INSERT INTO `wf_flow_info` VALUES ('10', '新增课程审批', '管理者', '2016-04-14', '新增课程审批', '是', null);
INSERT INTO `wf_flow_info` VALUES ('11', '大类专业培养方案', '管理者', '2016-04-18', '大类专业培养方案', '是', null);
INSERT INTO `wf_flow_info` VALUES ('12', '非大类专业培养方案', '管理者', '2016-04-18', '非大类专业培养方案', '是', null);

-- ----------------------------
-- Table structure for `wf_instant_action`
-- ----------------------------
DROP TABLE IF EXISTS `wf_instant_action`;
CREATE TABLE `wf_instant_action` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `instantid` int(20) DEFAULT NULL,
  `step` int(20) DEFAULT NULL,
  `result` varchar(1000) DEFAULT NULL,
  `pass` varchar(20) DEFAULT NULL,
  `oprator` varchar(2000) DEFAULT NULL,
  `handledate` date DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `handlertype` varchar(100) DEFAULT NULL,
  `pre` int(11) DEFAULT NULL,
  `next` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `opratorid` varchar(100) DEFAULT NULL,
  `roleid` int(11) DEFAULT NULL,
  `instantclass` varchar(100) DEFAULT NULL,
  `flowid` int(10) DEFAULT NULL,
  `rollback` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_instant_action
-- ----------------------------
INSERT INTO `wf_instant_action` VALUES ('50', '57', '1', '退回', '否', '李四', '2016-04-18', '系主任（签字）：', '系主任', '-1', '51', '课程归属系意见：', '5', '3', 'PlanCourseModel', '25', '');
INSERT INTO `wf_instant_action` VALUES ('51', '57', '2', 'dfsadfsdf', '是', '张三', '2016-04-18', '教学院长（签字）：', '副院长', '50', '52', '课程归属单位意见：', '2', '2', 'PlanCourseModel', '25', null);
INSERT INTO `wf_instant_action` VALUES ('52', '57', '3', '终于给你个Pass', '是', '李四', '2016-04-18', '负责人签字：', '教务处', '51', '-1', '教务处意见：', '5', '6', 'PlanCourseModel', '25', null);
INSERT INTO `wf_instant_action` VALUES ('53', '57', '1', 'dddddd', '否', '李四', '2016-04-18', '系主任（签字）：', '系主任', '-1', '51', '课程归属系意见：', '5', '3', 'PlanCourseModel', '25', '');
INSERT INTO `wf_instant_action` VALUES ('54', '57', '1', '给你过了吧', '是', '李四', '2016-04-18', '系主任（签字）：', '系主任', '-1', '51', '课程归属系意见：', '5', '3', 'PlanCourseModel', '25', null);
INSERT INTO `wf_instant_action` VALUES ('55', '56', '1', '撒旦发射点发', '否', '李四', '2016-04-18', '系主任（签字）：', '系主任', '-1', '56', '课程归属系意见：', '5', '3', 'PlanCourseModel', '26', '');
INSERT INTO `wf_instant_action` VALUES ('56', '56', '2', '', '', '张三', null, '教学院长（签字）：', '副院长', '55', '57', '课程归属单位意见：', '2', '2', 'PlanCourseModel', '26', null);
INSERT INTO `wf_instant_action` VALUES ('57', '56', '3', '', '', '李四', null, '负责人签字：', '教务处', '56', '-1', '教务处意见：', '5', '6', 'PlanCourseModel', '26', null);
INSERT INTO `wf_instant_action` VALUES ('58', '70', '1', 'sdfasdf', '否', '李四', '2016-04-18', '系主任（签字）：', '系主任', '-1', '59', '课程归属系意见：', '5', '3', 'PlanCourseModel', '27', '');
INSERT INTO `wf_instant_action` VALUES ('59', '70', '2', '', '', '张三', null, '教学院长（签字）：', '副院长', '58', '60', '课程归属单位意见：', '2', '2', 'PlanCourseModel', '27', null);
INSERT INTO `wf_instant_action` VALUES ('60', '70', '3', '', '', '李四', null, '负责人签字：', '教务处', '59', '-1', '教务处意见：', '5', '6', 'PlanCourseModel', '27', null);
INSERT INTO `wf_instant_action` VALUES ('61', '56', '1', '', '', '李四', null, '系主任（签字）：', '系主任', '-1', '56', '课程归属系意见：', '5', '3', 'PlanCourseModel', '26', 'submit');
INSERT INTO `wf_instant_action` VALUES ('62', '40', '1', '', '', '李四', null, '系主任', '系主任', '-1', '63', '系主任', '5', '3', 'PlanMainModel', '28', null);
INSERT INTO `wf_instant_action` VALUES ('63', '40', '2', '', '', '张三', null, '教学副院长', '副院长', '62', '64', '教学副院长', '2', '2', 'PlanMainModel', '28', null);
INSERT INTO `wf_instant_action` VALUES ('64', '40', '3', '', '', '李四', null, '教务处', '教务处', '63', '-1', '教务处', '5', '6', 'PlanMainModel', '28', null);
INSERT INTO `wf_instant_action` VALUES ('65', '34', '1', 'dddd', '否', '李四', '2016-04-18', '系主任', '系主任', '-1', '66', '系主任', '5', '3', 'PlanMainModel', '29', '');
INSERT INTO `wf_instant_action` VALUES ('66', '34', '2', '', '', '张三', null, '教学副院长', '副院长', '65', '67', '教学副院长', '2', '2', 'PlanMainModel', '29', null);
INSERT INTO `wf_instant_action` VALUES ('67', '34', '3', '', '', '李四', null, '教务处', '教务处', '66', '-1', '教务处', '5', '6', 'PlanMainModel', '29', null);
INSERT INTO `wf_instant_action` VALUES ('71', '34', '1', 'dsfadf', '是', '李四', '2016-04-18', '系主任', '系主任', '-1', '66', '系主任', '5', '3', 'PlanMainModel', '29', null);
INSERT INTO `wf_instant_action` VALUES ('72', '36', '1', '', '', '李四', null, '系主任', '系主任', '-1', '73', '系主任', '5', '3', 'PlanMainModel', '31', null);
INSERT INTO `wf_instant_action` VALUES ('73', '36', '2', '', '', '张三', null, '教学副院长', '副院长', '72', '74', '教学副院长', '2', '2', 'PlanMainModel', '31', null);
INSERT INTO `wf_instant_action` VALUES ('74', '36', '3', '', '', '李四', null, '教务处', '教务处', '73', '-1', '教务处', '5', '6', 'PlanMainModel', '31', null);
INSERT INTO `wf_instant_action` VALUES ('75', '70', '1', '', '', '李四', null, '系主任（签字）：', '系主任', '-1', '59', '课程归属系意见：', '5', '3', 'PlanCourseModel', '27', 'submit');

-- ----------------------------
-- Table structure for `wf_instant_flow`
-- ----------------------------
DROP TABLE IF EXISTS `wf_instant_flow`;
CREATE TABLE `wf_instant_flow` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `instantid` int(20) DEFAULT NULL,
  `flowid` int(20) DEFAULT NULL,
  `currentstep` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wf_instant_flow
-- ----------------------------
INSERT INTO `wf_instant_flow` VALUES ('25', '57', '10', '52');
INSERT INTO `wf_instant_flow` VALUES ('26', '56', '10', '61');
INSERT INTO `wf_instant_flow` VALUES ('27', '70', '10', '75');
INSERT INTO `wf_instant_flow` VALUES ('28', '40', '8', '62');
INSERT INTO `wf_instant_flow` VALUES ('29', '34', '11', '66');
INSERT INTO `wf_instant_flow` VALUES ('31', '36', '12', '72');

-- ----------------------------
-- View structure for `flow_works`
-- ----------------------------
DROP VIEW IF EXISTS `flow_works`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `flow_works` AS select `wf_instant_flow`.`currentstep` AS `currentstep`,`wf_instant_action`.`opratorid` AS `opratorid`,`wf_instant_action`.`pass` AS `pass`,`wf_instant_action`.`rollback` AS `rollback`,`wf_instant_action`.`id` AS `id` from (`wf_instant_flow` join `wf_instant_action`) where (`wf_instant_flow`.`currentstep` = `wf_instant_action`.`id`) ;

-- ----------------------------
-- View structure for `menu_view`
-- ----------------------------
DROP VIEW IF EXISTS `menu_view`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `menu_view` AS select `sys_role_permission`.`roleid` AS `roleid`,`sys_menu`.`sortIndex` AS `sortIndex`,`sys_menu`.`text` AS `text`,`sys_menu`.`pid` AS `pid`,`sys_menu`.`iconCls` AS `iconCls`,`sys_menu`.`leaf` AS `leaf`,`sys_menu`.`expanded` AS `expanded`,`sys_menu`.`disabled` AS `disabled`,`sys_menu`.`url` AS `url`,`sys_menu`.`id` AS `id`,`sys_menu`.`cancheck` AS `cancheck` from (`sys_role_permission` join `sys_menu`) where (`sys_role_permission`.`menuid` = `sys_menu`.`id`) ;

-- ----------------------------
-- View structure for `outrecords`
-- ----------------------------
DROP VIEW IF EXISTS `outrecords`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `outrecords` AS select `output`.`id` AS `id`,`output`.`eid` AS `eid`,`output`.`outpeople` AS `outpeople`,`output`.`usepeople` AS `usepeople`,`output`.`outdate` AS `outdate`,`output`.`returndate` AS `returndate`,`output`.`mark` AS `mark`,`equipment`.`name` AS `name`,`equipment`.`dno` AS `dno`,`equipment`.`state` AS `state`,`equipment`.`department` AS `department`,`equipment`.`lastinput` AS `lastinput` from (`equipment` join `output`) where (`equipment`.`id` = `output`.`eid`) ;

-- ----------------------------
-- View structure for `repair_records`
-- ----------------------------
DROP VIEW IF EXISTS `repair_records`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `repair_records` AS select `equipment`.`name` AS `name`,`equipment`.`dno` AS `dno`,`equipment`.`department` AS `department`,`equipment`.`lastinput` AS `lastinput`,`repairs`.`id` AS `id`,`repairs`.`eid` AS `eid`,`repairs`.`mark` AS `mark`,`repairs`.`repairman` AS `repairman`,`repairs`.`repairmark` AS `repairmark`,`repairs`.`assign` AS `assign`,`repairs`.`repairdate` AS `repairdate`,`repairs`.`returndate` AS `returndate`,`equipment`.`state` AS `state` from (`equipment` join `repairs`) where (`equipment`.`id` = `repairs`.`eid`) ;

-- ----------------------------
-- View structure for `role_user`
-- ----------------------------
DROP VIEW IF EXISTS `role_user`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `role_user` AS select `sys_user_role`.`roleid` AS `roleid`,`sys_user`.`id` AS `userid`,`sys_user`.`username` AS `username`,`sys_user`.`password` AS `password`,`sys_user`.`email` AS `email`,`sys_user`.`registerDate` AS `registerDate`,`sys_user`.`editable` AS `editable`,`sys_user`.`uindex` AS `uindex`,`sys_user`.`major` AS `major`,`sys_user`.`unum` AS `unum`,`sys_user`.`name` AS `name`,`sys_user`.`sex` AS `sex`,`sys_user`.`department` AS `department`,`sys_user_role`.`id` AS `id` from (`sys_user_role` join `sys_user`) where (`sys_user_role`.`userid` = `sys_user`.`id`) ;
